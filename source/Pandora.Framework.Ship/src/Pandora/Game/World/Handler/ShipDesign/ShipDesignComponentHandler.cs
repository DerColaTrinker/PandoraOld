﻿using Pandora.Game.World.Objects;
using Pandora.Game.World.Objects.ShipDesign;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Handler.ShipDesign
{
    public delegate void ShipDesignComponentHandler<TShipDesign, TComponentDescriptor>(TShipDesign shipdesign, TComponentDescriptor componentdescriptor);

    public class ShipDesignComponentHandler<TRace, TShipDesign, TComponentDescriptor> : HandlerBase
        where TRace : RaceBase
        where TShipDesign : ShipDesignBase<TRace>
        where TComponentDescriptor : WorldObjectBase, IShipDesignComponentDescriptor
    {
        private OwnerHashSet<TShipDesign, TComponentDescriptor> _collection = new OwnerHashSet<TShipDesign, TComponentDescriptor>();
        private ShipDesignHandler<TRace, TShipDesign> _shipdesignhandler;

        public event ShipDesignHandlerDelegate<TRace, TShipDesign> ShipDesignAdded;
        public event ShipDesignHandlerDelegate<TRace, TShipDesign> ShipDesignRemoved;

        public event ShipDesignComponentHandler<TShipDesign, TComponentDescriptor> ShipDesignComponentAdded;
        public event ShipDesignComponentHandler<TShipDesign, TComponentDescriptor> ShipDesignComponentRemoved;

        protected override void OnStart()
        {
            // Handler suchen
            _shipdesignhandler = World.GetHandler<ShipDesignHandler<TRace, TShipDesign>>();

            // Handler abonieren
            _shipdesignhandler.ShipDesignAdd += delegate(TRace race, TShipDesign shipdesign) { _collection.AddOwner(shipdesign); OnShipDesignAdd(race, shipdesign); };
            _shipdesignhandler.ShipDesignRemove += delegate(TRace race, TShipDesign shipdesign) { _collection.RemoveOwner(shipdesign); OnShipDesignRemove(race, shipdesign); };

            base.OnStart();
        }

        public void AddComponent(TShipDesign shipdesign, TComponentDescriptor component)
        {
            if (_collection.AddElement(shipdesign, component))
                OnComponentDescriptorAdd(shipdesign, component);
        }

        public void RemoveComponent(TShipDesign shipdesign, TComponentDescriptor component)
        {
            if (_collection.RemoveElement(shipdesign, component))
                OnComponentDescriptorRemove(shipdesign, component);
        }

        public IEnumerable<TComponentDescriptor > GetComponents(TShipDesign design)
        {
            return _collection.GetElements(design);
        }

        protected virtual void OnComponentDescriptorAdd(TShipDesign shipdesign, TComponentDescriptor component)
        {
            if (ShipDesignComponentAdded != null) ShipDesignComponentAdded.Invoke(shipdesign, component);
        }

        protected virtual void OnComponentDescriptorRemove(TShipDesign shipdesign, TComponentDescriptor component)
        {
            if (ShipDesignComponentRemoved != null) ShipDesignComponentRemoved.Invoke(shipdesign, component);
        }

        protected virtual void OnShipDesignAdd(TRace race, TShipDesign shipdesign)
        {
            if (ShipDesignAdded != null) ShipDesignAdded.Invoke(race, shipdesign);
        }

        protected virtual void OnShipDesignRemove(TRace race, TShipDesign shipdesign)
        {
            if (ShipDesignRemoved != null) ShipDesignRemoved.Invoke(race, shipdesign);
        }
    }
}
