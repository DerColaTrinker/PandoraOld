﻿using Pandora.Game.World.Objects;
using Pandora.Game.World.Objects.Ship;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.World.Handler.Ship;

namespace Pandora.Game.World.Handler.Ship
{
    public delegate void ShipComponentHandlerDelegate<TShip, TComponent>(TShip shipdesign, TComponent componentdescriptor);

    public class ShipComponentHandler<TRace, TShip, TComponent> : HandlerBase<TRace>
        where TRace : RaceBase
        where TShip : ShipBase
        where TComponent : WorldObjectBase, IShipComponent
    {
        private OwnerHashSet<TShip, TComponent> _collection = new OwnerHashSet<TShip, TComponent>();
        private ShipHandler<TRace, TShip> _shiphandler;

        public event ShipHandlerDelegate<TRace, TShip> ShipAdded;
        public event ShipHandlerDelegate<TRace, TShip> ShipRemoved;

        public event ShipComponentHandlerDelegate<TShip, TComponent> ShipComponentAdded;
        public event ShipComponentHandlerDelegate<TShip, TComponent> ShipComponentRemoved;

        protected override void OnStart()
        {
            // Handler suchen
            _shiphandler = World.GetHandler<ShipHandler<TRace, TShip>>();

            // Abonnieren
            _shiphandler.ShipAdd += delegate(TRace race, TShip ship) { _collection.AddOwner(ship); OnShipAdd(race, ship); };
            _shiphandler.ShipRemove += delegate(TRace race, TShip ship) { _collection.RemoveOwner(ship); OnShipRemove(race, ship); };
            base.OnStart();
        }

        public void AddComponent(TShip ship, TComponent component)
        {
            if (_collection.AddElement(ship, component))
                OnComponentAdd(ship, component);
        }

        public void RemoveComponent(TShip ship, TComponent component)
        {
            if (_collection.RemoveElement(ship, component))
                OnComponentRemove(ship, component);
        }

        public IEnumerable<TComponent> GetComponents(TShip design)
        {
            return _collection.GetElements(design);
        }

        protected virtual void OnComponentAdd(TShip ship, TComponent component)
        {
            if (ShipComponentAdded != null) ShipComponentAdded.Invoke(ship, component);
        }

        protected virtual void OnComponentRemove(TShip ship, TComponent component)
        {
            if (ShipComponentRemoved != null) ShipComponentRemoved.Invoke(ship, component);
        }

        protected virtual void OnShipAdd(TRace race, TShip ship)
        {
            if (ShipAdded != null) ShipAdded.Invoke(race, ship);
        }

        protected virtual void OnShipRemove(TRace race, TShip ship)
        {
            if (ShipRemoved != null) ShipRemoved.Invoke(race, ship);
        }
    }
}
