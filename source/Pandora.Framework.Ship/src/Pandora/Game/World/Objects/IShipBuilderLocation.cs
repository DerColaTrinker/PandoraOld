﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    /// <summary>
    /// Eine Schnittstelle die Spielobjekte markiert in denen Schiffe gebaut werden können
    /// </summary>
    public interface IShipBuilderLocation : IGameObject
    {
        /// <summary>
        /// Liefert die Anzahl Warteschlange oder legt sie fest
        /// </summary>
        int MaxShipBuilderQueue { get; set; }

        /// <summary>
        /// Liefert die Anzahl der Bauaufträge die parallel laufen sollen
        /// </summary>
        int ParallelShipUpdates { get; set; }
    }
}
