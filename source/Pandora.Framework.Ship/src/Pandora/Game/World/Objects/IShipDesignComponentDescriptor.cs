﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    /// <summary>
    /// Eine Schnittstelle die eine Komponente Beschreibt die in einem Schiffdesign eingefügt werden kann
    /// </summary>
    public interface IShipDesignComponentDescriptor : IGameObject
    {
    }
}
