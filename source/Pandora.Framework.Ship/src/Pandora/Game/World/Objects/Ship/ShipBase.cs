﻿using Pandora.Game.World.Objects.Ship;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects.Ship
{
    /// <summary>
    /// Stellt ein Raumschiff dar
    /// </summary>
    public abstract class ShipBase : SystemMapObjectBase
    {
        /// <summary>
        /// Liefert die Rasse
        /// </summary>
        public new RaceBase OwnerRace { get; internal set; }

        /// <summary>
        /// Liefert das Schiffdesign
        /// </summary>
        public ShipDesignBase ShipDesign { get; internal set; }

#pragma warning disable 1591
        public override void Serialize(Runtime.IO.IWriter writer)
        {
            base.Serialize(writer);

            writer.WriteUInt32(ShipDesign.ObjectID);
        }

        public override void Deserialize(Runtime.IO.IReader reader)
        {
            base.Deserialize(reader);

            ShipDesign = Instance.Accessor.GetObject<ShipDesignBase>(reader.ReadUInt32());
        }
#pragma warning restore 1591
    }

    /// <summary>
    /// Stellt ein Raumschiff dar
    /// </summary>
    /// <typeparam name="TShipDesign"></typeparam>
    public abstract class ShipBase<TShipDesign> : ShipBase
        where TShipDesign : ShipDesignBase
    {

        /// <summary>
        /// Liefert das verwendete Schiffdesign
        /// </summary>
        public new TShipDesign ShipDesign { get; internal set; }
    }

    /// <summary>
    /// Stellt ein Raumschiff dar
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    /// <typeparam name="TShipDesign"></typeparam>
    public abstract class ShipBase<TRace, TShipDesign> : ShipBase<TShipDesign>
        where TRace : RaceBase
        where TShipDesign : ShipDesignBase<TRace>
    {
        /// <summary>
        /// Liefert die Rasse die das Raumschiff gehört
        /// </summary>
        public new TRace OwnerRace { get { return (TRace)base.OwnerRace; } set { base.OwnerRace = value; } }
    }
}
