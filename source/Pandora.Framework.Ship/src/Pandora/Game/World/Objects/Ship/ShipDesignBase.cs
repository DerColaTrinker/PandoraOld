﻿using Pandora.Game.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects.Ship
{
    /// <summary>
    /// Stellt ein Schiffdesign dar
    /// </summary>
    public abstract class ShipDesignBase : GameObjectBase
    {
        internal ShipDesignBase()
            : base()
        { }
    }

    /// <summary>
    /// Stellt ein Schiffdesign dar
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    public class ShipDesignBase<TRace> : ShipDesignBase
        where TRace : RaceBase
    {
        /// <summary>
        /// Liefert die Rasse die das Design erstellt hat
        /// </summary>
        public new TRace OwnerRace { get { return (TRace)base.OwnerRace; } set { base.OwnerRace = value; } }
    }

    /// <summary>
    /// Stellt ein Schiffdesign dar
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    /// <typeparam name="TTemplate"></typeparam>
    public class ShipDesignBase<TRace, TTemplate> : ShipDesignBase<TRace>
        where TRace : RaceBase
        where TTemplate : TemplateBase
    {
        /// <summary>
        /// Liefert das Template das verwendet wird
        /// </summary>
        public new TTemplate Template { get { return (TTemplate)base.Template; } set { base.Template = value; } }
    }
}
