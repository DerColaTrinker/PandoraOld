﻿using Pandora.Runtime.IO;
using Pandora.Game.World.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Runtime.Collections;
using Pandora.Game.World.Objects.Ship;

namespace Pandora.Game.World.Handlers.ShipDesign
{
    /// <summary>
    /// Ein Delegate das im ShipDesignHandler verwendet wird
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    /// <typeparam name="TShipDesign"></typeparam>
    /// <param name="race"></param>
    /// <param name="design"></param>
    public delegate void ShipDesignHandlerDelegate<TRace, TShipDesign>(TRace race, TShipDesign design);

    /// <summary>
    /// Verwaltet die Schiffdesigns der Rassen
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    /// <typeparam name="TShipDesign"></typeparam>
    public class ShipDesignHandler<TRace, TShipDesign> : WorldHandlerBase<TRace>
        where TRace : RaceBase
        where TShipDesign : ShipDesignBase<TRace>
    {
        private OwnerHashSet<TRace, TShipDesign> _collection = new OwnerHashSet<TRace, TShipDesign>();

        /// <summary>
        /// Wird ausgelöst wenn eine neue Spieler-Rasse eingefügt wird
        /// </summary>
        public event RaceDelegate<TRace> RaceAdded;

        /// <summary>
        /// Wird ausgelöst wenn eine Spieler-Rasse entfernt wird
        /// </summary>
        public event RaceDelegate<TRace> RaceRemoved;

        /// <summary>
        /// Wird ausgelöst wenn ein neues Design eingefügt wird
        /// </summary>
        public event ShipDesignHandlerDelegate<TRace, TShipDesign> ShipDesignAdd;

        /// <summary>
        /// Wird ausgelöst wenn ein Design entfernt wird
        /// </summary>
        public event ShipDesignHandlerDelegate<TRace, TShipDesign> ShipDesignRemove;

#pragma warning disable 1591

        protected override void OnStart()
        {
            Races.RaceAdded += delegate(TRace race) { _collection.AddOwner(race); OnRaceAdd(race); };
            Races.RaceRemoved += delegate(TRace race) { _collection.RemoveOwner(race); OnRaceRemove(race); };

            base.OnStart();
        }

        public override void Serialize(IWriter writer)
        {
            var races = _collection.GetOwners();
            writer.WriteInt32(races.Count());
            foreach (var race in races)
            {
                writer.WriteUInt32(race.ObjectID);

                var desings = GetShipDesigns(race);
                writer.WriteInt32(desings.Count());
                foreach (var design in desings)
                {
                    OnSerializeShipDesign(writer, race, design);
                }
            }
        }

        public override void Deserialize(IReader reader)
        {
            var racecount = reader.ReadInt32();
            for (int raceindex = 0 ; raceindex < racecount ; raceindex++)
            {
                var race = Accessor.GetObject<TRace>(reader.ReadUInt32());
                _collection.AddOwner(race);

                var colonycount = reader.ReadInt32();
                for (int colonyindex = 0 ; colonyindex < colonycount ; colonyindex++)
                {
                    var design = (TShipDesign)null;
                    OnDeserializeShipDesign(reader, race, out design);
                    _collection.AddElement(race, design);
                }
            }
        }

#pragma warning restore 1591

        /// <summary>
        /// Fügt ein neues Schiffdesign hinzu
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="design"></param>
        public void AddDesign(TRace owner, TShipDesign design)
        {
            design.OwnerRace = owner;

            _collection.AddElement(owner, design);
            Accessor.Add(design);
            OnShipDesignAdd(owner, design);
        }

        /// <summary>
        /// Entfernt ein Schiffdesign
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="design"></param>
        public void RemoveDesign(TRace owner, TShipDesign design)
        {
            design.OwnerRace = null;

            _collection.RemoveElement(owner, design);
            Accessor.Remove(design);
            OnShipDesignRemove(owner, design);
        }

        /// <summary>
        /// Liefert eine Auflistung aller Schiffdesings
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TShipDesign> GetShipDesigns()
        {
            return _collection.GetElements().AsEnumerable();
        }

        /// <summary>
        /// Liefert eine Auflistung aller Schiffdesigns der Rasse
        /// </summary>
        /// <param name="race"></param>
        /// <returns></returns>
        public IEnumerable<TShipDesign> GetShipDesigns(TRace race)
        {
            return _collection.GetElements(race).AsEnumerable();
        }

        /// <summary>
        /// Liefert das Schiffdesign
        /// </summary>
        /// <param name="objectid"></param>
        /// <returns></returns>
        public TShipDesign GetShipDesign(uint objectid)
        {
            return Accessor.GetObject<TShipDesign>(objectid);
        }

        /// <summary>
        /// Löst das AddRace-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        protected virtual void OnRaceAdd(TRace race)
        {
            if (RaceAdded != null) RaceAdded.Invoke(race);
        }

        /// <summary>
        /// Löst das RemoveRace-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        protected virtual void OnRaceRemove(TRace race)
        {
            if (RaceRemoved != null) RaceRemoved.Invoke(race);
        }

        /// <summary>
        /// Löst das ShipDesignAdd-Ereignis aus
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="design"></param>
        protected virtual void OnShipDesignAdd(TRace owner, TShipDesign design)
        {
            if (ShipDesignAdd != null) ShipDesignAdd.Invoke(owner, design);
        }

        /// <summary>
        /// Löst das ShipDesignRemove-Ereignis aus
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="design"></param>
        protected virtual void OnShipDesignRemove(TRace owner, TShipDesign design)
        {
            if (ShipDesignRemove != null) ShipDesignRemove.Invoke(owner, design);
        }

        /// <summary>
        /// Wird aufgerufen wenn das Schiffdesign serialisiert wird
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="race"></param>
        /// <param name="design"></param>
        protected virtual void OnSerializeShipDesign(IWriter writer, TRace race, TShipDesign design)
        {
            design.Serialize(writer);
        }

        /// <summary>
        /// Wird aufgerufen wenn das Schiffdesign deserialisiert wird
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="race"></param>
        /// <param name="design"></param>
        protected virtual void OnDeserializeShipDesign(IReader reader, TRace race, out TShipDesign design)
        {
            design = GameObjectBase.CreateObject<TShipDesign>(Instance, reader);
        }
    }
}
