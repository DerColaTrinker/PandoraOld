﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.World.Handlers.Ship
{
    /// <summary>
    /// Zeigt den Status des Schiffbau Auftrags an
    /// </summary>
    public enum ShipBuildStatus
    {
        /// <summary>
        /// Gibt an das sich der Auftrag in der Warteschlage befindet
        /// </summary>
        Pending,

        /// <summary>
        /// Wird gebaut
        /// </summary>
        InProcess,

        /// <summary>
        /// Abgeschlossen
        /// </summary>
        Finished,

        /// <summary>
        /// Abgebrochen
        /// </summary>
        Canceled
    }
}
