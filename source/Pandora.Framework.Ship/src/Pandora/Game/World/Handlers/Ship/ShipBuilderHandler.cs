﻿using Pandora.Game.World.Objects;
using Pandora.Game.World.Objects.Ship;
using Pandora.Runtime.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Handlers.Ship
{
    /// <summary>
    /// Ein Delegate das im ShipBuildHandler verwendet wird und einen Status des Bauauftrags beinhaltet
    /// </summary>
    /// <typeparam name="TContract"></typeparam>
    /// <param name="contract"></param>
    public delegate void ShipBuildHandlerContractDelegate<TContract>(TContract contract);

    /// <summary>
    /// Ein Delegate das im ShipBuildHandler verwendet wird, wenn ein Schiff erzeugt wurde
    /// </summary>
    /// <typeparam name="TContract"></typeparam>
    /// <typeparam name="TShip"></typeparam>
    /// <param name="contract"></param>
    /// <param name="ship"></param>
    public delegate void ShipBuildHandlerShipDelegate<TContract, TShip>(TContract contract, TShip ship);

    /// <summary>
    /// Stellt einen Handler bereit, der den Bau von Schiffen verwaltet
    /// </summary>
    /// <typeparam name="TLocation"></typeparam>
    /// <typeparam name="TContract"></typeparam>
    /// <typeparam name="TShipDesign"></typeparam>
    /// <typeparam name="TShip"></typeparam>
    public class ShipBuildHandler<TLocation, TContract, TShipDesign, TShip> : WorldHandlerBase
        where TLocation : GameObjectBase, IShipBuilderLocation
        where TContract : ShipBuilderContractBase<TLocation, TShipDesign>
        where TShipDesign : ShipDesignBase
        where TShip : ShipBase<TShipDesign>, new()
    {
        private OwnerList<TLocation, TContract> _collection = new OwnerList<TLocation, TContract>() { AutoDeleteOwnerOnEmptyElements = true };

        /// <summary>
        /// Wird aufgerufen wenn ein Bauauftrag erteilt wurde
        /// </summary>
        public event ShipBuildHandlerContractDelegate<TContract> ContractAdded;

        /// <summary>
        /// Wird aufgerufen wenn ein Bauauftrag abgeschlossen wurde
        /// </summary>
        public event ShipBuildHandlerContractDelegate<TContract> ContractFinished;

        /// <summary>
        /// Wird aufgerufen wenn ein Bauauftrag abgebrochen wurde
        /// </summary>
        public event ShipBuildHandlerContractDelegate<TContract> ContractCanceled;

        /// <summary>
        /// Wird aufgerufen wenn ein Schiff erzeugt wurde
        /// </summary>
        public event ShipBuildHandlerShipDelegate<TContract, TShip> ShipCreated;

        /// <summary>
        /// Begint ein Bauauftrag
        /// </summary>
        /// <param name="location"></param>
        /// <param name="design"></param>
        public void BeginBuild(TLocation location, TShipDesign design)
        {
            // Prüfen ob ein Ausbau noch erlaubt ist
            if (_collection.ContainsKey(location))
            {
                if (_collection.GetElementCount(location) >= location.MaxShipBuilderQueue) return;
            }

            // Bauvertrag erstellen und das Prepare auslösen
            var contract = (TContract)Activator.CreateInstance(typeof(TContract));
            contract.ShipDesign = design;
            contract.Location = location;
            OnContractAdded(contract);
        }

        /// <summary>
        /// Löst das ContractAdded-Ereignis aus
        /// </summary>
        /// <param name="contract"></param>
        protected virtual void OnContractAdded(TContract contract)
        {
            if (ContractAdded != null) ContractAdded.Invoke(contract);
        }

        /// <summary>
        /// Löst das ContractFinished-Ereignis aus
        /// </summary>
        /// <param name="contract"></param>
        protected virtual void OnContractFinished(TContract contract)
        {
            if (ContractFinished != null) ContractFinished.Invoke(contract);
        }

        /// <summary>
        /// Löst das ContractCanceled-Ereignis aus
        /// </summary>
        /// <param name="contract"></param>
        protected virtual void OnContractCanceled(TContract contract)
        {
            if (ContractCanceled != null) ContractCanceled.Invoke(contract);
        }

        /// <summary>
        /// Löst das ShipCreated-Ereignis aus
        /// </summary>
        /// <param name="contract"></param>
        /// <param name="ship"></param>
        protected void OnCreateShip(TContract contract, TShip ship)
        {
            if (ShipCreated != null) ShipCreated.Invoke(contract, ship);
        }

#pragma warning disable 1591
        protected override void OnSystemUpdate(float ms, float s)
        {
            foreach (var collection in _collection.GetElementCollections())
            {
                var location = collection.Owner;
                var max = collection.Count > location.MaxShipBuilderQueue ? location.MaxShipBuilderQueue : collection.Count;

                for (int index = 0 ; index < max ; index++)
                {
                    var contract = collection.ElementAt(index);
                    switch (contract.Status)
                    {
                        case ShipBuildStatus.Pending:
                            contract.Begin();
                            break;

                        case ShipBuildStatus.InProcess:
                            contract.WorldUpdate(ms, s);
                            break;

                        case ShipBuildStatus.Finished:
                            _collection.RemoveElement(location, contract);
                            CreateShipAndCallEvents(contract);
                            OnContractFinished(contract);
                            break;

                        case ShipBuildStatus.Canceled:
                            _collection.RemoveElement(location, contract);
                            OnContractCanceled(contract);
                            break;
                    }
                }
            }
        }
#pragma warning restore 1591

        private void CreateShipAndCallEvents(TContract contract)
        {
            var ship = Activator.CreateInstance<TShip>();
            ship.ShipDesign = contract.ShipDesign;

            OnCreateShip(contract, ship);
        }
    }
}
