﻿using Pandora.Runtime.IO;
using Pandora.Game.World.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.World.Objects.Ship;
using Pandora.Runtime.Collections;

namespace Pandora.Game.World.Handlers.Ship
{
    /// <summary>
    /// Ein Delegate das in ShipHandler verwendet wird
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    /// <typeparam name="TShip"></typeparam>
    /// <param name="race"></param>
    /// <param name="ship"></param>
    public delegate void ShipHandlerDelegate<TRace, TShip>(TRace race, TShip ship);

    /// <summary>
    /// Verwaltet die Schiffe des Spielers
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    /// <typeparam name="TShipDesign"></typeparam>
    /// <typeparam name="TShip"></typeparam>
    public class ShipHandler<TRace, TShipDesign, TShip> : WorldHandlerBase<TRace>
        where TRace : RaceBase
        where TShipDesign : ShipDesignBase<TRace>
        where TShip : ShipBase<TRace, TShipDesign>
    {
        private OwnerHashSet<TRace, TShip> _collection = new OwnerHashSet<TRace, TShip>();

        /// <summary>
        /// Wird ausgelöst wenn eine neue Spieler-Rasse eingefügt wird
        /// </summary>
        public event RaceDelegate<TRace> RaceAdded;

        /// <summary>
        /// Wird ausgelöst wenn eine Spieler-Rasse entfernt wird
        /// </summary>
        public event RaceDelegate<TRace> RaceRemoved;

        /// <summary>
        /// Wird ausgelöst wenn ein Schiff hinzugefügt wird
        /// </summary>
        public event ShipHandlerDelegate<TRace, TShip> ShipAdd;

        /// <summary>
        /// Wird ausgelöst wenn ein Schiff entfernt wird
        /// </summary>
        public event ShipHandlerDelegate<TRace, TShip> ShipRemove;

#pragma warning disable 1591

        protected override void OnStart()
        {
            Races.RaceAdded += delegate(TRace race) { _collection.AddOwner(race); OnRaceAdd(race); };
            Races.RaceRemoved += delegate(TRace race) { _collection.RemoveOwner(race); OnRaceRemove(race); };

            base.OnStart();
        }

        public override void Serialize(IWriter writer)
        {
            var races = _collection.GetOwners();
            writer.WriteInt32(races.Count());
            foreach (var race in races)
            {
                writer.WriteUInt32(race.ObjectID);

                var ships = GetShips(race);
                writer.WriteInt32(ships.Count());
                foreach (var ship in ships)
                {
                    OnSerializeShip(writer, race, ship);
                }
            }
        }

        public override void Deserialize(IReader reader)
        {
            var racecount = reader.ReadInt32();
            for (int raceindex = 0 ; raceindex < racecount ; raceindex++)
            {
                var race = Accessor.GetObject<TRace>(reader.ReadUInt32());
                _collection.AddOwner(race);

                var colonycount = reader.ReadInt32();
                for (int colonyindex = 0 ; colonyindex < colonycount ; colonyindex++)
                {
                    var ship = (TShip)null;
                    OnDeserializeShip(reader, race, out ship);
                    _collection.AddElement(race, ship);
                }
            }
        }

#pragma warning restore
        /// <summary>
        /// Fügt ein Schiff hinzu
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="ship"></param>
        public void AddShip(TRace owner, TShip ship)
        {
            ship.OwnerRace = owner;

            _collection.AddElement(owner, ship);
            Accessor.Add(ship);
            OnShipAdd(owner, ship);
        }

        /// <summary>
        /// Entfernt ein Schiff
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="ship"></param>
        public void RemoveShip(TRace owner, TShip ship)
        {
            ship.OwnerRace = null;

            _collection.RemoveElement(owner, ship);
            Accessor.Remove(ship);
            OnShipRemove(owner, ship);
        }

        /// <summary>
        /// Liefert eine Auflistung aller Schiffe
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TShip> GetShips()
        {
            return _collection.GetElements().AsEnumerable();
        }

        /// <summary>
        /// Liefert eine Auflistung aller Schiffe einer Rasse
        /// </summary>
        /// <param name="race"></param>
        /// <returns></returns>
        public IEnumerable<TShip> GetShips(TRace race)
        {
            return _collection.GetElements(race).AsEnumerable();
        }

        /// <summary>
        /// Liefert ein Schiff
        /// </summary>
        /// <param name="objectid"></param>
        /// <returns></returns>
        public TShip GetShip(uint objectid)
        {
            return Accessor.GetObject<TShip>(objectid);
        }

        /// <summary>
        /// Löst das AddRace-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        protected virtual void OnRaceAdd(TRace race)
        {
            if (RaceAdded != null) RaceAdded.Invoke(race);
        }

        /// <summary>
        /// Löst das RemoveRace-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        protected virtual void OnRaceRemove(TRace race)
        {
            if (RaceRemoved != null) RaceRemoved.Invoke(race);
        }

        /// <summary>
        /// Löst das ShipAdd-Ereignis aus
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="ship"></param>
        protected virtual void OnShipAdd(TRace owner, TShip ship)
        {
            if (ShipAdd != null) ShipAdd.Invoke(owner, ship);
        }

        /// <summary>
        /// Löst das ShipRemove-Ereignis aus
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="ship"></param>
        protected virtual void OnShipRemove(TRace owner, TShip ship)
        {
            if (ShipRemove != null) ShipRemove.Invoke(owner, ship);
        }

        /// <summary>
        /// Wird aufgerufen wenn das Schiff serialisiert wird
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="race"></param>
        /// <param name="ship"></param>
        protected virtual void OnSerializeShip(IWriter writer, TRace race, TShip ship)
        {
            ship.Serialize(writer);
        }

        /// <summary>
        /// Wird aufgerufen wenn das Schiff deserialisiert wird
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="race"></param>
        /// <param name="ship"></param>
        protected virtual void OnDeserializeShip(IReader reader, TRace race, out TShip ship)
        {
            ship = GameObjectBase.CreateObject<TShip>(Instance, reader);
        }
    }
}
