﻿using Pandora.Game.World.Objects;
using Pandora.Game.World.Objects.Ship;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.World.Handlers.Ship
{
    /// <summary>
    /// Stellt einen Auftrag dar der ein Schiff erzeigt. Wird zum implementieren des Baufortschritts verwendet.
    /// </summary>
    /// <typeparam name="TLocation"></typeparam>
    /// <typeparam name="TShipDesign"></typeparam>
    public abstract class ShipBuilderContractBase<TLocation, TShipDesign> : GameObjectBase
        where TLocation : GameObjectBase, IShipBuilderLocation
        where TShipDesign : ShipDesignBase
    {
        /// <summary>
        /// Liefert den Ort wo das Schiff gebaut wird
        /// </summary>
        public TLocation Location { get; internal set; }

        /// <summary>
        /// Liefert das Design das gebaut wird
        /// </summary>
        public TShipDesign ShipDesign { get; internal set; }

        /// <summary>
        /// Liefert den Status 
        /// </summary>
        public ShipBuildStatus Status { get; internal set; }

        /// <summary>
        /// Legt den Status auf ShipBuildStatus.InProcess fest und beginnt somit die Produktion
        /// </summary>
        protected internal virtual void Begin()
        {
            Status = ShipBuildStatus.InProcess;
        }

        /// <summary>
        /// Gibt der Engine bekannt, das der Bau abgeschlossen wurde
        /// </summary>
        public void Finish()
        {
            Status = ShipBuildStatus.Finished;
        }

        /// <summary>
        /// Gibt der Engine bekannt, das der Bau abgebrochen wurde
        /// </summary>
        public void Cancel()
        {
            Status = ShipBuildStatus.Canceled;
        }
    }
}
