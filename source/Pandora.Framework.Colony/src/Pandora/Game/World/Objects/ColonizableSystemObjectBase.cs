﻿using Pandora.Game.World.Objects.Colony;
using Pandora.Game.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    /// <summary>
    /// Basisklasse eine SystemMapObject das kolonisiert werden kann
    /// </summary>
    public abstract class ColonizableSystemObjectBase : SystemMapObjectBase
    {
        /// <summary>
        /// Liefert die Kolonie
        /// </summary>
        public ColonyBase Colony { get; protected internal set; }

        /// <summary>
        /// Gibt an ob das Objekt kolonisiert ist
        /// </summary>
        public bool HasColoy { get { return Colony != null; } }
    }

    /// <summary>
    /// Basisklasse eine SystemMapObject das kolonisiert werden kann
    /// </summary>
    /// <typeparam name="TColony"></typeparam>
    public abstract class ColonizableSystemObjectBase<TColony> : ColonizableSystemObjectBase
        where TColony : ColonyBase
    {
        /// <summary>
        /// Liefert die Kolonie
        /// </summary>
        public new TColony Colony { get { return (TColony)base.Colony; } protected internal set { base.Colony = value; } }
    }

    /// <summary>
    /// Basisklasse eine SystemMapObject das kolonisiert werden kann
    /// </summary>
    /// <typeparam name="TTemplate"></typeparam>
    /// <typeparam name="TColony"></typeparam>
    public abstract class ColonizableSystemObjectBase<TTemplate, TColony> : ColonizableSystemObjectBase<TColony>, ITemplate<TTemplate>
        where TTemplate : TemplateBase
        where TColony : ColonyBase
    {
        /// <summary>
        /// Liefert das Template
        /// </summary>
        public new TTemplate Template { get { return (TTemplate)base.Template; } protected internal set { base.Template = value; } }
    }
}
