﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects.Colony
{
    /// <summary>
    /// Basisklasse einer Kolonie
    /// </summary>
    public abstract class ColonyBase : GameObjectBase
    {
        /// <summary>
        /// Liefert das Objekt auf dem die Kolonie errichtet wurde
        /// </summary>
        public ColonizableSystemObjectBase OwnerObject { get; internal set; }

#pragma warning disable 1591
        public override void Serialize(Runtime.IO.IWriter writer)
        {
            base.Serialize(writer);

            writer.WriteUInt32(OwnerObject.ObjectID);
        }

        public override void Deserialize(Runtime.IO.IReader reader)
        {
            base.Deserialize(reader);

            var ownerid = reader.ReadUInt32();

            OwnerObject = Instance.Accessor.GetObject<ColonizableSystemObjectBase>(ownerid);
        }
#pragma warning restore 1591
    }

    /// <summary>
    /// Basisklasse einer Kolonie
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    /// <typeparam name="TSystemObject"></typeparam>
    public abstract class ColonyBase<TRace, TSystemObject> : ColonyBase
        where TRace : RaceBase
        where TSystemObject : ColonizableSystemObjectBase
    {
        /// <summary>
        /// Liefert den Besitzer der Kolonie
        /// </summary>
        public new TRace OwnerRace { get { return (TRace)base.OwnerRace; } set { base.OwnerRace = value; } }

        /// <summary>
        /// Liefert das Objekt auf dem die Kolonie errichtet wurde
        /// </summary>
        public new TSystemObject OwnerObject { get { return (TSystemObject)base.OwnerObject; } protected internal set { base.OwnerObject = value; } }
    }
}
