﻿using Pandora.Runtime.IO;
using Pandora.Game.World.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.World.Objects.Colony;
using Pandora.Runtime.Collections;

namespace Pandora.Game.World.Handlers.Colony
{
    /// <summary>
    /// Ein Delegate das vom ColonyHandler verwendet wird
    /// </summary>
    /// <typeparam name="TColony"></typeparam>
    /// <param name="colony"></param>
    public delegate void ColonyDelegate<TColony>(TColony colony);

    /// <summary>
    /// Ein Delegate das im ColonyHandler verwendet wird
    /// </summary>
    /// <typeparam name="TColony"></typeparam>
    /// <param name="colony"></param>
    /// <param name="type"></param>
    public delegate void ColonizeDelegate<TColony>(TColony colony, ColonizeType type);

    /// <summary>
    /// Verwaltet die Kolonien im Spiel
    /// </summary>
    /// <typeparam name="TRace">Der Type der Rasse</typeparam>
    /// <typeparam name="TSystemObject">Das Objekt das kolonisiert wird, muss sich von ColonizableSystemObjectBase ableiten</typeparam>
    /// <typeparam name="TColony">Der Type der Kolonie</typeparam>
    public class ColonyHandler<TRace, TSystemObject, TColony> : WorldHandlerBase<TRace>
        where TRace : RaceBase
        where TSystemObject : ColonizableSystemObjectBase<TColony>
        where TColony : ColonyBase<TRace, TSystemObject>
    {
        private OwnerHashSet<TRace, TColony> _collection = new OwnerHashSet<TRace, TColony>();

        /// <summary>
        /// Wird ausgelöst wenn eine neue Kolonie erstellt wurde
        /// </summary>
        public event ColonizeDelegate<TColony> ColonyAdded;

        /// <summary>
        /// Wird ausgelöst wenn eine Kolonie entfernt wird
        /// </summary>
        public event ColonyDelegate<TColony> ColonyRemoved;

        /// <summary>
        /// Wird ausgelöst wenn eine neue Spieler-Rasse eingefügt wird
        /// </summary>
        public event RaceDelegate<TRace> RaceAdded;

        /// <summary>
        /// Wird ausgelöst wenn eine Spieler-Rasse entfernt wird
        /// </summary>
        public event RaceDelegate<TRace> RaceRemoved;

        /// <summary>
        /// Wird ausgelöst wenn eine Kolonie Neuberechnet werden muss
        /// </summary>
        public event ColonyDelegate<TColony> ColonyInvalidate;

#pragma warning disable 1591
        protected override void OnStart()
        {
            Races.RaceAdded += delegate(TRace race) { _collection.AddOwner(race); OnRaceAdd(race); };
            Races.RaceRemoved += delegate(TRace race) { _collection.RemoveOwner(race); OnRaceRemove(race); };

            base.OnStart();
        }
#pragma warning restore 1591

        /// <summary>
        /// Löst das AddRace-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        protected virtual void OnRaceAdd(TRace race)
        {
            if (RaceAdded != null) RaceAdded.Invoke(race);
        }

        /// <summary>
        /// Löst das RemoveRace-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        protected virtual void OnRaceRemove(TRace race)
        {
            if (RaceRemoved != null) RaceRemoved.Invoke(race);
        }

        /// <summary>
        /// Kolonisiert das angegebene Objekt
        /// </summary>
        /// <param name="race"></param>
        /// <param name="obj"></param>
        public void Colonize(TRace race, TSystemObject obj)
        {
            Colonize(race, obj, ColonizeType.Default);
        }

        /// <summary>
        /// Kolonisiert ein Objekt
        /// </summary>
        /// <param name="race"></param>
        /// <param name="obj"></param>
        /// <param name="type"></param>
        public void Colonize(TRace race, TSystemObject obj, ColonizeType type)
        {
            if (obj.HasColoy)
            {
                //TODO: Ähm, ist bereits kolonisiert, was nun?
                return;
            }

            var colony = Activator.CreateInstance<TColony>();

            // Koloniewerte festlegen
            colony.OwnerObject = obj;
            colony.OwnerRace = race;

            // Den Handlern zur Verfügung stellen
            _collection.AddElement(race, colony);
            Accessor.Add(colony);

            OnColonize(colony, type);
        }

        /// <summary>
        /// Zerstört eine Kolonie
        /// </summary>
        /// <param name="colony"></param>
        public void Destroy(TColony colony)
        {
            _collection.RemoveElement(colony);
            Accessor.Remove(colony);

            OnDestroy(colony);
        }

        /// <summary>
        /// Löst das ColonyInvalidate-Ereignis aus
        /// </summary>
        /// <param name="colony"></param>
        public virtual void Invalidate(TColony colony)
        {
            if (ColonyInvalidate != null) ColonyInvalidate.Invoke(colony);
        }

        /// <summary>
        /// Löst das AddColony-Ereignis aus
        /// </summary>
        /// <param name="colony"></param>
        /// <param name="type"></param>
        protected virtual void OnColonize(TColony colony, ColonizeType type)
        {
            if (ColonyAdded != null) ColonyAdded.Invoke(colony, type);
        }

        /// <summary>
        /// Löst das RemoveColony-Ereignis aus
        /// </summary>
        /// <param name="colony"></param>
        protected virtual void OnDestroy(TColony colony)
        {
            if (ColonyRemoved != null) ColonyRemoved.Invoke(colony);
        }

        /// <summary>
        /// Liefert eine Liste aller Kolonien
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TColony> GetColonies()
        {
            return _collection.GetElements();
        }

        /// <summary>
        /// Liefert eine Liste aller Kolonien der Rasse
        /// </summary>
        /// <param name="race"></param>
        /// <returns></returns>
        public IEnumerable<TColony> GetElements(TRace race)
        {
            return _collection.GetElements(race);
        }

#pragma warning disable 1591
        public override void Serialize(IWriter writer)
        {
            var races = _collection.GetOwners();
            writer.WriteInt32(races.Count());
            foreach (var race in races)
            {
                writer.WriteUInt32(race.ObjectID);

                var colonies = GetElements(race);
                writer.WriteInt32(colonies.Count());
                foreach (var colony in colonies)
                {
                    OnSerializeColony(writer, race, colony);
                }
            }
        }

        public override void Deserialize(IReader reader)
        {
            var racecount = reader.ReadInt32();
            for (int raceindex = 0 ; raceindex < racecount ; raceindex++)
            {
                var race = Accessor.GetObject<TRace>(reader.ReadUInt32());
                _collection.AddOwner(race);

                var colonycount = reader.ReadInt32();
                for (int colonyindex = 0 ; colonyindex < colonycount ; colonyindex++)
                {
                    var colony = (TColony)null;
                    OnDeserializeColony(reader, race, out colony);
                    _collection.AddElement(race, colony);
                }
            }
        }
#pragma warning restore 1591

        /// <summary>
        /// Wird aufgerufen wenn eine Kolonie serialisiert wird
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="race"></param>
        /// <param name="colony"></param>
        protected virtual void OnDeserializeColony(IReader reader, TRace race, out TColony colony)
        {
            colony = GameObjectBase.CreateObject<TColony>(Instance, reader);
        }

        /// <summary>
        /// Wird aufgerufen wenn eine Kolonie deserialisiert wird
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="race"></param>
        /// <param name="colony"></param>
        protected virtual void OnSerializeColony(IWriter writer, TRace race, TColony colony)
        {
            colony.Serialize(writer);
        }
    }
}
