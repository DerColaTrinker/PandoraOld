﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.World.Handlers.Colony
{
    /// <summary>
    /// Die Art wir eine Kolonie erstellt wurde
    /// </summary>
    public enum ColonizeType
    {
        /// <summary>
        /// Standard
        /// </summary>
        Default = 0,

        /// <summary>
        /// Bei Spielstart durch hinzufügen eines Spielers
        /// </summary>
        RaceAdding = 1
    }
}
