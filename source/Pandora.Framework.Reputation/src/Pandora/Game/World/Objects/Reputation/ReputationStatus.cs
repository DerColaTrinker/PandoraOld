﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.World.Objects.Reputation
{
    /// <summary>
    /// Status des Rufs
    /// </summary>
    public enum ReputationStatus
    {
        /// <summary>
        /// Ehrfürchtig
        /// </summary>
        Exalted,

        /// <summary>
        /// Respektvoll
        /// </summary>
        Revered,

        /// <summary>
        /// Wohlwollen
        /// </summary>
        Honored,

        /// <summary>
        /// Freundlich
        /// </summary>
        Friendly,

        /// <summary>
        /// Neutral
        /// </summary>
        Neutral,

        /// <summary>
        /// Unfreundlich
        /// </summary>
        Unfriendly,

        /// <summary>
        /// Feindselig
        /// </summary>
        Hostile,

        /// <summary>
        /// Hasserfüllt
        /// </summary>
        Hated
    }
}