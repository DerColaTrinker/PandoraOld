﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects.Reputation
{
    /// <summary>
    /// Basisklasse für den Ruf eines Spielers
    /// </summary>
    public abstract class ReputationBase : GameObjectBase
    {
        private int _value;

        /// <summary>
        /// Liefert die Spielerrasse
        /// </summary>
        public new RaceBase OwnerRace { get { return base.OwnerRace; } internal set { base.OwnerRace = value; } }

        /// <summary>
        /// Liefert die Zielrasse
        /// </summary>
        public RaceBase TargetRace { get; internal set; }

        /// <summary>
        /// Liefert den aktuellen Wert des Rufs
        /// </summary>
        public int Value { get { return _value; } internal set { _value = value; UpdateStatus(); } }

        /// <summary>
        /// Liefert den Status des Rufs
        /// </summary>
        public ReputationStatus Reputation { get; private set; }

        /// <summary>
        /// Gibt an ob sich die Zielrasse im Krieg bvefindet
        /// </summary>
        public bool War { get; internal set; }

        /// <summary>
        /// Gibt an ob sich die Zielrasse in einer Allianz befindet
        /// </summary>
        public bool Alliance { get; internal set; }

        private void UpdateStatus()
        {
            if (_value < 36000) Reputation = ReputationStatus.Hated;
            if (_value >= 36000 & _value < 39000) Reputation = ReputationStatus.Hostile;
            if (_value >= 39000 & _value < 42000) Reputation = ReputationStatus.Unfriendly;
            if (_value >= 42000 & _value < 45000) Reputation = ReputationStatus.Neutral;
            if (_value >= 45000 & _value < 51000) Reputation = ReputationStatus.Friendly;
            if (_value >= 51000 & _value < 63000) Reputation = ReputationStatus.Honored;
            if (_value >= 63000 & _value < 84000) Reputation = ReputationStatus.Revered;
            if (_value >= 84000) Reputation = ReputationStatus.Exalted;
        }
    }

    /// <summary>
    /// Basisklasse für de Ruf eines Spielers
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    public abstract class ReputationBase<TRace> : ReputationBase
        where TRace : RaceBase
    {
        /// <summary>
        /// Liefert die Rasse
        /// </summary>
        public new TRace OwnerRace { get { return (TRace)base.OwnerRace; } internal set { base.OwnerRace = value; } }
    }
}
