﻿using Pandora.Game.World.Objects;
using Pandora.Game.World.Objects.Reputation;
using Pandora.Runtime.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Handlers.Reputation
{
    /// <summary>
    /// Ein Delegate das im ReputationHandler verwendet wird
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    /// <typeparam name="TReputation"></typeparam>
    /// <param name="race"></param>
    /// <param name="reputation"></param>
    public delegate void ReputationHandlerDelegate<TRace, TReputation>(TRace race, TReputation reputation);

    /// <summary>
    /// Ein Handler der die Beziehungen zwischen Rassen verwaltet
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    /// <typeparam name="TReputation"></typeparam>
    public class ReputationHandler<TRace, TReputation> : WorldHandlerBase<TRace>
        where TRace : RaceBase
        where TReputation : ReputationBase
    {
        private OwnerHashSet<TRace, TReputation> _collections = new OwnerHashSet<TRace, TReputation>();

#pragma warning disable 1591
        protected override void OnStart()
        {
            Races.RaceAdded += InternalRaceAdd;
            Races.RaceRemoved += InternalRaceRemove;
        }

        private void InternalRaceRemove(TRace race)
        {
            _collections.RemoveOwner(race);
        }

        private void InternalRaceAdd(TRace newrace)
        {
            _collections.AddOwner(newrace);

            foreach (var race in Races.GetRaces().Where(m => m != newrace))
            {
                var instance = Activator.CreateInstance<TReputation>();
                instance.OwnerRace = race;
                _collections.AddElement(newrace, instance);
            }
        }
#pragma warning restore 1591

        /// <summary>
        /// Fügt die Rufpunkte hinzu
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <param name="value"></param>
        public void AddValue(TRace source, TRace target, int value)
        {
            var element = GetReputation(source, target);
            element.Value += value;
        }

        /// <summary>
        /// Entfernt die Rufpunkte
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <param name="value"></param>
        public void SubValue(TRace source, TRace target, int value)
        {
            var element = GetReputation(source, target);
            element.Value -= value;
        }

        /// <summary>
        /// Liefert eine Beziehung
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public TReputation GetReputation(TRace source, TRace target)
        {
            return _collections.GetElements(source).Where(m => m.TargetRace == target).FirstOrDefault();
        }

        /// <summary>
        /// Liefert eine Auflistung aller Beziehungen eines Spielers
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public IEnumerable<TReputation> GetReputations(TRace source)
        {
            return _collections.GetElements(source);
        }

        /// <summary>
        /// Liefert eine Auflistung aller Beziehungen
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TReputation> GetReputations()
        {
            return _collections.GetElements();
        }
    }
}

