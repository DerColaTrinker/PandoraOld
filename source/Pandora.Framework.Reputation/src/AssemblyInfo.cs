﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Pandora Framework - Reputation")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Projekt-Pandora.de")]
[assembly: AssemblyProduct("Pandora Framework - Reputation")]
[assembly: AssemblyCopyright("Copyright © Projekt-Pandora.de 2016-2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("91145ebf-3c58-4a3f-9b74-3a45379a81b2")]

[assembly: AssemblyVersion("0.0.2.0")]
[assembly: AssemblyFileVersion("0.0.2.0")]
