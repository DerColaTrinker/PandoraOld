﻿using Pandora.Interactions;
using Pandora.Interactions.Controllers;
using Pandora.Interactions.Events;
using Pandora.Interactions.Visuals;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Drawing.GLSL;
using Pandora.Interactions.Visuals.Drawing2D;
using Pandora.Interactions.Visuals.Renderer;
using Pandora.Runtime.SFML;
using Pandora.Runtime.Timer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

internal class UnsafeNativeMethods
{
    #region System

    [DllImport("csfml-system-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfClock_create();

    [DllImport("csfml-system-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfClock_destroy(IntPtr pointer);

    [DllImport("csfml-system-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Frame sfClock_getElapsedTime(IntPtr Clock);

    [DllImport("csfml-system-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Frame sfClock_restart(IntPtr Clock);

    [DllImport("csfml-system-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Frame sfSeconds(float Amount);

    [DllImport("csfml-system-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Frame sfMilliseconds(int Amount);

    [DllImport("csfml-system-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Frame sfMicroseconds(long Amount);

    [DllImport("csfml-system-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern float sfTime_asSeconds(Frame time);

    [DllImport("csfml-system-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern int sfTime_asMilliseconds(Frame time);

    [DllImport("csfml-system-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern long sfTime_asMicroseconds(Frame time);

    #endregion

    #region ViewMode

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern VideoMode sfVideoMode_getDesktopMode();

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal unsafe static extern VideoMode* sfVideoMode_getFullscreenModes(out uint Count);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfVideoMode_isValid(VideoMode Mode);

    #endregion

    #region Mouse

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfMouse_isButtonPressed(MouseButton button);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2i sfMouse_getPosition(IntPtr relativeTo);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfMouse_setPosition(Vector2i position, IntPtr relativeTo);

    #endregion

    #region Keyboard

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfKeyboard_isKeyPressed(KeyboardKey Key);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfKeyboard_setVirtualKeyboardVisible(bool visible);

    #endregion

    #region Sensor

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfSensor_isAvailable(SensorType Sensor);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfSensor_setEnabled(SensorType Sensor, bool Enabled);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector3f sfSensor_getValue(SensorType Sensor);

    #endregion

    #region Touch

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfTouch_isDown(uint Finger);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2i sfTouch_getPosition(uint Finger, IntPtr RelativeTo);

    #endregion

    #region Joystick

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfJoystick_isConnected(uint joystick);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern uint sfJoystick_getButtonCount(uint joystick);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfJoystick_hasAxis(uint joystick, JoystickAxis axis);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfJoystick_isButtonPressed(uint joystick, uint button);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern float sfJoystick_getAxisPosition(uint joystick, JoystickAxis axis);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfJoystick_update();

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern JoystickIdentificationMarshalData sfJoystick_getIdentification(uint joystick);

    #endregion

    #region Window

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfWindow_create(VideoMode Mode, string Title, WindowStyle Style, ref ContextSettings Params);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfWindow_createUnicode(VideoMode Mode, IntPtr Title, WindowStyle Style, ref ContextSettings Params);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfWindow_createFromHandle(IntPtr Handle, ref ContextSettings Params);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfWindow_destroy(IntPtr pointer);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfWindow_isOpen(IntPtr pointer);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfWindow_close(IntPtr pointer);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfWindow_pollEvent(IntPtr pointer, out Event Evt);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfWindow_waitEvent(IntPtr pointer, out Event Evt);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfWindow_display(IntPtr pointer);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern ContextSettings sfWindow_getSettings(IntPtr pointer);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2i sfWindow_getPosition(IntPtr pointer);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfWindow_setPosition(IntPtr pointer, Vector2i position);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2u sfWindow_getSize(IntPtr pointer);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfWindow_setSize(IntPtr pointer, Vector2u size);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfWindow_setTitle(IntPtr pointer, string title);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfWindow_setUnicodeTitle(IntPtr pointer, IntPtr title);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static unsafe extern void sfWindow_setIcon(IntPtr pointer, uint Width, uint Height, byte* Pixels);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfWindow_setVisible(IntPtr pointer, bool visible);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfWindow_setMouseCursorVisible(IntPtr pointer, bool Show);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfWindow_setVerticalSyncEnabled(IntPtr pointer, bool Enable);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfWindow_setKeyRepeatEnabled(IntPtr pointer, bool Enable);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfWindow_setActive(IntPtr pointer, bool Active);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfWindow_setFramerateLimit(IntPtr pointer, uint Limit);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern uint sfWindow_getFrameTime(IntPtr pointer);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfWindow_setJoystickThreshold(IntPtr pointer, float Threshold);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfWindow_getSystemHandle(IntPtr pointer);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfWindow_requestFocus(IntPtr pointer);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfWindow_hasFocus(IntPtr pointer);

    #endregion

    #region View

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfView_create();

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfView_createFromRect(FloatRect Rect);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfView_copy(IntPtr View);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfView_destroy(IntPtr View);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfView_setCenter(IntPtr View, Vector2f center);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfView_setSize(IntPtr View, Vector2f size);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfView_setRotation(IntPtr View, float Angle);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfView_setViewport(IntPtr View, FloatRect Viewport);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfView_reset(IntPtr View, FloatRect Rectangle);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2f sfView_getCenter(IntPtr View);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2f sfView_getSize(IntPtr View);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern float sfView_getRotation(IntPtr View);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern FloatRect sfView_getViewport(IntPtr View);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfView_move(IntPtr View, Vector2f offset);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfView_rotate(IntPtr View, float Angle);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfView_zoom(IntPtr View, float Factor);

    #endregion

    #region VertexArray

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfVertexArray_create();

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfVertexArray_copy(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfVertexArray_destroy(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern uint sfVertexArray_getVertexCount(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern unsafe Vertex* sfVertexArray_getVertex(IntPtr pointer, uint index);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfVertexArray_clear(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfVertexArray_resize(IntPtr pointer, uint vertexCount);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfVertexArray_append(IntPtr pointer, Vertex vertex);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfVertexArray_setPrimitiveType(IntPtr pointer, PrimitiveType type);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern PrimitiveType sfVertexArray_getPrimitiveType(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern FloatRect sfVertexArray_getBounds(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_drawVertexArray(IntPtr pointer, IntPtr VertexArray, ref RenderStates.MarshalData states);

    #endregion

    #region Transform

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Transform sfTransform_getInverse(ref Transform transform);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2f sfTransform_transformPoint(ref Transform transform, Vector2f point);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern FloatRect sfTransform_transformRect(ref Transform transform, FloatRect rectangle);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfTransform_combine(ref Transform transform, ref Transform other);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfTransform_translate(ref Transform transform, float x, float y);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfTransform_rotate(ref Transform transform, float angle);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfTransform_rotateWithCenter(ref Transform transform, float angle, float centerX, float centerY);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfTransform_scale(ref Transform transform, float scaleX, float scaleY);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfTransform_scaleWithCenter(ref Transform transform, float scaleX, float scaleY, float centerX, float centerY);

    #endregion

    #region Textures

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfTexture_create(uint width, uint height);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfTexture_createFromFile(string filename, ref IntRect area);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfTexture_createFromStream(IntPtr stream, ref IntRect area);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfTexture_createFromImage(IntPtr image, ref IntRect area);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfTexture_createFromMemory(IntPtr data, ulong size, ref IntRect area);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfTexture_copy(IntPtr texture);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfTexture_destroy(IntPtr texture);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2u sfTexture_getSize(IntPtr texture);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfTexture_copyToImage(IntPtr texture);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static unsafe extern void sfTexture_updateFromPixels(IntPtr texture, byte* pixels, uint width, uint height, uint x, uint y);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfTexture_updateFromImage(IntPtr texture, IntPtr image, uint x, uint y);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfTexture_updateFromWindow(IntPtr texture, IntPtr window, uint x, uint y);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfTexture_updateFromRenderWindow(IntPtr texture, IntPtr renderWindow, uint x, uint y);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfTexture_bind(IntPtr texture);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfTexture_setSmooth(IntPtr texture, bool smooth);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfTexture_isSmooth(IntPtr texture);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfTexture_setRepeated(IntPtr texture, bool repeated);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfTexture_isRepeated(IntPtr texture);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern FloatRect sfTexture_getTexCoords(IntPtr texture, IntRect rectangle);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern uint sfTexture_getMaximumSize();

    #endregion

    #region RenderTexture

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfRenderTexture_create(uint Width, uint Height, bool DepthBuffer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderTexture_destroy(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderTexture_clear(IntPtr pointer, Color ClearColor);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2u sfRenderTexture_getSize(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfRenderTexture_setActive(IntPtr pointer, bool Active);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfRenderTexture_saveGLStates(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfRenderTexture_restoreGLStates(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfRenderTexture_display(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderTexture_setView(IntPtr pointer, IntPtr View);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfRenderTexture_getView(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfRenderTexture_getDefaultView(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntRect sfRenderTexture_getViewport(IntPtr pointer, IntPtr TargetView);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2i sfRenderTexture_mapCoordsToPixel(IntPtr pointer, Vector2f point, IntPtr View);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2f sfRenderTexture_mapPixelToCoords(IntPtr pointer, Vector2i point, IntPtr View);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfRenderTexture_getTexture(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderTexture_setSmooth(IntPtr pointer, bool smooth);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfRenderTexture_isSmooth(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderTexture_setRepeated(IntPtr pointer, bool repeated);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfRenderTexture_isRepeated(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfRenderTexture_generateMipmap(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal unsafe static extern void sfRenderTexture_drawPrimitives(IntPtr pointer, Vertex* vertexPtr, uint vertexCount, PrimitiveType type, ref RenderStates.MarshalData renderStates);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderTexture_drawShape(IntPtr pointer, IntPtr Shape, ref RenderStates.MarshalData states);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderTexture_pushGLStates(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderTexture_popGLStates(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderTexture_resetGLStates(IntPtr pointer);

    #endregion

    #region RenderWindow

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfRenderWindow_create(VideoMode Mode, string Title, WindowStyle Style, ref ContextSettings Params);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfRenderWindow_createUnicode(VideoMode Mode, IntPtr Title, WindowStyle Style, ref ContextSettings Params);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfRenderWindow_createFromHandle(IntPtr Handle, ref ContextSettings Params);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_destroy(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfRenderWindow_isOpen(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_close(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfRenderWindow_pollEvent(IntPtr pointer, out Event Evt);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfRenderWindow_waitEvent(IntPtr pointer, out Event Evt);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_clear(IntPtr pointer, Color ClearColor);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_display(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern ContextSettings sfRenderWindow_getSettings(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2i sfRenderWindow_getPosition(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_setPosition(IntPtr pointer, Vector2i position);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2u sfRenderWindow_getSize(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_setSize(IntPtr pointer, Vector2u size);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_setTitle(IntPtr pointer, string title);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_setUnicodeTitle(IntPtr pointer, IntPtr title);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static unsafe extern void sfRenderWindow_setIcon(IntPtr pointer, uint Width, uint Height, byte* Pixels);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_setVisible(IntPtr pointer, bool visible);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_setMouseCursorVisible(IntPtr pointer, bool visible);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_setMouseCursorGrabbed(IntPtr pointer, bool grabbed);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_setVerticalSyncEnabled(IntPtr pointer, bool Enable);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_setKeyRepeatEnabled(IntPtr pointer, bool Enable);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfRenderWindow_setActive(IntPtr pointer, bool Active);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfRenderWindow_saveGLStates(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfRenderWindow_restoreGLStates(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_setFramerateLimit(IntPtr pointer, uint Limit);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern uint sfRenderWindow_getFrameTime(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_setJoystickThreshold(IntPtr pointer, float Threshold);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_setView(IntPtr pointer, IntPtr View);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfRenderWindow_getView(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfRenderWindow_getDefaultView(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntRect sfRenderWindow_getViewport(IntPtr pointer, IntPtr TargetView);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2i sfRenderWindow_mapCoordsToPixel(IntPtr pointer, Vector2f point, IntPtr View);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2f sfRenderWindow_mapPixelToCoords(IntPtr pointer, Vector2i point, IntPtr View);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static unsafe extern void sfRenderWindow_drawPrimitives(IntPtr pointer, Vertex* vertexPtr, uint vertexCount, PrimitiveType type, ref RenderStates.MarshalData renderStates);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_pushGLStates(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_popGLStates(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_resetGLStates(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfRenderWindow_getSystemHandle(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfRenderWindow_capture(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_requestFocus(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfRenderWindow_hasFocus(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2i sfMouse_getPositionRenderWindow(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfMouse_setPositionRenderWindow(Vector2i position, IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2i sfTouch_getPositionRenderWindow(uint Finger, IntPtr RelativeTo);

    #endregion

    #region Shader

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfShader_createFromFile(string vertexShaderFilename, string geometryShaderFilename, string fragmentShaderFilename);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfShader_createFromMemory(string vertexShader, string geometryShader, string fragmentShader);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfShader_createFromStream(IntPtr vertexShaderStream, IntPtr geometryShaderStream, IntPtr fragmentShaderStream);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_destroy(IntPtr shader);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_setFloatUniform(IntPtr shader, string name, float x);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_setVec2Uniform(IntPtr shader, string name, Vec2 vector);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_setVec3Uniform(IntPtr shader, string name, Vec3 vector);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_setVec4Uniform(IntPtr shader, string name, Vec4 vector);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_setIntUniform(IntPtr shader, string name, int x);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_setIvec2Uniform(IntPtr shader, string name, Ivec2 vector);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_setIvec3Uniform(IntPtr shader, string name, Ivec3 vector);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_setIvec4Uniform(IntPtr shader, string name, Ivec4 vector);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_setBoolUniform(IntPtr shader, string name, bool x);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_setBvec2Uniform(IntPtr shader, string name, Bvec2 vector);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_setBvec3Uniform(IntPtr shader, string name, Bvec3 vector);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_setBvec4Uniform(IntPtr shader, string name, Bvec4 vector);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_setMat3Uniform(IntPtr shader, string name, Mat3 matrix);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_setMat4Uniform(IntPtr shader, string name, Mat4 matrix);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_setTextureUniform(IntPtr shader, string name, IntPtr texture);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_setCurrentTextureUniform(IntPtr shader, string name);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern unsafe void sfShader_setFloatUniformArray(IntPtr shader, string name, float* data, uint length);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern unsafe void sfShader_setVec2UniformArray(IntPtr shader, string name, Vec2* data, uint length);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern unsafe void sfShader_setVec3UniformArray(IntPtr shader, string name, Vec3* data, uint length);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern unsafe void sfShader_setVec4UniformArray(IntPtr shader, string name, Vec4* data, uint length);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern unsafe void sfShader_setMat3UniformArray(IntPtr shader, string name, Mat3* data, uint length);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern unsafe void sfShader_setMat4UniformArray(IntPtr shader, string name, Mat4* data, uint length);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern uint sfShader_getNativeHandle(IntPtr shader);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShader_bind(IntPtr shader);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfShader_isAvailable();

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfShader_isGeometryAvailable();

    #endregion

    #region Image

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfImage_createFromColor(uint Width, uint Height, Color Col);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static unsafe extern IntPtr sfImage_createFromPixels(uint Width, uint Height, byte* Pixels);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfImage_createFromFile(string Filename);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static unsafe extern IntPtr sfImage_createFromStream(IntPtr stream);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static unsafe extern IntPtr sfImage_createFromMemory(IntPtr data, ulong size);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfImage_copy(IntPtr Image);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfImage_destroy(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfImage_saveToFile(IntPtr pointer, string Filename);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfImage_createMaskFromColor(IntPtr pointer, Color Col, byte Alpha);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfImage_copyImage(IntPtr pointer, IntPtr Source, uint DestX, uint DestY, IntRect SourceRect, bool applyAlpha);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfImage_setPixel(IntPtr pointer, uint X, uint Y, Color Col);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Color sfImage_getPixel(IntPtr pointer, uint X, uint Y);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfImage_getPixelsPtr(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2u sfImage_getSize(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern uint sfImage_flipHorizontally(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern uint sfImage_flipVertically(IntPtr pointer);

    #endregion

    #region Font

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfFont_createFromFile(string Filename);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfFont_createFromStream(IntPtr stream);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfFont_createFromMemory(IntPtr data, ulong size);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfFont_copy(IntPtr Font);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfFont_destroy(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Glyph sfFont_getGlyph(IntPtr pointer, uint codePoint, uint characterSize, bool bold);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern float sfFont_getKerning(IntPtr pointer, uint first, uint second, uint characterSize);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern float sfFont_getLineSpacing(IntPtr pointer, uint characterSize);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern float sfFont_getUnderlinePosition(IntPtr pointer, uint characterSize);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern float sfFont_getUnderlineThickness(IntPtr pointer, uint characterSize);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfFont_getTexture(IntPtr pointer, uint characterSize);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern InfoMarshalData sfFont_getInfo(IntPtr pointer);

    #endregion

    #region Shape

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfShape_create(GetPointCountCallbackType getPointCount, GetPointCallbackType getPoint, IntPtr userData);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfShape_copy(IntPtr Shape);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShape_destroy(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShape_setTexture(IntPtr pointer, IntPtr Texture, bool AdjustToNewSize);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShape_setTextureRect(IntPtr pointer, IntRect Rect);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntRect sfShape_getTextureRect(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShape_setFillColor(IntPtr pointer, Color Color);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Color sfShape_getFillColor(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShape_setOutlineColor(IntPtr pointer, Color Color);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Color sfShape_getOutlineColor(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShape_setOutlineThickness(IntPtr pointer, float Thickness);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern float sfShape_getOutlineThickness(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern FloatRect sfShape_getLocalBounds(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfShape_update(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_drawShape(IntPtr pointer, IntPtr Shape, ref RenderStates.MarshalData states);

    #endregion

    #region Context

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfContext_create();

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfContext_destroy(IntPtr pointer);

    /* Änderung SFML 2.4 */
    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern bool sfContext_setActive(IntPtr pointer, bool Active);

    [DllImport("csfml-window-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern ContextSettings sfContext_getSettings(IntPtr pointer);

    #endregion

    #region Sprite

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfSprite_create();

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfSprite_copy(IntPtr Sprite);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfSprite_destroy(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfSprite_setColor(IntPtr pointer, Color Color);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Color sfSprite_getColor(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_drawSprite(IntPtr pointer, IntPtr Sprite, ref RenderStates.MarshalData states);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderTexture_drawSprite(IntPtr pointer, IntPtr Sprite, ref RenderStates.MarshalData states);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfSprite_setTexture(IntPtr pointer, IntPtr Texture, bool AdjustToNewSize);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfSprite_setTextureRect(IntPtr pointer, IntRect Rect);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntRect sfSprite_getTextureRect(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern FloatRect sfSprite_getLocalBounds(IntPtr pointer);

    #endregion

    #region Text

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfText_create();

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfText_copy(IntPtr Text);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfText_destroy(IntPtr pointer);


    /* Änderung SFML 2.4 */
    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfText_setFillColor(IntPtr pointer, Color Color);

    /* Änderung SFML 2.4 */
    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Color sfText_getFillColor(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderWindow_drawText(IntPtr pointer, IntPtr Text, ref RenderStates.MarshalData states);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfRenderTexture_drawText(IntPtr pointer, IntPtr Text, ref RenderStates.MarshalData states);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfText_setUnicodeString(IntPtr pointer, IntPtr Text);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfText_setFont(IntPtr pointer, IntPtr Font);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfText_setCharacterSize(IntPtr pointer, uint Size);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfText_setStyle(IntPtr pointer, TextStyle Style);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfText_getString(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern IntPtr sfText_getUnicodeString(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern uint sfText_getCharacterSize(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern TextStyle sfText_getStyle(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern FloatRect sfText_getRect(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Vector2f sfText_findCharacterPos(IntPtr pointer, uint Index);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern FloatRect sfText_getLocalBounds(IntPtr pointer);

    /* Eingefügt SFML 2.4 */
    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern float sfText_getOutlineThickness(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfText_setOutlineThickness(IntPtr pointer, float thickness);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern Color sfText_getOutlineColor(IntPtr pointer);

    [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
    internal static extern void sfText_setOutlineColor(IntPtr pointer, Color Color);

    #endregion
}
