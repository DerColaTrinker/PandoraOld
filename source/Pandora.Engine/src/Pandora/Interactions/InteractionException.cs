﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#pragma warning disable 1591

namespace Pandora.Interactions
{
    [Serializable]
    public class InteractionException : Exception
    {
        public InteractionException()
        { }

        public InteractionException(string message)
            : base(message)
        { }

        public InteractionException(string message, Exception inner)
            : base(message, inner)
        { }

        protected InteractionException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        { }
    }
}

#pragma warning restore 1591