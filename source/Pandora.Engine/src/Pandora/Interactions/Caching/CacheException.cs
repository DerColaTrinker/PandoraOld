﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#pragma warning disable 1591

namespace Pandora.Interactions.Caching
{
    [Serializable]
    public class CacheException : InteractionException
    {
        public CacheException()
        { }

        public CacheException(string message)
            : base(message)
        { }

        public CacheException(string message, Exception inner)
            : base(message, inner)
        { }

        protected CacheException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        { }
    }
}

#pragma warning restore 1591