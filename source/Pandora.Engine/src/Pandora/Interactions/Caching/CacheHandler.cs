﻿using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Drawing2D;
using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Caching
{
    /// <summary>
    /// Stellt einen Speicher dar, der immer wiederkehrende Daten für die Interaktion bereit hält
    /// </summary>
    public class CacheHandler
    {
        internal CacheHandler()
        {
            Textures = new CacheObjectCollection<Texture>(m => { return new Texture(m); });
            Font = new CacheObjectCollection<Font>(m => { return new Font(m); });

            Color = new CacheObjectCollection<Color>(m =>
                {
                    var reader = new StreamBinaryReader(m);

                    return new Color(reader.ReadByte(), reader.ReadByte(), reader.ReadByte(), reader.ReadByte());
                }
            );    
        }

        /// <summary>
        /// Liefert eine Liste aller Texturen
        /// </summary>
        public CacheObjectCollection<Texture> Textures { get; private set; }

        /// <summary>
        /// Liefert eine Liste aller Schriftarten
        /// </summary>
        public CacheObjectCollection<Font> Font { get; private set; }

        /// <summary>
        /// Liefert eine Liste aller Farben
        /// </summary>
        public CacheObjectCollection<Color> Color { get; private set; }
    }
}
