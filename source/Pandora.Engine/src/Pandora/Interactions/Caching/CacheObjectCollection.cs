﻿using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Caching
{
    /// <summary>
    /// Stellt eine Auflistung für Objekte dar die im Zwischenspeicher gehalten werden
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class CacheObjectCollection<T>
    {
        private Dictionary<string, T> _objects = new Dictionary<string, T>();
        private Func<PandoraContentStream, T> _creator;

        internal CacheObjectCollection(Func<PandoraContentStream, T> creator)
        {
            _creator = creator;
        }

        /// <summary>
        /// Läd eine Resource in den Cache
        /// </summary>
        /// <param name="resourcecode"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public T Load(string resourcecode, string filename)
        {
            return Load(resourcecode, PandoraContentStream.Read(filename));
        }

        /// <summary>
        /// Läd eine Resource in den Cache
        /// </summary>
        /// <param name="resourcecode"></param>
        /// <param name="pathparts"></param>
        /// <returns></returns>
        public T Load(string resourcecode, params string[] pathparts)
        {
            return Load(resourcecode, PandoraContentStream.Read(Path.Combine(pathparts)));
        }

        /// <summary>
        /// Läd eine Resource in den Cache
        /// </summary>
        /// <param name="resourcecode"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public T Load(string resourcecode, PandoraContentStream stream)
        {
            if (_objects.ContainsKey(resourcecode)) throw new ArgumentException("resourcecode already exists");

            var result = _creator.Invoke(stream);
            _objects[resourcecode] = result;

            return result;
        }

        /// <summary>
        /// Versucht ein Objekt aus dem Speicher zu laden oder läd die Resource aus der Datei
        /// </summary>
        /// <param name="resourcecode"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public T GetOrLoad(string resourcecode, string filename)
        {
            return GetOrLoad(resourcecode, PandoraContentStream.Read(filename));
        }

        /// <summary>
        /// Versucht ein Objekt aus dem Speicher zu laden oder läd die Resource aus dem Stream
        /// </summary>
        /// <param name="resourcecode"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public T GetOrLoad(string resourcecode, PandoraContentStream stream)
        {
            if (_objects.ContainsKey(resourcecode))
                return _objects[resourcecode];

            var result = _creator.Invoke(stream);
            _objects[resourcecode] = result;

            return result;
        }

        /// <summary>
        /// Liefert ein Objekt
        /// </summary>
        /// <param name="resourcecode"></param>
        /// <returns></returns>
        public T Get(string resourcecode)
        {
            return _objects[resourcecode];
        }

        /// <summary>
        /// Gibt an ob der angegebene Code verfügbar ist
        /// </summary>
        /// <param name="resourcecode"></param>
        /// <returns></returns>
        public bool Contains(string resourcecode)
        {
            return _objects.ContainsKey(resourcecode);
        }

        /// <summary>
        /// Liefert die Anzahl der geladenen Objekte
        /// </summary>
        public int Count { get { return _objects.Count; } }
    }
}
