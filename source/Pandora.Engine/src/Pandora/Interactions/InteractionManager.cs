﻿using Pandora.Game;
using Pandora.Interactions.Caching;
using Pandora.Interactions.Visuals;
using Pandora.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions
{
    /// <summary>
    /// Stellt das SFML Interaction Modul dar
    /// </summary>
    public sealed class InteractionManager : Instance
    {
        internal InteractionManager()
        { }

#pragma warning disable 1591
        public override bool Initializable { get { return !PandoraRuntime.__disablevisuals; } }
        
        protected override void OnInitialize()
        {
            Cache = new CacheHandler();

            Visuals = new VisualHandler(this);

            Logger.Normal("Interaction initialized");
        }

        protected override void OnStart()
        {
            Visuals.Start();
        }

        protected override void OnSystemUpdate(float ms, float s)
        {
            Visuals.SystemUpdate(ms, s);
        }
#pragma warning restore 1591

        /// <summary>
        /// Liefert das Grafiksystem
        /// </summary>
        public VisualHandler Visuals { get; private set; }

        /// <summary>
        /// Liefert den Cache-Handler
        /// </summary>
        public CacheHandler Cache { get; private set; }
    }
}
