using Pandora.Interactions.Visuals.Drawing;
using System.Runtime.InteropServices;
using System.Security;

namespace Pandora.Interactions.Controllers
{
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// Give access to the real-time state of sensors
    /// </summary>
    ////////////////////////////////////////////////////////////
    public static class Sensor
    {
            ////////////////////////////////////////////////////////////
        /// <summary>
        /// Check if a sensor is available on the underlying platform
        /// </summary>
        /// <param name="Sensor">Sensor to check</param>
        /// <returns>True if the sensor is available, false otherwise</returns>
        ////////////////////////////////////////////////////////////
        public static bool IsAvailable(SensorType Sensor)
        {
            return UnsafeNativeMethods. sfSensor_isAvailable(Sensor);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Enable or disable a sensor
        /// </summary>
        /// <param name="Sensor">Sensor to check</param>
        /// <param name="Enabled">True to enable, false to disable</param>
        ////////////////////////////////////////////////////////////
        public static void SetEnabled(SensorType Sensor, bool Enabled)
        {
            UnsafeNativeMethods.sfSensor_setEnabled(Sensor, Enabled);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Get the current sensor value
        /// </summary>
        /// <param name="Sensor">Sensor to check</param>
        /// <returns>The current sensor value</returns>
        ////////////////////////////////////////////////////////////
        public static Vector3f GetValue(SensorType Sensor)
        {
            return UnsafeNativeMethods.sfSensor_getValue(Sensor);
        }
    }
}
