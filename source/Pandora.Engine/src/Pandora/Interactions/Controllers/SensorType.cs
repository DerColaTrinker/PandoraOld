﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions
{
    /// <summary>
    /// 
    /// </summary>
    public enum SensorType
    {
        /// <summary>
        /// 
        /// </summary>
        Accelerometer,

        /// <summary>
        /// 
        /// </summary>
        Gyroscope,

        /// <summary>
        /// 
        /// </summary>
        Magnetometer,

        /// <summary>
        /// 
        /// </summary>
        Gravity,

        /// <summary>
        /// 
        /// </summary>
        UserAcceleration,

        /// <summary>
        /// 
        /// </summary>
        Orientation,

        /// <summary>
        /// 
        /// </summary>
        TypeCount
    };
}
