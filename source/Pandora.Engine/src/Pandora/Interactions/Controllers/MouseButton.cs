﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions
{
    /// <summary>
    /// 
    /// </summary>
    public enum MouseButton
    {
        /// <summary>
        /// 
        /// </summary>
        Left,

        /// <summary>
        /// 
        /// </summary>
        Right,

        /// <summary>
        /// 
        /// </summary>
        Middle,

        /// <summary>
        /// 
        /// </summary>
        XButton1,

        /// <summary>
        /// 
        /// </summary>
        XButton2,

        /// <summary>
        /// 
        /// </summary>
        ButtonCount
    };
}
