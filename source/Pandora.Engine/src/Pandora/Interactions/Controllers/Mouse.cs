using Pandora.Interactions.Visuals;
using Pandora.Interactions.Visuals.Drawing;
using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Pandora.Interactions.Controllers
{
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// Give access to the real-time state of the mouse
    /// </summary>
    ////////////////////////////////////////////////////////////
    public static class Mouse
    {
        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Mouse wheels
        /// </summary>
        ////////////////////////////////////////////////////////////
        public enum Wheel
        {
            /// <summary>The vertical mouse wheel</summary>
            VerticalWheel,

            /// <summary>The horizontal mouse wheel</summary>
            HorizontalWheel
        };

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Check if a mouse button is pressed
        /// </summary>
        /// <param name="button">Button to check</param>
        /// <returns>True if the button is pressed, false otherwise</returns>
        ////////////////////////////////////////////////////////////
        public static bool IsButtonPressed(MouseButton button)
        {
            return UnsafeNativeMethods.sfMouse_isButtonPressed(button);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Get the current position of the mouse
        /// </summary>
        /// This function returns the current position of the mouse
        /// cursor in desktop coordinates.
        /// <returns>Current position of the mouse</returns>
        ////////////////////////////////////////////////////////////
        public static Vector2i GetPosition()
        {
            return GetPosition(null);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Get the current position of the mouse
        /// </summary>
        /// This function returns the current position of the mouse
        /// cursor relative to a window.
        /// <param name="relativeTo">Reference window</param>
        /// <returns>Current position of the mouse</returns>
        ////////////////////////////////////////////////////////////
        public static Vector2i GetPosition(VisualHandler relativeTo)
        {
            if (relativeTo != null)
                return relativeTo.InternalGetMousePosition();
            else
                return UnsafeNativeMethods.sfMouse_getPosition(IntPtr.Zero);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Set the current position of the mouse
        /// </summary>
        /// This function sets the current position of the mouse
        /// cursor in desktop coordinates.
        /// <param name="position">New position of the mouse</param>
        ////////////////////////////////////////////////////////////
        public static void SetPosition(Vector2i position)
        {
            SetPosition(position, null);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Set the current position of the mouse
        /// </summary>
        /// This function sets the current position of the mouse
        /// cursor relative to a window.
        /// <param name="position">New position of the mouse</param>
        /// <param name="relativeTo">Reference window</param>
        ////////////////////////////////////////////////////////////
        public static void SetPosition(Vector2i position, VisualHandler relativeTo)
        {
            if (relativeTo != null)
                relativeTo.InternalSetMousePosition(position);
            else
                UnsafeNativeMethods.sfMouse_setPosition(position, IntPtr.Zero);
        }
    }
}
