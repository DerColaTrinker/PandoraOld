﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public struct JoystickIdentification
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name;

        /// <summary>
        /// 
        /// </summary>
        public uint VendorId;

        /// <summary>
        /// 
        /// </summary>
        public uint ProductId;
    }
}
