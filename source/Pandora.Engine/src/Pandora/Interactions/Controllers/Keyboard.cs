using System.Runtime.InteropServices;
using System.Security;

namespace Pandora.Interactions.Controllers
{
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// Give access to the real-time state of the keyboard
    /// </summary>
    ////////////////////////////////////////////////////////////
    public static class Keyboard
    {       
        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Check if a key is pressed
        /// </summary>
        /// <param name="key">Key to check</param>
        /// <returns>True if the key is pressed, false otherwise</returns>
        ////////////////////////////////////////////////////////////
        public static bool IsKeyPressed(KeyboardKey key)
        {
            return UnsafeNativeMethods.sfKeyboard_isKeyPressed(key);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Enable/Disable visibility of the virtual keyboard
        /// </summary>
        /// <remarks>Applicable only on Android and iOS</remarks>
        /// <param name="visible">Whether to make the virtual keyboard visible (true) or not (false)</param>
        ////////////////////////////////////////////////////////////
        public static void SetVirtualKeyboardVisible(bool visible)
        {
            UnsafeNativeMethods.sfKeyboard_setVirtualKeyboardVisible(visible);
        }
    }
}
