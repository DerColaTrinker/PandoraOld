﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Renderer
{
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// Enumeration of the blending equations
    /// </summary>
    ////////////////////////////////////////////////////////////
    public enum Equation
    {
        /// <summary>
        /// Pixel = Src * SrcFactor + Dst * DstFactor
        /// </summary>
        Add,

        /// <summary>
        /// Pixel = Src * SrcFactor - Dst * DstFactor
        /// </summary>
        Subtract,

        /// <summary>
        /// Pixel = Dst * DstFactor - Src * SrcFactor
        /// </summary>
        ReverseSubtract
    }
}
