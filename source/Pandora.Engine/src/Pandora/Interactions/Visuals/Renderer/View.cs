﻿using Pandora.Interactions.Visuals.Drawing;
using Pandora.Runtime.SFML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Renderer
{
    /// <summary>
    /// 
    /// </summary>
    public class View : PointerObject
    {
        /// <summary>
        /// 
        /// </summary>
        public View() :
            base()
        {
            InternalPointer = UnsafeNativeMethods.sfView_create();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewRect"></param>
        public View(FloatRect viewRect)
        {
            InternalPointer = UnsafeNativeMethods.sfView_createFromRect(viewRect);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="center"></param>
        /// <param name="size"></param>
        public View(Vector2f center, Vector2f size)
            : this()
        {
            Center = center;
            Size = size;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public View(View copy)
        {
            InternalPointer = UnsafeNativeMethods.sfView_copy(copy.InternalPointer);
        }

        internal View(IntPtr pointer)
        {
            InternalPointer = pointer;
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector2f Center
        {
            get { return UnsafeNativeMethods.sfView_getCenter(InternalPointer); }
            set { UnsafeNativeMethods.sfView_setCenter(InternalPointer, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector2f Size
        {
            get { return UnsafeNativeMethods.sfView_getSize(InternalPointer); }
            set { UnsafeNativeMethods.sfView_setSize(InternalPointer, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Rotation
        {
            get { return UnsafeNativeMethods.sfView_getRotation(InternalPointer); }
            set { UnsafeNativeMethods.sfView_setRotation(InternalPointer, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public FloatRect Viewport
        {
            get { return UnsafeNativeMethods.sfView_getViewport(InternalPointer); }
            set { UnsafeNativeMethods.sfView_setViewport(InternalPointer, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rectangle"></param>
        public void Reset(FloatRect rectangle)
        {
            UnsafeNativeMethods.sfView_reset(InternalPointer, rectangle);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offset"></param>
        public void Move(Vector2f offset)
        {
            UnsafeNativeMethods.sfView_move(InternalPointer, offset);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle"></param>
        public void Rotate(float angle)
        {
            UnsafeNativeMethods.sfView_rotate(InternalPointer, angle);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="factor"></param>
        public void Zoom(float factor)
        {
            UnsafeNativeMethods.sfView_zoom(InternalPointer, factor);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "[View]" +
                   " Center(" + Center + ")" +
                   " Size(" + Size + ")" +
                   " Rotation(" + Rotation + ")" +
                   " Viewport(" + Viewport + ")";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Destroy(bool disposing)
        {
            UnsafeNativeMethods.sfView_destroy(InternalPointer);
        }
    }
}
