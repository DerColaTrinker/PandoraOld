using System;
using System.Runtime.InteropServices;

namespace Pandora.Interactions.Visuals.Renderer
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    internal struct ContextSettings
    {
        [Flags]
        public enum Attribute
        {
            Default = 0,
            Core = 1 << 0,
            Debug = 1 << 2
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="depthBits"></param>
        /// <param name="stencilBits"></param>
        public ContextSettings(uint depthBits, uint stencilBits) :
            this(depthBits, stencilBits, 0)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="depthBits"></param>
        /// <param name="stencilBits"></param>
        /// <param name="antialiasingLevel"></param>
        public ContextSettings(uint depthBits, uint stencilBits, uint antialiasingLevel) :
            this(depthBits, stencilBits, antialiasingLevel, 2, 0, Attribute.Default, false)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="depthBits"></param>
        /// <param name="stencilBits"></param>
        /// <param name="antialiasingLevel"></param>
        /// <param name="majorVersion"></param>
        /// <param name="minorVersion"></param>
        /// <param name="attributes"></param>
        /// <param name="sRgbCapable"></param>
        public ContextSettings(uint depthBits, uint stencilBits, uint antialiasingLevel, uint majorVersion, uint minorVersion, Attribute attributes, bool sRgbCapable)
        {
            DepthBits = depthBits;
            StencilBits = stencilBits;
            AntialiasingLevel = antialiasingLevel;
            MajorVersion = majorVersion;
            MinorVersion = minorVersion;
            AttributeFlags = attributes;
            SRgbCapable = sRgbCapable;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "[ContextSettings]" +
                   " DepthBits(" + DepthBits + ")" +
                   " StencilBits(" + StencilBits + ")" +
                   " AntialiasingLevel(" + AntialiasingLevel + ")" +
                   " MajorVersion(" + MajorVersion + ")" +
                   " MinorVersion(" + MinorVersion + ")" +
                   " AttributeFlags(" + AttributeFlags + ")";
        }

        /// <summary>
        /// 
        /// </summary>
        public uint DepthBits;

        /// <summary>
        /// 
        /// </summary>
        public uint StencilBits;

        /// <summary>
        /// 
        /// </summary>
        public uint AntialiasingLevel;

        /// <summary>
        /// 
        /// </summary>
        public uint MajorVersion;

        /// <summary>
        /// 
        /// </summary>
        public uint MinorVersion;

        /// <summary>
        /// 
        /// </summary>
        public Attribute AttributeFlags;

        /// <summary>
        /// 
        /// </summary>
        public bool SRgbCapable;
    }
}