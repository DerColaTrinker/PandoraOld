using System;
using System.Runtime.InteropServices;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Runtime.SFML;
using Pandora.Interactions.Visuals.Drawing2D;

namespace Pandora.Interactions.Visuals.Renderer
{
    /// <summary>
    /// 
    /// </summary>
    public class RenderTexture : PointerObject, IRenderTarget
    {
        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Create the render-texture with the given dimensions
        /// </summary>
        /// <param name="width">Width of the render-texture</param>
        /// <param name="height">Height of the render-texture</param>
        ////////////////////////////////////////////////////////////
        public RenderTexture(uint width, uint height) :
            this(width, height, false)
        {
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Create the render-texture with the given dimensions and
        /// an optional depth-buffer attached
        /// </summary>
        /// <param name="width">Width of the render-texture</param>
        /// <param name="height">Height of the render-texture</param>
        /// <param name="depthBuffer">Do you want a depth-buffer attached?</param>
        ////////////////////////////////////////////////////////////
        public RenderTexture(uint width, uint height, bool depthBuffer) :
            base()
        {
            InternalPointer = UnsafeNativeMethods.sfRenderTexture_create(width, height, depthBuffer);
            _defaultview = new View(UnsafeNativeMethods.sfRenderTexture_getDefaultView(Pointer));
            _texture = new Texture(UnsafeNativeMethods.sfRenderTexture_getTexture(Pointer));
            GC.SuppressFinalize(_defaultview);
            GC.SuppressFinalize(_texture);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Activate of deactivate the render texture as the current target
        /// for rendering
        /// </summary>
        /// <param name="active">True to activate, false to deactivate (true by default)</param>
        /// <returns>True if operation was successful, false otherwise</returns>
        ////////////////////////////////////////////////////////////
        public bool SetActive(bool active)
        {
            return UnsafeNativeMethods.sfRenderTexture_setActive(Pointer, active);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Enable or disable texture repeating
        /// </summary>
        /// 
        /// <remarks>
        /// This property is similar to <see cref="RenderTexture.Repeated"/>.
        /// This parameter is disabled by default.
        /// </remarks>
        ////////////////////////////////////////////////////////////
        public bool Repeated
        {
            get { return UnsafeNativeMethods.sfRenderTexture_isRepeated(Pointer); }
            set { UnsafeNativeMethods.sfRenderTexture_setRepeated(Pointer, value); }
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Size of the rendering region of the render texture
        /// </summary>
        ////////////////////////////////////////////////////////////
        public Vector2u Size
        {
            get { return UnsafeNativeMethods.sfRenderTexture_getSize(Pointer); }
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Default view of the render texture
        /// </summary>
        ////////////////////////////////////////////////////////////
        public View DefaultView
        {
            get { return new View(_defaultview); }
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Return the current active view
        /// </summary>
        /// <returns>The current view</returns>
        ////////////////////////////////////////////////////////////
        public View GetView()
        {
            return new View(UnsafeNativeMethods.sfRenderTexture_getView(Pointer));
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Change the current active view
        /// </summary>
        /// <param name="view">New view</param>
        ////////////////////////////////////////////////////////////
        public void SetView(View view)
        {
            UnsafeNativeMethods.sfRenderTexture_setView(Pointer, view.Pointer);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Get the viewport of a view applied to this target
        /// </summary>
        /// <param name="view">Target view</param>
        /// <returns>Viewport rectangle, expressed in pixels in the current target</returns>
        ////////////////////////////////////////////////////////////
        public IntRect GetViewport(View view)
        {
            return UnsafeNativeMethods.sfRenderTexture_getViewport(Pointer, view.Pointer);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Convert a point from target coordinates to world
        /// coordinates, using the current view
        ///
        /// This function is an overload of the MapPixelToCoords
        /// function that implicitly uses the current view.
        /// It is equivalent to:
        /// target.MapPixelToCoords(point, target.GetView());
        /// </summary>
        /// <param name="point">Pixel to convert</param>
        /// <returns>The converted point, in "world" coordinates</returns>
        ////////////////////////////////////////////////////////////
        public Vector2f MapPixelToCoords(Vector2i point)
        {
            return MapPixelToCoords(point, GetView());
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Convert a point from target coordinates to world coordinates
        ///
        /// This function finds the 2D position that matches the
        /// given pixel of the render-target. In other words, it does
        /// the inverse of what the graphics card does, to find the
        /// initial position of a rendered pixel.
        ///
        /// Initially, both coordinate systems (world units and target pixels)
        /// match perfectly. But if you define a custom view or resize your
        /// render-target, this assertion is not true anymore, ie. a point
        /// located at (10, 50) in your render-target may map to the point
        /// (150, 75) in your 2D world -- if the view is translated by (140, 25).
        ///
        /// For render-windows, this function is typically used to find
        /// which point (or object) is located below the mouse cursor.
        ///
        /// This version uses a custom view for calculations, see the other
        /// overload of the function if you want to use the current view of the
        /// render-target.
        /// </summary>
        /// <param name="point">Pixel to convert</param>
        /// <param name="view">The view to use for converting the point</param>
        /// <returns>The converted point, in "world" coordinates</returns>
        ////////////////////////////////////////////////////////////
        public Vector2f MapPixelToCoords(Vector2i point, View view)
        {
            return UnsafeNativeMethods.sfRenderTexture_mapPixelToCoords(Pointer, point, view != null ? view.Pointer : IntPtr.Zero);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Convert a point from world coordinates to target
        /// coordinates, using the current view
        ///
        /// This function is an overload of the mapCoordsToPixel
        /// function that implicitly uses the current view.
        /// It is equivalent to:
        /// target.MapCoordsToPixel(point, target.GetView());
        /// </summary>
        /// <param name="point">Point to convert</param>
        /// <returns>The converted point, in target coordinates (pixels)</returns>
        ////////////////////////////////////////////////////////////
        public Vector2i MapCoordsToPixel(Vector2f point)
        {
            return MapCoordsToPixel(point, GetView());
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Convert a point from world coordinates to target coordinates
        ///
        /// This function finds the pixel of the render-target that matches
        /// the given 2D point. In other words, it goes through the same process
        /// as the graphics card, to compute the final position of a rendered point.
        ///
        /// Initially, both coordinate systems (world units and target pixels)
        /// match perfectly. But if you define a custom view or resize your
        /// render-target, this assertion is not true anymore, ie. a point
        /// located at (150, 75) in your 2D world may map to the pixel
        /// (10, 50) of your render-target -- if the view is translated by (140, 25).
        ///
        /// This version uses a custom view for calculations, see the other
        /// overload of the function if you want to use the current view of the
        /// render-target.
        /// </summary>
        /// <param name="point">Point to convert</param>
        /// <param name="view">The view to use for converting the point</param>
        /// <returns>The converted point, in target coordinates (pixels)</returns>
        ////////////////////////////////////////////////////////////
        public Vector2i MapCoordsToPixel(Vector2f point, View view)
        {
            return UnsafeNativeMethods.sfRenderTexture_mapCoordsToPixel(Pointer, point, view != null ? view.Pointer : IntPtr.Zero);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Generate a mipmap using the current texture data
        /// </summary>
        /// 
        /// <remarks>
        /// This function is similar to <see cref="RenderTexture.GenerateMipmap"/> and operates
        /// on the texture used as the target for drawing.
        /// Be aware that any draw operation may modify the base level image data.
        /// For this reason, calling this function only makes sense after all
        /// drawing is completed and display has been called. Not calling display
        /// after subsequent drawing will lead to undefined behavior if a mipmap
        /// had been previously generated.
        /// </remarks>
        /// 
        /// <returns>True if mipmap generation was successful, false if unsuccessful</returns>
        ////////////////////////////////////////////////////////////
        public bool GenerateMipmap()
        {
            return UnsafeNativeMethods.sfRenderTexture_generateMipmap(Pointer);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Clear the entire render texture with black color
        /// </summary>
        ////////////////////////////////////////////////////////////
        public void Clear()
        {
            UnsafeNativeMethods.sfRenderTexture_clear(Pointer, Color.Black);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Clear the entire render texture with a single color
        /// </summary>
        /// <param name="color">Color to use to clear the texture</param>
        ////////////////////////////////////////////////////////////
        public void Clear(Color color)
        {
            UnsafeNativeMethods.sfRenderTexture_clear(Pointer, color);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Update the contents of the target texture
        /// </summary>
        ////////////////////////////////////////////////////////////
        public void Display()
        {
            UnsafeNativeMethods.sfRenderTexture_display(Pointer);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Target texture of the render texture
        /// </summary>
        ////////////////////////////////////////////////////////////
        public Texture Texture
        {
            get { return _texture; }
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Control the smooth filter
        /// </summary>
        ////////////////////////////////////////////////////////////
        public bool Smooth
        {
            get { return UnsafeNativeMethods.sfRenderTexture_isSmooth(Pointer); }
            set { UnsafeNativeMethods.sfRenderTexture_setSmooth(Pointer, value); }
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Save the current OpenGL render states and matrices.
        ///
        /// This function can be used when you mix SFML drawing
        /// and direct OpenGL rendering. Combined with PopGLStates,
        /// it ensures that:
        /// \li SFML's internal states are not messed up by your OpenGL code
        /// \li your OpenGL states are not modified by a call to a SFML function
        ///
        /// More specifically, it must be used around code that
        /// calls Draw functions. Example:
        ///
        /// // OpenGL code here...
        /// window.PushGLStates();
        /// window.Draw(...);
        /// window.Draw(...);
        /// window.PopGLStates();
        /// // OpenGL code here...
        ///
        /// Note that this function is quite expensive: it saves all the
        /// possible OpenGL states and matrices, even the ones you
        /// don't care about. Therefore it should be used wisely.
        /// It is provided for convenience, but the best results will
        /// be achieved if you handle OpenGL states yourself (because
        /// you know which states have really changed, and need to be
        /// saved and restored). Take a look at the ResetGLStates
        /// function if you do so.
        /// </summary>
        ////////////////////////////////////////////////////////////
        public void PushGLStates()
        {
            UnsafeNativeMethods.sfRenderTexture_pushGLStates(Pointer);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Restore the previously saved OpenGL render states and matrices.
        ///
        /// See the description of PushGLStates to get a detailed
        /// description of these functions.
        /// </summary>
        ////////////////////////////////////////////////////////////
        public void PopGLStates()
        {
            UnsafeNativeMethods.sfRenderTexture_popGLStates(Pointer);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Reset the internal OpenGL states so that the target is ready for drawing.
        ///
        /// This function can be used when you mix SFML drawing
        /// and direct OpenGL rendering, if you choose not to use
        /// PushGLStates/PopGLStates. It makes sure that all OpenGL
        /// states needed by SFML are set, so that subsequent Draw()
        /// calls will work as expected.
        ///
        /// Example:
        ///
        /// // OpenGL code here...
        /// glPushAttrib(...);
        /// window.ResetGLStates();
        /// window.Draw(...);
        /// window.Draw(...);
        /// glPopAttrib(...);
        /// // OpenGL code here...
        /// </summary>
        ////////////////////////////////////////////////////////////
        public void ResetGLStates()
        {
            UnsafeNativeMethods.sfRenderTexture_resetGLStates(Pointer);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Provide a string describing the object
        /// </summary>
        /// <returns>String description of the object</returns>
        ////////////////////////////////////////////////////////////
        public override string ToString()
        {
            return "[RenderTexture]" +
                   " Size(" + Size + ")" +
                   " Texture(" + Texture + ")" +
                   " DefaultView(" + DefaultView + ")" +
                   " View(" + GetView() + ")";
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Handle the destruction of the object
        /// </summary>
        /// <param name="disposing">Is the GC disposing the object, or is it an explicit call ?</param>
        ////////////////////////////////////////////////////////////
        protected override void Destroy(bool disposing)
        {
            if (!disposing)
                Context.Global.SetActive(true);

            UnsafeNativeMethods.sfRenderTexture_destroy(Pointer);

            if (disposing)
            {
                _defaultview.Dispose();
                _texture.Dispose();
            }

            if (!disposing)
                Context.Global.SetActive(false);
        }

        private View _defaultview = null;
        private Texture _texture = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pointer"></param>
        /// <param name="state"></param>
        public void DrawShape(IntPtr pointer, RenderStates state)
        {
            var marshal = state.Marshal();

            UnsafeNativeMethods.sfRenderTexture_drawShape(Pointer, pointer, ref marshal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pointer"></param>
        /// <param name="state"></param>
        public void DrawText(IntPtr pointer, RenderStates state)
        {
            var marshal = state.Marshal();

            UnsafeNativeMethods.sfRenderTexture_drawText(Pointer, pointer, ref marshal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertexarray"></param>
        public void DrawVertexArray(VertexArray vertexarray)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pointer"></param>
        /// <param name="states"></param>
        public void DrawVertexArray(IntPtr pointer, RenderStates states)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="type"></param>
        public void Draw(Vertex[] vertices, PrimitiveType type)
        {
            Draw(vertices, type, RenderStates.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="type"></param>
        /// <param name="states"></param>
        public void Draw(Vertex[] vertices, PrimitiveType type, RenderStates states)
        {
            Draw(vertices, 0, (uint)vertices.Length, type, states);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="start"></param>
        /// <param name="count"></param>
        /// <param name="type"></param>
        public void Draw(Vertex[] vertices, uint start, uint count, PrimitiveType type)
        {
            Draw(vertices, start, count, type, RenderStates.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="start"></param>
        /// <param name="count"></param>
        /// <param name="type"></param>
        /// <param name="states"></param>
        public void Draw(Vertex[] vertices, uint start, uint count, PrimitiveType type, RenderStates states)
        {
            RenderStates.MarshalData marshaledStates = states.Marshal();

            unsafe
            {
                fixed (Vertex* vertexPtr = vertices)
                {
                    UnsafeNativeMethods.sfRenderTexture_drawPrimitives(Pointer, vertexPtr + start, count, type, ref marshaledStates);
                }
            }
        }
    }
}
