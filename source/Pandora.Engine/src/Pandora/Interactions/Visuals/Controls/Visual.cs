﻿using Pandora.Interactions.Caching;
using Pandora.Interactions.Visuals.BindingProperties;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Renderer;
using Pandora.Interactions.Visuals.Styles;
using Pandora.Runtime.SFML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Controls
{
    /// <summary>
    /// Ein Delegate das im Objekt Visual verwendet wird
    /// </summary>
    /// <param name="visual"></param>
    public delegate void VisualEventDelegate(Visual visual);

    /// <summary>
    /// Ein Delegate das aufgerufen wird wenn ein RenderUpdate ausgelöst wurde
    /// </summary>
    /// <param name="control"></param>
    /// <param name="target"></param>
    public delegate void RenderUpdateDelegate(Visual control, IRenderTarget target);

    /// <summary>
    /// Basisklasse des Steuerelementsystems. Dient als Basis für alle Steuerelemente und übernimmt die Kommunikation mit den Subsystemen
    /// </summary>
    public abstract class Visual : PointerObject, IRotationProperty, IScaleProperty, IOriginProperty
    {
        internal HashSet<ControlEffectTrigger> _controleffects = new HashSet<ControlEffectTrigger>();

        private Control _parentcontrol;
        private Vector2f _size = new Vector2f();
        private Vector2f _position;
        private string _name = string.Empty;
        private string _key = string.Empty;
        private uint _zorder;
        private bool _visible;
        private Style _style;
        private Vector2f _origin = new Vector2f(0, 0);
        private float _rotation = 0;
        private Vector2f _scale = new Vector2f(1, 1);
        private Transform _transform;
        private Transform _inversetransform;
        private bool _transformneedupdate = true;
        private bool _inverseneedupdate = true;

        /// <summary>
        /// Erstellt eine neue Instanz der Visual-Klasse
        /// </summary>
        protected Visual()
        {
            _visible = true;
            Name = string.Empty;
        }

        #region Events

        /// <summary>
        /// Wird ausgelöst wenn der Name des Steuerelements geändert wird
        /// </summary>
        public event VisualEventDelegate NameChanged;

        /// <summary>
        /// Wird ausgelöst wenn der Key des Steuerelements geändert wird
        /// </summary>
        public event VisualEventDelegate KeyChanged;

        /// <summary>
        /// Wird aufgerufen wenn das Steuerelement geladen wird
        /// </summary>
        public event VisualEventDelegate Load;

        /// <summary>
        /// Wird aufgerufen wenn die ParentControl Eigenschaft verändert wird
        /// </summary>
        public event VisualEventDelegate ParentControlChanged;

        /// <summary>
        /// Wird aufgerufen wenn die ParentLocation Eigenschaft verändert wird
        /// </summary>
        public event VisualEventDelegate ParentLocationChanged;

        /// <summary>
        /// Wird aufgerufen wenn die ParentSize Eigenschaft verändert wird
        /// </summary>
        public event VisualEventDelegate ParentSizeChanged;

        /// <summary>
        /// Wird aufgerufen wenn die Size Eigenschaft verändert wird
        /// </summary>
        public event VisualEventDelegate SizeChanged;

        /// <summary>
        /// Wird aufgerufen wenn die Location Eigenschaft verändert wird
        /// </summary>
        public event VisualEventDelegate PositionChanged;

        /// <summary>
        /// Wird aufgerufen wenn die Padding Eigenschaft verändert wird
        /// </summary>
        public event VisualEventDelegate PaddingChanged;

        /// <summary>
        /// Wird ausgelöst wenn die Rotation geändert wird
        /// </summary>
        public event VisualEventDelegate RotationChanged;

        /// <summary>
        /// Wird ausgelöst wenn die Skallierung geändert wird
        /// </summary>
        public event VisualEventDelegate ScaleChanged;

        /// <summary>
        /// Wird ausgelöst wenn die Ausrichtung geändert wird
        /// </summary>
        public event VisualEventDelegate OriginChanged;

        /// <summary>
        /// Löst aus wenn der Style geändert wird
        /// </summary>
        public event VisualEventDelegate StyleChanged;

        /// <summary>
        /// Wird aufgerufen wenn ein neuer Style geladen wurde
        /// </summary>
        public event VisualEventDelegate StyleSetChanged;

        /// <summary>
        /// Wird aufgerufen wenn das Steuerelement neu gezeichnet werden muss
        /// </summary>
        public event RenderUpdateDelegate RenderUpdate;

        /// <summary>
        /// Wird aufgerufen wenn die ZOrder Eigenschaft verändert wird
        /// </summary>
        public event VisualEventDelegate ZOrderChanged;

        /// <summary>
        /// Wird aufgerufen wenn die Sichtbarkeit geändert wird
        /// </summary>
        public event VisualEventDelegate VisibleChanged;
        private Thickness _padding;

        #endregion

        #region Internals

        internal virtual void InternalLoad(Visual control)
        {
            if (!IsInitialized)
            {
                _parentcontrol = control as Control;
                OnParentControlChanged();

                // Hier den Style nachladen falls vorhanden
                Style = InstanceHolder.__VisualHandler.StyleSet.FindStyle(this);

                OnLoad();
            }

            IsInitialized = true;
        }

        internal virtual void InternalRenderUpdate(IRenderTarget target)
        {
            if (!Visible) return;

            OnRenderUpdate(target);
        }

        internal virtual void InternalStyleSetChanged(StyleSet styleset)
        {
            if (styleset == null) return;

            // Style für dieses Control festlegen
            this.Style = styleset.FindStyle(this);

            OnStyleSetChanged();
        }

        internal virtual void InternalStyleChanged(Style style)
        {
            // Style anwenden wenn vorhanden
            if (_style != null) _style.ApplyStyle(this);

            OnStyleChanged();
        }

        internal virtual void InternalParentLocationChanged(Visual controlCollection)
        {
            OnParentlLocationChanged();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gibt an ob diese Steuerelement initialisiert wurde
        /// </summary>
        public bool IsInitialized { get; private set; }

        /// <summary>
        /// Liefert den Namen des Steuerelements
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; OnNameChanged(); }
        }

        /// <summary>
        /// Liefert einen Key oder legt ihn fest
        /// </summary>
        public string Key
        {
            get { return _key; }
            set { _key = value; OnKeyChanged(); }
        }

        /// <summary>
        /// Liefert das Eltern-Steuerelement
        /// </summary>
        public virtual Control ParentControl
        {
            get { return _parentcontrol; }
            internal set
            {
                _parentcontrol = value;

                OnParentControlChanged();

                if (value == null) return;

                _parentcontrol.PositionChanged += delegate(Visual control) { OnParentlLocationChanged(); };
                _parentcontrol.SizeChanged += delegate(Visual control) { OnParentControlSizeChanged(); };
            }
        }

        /// <summary>
        /// Liefert die größe des Steuerelement oder legt sie fest
        /// </summary>
        public virtual Vector2f Size
        {
            get { return _size; }
            set
            {
                _transformneedupdate = true;
                _inverseneedupdate = true;
                _size = value;
                if (IsInitialized) OnSizeChanged();
            }
        }

        /// <summary>
        /// Liefert die Position des Steuerelements oder legt sie fest
        /// </summary>
        public virtual Vector2f Position
        {
            get
            {
                return _position;
            }
            set
            {
                _transformneedupdate = true;
                _inverseneedupdate = true;
                _position = value;
                if (IsInitialized) OnPositionChanged();
            }
        }

        /// <summary>
        /// Liefert die Bildschirmposition
        /// </summary>
        public Vector2f ScreenPosition
        {
            get
            {
                if (ParentControl == null)
                {
                    return Position;
                }
                else
                {
                    return new Vector2f(ParentControl.Position.X + Position.X + Padding.Left, ParentControl.Position.Y + Position.Y + Padding.Top);
                }
            }
        }

        /// <summary>
        /// Liefert die Bildschirmauflösung
        /// </summary>
        public Vector2f ScreenSize
        {
            get
            {
                if (ParentControl == null)
                {
                    return Size;
                }
                else
                {
                    return new Vector2f(ParentControl.Size.X + Size.X - Padding.Right, ParentControl.Size.Y + Size.Y - Padding.Bottom);
                }
            }
        }

        /// <summary>
        /// Liefert die Dimensionen des Steuerelements
        /// </summary>
        public virtual FloatRect Bounds { get { return new FloatRect(Position, Size); } }

        /// <summary>
        /// Liefert die Dimensionen des Steurelements bezogen auf den Bildschirm
        /// </summary>
        public FloatRect ScreenBounds { get { return new FloatRect(ScreenPosition, ScreenSize); } }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected virtual FloatRect GetLocalBounds()
        {
            return Transform.TransformRect(new FloatRect(Position, Size));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected virtual FloatRect GetGlobalBounds()
        {
            return Transform.TransformRect(new FloatRect(Position, Size));
        }

        /// <summary>
        /// Liefert den angewendeten Style oder legt ihn fest
        /// </summary>
        public Style Style
        {
            get { return _style; }
            set
            {
                _style = value;

                InternalStyleChanged(value);

                if (value != null)
                    OnStyleChanged();
            }
        }

        /// <summary>
        /// Liefert die Reihenfolge in der tiefe.
        /// </summary>
        /// <remarks>Diese Eingenschaft wird meist durch den Container festgelegt und ist abhängig von der RenderPipeline</remarks>
        public virtual uint ZOrder { get { return _zorder; } set { _zorder = value; ; OnZOrderChanged(); } }

        /// <summary>
        /// Gibt an ob das Steuerelement sichtbar ist oder legt es fest
        /// </summary>
        public virtual bool Visible { get { return _visible; } set { _visible = value; OnVisibleChange(); } }

        /// <summary>
        /// Liefert die den Grad der Rotation oder legt ihn fest
        /// </summary>
        public virtual float Rotation
        {
            get
            {
                return _rotation;
            }
            set
            {
                _transformneedupdate = true;
                _inverseneedupdate = true;
                _rotation = value;
                OnRotationChanged();
            }
        }

        /// <summary>
        /// Liefert die Skalierung oder legt sie fest
        /// </summary>
        public virtual Vector2f Scale
        {
            get
            {
                return _scale;
            }
            set
            {
                _transformneedupdate = true;
                _inverseneedupdate = true;
                _scale = value;
                OnScaleChanged();
            }
        }

        /// <summary>
        /// Liefert die Ausrichtungspunkt der Rotation oder legt ihn fest
        /// </summary>
        public virtual Vector2f Origin
        {
            get
            {
                return _origin;
            }
            set
            {
                _transformneedupdate = true;
                _inverseneedupdate = true;
                _origin = value;
                OnOriginChanged();
            }
        }

        /// <summary>
        /// Liefert den Cache-Handler
        /// </summary>
        public CacheHandler Cache { get { return InstanceHolder.__VisualHandler.Manager.Cache; } }

        #endregion

        #region PreEvents

        /// <summary>
        /// Löst das Load-Ereignis aus
        /// </summary>
        protected virtual void OnLoad()
        {
            if (Load != null) Load.Invoke(this);
        }

        /// <summary>
        /// Löst das NameChanged-Ereignis aus
        /// </summary>
        protected virtual void OnNameChanged()
        {
            if (NameChanged != null) NameChanged.Invoke(this);
        }

        /// <summary>
        /// Löst das KeyChanged-Ereignis aus
        /// </summary>
        protected virtual void OnKeyChanged()
        {
            if (KeyChanged != null) KeyChanged.Invoke(this);
        }

        /// <summary>
        /// Löst das -Ereignis aus
        /// </summary>
        protected virtual void OnParentControlChanged()
        {
            if (ParentControlChanged != null) ParentControlChanged.Invoke(this);
        }

        /// <summary>
        /// Wird aufgerufen wenn die ParentControlLocation Eingeschaft geändert wurde
        /// </summary>
        protected virtual void OnParentlLocationChanged()
        {
            Position = ScreenPosition;
            if (ParentLocationChanged != null) ParentLocationChanged.Invoke(this);
        }

        /// <summary>
        /// Löst das ParentSizeChanged-Ereignis aus
        /// </summary>
        protected virtual void OnParentControlSizeChanged()
        {
            if (ParentSizeChanged != null) ParentSizeChanged.Invoke(this);
        }

        /// <summary>
        /// Löst das -Ereignis aus
        /// </summary>
        protected virtual void OnSizeChanged()
        {
            if (SizeChanged != null) SizeChanged.Invoke(this);
        }

        /// <summary>
        /// Löst das -Ereignis aus
        /// </summary>
        protected virtual void OnPositionChanged()
        {
            if (PositionChanged != null) PositionChanged.Invoke(this);
        }

        /// <summary>
        /// Löst das RotationChanged-Ereignis aus
        /// </summary>
        protected virtual void OnRotationChanged()
        {
            if (RotationChanged != null) RotationChanged.Invoke(this);
        }

        /// <summary>
        /// Löst das ScaleChanged-Ereignis aus
        /// </summary>
        protected virtual void OnScaleChanged()
        {
            if (ScaleChanged != null) ScaleChanged.Invoke(this);
        }

        /// <summary>
        /// Löst das OriginChanged-Ereignis aus
        /// </summary>
        protected virtual void OnOriginChanged()
        {
            if (OriginChanged != null) OriginChanged.Invoke(this);
        }

        /// <summary>
        /// Löst das StyleChanged-Ereignis aus
        /// </summary>
        private void OnStyleSetChanged()
        {
            if (StyleSetChanged != null) StyleSetChanged.Invoke(this);
        }

        /// <summary>
        /// Löst das StyleChanged-Ereignis aus
        /// </summary>
        protected virtual void OnStyleChanged()
        {
            if (StyleChanged != null) StyleChanged.Invoke(this);
        }

        /// <summary>
        /// Löst das -Ereignis aus
        /// </summary>
        protected virtual void OnZOrderChanged()
        {
            if (ZOrderChanged != null) ZOrderChanged.Invoke(this);
        }

        /// <summary>
        /// Löst das -Ereignis aus
        /// </summary>
        protected virtual void OnVisibleChange()
        {
            if (VisibleChanged != null) VisibleChanged.Invoke(this);
        }

        /// <summary>
        /// Löst das RenderUpdate-Ereignis aus
        /// </summary>
        /// <param name="target"></param>
        protected virtual void OnRenderUpdate(IRenderTarget target)
        {
            if (RenderUpdate != null) RenderUpdate.Invoke(this, target);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Erstellt eine Transformation aus den aktuellen Daten
        /// </summary>
        public virtual Transform Transform
        {
            get
            {
                if (_transformneedupdate)
                {
                    _transformneedupdate = false;

                    var angle = -_rotation * 3.141592654F / 180.0F;
                    var cosine = (float)Math.Cos(angle);
                    var sine = (float)Math.Sin(angle);
                    var sxc = _scale.X * cosine;
                    var syc = _scale.Y * cosine;
                    var sxs = _scale.X * sine;
                    var sys = _scale.Y * sine;
                    var tx = -_origin.X * sxc - _origin.Y * sys + ScreenPosition.X;
                    var ty = _origin.X * sxs - _origin.Y * syc + ScreenPosition.Y;

                    _transform = new Transform(sxc, sys, tx, -sxs, syc, ty, 0.0F, 0.0F, 1.0F);
                }

                return _transform;
            }
        }

        /// <summary>
        /// Erstellt eine invertierte Transformation
        /// </summary>
        public virtual Transform InverseTransform
        {
            get
            {
                if (_inverseneedupdate)
                {
                    _inversetransform = Transform.GetInverse();
                    _inverseneedupdate = false;
                }
                return _inversetransform;
            }
        }

        /// <summary>
        /// Erstellt eine alternative Positionstransformation
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        protected virtual Transform CustomPositionTransformation(Vector2f position)
        {
            var angle = -_rotation * 3.141592654F / 180.0F;
            var cosine = (float)Math.Cos(angle);
            var sine = (float)Math.Sin(angle);
            var sxc = _scale.X * cosine;
            var syc = _scale.Y * cosine;
            var sxs = _scale.X * sine;
            var sys = _scale.Y * sine;
            var tx = -_origin.X * sxc - _origin.Y * sys + position.X;
            var ty = _origin.X * sxs - _origin.Y * syc + position.Y;

            return new Transform(sxc, sys, tx, -sxs, syc, ty, 0.0F, 0.0F, 1.0F);
        }

        internal RenderStates GetRenderStates()
        {
            return new RenderStates(Transform);
        }

        #endregion

        /// <summary>
        /// Liefert das Padding oder legt es fest
        /// </summary>
        public virtual Thickness Padding
        {
            get { return _padding; }
            set { _padding = value; OnPaddingChanged(); }
        }

        /// <summary>
        /// Wird aufgerufen wenn das Padding geändert wird
        /// </summary>
        protected virtual void OnPaddingChanged()
        {
            if (PaddingChanged != null) PaddingChanged.Invoke(this);
        }
    }
}
