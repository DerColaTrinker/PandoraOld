﻿using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Renderer;
using Pandora.Interactions.Visuals.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Controls
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="window"></param>
    public delegate void SceneEventDelegate(Scene window);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="control"></param>
    public delegate void ControlEventDelegate(Control control);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="control"></param>
    /// <param name="position"></param>
    /// <param name="button"></param>
    public delegate void ControlMouseButtonDelegate(Control control, Vector2f position, MouseButton button);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="control"></param>
    /// <param name="position"></param>
    public delegate void ControlMousePositionDelegate(Control control, Vector2f position);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="control"></param>
    /// <param name="position"></param>
    /// <param name="delta"></param>
    public delegate void ControlMouseWheelDelegate(Control control, Vector2f position, int delta);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="control"></param>
    /// <param name="parent"></param>
    public delegate void ParentLocationChangedDelegate(Control control, Control parent);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="control"></param>
    /// <param name="parent"></param>
    public delegate void ParentSizeChangedDelegate(Control control, Control parent);

}
