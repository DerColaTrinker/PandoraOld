﻿using Pandora.Interactions.Visuals.Drawing;
using System;

#pragma warning disable CS1591

namespace Pandora.Interactions.Visuals.Controls
{
    public interface IPadding
    {
        Thickness Padding { get; set; }
    }
}
