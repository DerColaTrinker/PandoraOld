﻿using Pandora.Interactions.Visuals.Animations;
using Pandora.Interactions.Visuals.BindingProperties;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Drawing2D;
using Pandora.Interactions.Visuals.Renderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Controls.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    public class SpriteControl : TextureControl, IFillColorProperty
    {
        private Texture _texture;

        /// <summary>
        /// 
        /// </summary>
        public SpriteControl()
        {
            InternalPointer = UnsafeNativeMethods.sfSprite_create();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="texture"></param>
        public SpriteControl(Texture texture)
            : this()
        {
            Texture = texture;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="rectangle"></param>
        public SpriteControl(Texture texture, IntRect rectangle) :
            this()
        {
            Texture = texture;
            TextureRect = rectangle;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public SpriteControl(SpriteControl copy)
        {
            InternalPointer = UnsafeNativeMethods.sfSprite_copy(copy.Pointer);
            Origin = copy.Origin;
            Position = copy.Position;
            Rotation = copy.Rotation;
            Scale = copy.Scale;
            Texture = copy.Texture;

        }

        /// <summary>
        /// 
        /// </summary>
        public Color FillColor
        {
            get { return UnsafeNativeMethods.sfSprite_getColor(Pointer); }
            set { UnsafeNativeMethods.sfSprite_setColor(Pointer, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Texture Texture
        {
            get { return _texture; }
            set { _texture = value; UnsafeNativeMethods.sfSprite_setTexture(Pointer, value != null ? value.Pointer : IntPtr.Zero, false); }
        }

        /// <summary>
        /// 
        /// </summary>
        public override IntRect TextureRect
        {
            get { return UnsafeNativeMethods.sfSprite_getTextureRect(Pointer); }
            set { UnsafeNativeMethods.sfSprite_setTextureRect(Pointer, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override FloatRect GetLocalBounds()
        {
            return UnsafeNativeMethods.sfSprite_getLocalBounds(Pointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override FloatRect GetGlobalBounds()
        {
            // we don't use the native getGlobalBounds function,
            // because we override the object's transform
            return Transform.TransformRect(GetLocalBounds());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "[Sprite]" +
                   " Color(" + FillColor + ")" +
                   " Texture(" + Texture + ")" +
                   " TextureRect(" + TextureRect + ")";
        }

        internal override void InternalRenderUpdate(IRenderTarget target)
        {
            base.InternalRenderUpdate(target);

            var state = RenderStates.Default.Marshal();
            UnsafeNativeMethods.sfRenderWindow_drawSprite(target.Pointer, Pointer, ref state);
        }
    }
}
