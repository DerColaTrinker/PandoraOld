﻿using Pandora.Interactions.Visuals.Animations;
using Pandora.Interactions.Visuals.BindingProperties;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Drawing2D;
using Pandora.Interactions.Visuals.Renderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Controls.Primitives
{
    /// <summary>
    /// Stellt ein Text Steuerelement dar
    /// </summary>
    public class TextControl : Visual,
        /* Property Bindings */ ITextColorProperty, IOutlineColorProperty, IOutlineThicknessProperty, IFontProperty, IFontSizeProperty, ITextAlignmentProperty, ITextStyleProperty, IFontResourceCode
    {
        private TextAlignment _textalignment = TextAlignment.TopLeft;
        private Font _font;
        private string _fontresourcecode = string.Empty;

        /// <summary>
        /// Wird aufgerufen wenn die Schriftfarbe geändert wird
        /// </summary>
        public event VisualEventDelegate ColorChanged;

        /// <summary>
        /// Wird aufgerufen wenn der Text geändert wird
        /// </summary>
        public event VisualEventDelegate TextChanged;

        /// <summary>
        /// Wird aufgerufen wenn der Schriftstiel geändert wird
        /// </summary>
        public event VisualEventDelegate TextStyleChanged;

        /// <summary>
        /// 
        /// </summary>
        public event VisualEventDelegate FontSizeChanged;

        /// <summary>
        /// 
        /// </summary>
        public event VisualEventDelegate FontChanged;

        /// <summary>
        /// Erstellt eine neue Instanz der TextControl-Klasse
        /// </summary>
        public TextControl()
        {
            InternalPointer = UnsafeNativeMethods.sfText_create();
        }

        /// <summary>
        /// Löst das ParentControlChanged-Ereginis aus
        /// </summary>
        protected override void OnParentControlChanged()
        {
            ResetTextPosition();

            base.OnParentControlChanged();
        }

        /// <summary>
        /// Löst das ParentlLocationChanged-Ereginis aus
        /// </summary>
        protected override void OnParentlLocationChanged()
        {
            ResetTextPosition();

            base.OnParentlLocationChanged();
        }

        private void ResetTextPosition()
        {
            if (Font == null || FontSize == 0 || string.IsNullOrEmpty(Text)) return;

            TextBounds = GetLocalBounds();

            switch (TextAlignment)
            {
                case TextAlignment.TopLeft: TextPosition = new Vector2f(ScreenPosition.X - TextBounds.Left, ScreenPosition.Y - TextBounds.Top); break;
                case TextAlignment.TopCenter: TextPosition = new Vector2f(ScreenPosition.X + (Size.X / 2F) - ((TextBounds.Left + TextBounds.Width) / 2F), ScreenPosition.Y - TextBounds.Top); break;
                case TextAlignment.TopRight: TextPosition = new Vector2f(ScreenPosition.X + Size.X - (TextBounds.Left + TextBounds.Width), ScreenPosition.Y - TextBounds.Top); break;

                case TextAlignment.MiddleLeft: TextPosition = new Vector2f(ScreenPosition.X - TextBounds.Left, ScreenPosition.Y + (Size.Y / 2F) - ((TextBounds.Top + TextBounds.Height) / 2F)); break;
                case TextAlignment.MiddleCenter: TextPosition = new Vector2f(ScreenPosition.X + (Size.X / 2F) - ((TextBounds.Left + TextBounds.Width) / 2F), ScreenPosition.Y + (Size.Y / 2F) - ((TextBounds.Top + TextBounds.Height) / 2F)); break;
                case TextAlignment.MiddleRight: TextPosition = new Vector2f(ScreenPosition.X + Size.X - (TextBounds.Left + TextBounds.Width), ScreenPosition.Y + (Size.Y / 2F) - ((TextBounds.Top + TextBounds.Height) / 2F)); break;

                case TextAlignment.BottomLeft: TextPosition = new Vector2f(ScreenPosition.X - TextBounds.Left, ScreenPosition.Y + Size.Y - (TextBounds.Top + TextBounds.Height)); break;
                case TextAlignment.BottomCenter: TextPosition = new Vector2f(ScreenPosition.X + (Size.X / 2F) - (TextBounds.Width / 2F), ScreenPosition.Y + Size.Y - (TextBounds.Top + TextBounds.Height)); break;
                case TextAlignment.BottomRight: TextPosition = new Vector2f(ScreenPosition.X + Size.X - TextBounds.Width, ScreenPosition.Y + Size.Y - (TextBounds.Top + TextBounds.Height)); break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override FloatRect GetGlobalBounds()
        {
            return Transform.TransformRect(GetLocalBounds());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override FloatRect GetLocalBounds()
        {
            return UnsafeNativeMethods.sfText_getLocalBounds(Pointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Vector2f FindCharacterPos(uint index)
        {
            return UnsafeNativeMethods.sfText_findCharacterPos(Pointer, index);
        }

        internal Vector2f TextPosition
        {
            get;
            set;
        }

        /// <summary>
        /// Liefert die Ausmaße des Textes
        /// </summary>
        public FloatRect TextBounds { get; private set; }

        /// <summary>
        /// Gibt an ob die Größe des Steuerelements sich des Textes anpassen sollen oder legt es fest
        /// </summary>
        public virtual bool AutoSize { get; set; }

        /// <summary>
        /// Liefert die Schriftfarbe oder legt sie fest
        /// </summary>
        public Color TextColor
        {
            get
            {
                return UnsafeNativeMethods.sfText_getFillColor(Pointer);
            }
            set
            {
                UnsafeNativeMethods.sfText_setFillColor(Pointer, value);
                OnColorChanged();
            }
        }

        /// <summary>
        /// Liefert die Rahmenfarbe oder legt sie fest
        /// </summary>
        public Color OutlineColor
        {
            get { return UnsafeNativeMethods.sfText_getOutlineColor(Pointer); }
            set { UnsafeNativeMethods.sfText_setOutlineColor(Pointer, value); }
        }

        /// <summary>
        /// Liefrt die Rahmendicke oder legt sie fest
        /// </summary>
        public float OutlineThickness
        {
            get { return UnsafeNativeMethods.sfText_getOutlineThickness(Pointer); }
            set { UnsafeNativeMethods.sfText_setOutlineThickness(Pointer, value); }
        }

        /// <summary>
        /// Liefert den Text der Angezeigt wird order legt ihn fest
        /// </summary>
        public virtual string Text
        {
            get
            {
                // Get a pointer to the source string (UTF-32)
                IntPtr source = UnsafeNativeMethods.sfText_getUnicodeString(Pointer);

                // Find its length (find the terminating 0)
                uint length = 0;
                unsafe
                {
                    for (uint* ptr = (uint*)source.ToPointer() ; *ptr != 0 ; ++ptr)
                        length++;
                }

                // Copy it to a byte array
                byte[] sourceBytes = new byte[length * 4];
                Marshal.Copy(source, sourceBytes, 0, sourceBytes.Length);

                // Convert it to a C# string
                return Encoding.UTF32.GetString(sourceBytes);
            }

            set
            {
                // Copy the string to a null-terminated UTF-32 byte array
                byte[] utf32 = Encoding.UTF32.GetBytes(value + '\0');

                // Pass it to the C API
                unsafe
                {
                    fixed (byte* ptr = utf32)
                    {
                        UnsafeNativeMethods.sfText_setUnicodeString(Pointer, (IntPtr)ptr);
                    }
                }

                ResetTextPosition();
            }
        }

        /// <summary>
        /// Liefert die Schiftgröße oder legt sie fest
        /// </summary>
        public virtual uint FontSize
        {
            get
            {
                return UnsafeNativeMethods.sfText_getCharacterSize(Pointer);
            }
            set
            {
                UnsafeNativeMethods.sfText_setCharacterSize(Pointer, value);
                ResetTextPosition();
                OnFontSizeChanged();
            }
        }

        /// <summary>
        /// Liefert den Source-Code für die Schriftart oder legt sie fest
        /// </summary>
        public virtual string FontResourceCode
        {
            get { return _fontresourcecode; }
            set
            {
                if (string.IsNullOrEmpty(value)) return;

                _font = Cache.Font.Get(value);

                UnsafeNativeMethods.sfText_setFont(Pointer, _font.Pointer);
                ResetTextPosition();
                OnFontChanged();
            }
        }

        /// <summary>
        /// Gibt die aktuelle Schriftart zurück oder legt sie fest
        /// </summary>
        public virtual Font Font
        {
            get
            {
                return _font;
            }
            set
            {
                _font = value;
                _fontresourcecode = string.Empty;

                UnsafeNativeMethods.sfText_setFont(Pointer, value.Pointer);
                ResetTextPosition();
                OnFontChanged();
            }
        }

        /// <summary>
        /// Gibt die Position des Steuerelements zurück oder legt es fest
        /// </summary>
        public override Vector2f Position
        {
            get
            {
                return base.Position;
            }
            set
            {
                base.Position = value;
                ResetTextPosition();
            }
        }

        /// <summary>
        /// Gibt die Ausmaße des Steurelements zurück oder legt es fest
        /// </summary>
        public override Vector2f Size
        {
            get
            {
                return base.Size;
            }
            set
            {
                // Wenn Autosize ausgeschaltet ist, kann die größe erst weitergegeben werden
                if (!AutoSize)
                {
                    base.Size = value;
                }

                ResetTextPosition();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public TextAlignment TextAlignment
        {
            get { return _textalignment; }
            set { _textalignment = value; ResetTextPosition(); }
        }

        /// <summary>
        /// Liefert den Schiftstiel oder legt ihn fest
        /// </summary>
        public TextStyle TextStyle
        {
            get
            {
                return UnsafeNativeMethods.sfText_getStyle(Pointer);
            }
            set
            {
                UnsafeNativeMethods.sfText_setStyle(Pointer, value);
                ResetTextPosition();
                OnTextStyleChanged();
            }
        }

        /// <summary>
        /// Zeichnet das Steuerelement
        /// </summary>
        /// <param name="target"></param>
        internal override void InternalRenderUpdate(IRenderTarget target)
        {
            var state = new RenderStates(CustomPositionTransformation(TextPosition));

            target.DrawText(Pointer, state);
        }

        /// <summary>
        /// Wird aufgerufen wenn die Schriftfarbe geändert wird
        /// </summary>
        protected virtual void OnColorChanged()
        {
            if (ColorChanged != null) ColorChanged.Invoke(this);
        }

        /// <summary>
        /// Wird aufgerufen wenn der Schriftstiel geändert wird
        /// </summary>
        protected virtual void OnTextStyleChanged()
        {
            if (TextStyleChanged != null) TextStyleChanged.Invoke(this);
        }

        /// <summary>
        /// Wird aufgerufen wenn der Text geändert wird
        /// </summary>
        protected virtual void OnTextChanged()
        {
            if (TextChanged != null) TextChanged.Invoke(this);
        }

        /// <summary>
        /// Wird aufgerufen wenn die Schriftgröße geändert wird
        /// </summary>
        protected virtual void OnFontSizeChanged()
        {
            if (FontSizeChanged != null) FontSizeChanged.Invoke(this);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnFontChanged()
        {
            if (FontChanged != null) FontChanged.Invoke(this);
        }
    }
}
