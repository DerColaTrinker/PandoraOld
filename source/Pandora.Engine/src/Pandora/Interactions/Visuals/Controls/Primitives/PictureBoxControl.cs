﻿using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Drawing2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Controls.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    public class PictureBoxControl : RectShapeControl
    {
        /// <summary>
        /// 
        /// </summary>
        public PictureBoxControl()
            : this(new Vector2f(0, 0))
        {
          
        }

        /// <summary>
        /// 
        /// </summary>
        public PictureBoxControl(Texture texture)
            : this(new Vector2f(0, 0),texture)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="size"></param>
        public PictureBoxControl(Vector2f size)
        {
            Size = size;
    
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="size"></param>
        /// <param name="texture"></param>
        public PictureBoxControl(Vector2f size, Texture texture)
        {
            Size = size;
            Texture = texture;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public PictureBoxControl(PictureBoxControl copy) :
            base(copy)
        {
            Size = copy.Size;
            base.PointCount = 4;
        }
    }
}
