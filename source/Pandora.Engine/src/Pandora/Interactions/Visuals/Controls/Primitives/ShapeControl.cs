using Pandora.Interactions.Visuals.Animations;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Renderer;
using Pandora.Runtime.SFML;
using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Pandora.Interactions.Visuals.Controls.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class ShapeControl : TextureControl
    {
        private GetPointCountCallbackType myGetPointCountCallback;
        private GetPointCallbackType myGetPointCallback;

        private uint _pointcount;

        /// <summary>
        /// 
        /// </summary>
        protected ShapeControl()
        {
            InternalPointer = IntPtr.Zero;
            myGetPointCountCallback = new GetPointCountCallbackType(InternalGetPointCount);
            myGetPointCallback = new GetPointCallbackType(InternalGetPoint);
            InternalPointer = UnsafeNativeMethods.sfShape_create(myGetPointCountCallback, myGetPointCallback, IntPtr.Zero);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public ShapeControl(ShapeControl copy)
            : base(copy)
        {
            InternalPointer = IntPtr.Zero;
            myGetPointCountCallback = new GetPointCountCallbackType(InternalGetPointCount);
            myGetPointCallback = new GetPointCallbackType(InternalGetPoint);
            InternalPointer = UnsafeNativeMethods.sfShape_create(myGetPointCountCallback, myGetPointCallback, IntPtr.Zero);

            Origin = copy.Origin;
            Position = copy.Position;
            Rotation = copy.Rotation;
            Scale = copy.Scale;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Destroy(bool disposing)
        {
            UnsafeNativeMethods.sfShape_destroy(Pointer);
        }

        /// <summary>
        /// 
        /// </summary>
        public override Vector2f Size
        {
            get
            {
                return base.Size;
            }
            set
            {
                base.Size = value;
                UpdatePoints();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual uint PointCount
        {
            get { return _pointcount; }
            set
            {
                _pointcount = value;
                UpdatePoints();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        protected abstract Vector2f GetPoint(uint index);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override FloatRect GetLocalBounds()
        {
            return UnsafeNativeMethods.sfShape_getLocalBounds(Pointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override FloatRect GetGlobalBounds()
        {
            return Transform.TransformRect(GetLocalBounds());
        }

        /// <summary>
        /// 
        /// </summary>
        protected void UpdatePoints()
        {
            UnsafeNativeMethods.sfShape_update(Pointer);
        }

        internal override void InternalRenderUpdate(IRenderTarget target)
        {
            var state = new RenderStates(Transform);

            target.DrawShape(Pointer, state);
        }

        private uint InternalGetPointCount(IntPtr userData)
        {
            return PointCount;
        }

        private Vector2f InternalGetPoint(uint index, IntPtr userData)
        {
            return GetPoint(index);
        }


    }
}
