using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Drawing2D;
using Pandora.Interactions.Visuals.Renderer;
using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Pandora.Interactions.Visuals.Controls.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    public class VertexArrayControl : Visual
    {
        /// <summary>
        /// 
        /// </summary>
        public VertexArrayControl()
        {
            InternalPointer = UnsafeNativeMethods.sfVertexArray_create();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        public VertexArrayControl(PrimitiveType type) :
            this()
        {
            PrimitiveType = type;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="vertexCount"></param>
        public VertexArrayControl(PrimitiveType type, uint vertexCount) :
            this()
        {
            PrimitiveType = type;
            Resize(vertexCount);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public VertexArrayControl(VertexArrayControl copy)
        {
            InternalPointer = UnsafeNativeMethods.sfVertexArray_copy(copy.InternalPointer);
        }

        /// <summary>
        /// 
        /// </summary>
        public uint VertexCount
        {
            get { return UnsafeNativeMethods.sfVertexArray_getVertexCount(InternalPointer); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Vertex this[uint index]
        {
            get
            {
                unsafe
                {
                    return *UnsafeNativeMethods.sfVertexArray_getVertex(InternalPointer, index);
                }
            }
            set
            {
                unsafe
                {
                    *UnsafeNativeMethods.sfVertexArray_getVertex(InternalPointer, index) = value;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Clear()
        {
            UnsafeNativeMethods.sfVertexArray_clear(InternalPointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertexCount"></param>
        public void Resize(uint vertexCount)
        {
            UnsafeNativeMethods.sfVertexArray_resize(InternalPointer, vertexCount);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertex"></param>
        public void Append(Vertex vertex)
        {
            UnsafeNativeMethods.sfVertexArray_append(InternalPointer, vertex);
        }

        /// <summary>
        /// 
        /// </summary>
        public PrimitiveType PrimitiveType
        {
            get { return UnsafeNativeMethods.sfVertexArray_getPrimitiveType(InternalPointer); }
            set { UnsafeNativeMethods.sfVertexArray_setPrimitiveType(InternalPointer, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public override FloatRect Bounds
        {
            get { return UnsafeNativeMethods.sfVertexArray_getBounds(InternalPointer); }
        }

        internal override void InternalRenderUpdate(IRenderTarget target)
        {
            var state = new RenderStates(Transform);

            target.DrawVertexArray(Pointer, state);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Destroy(bool disposing)
        {
            UnsafeNativeMethods.sfVertexArray_destroy(InternalPointer);
        }
    }
}
