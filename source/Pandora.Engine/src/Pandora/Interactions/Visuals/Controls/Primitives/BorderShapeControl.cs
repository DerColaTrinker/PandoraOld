﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Interactions.Visuals.Animations;
using Pandora.Interactions.Visuals.Drawing;
using System.Diagnostics.CodeAnalysis;
using Pandora.Interactions.Visuals.BindingProperties;

namespace Pandora.Interactions.Visuals.Controls.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class BorderShape : ShapeControl, IFillColorProperty, IOutlineColorProperty
    {
        /// <summary>
        /// 
        /// </summary>
        public event VisualEventDelegate FillColorChanged;

        /// <summary>
        /// 
        /// </summary>
        public event VisualEventDelegate OutlineColorChanged;

        /// <summary>
        /// 
        /// </summary>
        public event VisualEventDelegate OutlineThicknessChanged;

        /// <summary>
        /// 
        /// </summary>
        public BorderShape()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public BorderShape(BorderShape copy)
            : base(copy)
        {
            FillColor = copy.FillColor;
            OutlineColor = copy.OutlineColor;
            OutlineThickness = copy.OutlineThickness;
        }

        /// <summary>
        /// 
        /// </summary>
        public Color FillColor
        {
            get { return UnsafeNativeMethods.sfShape_getFillColor(Pointer); }
            set
            {
                UnsafeNativeMethods.sfShape_setFillColor(Pointer, value);
                OnFillColorChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Color OutlineColor
        {
            get { return UnsafeNativeMethods.sfShape_getOutlineColor(Pointer); }
            set
            {
                UnsafeNativeMethods.sfShape_setOutlineColor(Pointer, value);
                OnOutlineColorChanged();
                UpdatePoints();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float OutlineThickness
        {
            get { return UnsafeNativeMethods.sfShape_getOutlineThickness(Pointer); }
            set
            {
                UnsafeNativeMethods.sfShape_setOutlineThickness(Pointer, value);
                OnOutlineThicknessChanged();
                UpdatePoints();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnFillColorChanged()
        {
            if (FillColorChanged != null) FillColorChanged.Invoke(this);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnOutlineColorChanged()
        {
            if (OutlineColorChanged != null) OutlineColorChanged.Invoke(this);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnOutlineThicknessChanged()
        {
            if (OutlineThicknessChanged != null) OutlineThicknessChanged.Invoke(this);
        }
    }
}
