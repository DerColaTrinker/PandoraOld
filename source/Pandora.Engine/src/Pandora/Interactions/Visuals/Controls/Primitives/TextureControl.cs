﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Drawing2D;

namespace Pandora.Interactions.Visuals.Controls.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class TextureControl : Visual
    {
        private Texture _texture;

        /// <summary>
        /// 
        /// </summary>
        public event VisualEventDelegate TextureChanged;

        /// <summary>
        /// 
        /// </summary>
        public event VisualEventDelegate TextureRectChanged;

        /// <summary>
        /// 
        /// </summary>
        public TextureControl()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public TextureControl(TextureControl copy)
        {
            Texture = copy.Texture;
            TextureRect = copy.TextureRect;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Texture Texture
        {
            get { return _texture; }
            set
            {
                _texture = value;
                UnsafeNativeMethods.sfShape_setTexture(Pointer, value != null ? value.Pointer : IntPtr.Zero, false);
                OnTextureChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual IntRect TextureRect
        {
            get { return UnsafeNativeMethods.sfShape_getTextureRect(Pointer); }
            set
            {
                UnsafeNativeMethods.sfShape_setTextureRect(Pointer, value);
                OnTextureRectChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnTextureChanged()
        {
            if (TextureChanged != null) TextureChanged.Invoke(this);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnTextureRectChanged()
        {
            if (TextureRectChanged != null) TextureRectChanged.Invoke(this);
        }
    }
}
