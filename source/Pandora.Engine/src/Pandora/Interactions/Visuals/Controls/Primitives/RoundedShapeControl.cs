﻿using Pandora.Interactions.Visuals.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Controls.Primitives
{
    /// <summary>
    /// 
    /// </summary>
    public class RoundedShapeControl : BorderShape
    {
        private float _radius = 3F;
        private uint _cornerpointcount;

        /// <summary>
        /// 
        /// </summary>
        public RoundedShapeControl()
            : this(new Vector2f(0, 0))
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="size"></param>
        public RoundedShapeControl(Vector2f size)
        {
            Size = size;
            CornerPointerCount = 5;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public RoundedShapeControl(RoundedShapeControl copy) :
            base(copy)
        {
            Size = copy.Size;
            Radius = copy.Radius;
            CornerPointerCount = copy.CornerPointerCount;
        }

        /// <summary>
        /// 
        /// </summary>
        public float Radius { get { return _radius; } set { _radius = value; UpdatePoints(); } }

        /// <summary>
        /// 
        /// </summary>
        public uint CornerPointerCount { get { return _cornerpointcount; } set { _cornerpointcount = value; PointCount = value * 4; } }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        protected override Vector2f GetPoint(uint index)
        {
            if (index >= PointCount) return new Vector2f();

            var deltaangle = 90F / (_cornerpointcount - 1);
            var center = new Vector2f();
            var centerindex = index / _cornerpointcount;
         
            switch (centerindex)
            {
                case 0: center.X = Size.X - _radius; center.Y = _radius; break;
                case 1: center.X = _radius; center.Y = _radius; break;
                case 2: center.X = _radius; center.Y = Size.Y - _radius; break;
                case 3: center.X = Size.X - _radius; center.Y = Size.Y - _radius; break;
            }

            return new Vector2f(_radius * (float)Math.Cos(deltaangle * (index - centerindex) * (float)Math.PI / 180) + center.X, -_radius * (float)Math.Sin(deltaangle * (index - centerindex) * (float)Math.PI / 180) + center.Y);
        }
    }
}
