﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Controls
{
    /// <summary>
    /// Stellt ein Antribut dar, das Scenen Namen vergeben kann
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public sealed class SceneAttribute : Attribute
    {
        /// <summary>
        /// Erstellt eine neue Instanz der SceneAttribute-Klasse
        /// </summary>
        /// <param name="scenename"></param>
        public SceneAttribute(string scenename)
        {
            Scenename = scenename;
        }

        /// <summary>
        /// Liefert den Scenen-Namen
        /// </summary>
        public string Scenename { get; private set; }

        /// <summary>
        /// Gibt an das die Scene die Startscene ist oder legt es fest
        /// </summary>
        public bool IsStartScene { get; set; }
    }
}
