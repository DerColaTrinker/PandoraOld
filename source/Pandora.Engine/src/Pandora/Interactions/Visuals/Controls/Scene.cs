﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Drawing2D;
using Pandora.Interactions.Visuals.Renderer;
using Pandora.Interactions.Visuals.Controls.Primitives;

namespace Pandora.Interactions.Visuals.Controls
{
    /// <summary>
    /// Stellt ein Ausgabefenster dar, das Steuerelemente aufnehmen kann
    /// </summary>
    public abstract class Scene : ControlContainer
    {
        private RectShapeControl _backgroundshape;

        /// <summary>
        /// Wird aufgerufen wenn das Fenster wieder aktiv ist
        /// </summary>
        public event SceneEventDelegate SceneShow;

        /// <summary>
        /// 
        /// </summary>
        public event SceneEventDelegate SceneClose;

        /// <summary>
        /// 
        /// </summary>
        public event SceneEventDelegate SceneResize;

        /// <summary>
        /// Erstellt eine neue Instanz der Scene-Klasse
        /// </summary>
        public Scene()
        {
            _backgroundshape = new RectShapeControl();
        }

        internal override void InternalLoad(Visual control)
        {
            base.InternalLoad(control);

            _backgroundshape.FillColor = new Color(30, 30, 30);

            UIControls.Add(_backgroundshape);
        }

        internal void InternalShow()
        {
            _backgroundshape.Position = Position;
            _backgroundshape.Size = Visuals.Size;

            OnShow();
        }

        internal void InternalClose()
        {
            OnClose();
        }

        internal void InternalSceneResize(Vector2f size)
        {
            base.Size = size;
            _backgroundshape.Size = Size;

            OnResize();
        }

        /// <summary>
        /// Zeigt die Szene an
        /// </summary>
        public void Show()
        {
            Show(this);
        }

        /// <summary>
        /// Zeigt die Szene an
        /// </summary>
        /// <param name="scene"></param>
        public void Show(Scene scene)
        {
            if (InstanceHolder.__VisualHandler == null)
                throw new ArgumentNullException("StaticHandler", "Window show not allowed, Visual handler not initialized.");

            ParentControl = null;

            InstanceHolder.__VisualHandler.Show(scene);
        }

        /// <summary>
        /// Schließt die Szene
        /// </summary>
        public void Close()
        {
            if (InstanceHolder.__VisualHandler == null)
                throw new ArgumentNullException("StaticHandler", "Window show not allowed, Visual handler not initialized.");

            InstanceHolder.__VisualHandler.Close(this);
        }

        /// <summary>
        /// Liefert die Hintergrundfarbe oder legt sie Fest
        /// </summary>
        public Color BackgroundColor
        {
            get { return _backgroundshape.FillColor; }
            set { _backgroundshape.FillColor = value; }
        }

        /// <summary>
        /// Liefert die Größe der Szene oder legt sie fest
        /// </summary>
        public override Vector2f Size
        {
            get
            {
                return base.Size;
            }

            set
            {
                /* Nichts machen, die Größe ist nicht änderbar */
            }
        }

        /// <summary>
        /// Liefert die Position der Szene oder legt sie fest
        /// </summary>
        public override Vector2f Position
        {
            get
            {
                return base.Position;
            }

            set
            {
                /* Die Position ist nicht änderbar */
            }
        }

        /// <summary>
        /// Löst das SceneShow-Ereignis aus
        /// </summary>
        protected virtual void OnShow()
        {
            if (SceneShow != null) SceneShow.Invoke(this);
        }

        /// <summary>
        /// Löst das SceneClose-Ereignis aus
        /// </summary>
        protected virtual void OnClose()
        {
            if (SceneClose != null) SceneClose.Invoke(this);
        }

        /// <summary>
        /// Löst das SceneResize-Ereignis aus
        /// </summary>
        protected virtual void OnResize()
        {
            if (SceneResize != null) SceneResize.Invoke(this);
        }
    }
}
