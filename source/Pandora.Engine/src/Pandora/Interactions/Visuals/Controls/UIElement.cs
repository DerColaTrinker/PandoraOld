﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Controls
{
    /// <summary>
    /// Stellt ein Grafisches Element dar, das auf dem Bildschirm angezeigt werden kann
    /// </summary>
    public abstract class UIElement : Visual
    {
        /// <summary>
        /// Erstellt eine neue Instanz der UIElement-Klasse
        /// </summary>
        protected UIElement()
            : base()
        {
            UIControls = new UIElementCollection(this);
        }

        /// <summary>
        /// Liefert eine Auflistung von Controls die nur gezeichnet werden jedoch keine weiteren Events auswerten
        /// </summary>
        public UIElementCollection UIControls { get; private set; }

        internal override void InternalRenderUpdate(Renderer.IRenderTarget target)
        {
            base.InternalRenderUpdate(target);

            if (Visible) UIControls.InternalRenderUpdate(target);
        }

        internal override void InternalStyleSetChanged(Styles.StyleSet styleset)
        {
            base.InternalStyleSetChanged(styleset);

            // UI Elemente übernehmen
            UIControls.InternalStyleSetChanged(styleset);
        }

        internal override void InternalStyleChanged(Styles.Style style)
        {
            base.InternalStyleChanged(style);

            // UI Elemente übernehmen
            UIControls.InternalStyleChanged(style);
        }

        internal override void InternalParentLocationChanged(Visual controlCollection)
        {
            UIControls.InternalLocationChanged();
            base.InternalParentLocationChanged(controlCollection);
        }
    }
}
