﻿using Pandora.Interactions.Visuals.Animations;
using Pandora.Interactions.Visuals.Animations.Effects;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Styles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Interactions.Visuals.Controls
{
    sealed class ControlEffectTrigger
    {
        public ControlEffectTrigger(IControl control, StyleTrigger trigger)
        {
            Effects = new HashSet<EffectBase>();
            Control = control as Control;
            Trigger = trigger;

            // Das Event umleiten
            switch (trigger.Event)
            {
                case TriggerEventTypes.MouseEnter:
                    Control.MouseEnter += EventTarget;
                    break;

                case TriggerEventTypes.MouseLeave:
                    Control.MouseLeave += EventTarget;
                    break;

                default:
                    break;
            }

            var triggereffects = trigger.GetEffects();
            foreach (var triggereffect in triggereffects)
            {
                switch (triggereffect.Effecttype.Name)
                {
                    case "FillColorBlendEffect":
                        Effects.Add((EffectBase)Activator.CreateInstance(triggereffect.Effecttype, control, triggereffect.Value, triggereffect.Time));
                        break;

                    case "FillColorEffect":
                        Effects.Add((EffectBase)Activator.CreateInstance(triggereffect.Effecttype, control, (Color)triggereffect.Value, triggereffect.Time));
                        break;

                    case "FillColorFadeInEffect":
                        Effects.Add((EffectBase)Activator.CreateInstance(triggereffect.Effecttype, control, triggereffect.Time));
                        break;

                    case "FillColorFadeOutEffect":
                        Effects.Add((EffectBase)Activator.CreateInstance(triggereffect.Effecttype, control, triggereffect.Time));
                        break;

                    case "OuterLineColorEffect":
                        Effects.Add((EffectBase)Activator.CreateInstance(triggereffect.Effecttype, control, triggereffect.Value, triggereffect.Time));
                        break;

                    case "TextColorBlendEffect":
                        Effects.Add((EffectBase)Activator.CreateInstance(triggereffect.Effecttype, control, triggereffect.Time));
                        break;

                    case "TextColorEffect":
                        Effects.Add((EffectBase)Activator.CreateInstance(triggereffect.Effecttype, control, triggereffect.Time));
                        break;

                    case "TextColorFadeInEffect":
                        Effects.Add((EffectBase)Activator.CreateInstance(triggereffect.Effecttype, control, triggereffect.Time));
                        break;

                    case "TextColorFadeOutEffect":
                        Effects.Add((EffectBase)Activator.CreateInstance(triggereffect.Effecttype, control, triggereffect.Time));
                        break;

                    default:
                        break;
                }
            }
        }

        internal HashSet<EffectBase> Effects { get; private set; }

        public StyleTrigger Trigger { get; private set; }

        public Control Control { get; set; }

        internal void EventTarget(Control control, Vector2f position)
        {
            control.Animation.Start(Effects);
        }
    }
}
