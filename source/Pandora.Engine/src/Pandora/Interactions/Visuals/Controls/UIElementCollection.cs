﻿using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Renderer;
using Pandora.Interactions.Visuals.Styles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Controls
{
    /// <summary>
    /// Stellt eine Sammlung von Steuerelementen dar die nur gezeichnet werden.
    /// </summary>
    public sealed class UIElementCollection : IEnumerable<Visual>
    {
        #region neasted

        class ControlCompare : IComparer<Visual>
        {
            #region IComparer<Control> Member

            public int Compare(Visual x, Visual y)
            {
                if (x.ZOrder > y.ZOrder)
                    return 1;
                else if (x.ZOrder < y.ZOrder)
                    return -1;

                return 0;
            }

            #endregion
        }

        #endregion

        private UIElement _parentcontrol;
        private List<Visual> _controls = new List<Visual>();

        internal UIElementCollection(UIElement control)
        {
            _parentcontrol = control;
        }

        /// <summary>
        /// Fügt eine Gruppe von Steuerelementen hinzu
        /// </summary>
        /// <param name="controls"></param>
        public void AddRange(IEnumerable<Visual> controls)
        {
            foreach (var control in controls)
            {
                _controls.Add(control);
                control.InternalLoad(_parentcontrol);
            }

            ReorderControls();
        }

        /// <summary>
        /// Fügt ein Steuerelement hinzu
        /// </summary>
        /// <param name="control"></param>
        public void Add(Visual control)
        {
            _controls.Add(control);
            control.InternalLoad(_parentcontrol);

            ReorderControls();
        }

        /// <summary>
        /// Entfernt ein Steuerelement
        /// </summary>
        /// <param name="control"></param>
        public void Remove(Visual control)
        {
            _controls.Remove(control);

            ReorderControls();
        }

        /// <summary>
        /// Liefert die Anzahl der Steuerelemente
        /// </summary>
        public int Count { get { return _controls.Count; } }

        private void ReorderControls()
        {
            _controls.Sort(new ControlCompare());
        }

        internal void InternalRenderUpdate(IRenderTarget target)
        {
            foreach (var control in _controls)
            {
                control.InternalRenderUpdate(target);
            }
        }

        /// <summary>
        /// Liefert das Steuerelement an der angegebenen Position
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public Visual GetUIControl(Vector2f position)
        {
            return _controls.Where(m => m.ScreenBounds.Contains(position)).LastOrDefault();
        }

        /// <summary>
        /// Liefert eine Auflistung aller Steuerelemente die an dieser Position zu finden sind
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public IEnumerable<Visual> GetUIControls(Vector2f position)
        {
            return _controls.Where(m => m.ScreenBounds.Contains(position)).Reverse();
        }

        #region IEnumerable<Control> Member

        /// <summary>
        /// Liefert eine Auflistung der Steuerelemente
        /// </summary>
        /// <returns></returns>
        public IEnumerator<Visual> GetEnumerator()
        {
            return _controls.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _controls.GetEnumerator();
        }

        #endregion

        internal void InternalLocationChanged()
        {
            foreach (var control in _controls)
            {
                control.InternalParentLocationChanged(_parentcontrol);
            }
        }

        internal void InternalStyleSetChanged(StyleSet styleset)
        {
            foreach (var control in _controls)
            {
                control.InternalStyleSetChanged(styleset);
            }
        }

        internal void InternalStyleChanged(Style style)
        {
            foreach (var control in _controls)
            {
                control.InternalStyleChanged(style);
            }
        }
    }
}
