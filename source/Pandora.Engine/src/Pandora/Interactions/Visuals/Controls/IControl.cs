﻿using System;

#pragma warning disable CS1591

namespace Pandora.Interactions.Visuals.Controls
{
    /// <summary>
    /// Stellt eine Schnittstelle dar die ein Steuerelement eindeutig identifiziert
    /// </summary>
    public interface IControl
    { }
}
