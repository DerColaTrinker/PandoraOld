﻿using Pandora.Interactions.Caching;
using Pandora.Interactions.Visuals.Animations;
using Pandora.Interactions.Visuals.BindingProperties;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Renderer;
using Pandora.Interactions.Visuals.Styles;
using Pandora.Runtime.SFML;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Controls
{
    /// <summary>
    /// Stellt die Basis eines Steuerelements dar
    /// </summary>
    public abstract class Control : UIElement, IPositionProperty, ISizeProperty, IControl
    {
        /// <summary>
        /// Wird aufgerufen wenn eine Maustaste gedrückt wurde
        /// </summary>
        public event ControlMouseButtonDelegate MouseButtonPressed;

        /// <summary>
        /// Wird aufgerufen wenn eine Maustaste losgelassen wird
        /// </summary>
        public event ControlMouseButtonDelegate MouseButtonReleased;

        /// <summary>
        /// Wird aufgerufen wenn die Maus bewegt wird
        /// </summary>
        public event ControlMousePositionDelegate MouseMoved;

        /// <summary>
        /// Wird aufgerufen wenn die Maus ein Doppelklick auslöst
        /// </summary>
        public event ControlMousePositionDelegate MouseDoubleClick;

        /// <summary>
        /// Wird aufgerufen wenn der Mauszeiger das Steuerelement betritt
        /// </summary>
        public event ControlMousePositionDelegate MouseEnter;

        /// <summary>
        /// Wird aufgerufen wenn der Mauszeiger das Steuerelement verlässt
        /// </summary>
        public event ControlMousePositionDelegate MouseLeave;

        /// <summary>
        /// 
        /// </summary>
        public event ControlMouseWheelDelegate MouseWheel;

        /// <summary>
        /// Erstellt eine neue Instanz der Control-Klasse
        /// </summary>
        protected Control()
            : base()
        {
            InternalPointer = IntPtr.Zero;
        }

        /// <summary>
        /// Liefert die Interaction Manager
        /// </summary>
        public InteractionManager Interactions { get { return InstanceHolder.__VisualHandler.Manager; } }

        /// <summary>
        /// Liefert die UI Steuerung 
        /// </summary>
        public IVisualHandler Visuals { get { return InstanceHolder.__VisualHandler; } }

        /// <summary>
        /// Liefert die Animation-Engine
        /// </summary>
        public AnimationHandler Animation { get { return InstanceHolder.__VisualHandler.Animation; } }

        internal override void InternalStyleChanged(Style style)
        {
            base.InternalStyleChanged(style);

            if (Style != null) Style.ApplyAnimations(this);
        }

        internal virtual bool InternalMouseButtonPressed(Vector2f position, MouseButton button)
        {
            return OnMouseButtonPressed(position - ScreenPosition, button);
        }

        internal virtual bool InternalMouseButtonReleased(Vector2f position, MouseButton button)
        {
            InstanceHolder.__LastMouseButtonReleaseControl = this;

            return OnMouseButtonReleased(position - ScreenPosition, button);
        }

        internal virtual bool InternalMouseDoubleClick(Vector2f position)
        {
            if (InstanceHolder.__LastMouseButtonReleaseControl != this) return false;

            return OnMouseDoubleClick(position - ScreenPosition);
        }

        internal virtual bool InternalMouseMoved(Vector2f position)
        {
            return OnMouseMoved(position - ScreenPosition);
        }

        internal virtual Control InternalMouseHover(Vector2f position)
        {
            if (MouseEnter != null)
                return this;

            return null;
        }

        internal virtual bool InternalMouseEnter(Vector2f position)
        {
            if (OnMouseEnter(position - ScreenPosition))
                return true;

            return false;
        }

        internal virtual bool InternalMouseLeave(Vector2f position)
        {
            return OnMouseLeave(position - ScreenPosition);
        }

        internal virtual bool InternalMouseWheel(Vector2f position, int delta)
        {
            return OnMouseWheel(position - ScreenPosition, delta);
        }

        /// <summary>
        /// Löst das -Ereignis aus
        /// </summary>
        /// <param name="position"></param>
        /// <param name="button"></param>
        protected virtual bool OnMouseButtonPressed(Vector2f position, MouseButton button)
        {
            if (MouseButtonPressed != null)
            {
                MouseButtonPressed.Invoke(this, position, button);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Löst das -Ereignis aus
        /// </summary>
        /// <param name="position"></param>
        /// <param name="button"></param>
        protected virtual bool OnMouseButtonReleased(Vector2f position, MouseButton button)
        {
            if (MouseButtonReleased != null)
            {
                MouseButtonReleased.Invoke(this, position, button);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Löst das -Ereignis aus
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        protected virtual bool OnMouseDoubleClick(Vector2f position)
        {
            if (MouseDoubleClick != null)
            {
                MouseDoubleClick.Invoke(this, position);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Löst das -Ereignis aus
        /// </summary>
        /// <param name="position"></param>
        protected virtual bool OnMouseMoved(Vector2f position)
        {
            if (MouseMoved != null)
            {
                MouseMoved.Invoke(this, position);
                return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="delta"></param>
        protected virtual bool OnMouseWheel(Vector2f position, int delta)
        {
            if (MouseWheel != null)
            {
                MouseWheel.Invoke(this, position, delta);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Löst das -Ereignis aus
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        internal virtual bool OnMouseEnter(Vector2f position)
        {
            if (MouseEnter != null)
            {
                MouseEnter.Invoke(this, position);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Löst das -Ereignis aus
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        internal virtual bool OnMouseLeave(Vector2f position)
        {
            if (MouseLeave != null)
            {
                MouseLeave.Invoke(this, position);
                return true;
            }

            return false;
        }
    }
}
