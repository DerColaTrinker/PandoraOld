﻿using Pandora.Interactions.Visuals.Controls;
using Pandora.Interactions.Visuals.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#pragma warning disable 1591

namespace Pandora.Interactions.Visuals
{
    internal static class InstanceHolder
    {
        internal static IVisualHandler __VisualHandler { get; set; }
        internal static Vector2f __zerovector = new Vector2f();
        internal static Control __LastMouseButtonReleaseControl;

                //internal static Control __hovercontrol;
    }
}
