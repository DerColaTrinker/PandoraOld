﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Events
{
    [StructLayout(LayoutKind.Explicit, Size = 20)]
    internal struct Event
    {
        [FieldOffset(0)]
        public EventType Type;

        [FieldOffset(4)]
        public SizeEvent Size;

        [FieldOffset(4)]
        public KeyEvent Key;

        [FieldOffset(4)]
        public TextEvent Text;

        [FieldOffset(4)]
        public MouseMoveEvent MouseMove;

        [FieldOffset(4)]
        public MouseButtonEvent MouseButton;

        [FieldOffset(4)]
        public MouseWheelEvent MouseWheel;

        [FieldOffset(4)]
        public JoystickMoveEvent JoystickMove;

        [FieldOffset(4)]
        public JoystickButtonEvent JoystickButton;

        [FieldOffset(4)]
        public JoystickConnectEvent JoystickConnect;

        [FieldOffset(4)]
        public TouchEvent Touch;

        [FieldOffset(4)]
        public SensorEvent Sensor;
    }
}
