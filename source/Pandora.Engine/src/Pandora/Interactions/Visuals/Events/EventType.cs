﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Events
{
    internal enum EventType
    {
        Closed,

        Resized,

        LostFocus,

        GainedFocus,

        TextEntered,

        KeyPressed,

        KeyReleased,

        MouseWheelMoved,

        MouseWheelScrolled,

        MouseButtonPressed,

        MouseButtonReleased,

        MouseMoved,

        MouseEntered,

        MouseLeft,

        JoystickButtonPressed,

        JoystickButtonReleased,

        JoystickMoved,

        JoystickConnected,

        JoystickDisconnected,

        TouchBegan,

        TouchMoved,

        TouchEnded,

        SensorChanged
    }
}
