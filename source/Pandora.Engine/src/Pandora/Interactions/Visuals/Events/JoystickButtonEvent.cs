﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Events
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct JoystickButtonEvent
    {
        public uint JoystickId;

        public uint Button;
    }
}
