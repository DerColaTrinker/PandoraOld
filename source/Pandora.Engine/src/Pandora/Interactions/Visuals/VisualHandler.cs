﻿using Pandora.Interactions.Events;
using Pandora.Interactions.Visuals.Controls;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Drawing2D;
using Pandora.Interactions.Visuals.Renderer;
using Pandora.Interactions.Visuals.Styles;
using Pandora.Runtime;
using Pandora.Runtime.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class VisualHandler : IVisualHandler, IRenderTarget
    {
        private StyleSet _styleset;

        internal VisualHandler(InteractionManager manager)
        {
            Manager = manager;

            Configuration = manager.Runtime.Configurations.GetOrCreateSection("Visuals", Runtime.Configuration.ConfigurationSectionOptions.NotDeletable | Runtime.Configuration.ConfigurationSectionOptions.NotRenameable);

            InstanceHolder.__VisualHandler = this;
        }

        internal void Start()
        {
            Animation = new Animations.AnimationHandler(this);

            Logger.Trace("Loading context settings");
            _contextsettings.DepthBits = Configuration.GetValue<uint>("DepthBits", 0);
            _contextsettings.StencilBits = Configuration.GetValue<uint>("StencilBits", 0);
            _contextsettings.AntialiasingLevel = Configuration.GetValue<uint>("AntialiasingLevel", 1);
            _contextsettings.MajorVersion = Configuration.GetValue<uint>("OpenGLMajorVersion", 2);
            _contextsettings.MinorVersion = Configuration.GetValue<uint>("OpenGLMinorVersion", 0);

            Logger.Trace("Loading video settings");
            var width = Configuration.GetValue<uint>("ScreenWidth", 1024);
            var height = Configuration.GetValue<uint>("ScreenHeight", 786);
            var bpp = Configuration.GetValue<uint>("BitPerPixel", 32);
            var style = WindowStyle.Close | WindowStyle.Titlebar | WindowStyle.Resize;

            var video = new VideoMode(width, height, bpp);

            try
            {
                _window = UnsafeNativeMethods.sfRenderWindow_create(video, "Pandora", style, ref _contextsettings);
                DefaultView = new View(UnsafeNativeMethods.sfRenderWindow_getDefaultView(_window));
            }
            catch (Exception ex)
            {
                throw new VisualException("VisualHandler target Window failed", ex);
            }

            Logger.Trace("Configurate target window");
            var vsync = Configuration.GetValue("VSync", false);
            var maxfps = Configuration.GetValue<uint>("MaxFramerate", 30);
            UnsafeNativeMethods.sfRenderWindow_setActive(_window, true);
            UnsafeNativeMethods.sfRenderWindow_setVerticalSyncEnabled(_window, vsync);
            if (!vsync) UnsafeNativeMethods.sfRenderWindow_setFramerateLimit(_window, maxfps);

            // Configuration laden
            DoubleClickMillisecond = Configuration.GetValue<uint>("DoubleClickTime", 1000);

            Logger.Normal("SFML Visual initialized");
        }

        /// <summary>
        /// Liefert den Interaction Manager
        /// </summary>
        public InteractionManager Manager { get; private set; }

        /// <summary>
        /// Liefert die Konfiguration
        /// </summary>
        public ConfigurationSection Configuration { get; private set; }

        /// <summary>
        /// Liefert die Anzahl an Millisekunden in der ein Doppelklick erkannt wird oder legt ihn fest
        /// </summary>
        public float DoubleClickMillisecond { get; set; }

        #region Styles

        /// <summary>
        /// Liefert den aktuellen Style oder legt ihn fest
        /// </summary>
        public StyleSet StyleSet
        {
            get { return _styleset; }
            set { _styleset = value; InternalUpdateStyleSet(value); }
        }

        private void InternalUpdateStyleSet(StyleSet styleset)
        {
            if (styleset == null) return;
            foreach (var scene in _scenerenderpipeline)
            {
                scene.InternalStyleSetChanged(styleset);
            }
        }

        #endregion

        #region Visual Handler Impl

        private Color _clearcolor = new Color(60, 60, 60);
        private List<Scene> _scenes = new List<Scene>();
        private Scene[] _scenerenderpipeline = new Scene[] { };

        /// <summary>
        /// Zeigt ein Fenster an
        /// </summary>
        /// <param name="scene"></param>
        public void Show(Scene scene)
        {
            if (PandoraRuntime.__disablevisuals) return;

            if (!scene.IsInitialized)
            {
                scene.InternalLoad(null);
                //scene.InternalStyleSetChanged(StyleSet);
            }

            scene.ZOrder = _scenes.Count > 0 ? _scenes.Max(m => m.ZOrder) + 1 : 1;

            if (!_scenes.Contains(scene))
            {
                _scenes.Add(scene);
                scene.InternalShow();
            }

            ReorderScenes();
        }

        /// <summary>
        /// schließt die angegebene Szene
        /// </summary>
        /// <param name="scene"></param>
        public void Close(Scene scene)
        {
            if (PandoraRuntime.__disablevisuals) return;

            if (_scenes.Remove(scene))
            {
                scene.InternalClose();
                ReorderScenes();
            }
        }

        private void ReorderScenes()
        {
            var lowlimit = false;

            _scenerenderpipeline = _scenes.OrderByDescending(scene => scene.ZOrder).Select(scene =>
                {
                    if (lowlimit) return null;
                    if (scene.BackgroundColor.A == 255) lowlimit = true;

                    return scene;
                }).Where(scene => scene != null).ToArray();
        }

        /// <summary>
        /// Bringt ein Fenster aus dem Hintergrund nach vorn
        /// </summary>
        /// <param name="scene"></param>
        public void BringToFront(Scene scene)
        {
            scene.ZOrder = _scenes.Max(m => m.ZOrder) + 1;
        }

        internal void RenderUpdate()
        {
            // Wenn das Fenster nicht mehr offen ist, die Runtime verlassen
            if (!UnsafeNativeMethods.sfRenderWindow_isOpen(_window))
            {
                //TODO: Das Fenster wurde geschlossen
            }

            // Das Rendern neu auslösen           
            UnsafeNativeMethods.sfRenderWindow_clear(_window, _enginebackgroundcolor);

            // Alle Window Rendern die Sichbar sind.
            foreach (var window in _scenerenderpipeline)
            {
                if (!window.Visible) continue;

                window.InternalRenderUpdate(this);
            }

            UnsafeNativeMethods.sfRenderWindow_display(_window);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        internal void SystemUpdate(float ms, float s)
        {
            if (PandoraRuntime.__disablevisuals) return;

            DispatchEvents(ms);
            Animation.SystemUpdate(ms, s);
            RenderUpdate();
        }

        #endregion

        #region SFML RenderWindow Impl

        private ContextSettings _contextsettings;
        private IntPtr _window;
        private float _doubleclicktimer;
        private Vector2f _position;
        private Color _enginebackgroundcolor = new Color(30, 0, 30);
        private Control _hovercontrol;

        internal Vector2i InternalGetMousePosition()
        {
            return UnsafeNativeMethods.sfMouse_getPosition(_window);
        }

        internal void InternalSetMousePosition(Vector2i position)
        {
            UnsafeNativeMethods.sfMouse_setPosition(position, _window);
        }

        internal Vector2i InternalGetTouchPosition(uint finger)
        {
            return UnsafeNativeMethods.sfTouch_getPosition(finger, _window);
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector2i Position
        {
            get { return UnsafeNativeMethods.sfRenderWindow_getPosition(_window); }
            set { UnsafeNativeMethods.sfRenderWindow_setPosition(_window, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector2u Size
        {
            get { return UnsafeNativeMethods.sfRenderWindow_getSize(_window); }
            set { UnsafeNativeMethods.sfRenderWindow_setSize(_window, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        public void SetTitle(string title)
        {
            // Copy the title to a null-terminated UTF-32 byte array
            byte[] titleAsUtf32 = Encoding.UTF32.GetBytes(title + '\0');

            unsafe
            {
                fixed (byte* titlePtr = titleAsUtf32)
                {
                    UnsafeNativeMethods.sfRenderWindow_setUnicodeTitle(_window, (IntPtr)titlePtr);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="pixels"></param>
        public void SetIcon(uint width, uint height, byte[] pixels)
        {
            unsafe
            {
                fixed (byte* PixelsPtr = pixels)
                {
                    UnsafeNativeMethods.sfRenderWindow_setIcon(_window, width, height, PixelsPtr);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="visible"></param>
        public void SetVisible(bool visible)
        {
            UnsafeNativeMethods.sfRenderWindow_setVisible(_window, visible);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="visible"></param>
        public void SetMouseCursorVisible(bool visible)
        {
            UnsafeNativeMethods.sfRenderWindow_setMouseCursorVisible(_window, visible);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="grabbed"></param>
        public void SetMouseCursorGrabbed(bool grabbed)
        {
            UnsafeNativeMethods.sfRenderWindow_setMouseCursorGrabbed(_window, grabbed);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enable"></param>
        public void SetKeyRepeatEnabled(bool enable)
        {
            UnsafeNativeMethods.sfRenderWindow_setKeyRepeatEnabled(_window, enable);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="active"></param>
        /// <returns></returns>
        public bool SetActive(bool active)
        {
            return UnsafeNativeMethods.sfRenderWindow_setActive(_window, active);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="threshold"></param>
        public void SetJoystickThreshold(float threshold)
        {
            UnsafeNativeMethods.sfRenderWindow_setJoystickThreshold(_window, threshold);
        }

        /// <summary>
        /// 
        /// </summary>
        public IntPtr SystemHandle
        {
            get { return UnsafeNativeMethods.sfRenderWindow_getSystemHandle(_window); }
        }

        /// <summary>
        /// 
        /// </summary>
        public View DefaultView
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public View GetView()
        {
            return new View(UnsafeNativeMethods.sfRenderWindow_getView(_window));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="view"></param>
        public void SetView(View view)
        {
            UnsafeNativeMethods.sfRenderWindow_setView(_window, view.Pointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="view"></param>
        /// <returns></returns>
        public IntRect GetViewport(View view)
        {
            return UnsafeNativeMethods.sfRenderWindow_getViewport(_window, view.Pointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public Vector2f MapPixelToCoords(Vector2i point)
        {
            return MapPixelToCoords(point, GetView());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <param name="view"></param>
        /// <returns></returns>
        public Vector2f MapPixelToCoords(Vector2i point, View view)
        {
            return UnsafeNativeMethods.sfRenderWindow_mapPixelToCoords(_window, point, view != null ? view.Pointer : IntPtr.Zero);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public Vector2i MapCoordsToPixel(Vector2f point)
        {
            return MapCoordsToPixel(point, GetView());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point"></param>
        /// <param name="view"></param>
        /// <returns></returns>
        public Vector2i MapCoordsToPixel(Vector2f point, View view)
        {
            return UnsafeNativeMethods.sfRenderWindow_mapCoordsToPixel(_window, point, view != null ? view.Pointer : IntPtr.Zero);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Clear()
        {
            UnsafeNativeMethods.sfRenderWindow_clear(_window, Color.Black);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="color"></param>
        public void Clear(Color color)
        {
            UnsafeNativeMethods.sfRenderWindow_clear(_window, color);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pointer"></param>
        /// <param name="state"></param>
        public void DrawShape(IntPtr pointer, RenderStates state)
        {
            var marshal = state.Marshal();

            UnsafeNativeMethods.sfRenderWindow_drawShape(_window, pointer, ref marshal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pointer"></param>
        /// <param name="state"></param>
        public void DrawText(IntPtr pointer, RenderStates state)
        {
            var marshal = state.Marshal();

            UnsafeNativeMethods.sfRenderWindow_drawText(_window, pointer, ref marshal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertexarray"></param>
        public void DrawVertexArray(VertexArray vertexarray)
        {
            var marshal = vertexarray.GetRenderStates().Marshal();

            UnsafeNativeMethods.sfRenderWindow_drawVertexArray(_window, vertexarray.Pointer, ref marshal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pointer"></param>
        /// <param name="states"></param>
        public void DrawVertexArray(IntPtr pointer, RenderStates states)
        {
            var marshal = states.Marshal();

            UnsafeNativeMethods.sfRenderWindow_drawVertexArray(_window, pointer, ref marshal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="type"></param>
        public void Draw(Vertex[] vertices, PrimitiveType type)
        {
            Draw(vertices, type, RenderStates.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="type"></param>
        /// <param name="states"></param>
        public void Draw(Vertex[] vertices, PrimitiveType type, RenderStates states)
        {
            Draw(vertices, 0, (uint)vertices.Length, type, states);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="start"></param>
        /// <param name="count"></param>
        /// <param name="type"></param>
        public void Draw(Vertex[] vertices, uint start, uint count, PrimitiveType type)
        {
            Draw(vertices, start, count, type, RenderStates.Default);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="start"></param>
        /// <param name="count"></param>
        /// <param name="type"></param>
        /// <param name="states"></param>
        public void Draw(Vertex[] vertices, uint start, uint count, PrimitiveType type, RenderStates states)
        {
            RenderStates.MarshalData marshaledStates = states.Marshal();

            unsafe
            {
                fixed (Vertex* vertexPtr = vertices)
                {
                    UnsafeNativeMethods.sfRenderWindow_drawPrimitives(_window, vertexPtr + start, count, type, ref marshaledStates);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void PushGLStates()
        {
            UnsafeNativeMethods.sfRenderWindow_pushGLStates(_window);
        }

        /// <summary>
        /// 
        /// </summary>
        public void PopGLStates()
        {
            UnsafeNativeMethods.sfRenderWindow_popGLStates(_window);
        }

        /// <summary>
        /// 
        /// </summary>
        public void ResetGLStates()
        {
            UnsafeNativeMethods.sfRenderWindow_resetGLStates(_window);
        }

        #endregion

        #region Events

        /// <summary>
        /// 
        /// </summary>
        public void WaitAndDispatchEvents()
        {
            Event e;
            if (UnsafeNativeMethods.sfRenderWindow_waitEvent(_window, out e))
                CallEventHandler(e);
        }

        /// <summary>
        /// 
        /// </summary>
        public void DispatchEvents(float ms)
        {
            var e = new Event();

            _doubleclicktimer += ms;

            while (UnsafeNativeMethods.sfRenderWindow_pollEvent(_window, out e))
                CallEventHandler(e);
        }

        private void CallEventHandler(Event e)
        {
            var controlcallstack = new Stack<Control>();

            switch (e.Type)
            {
                case EventType.Closed:
                    //if (Closed != null)
                    //    Closed(this);
                    break;

                case EventType.GainedFocus:
                    //if (GainedFocus != null)
                    //    GainedFocus(this);
                    break;

                case EventType.JoystickButtonPressed:
                    //if (JoystickButtonPressed != null)
                    //    JoystickButtonPressed(this, e.JoystickButton.JoystickId, e.JoystickButton.Button);
                    break;

                case EventType.JoystickButtonReleased:
                    //if (JoystickButtonReleased != null)
                    //    JoystickButtonReleased(this, e.JoystickButton.JoystickId, e.JoystickButton.Button);
                    break;

                case EventType.JoystickMoved:
                    //if (JoystickMoved != null)
                    //    JoystickMoved(this, e.JoystickMove.JoystickId, e.JoystickMove.Axis, e.JoystickMove.Position);
                    break;

                case EventType.JoystickConnected:
                    //if (JoystickConnected != null)
                    //    JoystickConnected(this, e.JoystickConnect.JoystickId);
                    break;

                case EventType.JoystickDisconnected:
                    //if (JoystickDisconnected != null)
                    //    JoystickDisconnected(this, e.JoystickConnect.JoystickId);
                    break;

                case EventType.KeyPressed:
                    //if (KeyPressed != null)
                    //    KeyPressed(this, e.Key.Code, e.Key.Alt != 0, e.Key.Control != 0, e.Key.Shift != 0, e.Key.System != 0);
                    break;

                case EventType.KeyReleased:
                    //if (KeyReleased != null)
                    //    KeyReleased(this, e.Key.Code, e.Key.Alt != 0, e.Key.Control != 0, e.Key.Shift != 0, e.Key.System != 0);
                    break;

                case EventType.LostFocus:
                    //if (LostFocus != null)
                    //    LostFocus(this);
                    break;

                case EventType.MouseButtonPressed:
                    {
                        _position = new Vector2f(e.MouseButton.X, e.MouseButton.Y);
                        _scenerenderpipeline[0].InternalMouseButtonPressed(_position, e.MouseButton.Button);
                    }
                    break;

                case EventType.MouseButtonReleased:
                    {
                        _position = new Vector2f(e.MouseButton.X, e.MouseButton.Y);

                        if (_doubleclicktimer > DoubleClickMillisecond)
                        {
                            _doubleclicktimer = 0;
                        }
                        else
                        {
                            _scenerenderpipeline[0].InternalMouseDoubleClick(_position);

                            _doubleclicktimer = DoubleClickMillisecond;
                        }

                        _scenerenderpipeline[0].InternalMouseButtonReleased(_position, e.MouseButton.Button);
                    }
                    break;

                case EventType.MouseEntered:
                    //if (MouseEntered != null)
                    //    MouseEntered(this);
                    break;

                case EventType.MouseLeft:
                    //if (MouseLeft != null)
                    //    MouseLeft(this);
                    break;

                case EventType.MouseMoved:
                    {
                        _position = new Vector2f(e.MouseMove.X, e.MouseMove.Y);

                        _scenerenderpipeline[0].InternalMouseMoved(_position);
                        var control = _scenerenderpipeline[0].InternalMouseHover(_position);

                        if (control == null)
                        {
                            if (_hovercontrol != null) _hovercontrol.InternalMouseLeave(_position);
                            _hovercontrol = null;
                        }
                        else
                        {
                            if (_hovercontrol != control)
                            {
                                if (_hovercontrol != null) _hovercontrol.InternalMouseLeave(_position);
                                _hovercontrol = control;
                                _hovercontrol.InternalMouseEnter(_position);
                            }
                        }
                    }
                    break;

                case EventType.MouseWheelMoved:
                    {
                        _position = new Vector2f(e.MouseWheel.X, e.MouseWheel.Y);

                        _scenerenderpipeline[0].InternalMouseWheel(_position, e.MouseWheel.Delta);
                    }
                    break;

                case EventType.Resized:
                    {
                        //sf::FloatRect visibleArea(0, 0, event.size.width, event.size.height);
                        //window.setView(sf::View(visibleArea));

                        var center = Vector2f.Zero;
                        var size = new Vector2f(e.Size.Width, e.Size.Height);
                        Size = size.ToVector2u();

                        SetView(new View(new FloatRect(center, Size.ToVector2f())));

                        _scenerenderpipeline[0].InternalSceneResize(size);
                    }
                    break;

                case EventType.TextEntered:
                    //if (TextEntered != null)
                    //{
                    //    var unicode = Char.ConvertFromUtf32((int)e.Text.Unicode);
                    //    TextEntered(this, unicode);
                    //}
                    break;

                case EventType.TouchBegan:
                    //if (TouchBegan != null)
                    //    TouchBegan(this, e.Touch.Finger, e.Touch.X, e.Touch.Y);
                    break;

                case EventType.TouchMoved:
                    //if (TouchMoved != null)
                    //    TouchMoved(this, e.Touch.Finger, e.Touch.X, e.Touch.Y);
                    break;

                case EventType.TouchEnded:
                    //if (TouchEnded != null)
                    //    TouchEnded(this, e.Touch.Finger, e.Touch.X, e.Touch.Y);
                    break;

                case EventType.SensorChanged:
                    //if (SensorChanged != null)
                    //    SensorChanged(this, e.Sensor.Type, e.Sensor.X, e.Sensor.Y, e.Sensor.Z);
                    break;

                default:
                    break;
            }
        }

        #endregion

        #region IVisualHandler Member

        /// <summary>
        /// 
        /// </summary>
        public Animations.AnimationHandler Animation
        {
            get;
            private set;
        }

        #endregion

        #region IPointerObject Member

        /// <summary>
        /// 
        /// </summary>
        public IntPtr Pointer
        {
            get { return _window; }
        }

        #endregion
    }
}
