using Pandora.Interactions.Visuals.Animations;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Renderer;
using Pandora.Runtime.SFML;
using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Pandora.Interactions.Visuals.Drawing2D
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class Shape : Transformable
    {
        private GetPointCountCallbackType myGetPointCountCallback;
        private GetPointCallbackType myGetPointCallback;

        private uint _pointcount;
        private Texture _texture;

        /// <summary>
        /// 
        /// </summary>
        protected Shape()
        {
            InternalPointer = IntPtr.Zero;
            myGetPointCountCallback = new GetPointCountCallbackType(InternalGetPointCount);
            myGetPointCallback = new GetPointCallbackType(InternalGetPoint);
            InternalPointer = UnsafeNativeMethods.sfShape_create(myGetPointCountCallback, myGetPointCallback, IntPtr.Zero);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public Shape(Shape copy)
            : base(copy)
        {
            InternalPointer = IntPtr.Zero;
            myGetPointCountCallback = new GetPointCountCallbackType(InternalGetPointCount);
            myGetPointCallback = new GetPointCallbackType(InternalGetPoint);
            InternalPointer = UnsafeNativeMethods.sfShape_create(myGetPointCountCallback, myGetPointCallback, IntPtr.Zero);

            FillColor = copy.FillColor;
            OutlineColor = copy.OutlineColor;
            OutlineThickness = copy.OutlineThickness;

            Texture = copy.Texture;
            TextureRect = copy.TextureRect;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Destroy(bool disposing)
        {
            UnsafeNativeMethods.sfShape_destroy(Pointer);
        }

        /// <summary>
        /// 
        /// </summary>
        public override Vector2f Size
        {
            get
            {
                return base.Size;
            }
            set
            {
                base.Size = value;
                Update();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual uint PointCount
        {
            get { return _pointcount; }
            set
            {
                _pointcount = value;
                Update();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        protected abstract Vector2f GetPoint(uint index);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override FloatRect GetLocalBounds()
        {
            return UnsafeNativeMethods.sfShape_getLocalBounds(Pointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override FloatRect GetGlobalBounds()
        {
            return Transform.TransformRect(GetLocalBounds());
        }

        /// <summary>
        /// 
        /// </summary>
        protected void Update()
        {
            UnsafeNativeMethods.sfShape_update(Pointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        public void Draw(IRenderTarget target)
        {
            var state = new RenderStates(BlendMode.Alpha, Transform, Texture, null);

            target.DrawShape(Pointer, state);
        }

        private uint InternalGetPointCount(IntPtr userData)
        {
            return PointCount;
        }

        private Vector2f InternalGetPoint(uint index, IntPtr userData)
        {
            return GetPoint(index);
        }

        /// <summary>
        /// 
        /// </summary>
        public Color FillColor
        {
            get { return UnsafeNativeMethods.sfShape_getFillColor(Pointer); }
            set
            {
                UnsafeNativeMethods.sfShape_setFillColor(Pointer, value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Color OutlineColor
        {
            get { return UnsafeNativeMethods.sfShape_getOutlineColor(Pointer); }
            set
            {
                UnsafeNativeMethods.sfShape_setOutlineColor(Pointer, value);
                Update();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float OutlineThickness
        {
            get { return UnsafeNativeMethods.sfShape_getOutlineThickness(Pointer); }
            set
            {
                UnsafeNativeMethods.sfShape_setOutlineThickness(Pointer, value);
                Update();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Texture Texture
        {
            get { return _texture; }
            set
            {
                _texture = value;
                UnsafeNativeMethods.sfShape_setTexture(Pointer, value != null ? value.Pointer : IntPtr.Zero, false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual IntRect TextureRect
        {
            get { return UnsafeNativeMethods.sfShape_getTextureRect(Pointer); }
            set
            {
                UnsafeNativeMethods.sfShape_setTextureRect(Pointer, value);
            }
        }
    }
}
