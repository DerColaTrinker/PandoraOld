using System;
using System.Runtime.InteropServices;

namespace Pandora.Interactions.Visuals.Drawing2D
{
    /// <summary>
    /// 
    /// </summary>
    public enum PrimitiveType
    {
        /// List of individual points
        Points,

        /// List of individual lines
        Lines,

        /// List of connected lines, a point uses the previous point to form a line
        LineStrip,

        /// List of individual triangles
        Triangles,

        /// List of connected triangles, a point uses the two previous points to form a triangle
        TriangleStrip,

        /// List of connected triangles, a point uses the common center and the previous point to form a triangle
        TriangleFan,

        /// List of individual quads
        Quads
    }
}