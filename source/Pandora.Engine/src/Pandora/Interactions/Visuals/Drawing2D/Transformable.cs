﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Interactions.Visuals.Animations;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Renderer;
using Pandora.Runtime.SFML;
using Pandora.Interactions.Visuals.BindingProperties;

namespace Pandora.Interactions.Visuals.Drawing2D
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class Transformable : PointerObject, IRotationProperty, IScaleProperty, IOriginProperty
    {
        private Vector2f _origin = new Vector2f(0, 0);
        private float _rotation = 0;
        private Vector2f _scale = new Vector2f(1, 1);
        private Transform _transform;
        private Transform _inversetransform;
        private bool _transformneedupdate = true;
        private bool _inverseneedupdate = true;
        private Vector2f _position;
        private Vector2f _size;

        /// <summary>
        /// 
        /// </summary>
        public Transformable()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public Transformable(Transformable copy)
        {
            Origin = copy.Origin;
            Position = copy.Position;
            Rotation = copy.Rotation;
            Scale = copy.Scale;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Vector2f Position
        {
            get
            {
                return _position;
            }

            set
            {
                _transformneedupdate = true;
                _inverseneedupdate = true;
                _position = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Vector2f Size
        {
            get
            {
                return _size;
            }
            set
            {
                _transformneedupdate = true;
                _inverseneedupdate = true;
                _size = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual float Rotation
        {
            get
            {
                return _rotation;
            }
            set
            {
                _transformneedupdate = true;
                _inverseneedupdate = true;
                _rotation = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Vector2f Scale
        {
            get
            {
                return _scale;
            }
            set
            {
                _transformneedupdate = true;
                _inverseneedupdate = true;
                _scale = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Vector2f Origin
        {
            get
            {
                return _origin;
            }
            set
            {
                _transformneedupdate = true;
                _inverseneedupdate = true;
                _origin = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Transform Transform
        {
            get
            {
                if (_transformneedupdate)
                {
                    _transformneedupdate = false;

                    var angle = -_rotation * 3.141592654F / 180.0F;
                    var cosine = (float)Math.Cos(angle);
                    var sine = (float)Math.Sin(angle);
                    var sxc = _scale.X * cosine;
                    var syc = _scale.Y * cosine;
                    var sxs = _scale.X * sine;
                    var sys = _scale.Y * sine;
                    var tx = -_origin.X * sxc - _origin.Y * sys + _position.X;
                    var ty = _origin.X * sxs - _origin.Y * syc + _position.Y;

                    _transform = new Transform(sxc, sys, tx, -sxs, syc, ty, 0.0F, 0.0F, 1.0F);
                }

                return _transform;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Transform InverseTransform
        {
            get
            {
                if (_inverseneedupdate)
                {
                    _inversetransform = Transform.GetInverse();
                    _inverseneedupdate = false;
                }
                return _inversetransform;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        protected virtual Transform AlternativPositionTransformation(Vector2f position)
        {
            var angle = -_rotation * 3.141592654F / 180.0F;
            var cosine = (float)Math.Cos(angle);
            var sine = (float)Math.Sin(angle);
            var sxc = _scale.X * cosine;
            var syc = _scale.Y * cosine;
            var sxs = _scale.X * sine;
            var sys = _scale.Y * sine;
            var tx = -_origin.X * sxc - _origin.Y * sys + position.X;
            var ty = _origin.X * sxs - _origin.Y * syc + position.Y;

            return new Transform(sxc, sys, tx, -sxs, syc, ty, 0.0F, 0.0F, 1.0F);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual FloatRect GetLocalBounds()
        {
            return Transform.TransformRect(new FloatRect(Position, Size));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual FloatRect GetGlobalBounds()
        {
            return Transform.TransformRect(new FloatRect(Position, Size));
        }

        internal RenderStates GetRenderStates()
        {
            return new RenderStates(Transform);
        }
    }
}
