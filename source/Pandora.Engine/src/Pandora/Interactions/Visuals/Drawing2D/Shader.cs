using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Collections.Generic;
using Pandora.Runtime;
using Pandora.Interactions.Visuals.Renderer;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Runtime.SFML;
using Pandora.Interactions.Visuals.Drawing2D;
using Pandora.Interactions.Visuals.Drawing.GLSL;

namespace Pandora.Interactions.Visuals.Renderer
{
    /// <summary>
    /// 
    /// </summary>
    public class Shader : PointerObject
    {
        /// <summary>
        /// 
        /// </summary>
        public class CurrentTextureType { }

        /// <summary>
        /// 
        /// </summary>
        public static readonly CurrentTextureType CurrentTexture = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertexShaderFilename"></param>
        /// <param name="geometryShaderFilename"></param>
        /// <param name="fragmentShaderFilename"></param>
        public Shader(string vertexShaderFilename, string geometryShaderFilename, string fragmentShaderFilename) :
            this(UnsafeNativeMethods.sfShader_createFromFile(vertexShaderFilename, geometryShaderFilename, fragmentShaderFilename))
        {
            if (Pointer == IntPtr.Zero)
                throw new LoadingFailedException("shader", vertexShaderFilename + " " + fragmentShaderFilename);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Construct the shader from a pointer
        /// </summary>
        /// <param name="pointer">Pointer to the shader instance</param>
        ////////////////////////////////////////////////////////////
        public Shader(IntPtr pointer) :
            base()
        {
            InternalPointer = pointer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertexShaderStream"></param>
        /// <param name="geometryShaderStream"></param>
        /// <param name="fragmentShaderStream"></param>
        public Shader(Stream vertexShaderStream, Stream geometryShaderStream, Stream fragmentShaderStream) :
            base()
        {
            // using these funky conditional operators because StreamAdaptor doesn't have some method for dealing with
            // its constructor argument being null
            using (StreamAdaptor vertexAdaptor = vertexShaderStream != null ? new StreamAdaptor(vertexShaderStream) : null,
                                geometryAdaptor = geometryShaderStream != null ? new StreamAdaptor(geometryShaderStream) : null,
                                fragmentAdaptor = fragmentShaderStream != null ? new StreamAdaptor(fragmentShaderStream) : null)
            {
                InternalPointer = UnsafeNativeMethods.sfShader_createFromStream(vertexAdaptor.InputStreamPtr, geometryAdaptor.InputStreamPtr, fragmentAdaptor.InputStreamPtr);
            }

            if (InternalPointer == IntPtr.Zero)
                throw new LoadingFailedException("shader");
        }

        ///
        public uint NativeHandle
        {
            get { return UnsafeNativeMethods.sfShader_getNativeHandle(Pointer); }
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Load the vertex, geometry and fragment shaders from source code in memory
        /// </summary>
        /// <remarks>
        /// This function loads the vertex, geometry and fragment
        /// shaders. Pass NULL if you don't want to load
        /// a specific shader.
        /// The sources must be valid shaders in GLSL language. GLSL is
        /// a C-like language dedicated to OpenGL shaders; you'll
        /// probably need to read a good documentation for it before
        /// writing your own shaders.
        /// </remarks>
        /// <param name="vertexShader">String containing the source code of the vertex shader</param>
        /// <param name="geometryShader">String containing the source code of the geometry shader</param>
        /// <param name="fragmentShader">String containing the source code of the fragment shader</param>
        /// <returns>New shader instance</returns>
        /// <exception cref="LoadingFailedException" />
        ////////////////////////////////////////////////////////////
        public static Shader FromString(string vertexShader, string geometryShader, string fragmentShader)
        {
            IntPtr ptr = UnsafeNativeMethods.sfShader_createFromMemory(vertexShader, geometryShader, fragmentShader);
            if (ptr == IntPtr.Zero)
                throw new LoadingFailedException("shader");

            return new Shader(ptr);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify value for <c>float</c> uniform
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="x">Value of the float scalar</param>
        ////////////////////////////////////////////////////////////
        public void SetUniform(string name, float x)
        {
            UnsafeNativeMethods.sfShader_setFloatUniform(Pointer, name, x);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify value for <c>vec2</c> uniform
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="vector">Value of the vec2 vector</param>
        ////////////////////////////////////////////////////////////
        public void SetUniform(string name, Vec2 vector)
        {
            UnsafeNativeMethods.sfShader_setVec2Uniform(Pointer, name, vector);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify value for <c>vec3</c> uniform
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="vector">Value of the vec3 vector</param>
        ////////////////////////////////////////////////////////////
        public void SetUniform(string name, Vec3 vector)
        {
            UnsafeNativeMethods.sfShader_setVec3Uniform(Pointer, name, vector);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify value for <c>vec4</c> uniform
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="vector">Value of the vec4 vector</param>
        ////////////////////////////////////////////////////////////
        public void SetUniform(string name, Vec4 vector)
        {
            UnsafeNativeMethods.sfShader_setVec4Uniform(Pointer, name, vector);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify value for <c>int</c> uniform
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="x">Value of the int scalar</param>
        ////////////////////////////////////////////////////////////
        public void SetUniform(string name, int x)
        {
            UnsafeNativeMethods.sfShader_setIntUniform(Pointer, name, x);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify value for <c>ivec2</c> uniform
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="vector">Value of the ivec2 vector</param>
        ////////////////////////////////////////////////////////////
        public void SetUniform(string name, Ivec2 vector)
        {
            UnsafeNativeMethods.sfShader_setIvec2Uniform(Pointer, name, vector);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify value for <c>ivec3</c> uniform
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="vector">Value of the ivec3 vector</param>
        ////////////////////////////////////////////////////////////
        public void SetUniform(string name, Ivec3 vector)
        {
            UnsafeNativeMethods.sfShader_setIvec3Uniform(Pointer, name, vector);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify value for <c>ivec4</c> uniform
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="vector">Value of the ivec4 vector</param>
        ////////////////////////////////////////////////////////////
        public void SetUniform(string name, Ivec4 vector)
        {
            UnsafeNativeMethods.sfShader_setIvec4Uniform(Pointer, name, vector);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify value for <c>bool</c> uniform
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="x">Value of the bool scalar</param>
        ////////////////////////////////////////////////////////////
        public void SetUniform(string name, bool x)
        {
            UnsafeNativeMethods.sfShader_setBoolUniform(Pointer, name, x);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify value for <c>bvec2</c> uniform
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="vector">Value of the bvec2 vector</param>
        ////////////////////////////////////////////////////////////
        public void SetUniform(string name, Bvec2 vector)
        {
            UnsafeNativeMethods.sfShader_setBvec2Uniform(Pointer, name, vector);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify value for <c>bvec3</c> uniform
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="vector">Value of the bvec3 vector</param>
        ////////////////////////////////////////////////////////////
        public void SetUniform(string name, Bvec3 vector)
        {
            UnsafeNativeMethods.sfShader_setBvec3Uniform(Pointer, name, vector);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify value for <c>bvec4</c> uniform
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="vector">Value of the bvec4 vector</param>
        ////////////////////////////////////////////////////////////
        public void SetUniform(string name, Bvec4 vector)
        {
            UnsafeNativeMethods.sfShader_setBvec4Uniform(Pointer, name, vector);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify value for <c>mat3</c> uniform
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="matrix">Value of the mat3 matrix</param>
        ////////////////////////////////////////////////////////////
        public void SetUniform(string name, Mat3 matrix)
        {
            UnsafeNativeMethods.sfShader_setMat3Uniform(Pointer, name, matrix);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify value for <c>mat4</c> uniform
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="matrix">Value of the mat4 matrix</param>
        ////////////////////////////////////////////////////////////
        public void SetUniform(string name, Mat4 matrix)
        {
            UnsafeNativeMethods.sfShader_setMat4Uniform(Pointer, name, matrix);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify a texture as <c>sampler2D</c> uniform
        /// </summary>
        ///
        /// <remarks>
        /// <para>name is the name of the variable to change in the shader.
        /// The corresponding parameter in the shader must be a 2D texture
        /// (<c>sampler2D</c> GLSL type).</para>
        ///
        /// <para>Example:
        /// <code>
        /// uniform sampler2D the_texture; // this is the variable in the shader
        /// </code>
        /// <code>
        /// sf::Texture texture;
        /// ...
        /// shader.setUniform("the_texture", texture);
        /// </code>
        /// It is important to note that <paramref name="texture"/> must remain alive as long
        /// as the shader uses it, no copy is made internally.</para>
        ///
        /// <para>To use the texture of the object being drawn, which cannot be
        /// known in advance, you can pass the special value
        /// Shader.CurrentTexture:
        /// <code>
        /// shader.setUniform("the_texture", Shader.CurrentTexture);
        /// </code>
        /// </para>
        /// </remarks>
        /// 
        /// <param name="name">Name of the texture in the shader</param>
        /// <param name="texture">Texture to assign</param>
        ///
        ////////////////////////////////////////////////////////////
        public void SetUniform(string name, Texture texture)
        {
            // Keep a reference to the Texture so it doesn't get GC'd
            myTextures[name] = texture;
            UnsafeNativeMethods.sfShader_setTextureUniform(Pointer, name, texture.Pointer);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify current texture as \p sampler2D uniform
        /// </summary>
        ///
        /// <remarks>
        /// <para>This overload maps a shader texture variable to the
        /// texture of the object being drawn, which cannot be
        /// known in advance. The second argument must be
        /// <see cref="CurrentTexture"/>.
        /// The corresponding parameter in the shader must be a 2D texture
        /// (<c>sampler2D</c> GLSL type).</para>
        ///
        /// <para>Example:
        /// <code>
        /// uniform sampler2D current; // this is the variable in the shader
        /// </code>
        /// <code>
        /// shader.setUniform("current", Shader.CurrentTexture);
        /// </code>
        /// </para>
        /// </remarks>
        /// 
        /// <param name="name">Name of the texture in the shader</param>
        /// <param name="current"/>
        ////////////////////////////////////////////////////////////
        public void SetUniform(string name, CurrentTextureType current)
        {
            UnsafeNativeMethods.sfShader_setCurrentTextureUniform(Pointer, name);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify values for <c>float[]</c> array uniforms
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="array">array of <c>float</c> values</param>
        ////////////////////////////////////////////////////////////
        public unsafe void SetUniformArray(string name, float[] array)
        {
            fixed (float* data = array)
            {
                UnsafeNativeMethods.sfShader_setFloatUniformArray(Pointer, name, data, (uint)array.Length);
            }
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify values for <c>vec2[]</c> array uniforms
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="array">array of <c>vec2</c> values</param>
        ////////////////////////////////////////////////////////////
        public unsafe void SetUniformArray(string name, Vec2[] array)
        {
            fixed (Vec2* data = array)
            {
                UnsafeNativeMethods.sfShader_setVec2UniformArray(Pointer, name, data, (uint)array.Length);
            }
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify values for <c>vec3[]</c> array uniforms
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="array">array of <c>vec3</c> values</param>
        ////////////////////////////////////////////////////////////
        public unsafe void SetUniformArray(string name, Vec3[] array)
        {
            fixed (Vec3* data = array)
            {
                UnsafeNativeMethods.sfShader_setVec3UniformArray(Pointer, name, data, (uint)array.Length);
            }
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify values for <c>vec4[]</c> array uniforms
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="array">array of <c>vec4</c> values</param>
        ////////////////////////////////////////////////////////////
        public unsafe void SetUniformArray(string name, Vec4[] array)
        {
            fixed (Vec4* data = array)
            {
                UnsafeNativeMethods.sfShader_setVec4UniformArray(Pointer, name, data, (uint)array.Length);
            }
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify values for <c>mat3[]</c> array uniforms
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="array">array of <c>mat3</c> values</param>
        ////////////////////////////////////////////////////////////
        public unsafe void SetUniformArray(string name, Mat3[] array)
        {
            fixed (Mat3* data = array)
            {
                UnsafeNativeMethods.sfShader_setMat3UniformArray(Pointer, name, data, (uint)array.Length);
            }
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Specify values for <c>mat4[]</c> array uniforms
        /// </summary>
        /// <param name="name">Name of the uniform variable in GLSL</param>
        /// <param name="array">array of <c>mat4</c> values</param>
        ////////////////////////////////////////////////////////////
        public unsafe void SetUniformArray(string name, Mat4[] array)
        {
            fixed (Mat4* data = array)
            {
                UnsafeNativeMethods.sfShader_setMat4UniformArray(Pointer, name, data, (uint)array.Length);
            }
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Bind a shader for rendering
        /// </summary>
        /// <param name="shader">Shader to bind (can be null to use no shader)</param>
        ////////////////////////////////////////////////////////////
        public static void Bind(Shader shader)
        {
            UnsafeNativeMethods.sfShader_bind(shader != null ? shader.Pointer : IntPtr.Zero);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Tell whether or not the system supports shaders.
        /// </summary>
        ///
        /// <remarks>
        /// This property should always be checked before using
        /// the shader features. If it returns false, then
        /// any attempt to use Shader will fail.
        /// </remarks>
        ////////////////////////////////////////////////////////////
        public static bool IsAvailable
        {
            get { return UnsafeNativeMethods.sfShader_isAvailable(); }
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Tell whether or not the system supports geometry shaders.
        /// </summary>
        /// 
        /// <remarks>
        /// <para>This property should always be checked before using
        /// the geometry shader features. If it returns false, then
        /// any attempt to use geometry shader features will fail.</para>
        /// 
        /// <para>Note: The first call to this function, whether by your
        /// code or SFML will result in a context switch.</para>
        /// </remarks>
        ////////////////////////////////////////////////////////////
        public static bool IsGeometryAvailable
        {
            get { return UnsafeNativeMethods.sfShader_isGeometryAvailable(); }
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Provide a string describing the object
        /// </summary>
        /// <returns>String description of the object</returns>
        ////////////////////////////////////////////////////////////
        public override string ToString()
        {
            return "[Shader]";
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Handle the destruction of the object
        /// </summary>
        /// <param name="disposing">Is the GC disposing the object, or is it an explicit call ?</param>
        ////////////////////////////////////////////////////////////
        protected override void Destroy(bool disposing)
        {
            if (!disposing)
                Context.Global.SetActive(true);

            myTextures.Clear();
            UnsafeNativeMethods.sfShader_destroy(Pointer);

            if (!disposing)
                Context.Global.SetActive(false);
        }

        // Keeps references to used Textures for GC prevention during use
        private Dictionary<string, Texture> myTextures = new Dictionary<string, Texture>();
    }
}