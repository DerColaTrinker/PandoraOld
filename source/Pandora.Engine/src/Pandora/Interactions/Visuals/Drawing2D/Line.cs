﻿using Pandora.Interactions.Visuals.Animations;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Renderer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Drawing2D
{
    /// <summary>
    /// 
    /// </summary>
    public class Line : VertexArray
    {
        private Vertex[] _vertices = new Vertex[4];
        private Vector2f _position1;
        private Vector2f _position2;
        private float _thick = 1F;
        private Drawing.Color _color = Color.White;

        /// <summary>
        /// 
        /// </summary>
        public Line()
            : base(PrimitiveType.Lines, 2)
        { }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public Line(Line copy)
            : base(copy)
        {
            Position1 = copy.Position1;
            Position2 = copy.Position2;
            Color = copy.Color;
            LineThickness = copy.LineThickness;
        }

        private void Update()
        {
            var direction = Position1 - Position2;
            var unitDirection = direction / (float)Math.Sqrt(direction.X * direction.X + direction.Y * direction.Y);
            var unitPerpendicular = new Vector2f(-unitDirection.Y, unitDirection.X);
            var offset = (LineThickness / 2F) * unitPerpendicular;

            _vertices[0].Position = Position1 + offset;
            _vertices[1].Position = Position2 + offset;
            _vertices[2].Position = Position1 - offset;
            _vertices[3].Position = Position2 - offset;

            for (uint i = 0 ; i < 4 ; ++i)
            {
                _vertices[i].Color = Color;

                base[i] = _vertices[i];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector2f Position2 { get { return _position1; } set { _position1 = value; Update(); } }

        /// <summary>
        /// 
        /// </summary>
        public Vector2f Position1 { get { return _position2; } set { _position2 = value; Update(); } }

        /// <summary>
        /// 
        /// </summary>
        public float LineThickness { get { return _thick; } set { _thick = value; Update(); } }

        /// <summary>
        /// 
        /// </summary>
        public Color Color { get { return _color; } set { _color = value; Update(); } }
    }
}
