﻿using Pandora.Interactions.Visuals.Drawing;
using Pandora.Runtime;
using Pandora.Runtime.SFML;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Drawing2D
{
    /// <summary>
    /// 
    /// </summary>
    public class Texture : PointerObject
    {
        private bool _external;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public Texture(uint width, uint height)
        {
            InternalPointer = UnsafeNativeMethods.sfTexture_create(width, height);

            if (InternalPointer == IntPtr.Zero)
                throw new LoadingFailedException("texture");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public Texture(string filename) :
            this(filename, new IntRect(0, 0, 0, 0))
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="area"></param>
        public Texture(string filename, IntRect area)
        {
            InternalPointer = UnsafeNativeMethods.sfTexture_createFromFile(filename, ref area);

            if (InternalPointer == IntPtr.Zero)
                throw new LoadingFailedException("texture", filename);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        public Texture(Stream stream) :
            this(stream, new IntRect(0, 0, 0, 0))
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="area"></param>
        public Texture(Stream stream, IntRect area)
        {
            using (StreamAdaptor adaptor = new StreamAdaptor(stream))
            {
                InternalPointer = UnsafeNativeMethods.sfTexture_createFromStream(adaptor.InputStreamPtr, ref area);
            }

            if (InternalPointer == IntPtr.Zero)
                throw new LoadingFailedException("texture");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        public Texture(Image image) :
            this(image, new IntRect(0, 0, 0, 0))
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="area"></param>
        public Texture(Image image, IntRect area)
        {
            InternalPointer = UnsafeNativeMethods.sfTexture_createFromImage(image.Pointer, ref area);

            if (InternalPointer == IntPtr.Zero)
                throw new LoadingFailedException("texture");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        public Texture(byte[] bytes)
        {
            GCHandle pin = GCHandle.Alloc(bytes, GCHandleType.Pinned);

            try
            {
                IntRect rect = new IntRect(0, 0, 0, 0);
                InternalPointer = UnsafeNativeMethods.sfTexture_createFromMemory(pin.AddrOfPinnedObject(), Convert.ToUInt64(bytes.Length), ref rect);
            }
            finally
            {
                pin.Free();
            }
            if (InternalPointer == IntPtr.Zero)
                throw new LoadingFailedException("texture");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="copy"></param>
        public Texture(Texture copy)
        {
            InternalPointer = UnsafeNativeMethods.sfTexture_copy(copy.InternalPointer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Image CopyToImage()
        {
            return new Image(UnsafeNativeMethods.sfTexture_copyToImage(InternalPointer));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pixels"></param>
        public void Update(byte[] pixels)
        {
            Vector2u size = Size;
            Update(pixels, size.X, size.Y, 0, 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pixels"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void Update(byte[] pixels, uint width, uint height, uint x, uint y)
        {
            unsafe
            {
                fixed (byte* ptr = pixels)
                {
                    UnsafeNativeMethods.sfTexture_updateFromPixels(InternalPointer, ptr, width, height, x, y);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        public void Update(Image image)
        {
            Update(image, 0, 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void Update(Image image, uint x, uint y)
        {
            UnsafeNativeMethods.sfTexture_updateFromImage(InternalPointer, image.Pointer, x, y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="window"></param>
        public void Update(IPointerObject window)
        {
            Update(window, 0, 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="window"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void Update(IPointerObject window, uint x, uint y)
        {
            UnsafeNativeMethods.sfTexture_updateFromWindow(InternalPointer, window.Pointer, x, y);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Smooth
        {
            get { return UnsafeNativeMethods.sfTexture_isSmooth(InternalPointer); }
            set { UnsafeNativeMethods.sfTexture_setSmooth(InternalPointer, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Repeated
        {
            get { return UnsafeNativeMethods.sfTexture_isRepeated(InternalPointer); }
            set { UnsafeNativeMethods.sfTexture_setRepeated(InternalPointer, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector2u Size
        {
            get { return UnsafeNativeMethods.sfTexture_getSize(InternalPointer); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="texture"></param>
        public static void Bind(Texture texture)
        {
            UnsafeNativeMethods.sfTexture_bind(texture != null ? texture.InternalPointer : IntPtr.Zero);
        }

        /// <summary>
        /// 
        /// </summary>
        public static uint MaximumSize
        {
            get { return UnsafeNativeMethods.sfTexture_getMaximumSize(); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "[Texture]" +
                   " Size(" + Size + ")" +
                   " Smooth(" + Smooth + ")" +
                   " Repeated(" + Repeated + ")";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pointer"></param>
        internal Texture(IntPtr pointer)
        {
            InternalPointer = pointer;
            _external = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Destroy(bool disposing)
        {
            if (!_external)
            {
                if (!disposing)
                    Context.Global.SetActive(true);

                UnsafeNativeMethods.sfTexture_destroy(InternalPointer);

                if (!disposing)
                    Context.Global.SetActive(false);
            }
        }
    }
}
