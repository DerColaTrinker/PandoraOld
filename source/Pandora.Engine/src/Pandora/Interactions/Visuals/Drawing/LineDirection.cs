﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Drawing
{
    /// <summary>
    /// 
    /// </summary>
    public enum LineDirection
    {
        /// <summary>
        /// 
        /// </summary>
        LeftToRight,

        /// <summary>
        /// 
        /// </summary>
        Top,

        /// <summary>
        /// 
        /// </summary>
        Bottom,

        /// <summary>
        /// 
        /// </summary>
        Left,

        /// <summary>
        /// 
        /// </summary>
        Right,

        /// <summary>
        /// 
        /// </summary>
        Horizontal,

        /// <summary>
        /// 
        /// </summary>
        Vertical,

        /// <summary>
        /// 
        /// </summary>
        RightToLeft,

        /// <summary>
        /// 
        /// </summary>
        VectorPoint
    }
}
