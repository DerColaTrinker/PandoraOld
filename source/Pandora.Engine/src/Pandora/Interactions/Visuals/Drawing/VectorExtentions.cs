﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Drawing
{
    /// <summary>
    /// 
    /// </summary>
    public static class VectorExtentions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static Vector2u ToVector2u(this Vector2f vector)
        {
            return new Vector2u(Convert.ToUInt32(vector.X), Convert.ToUInt32(vector.Y));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static Vector2i ToVector2i(this Vector2f vector)
        {
            return new Vector2i(Convert.ToInt32(vector.X), Convert.ToInt32(vector.Y));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static Vector2f ToVector2f(this Vector2u vector)
        {
            return new Vector2f(Convert.ToSingle(vector.X), Convert.ToSingle(vector.Y));
        }
    }
}
