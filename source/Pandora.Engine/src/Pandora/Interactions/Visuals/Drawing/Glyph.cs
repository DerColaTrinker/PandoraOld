﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Drawing
{
    /// <summary>
    /// Stellt ein einzellnes Zeichen dar
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct Glyph
    {
        /// <summary>
        /// Keine AHNUNG
        /// </summary>
        public int Advance;

        /// <summary>
        /// Bereich
        /// </summary>
        public FloatRect Bounds;

        /// <summary>
        /// Texturebereich
        /// </summary>
        public IntRect TextureRect;
    }
}
