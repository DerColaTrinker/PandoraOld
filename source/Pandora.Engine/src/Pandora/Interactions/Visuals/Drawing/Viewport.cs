﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Drawing
{
    /// <summary>
    /// 
    /// </summary>
    public class Viewport
    {
        private FloatRect _source;
        private FloatRect _target;
        private Vector2f _factor;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        public Viewport(Vector2f source, Vector2f target)
        {
            _source = new FloatRect(Vector2f.Zero, source);
            _target = new FloatRect(Vector2f.Zero, target);

            UpdateFactor();
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateFactor()
        {
            _factor = _target.Size / _source.Size;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool MapVisible(float x, float y)
        {
            return _source.Intersects(x, y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Vector2f MapToScreenCoords(float x, float y)
        {
            var v = new Vector2f(x, y);
            return v * _factor;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public Vector2f MapToScreenCoords(Vector2f v)
        {
            return v * _factor;
        }
    }
}
