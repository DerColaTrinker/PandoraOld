using System;
using System.Runtime.InteropServices;

namespace Pandora.Interactions.Visuals.Drawing
{
    /// <summary>
    /// Stellt eine Farbe dar
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct Color : IEquatable<Color>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="red"></param>
        /// <param name="green"></param>
        /// <param name="blue"></param>
        public Color(byte red, byte green, byte blue) :
            this(red, green, blue, 255)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="red"></param>
        /// <param name="green"></param>
        /// <param name="blue"></param>
        /// <param name="alpha"></param>
        public Color(byte red, byte green, byte blue, byte alpha)
        {
            R = red;
            G = green;
            B = blue;
            A = alpha;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="color"></param>
        public Color(uint color)
        {
            unchecked
            {
                R = (byte)(color >> 24);
                G = (byte)(color >> 16);
                B = (byte)(color >> 8);
                A = (byte)color;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="color"></param>
        public Color(Color color) :
            this(color.R, color.G, color.B, color.A)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public uint ToInteger()
        {
            return (uint)((R << 24) | (G << 16) | (B << 8) | A);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "[Color]" +
                   " R(" + R + ")" +
                   " G(" + G + ")" +
                   " B(" + B + ")" +
                   " A(" + A + ")";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return (obj is Color) && Equals((Color)obj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Color other)
        {
            return (R == other.R) &&
                   (G == other.G) &&
                   (B == other.B) &&
                   (A == other.A);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (R << 24) | (G << 16) | (B << 8) | A;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator ==(Color left, Color right)
        {
            return left.Equals(right);
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Compare two colors and checks if they are not equal
        /// </summary>
        /// <returns>Colors are not equal</returns>
        ////////////////////////////////////////////////////////////
        public static bool operator !=(Color left, Color right)
        {
            return !left.Equals(right);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Color operator +(Color left, Color right)
        {
            return new Color((byte)Math.Min(left.R + right.R, 255),
                             (byte)Math.Min(left.G + right.G, 255),
                             (byte)Math.Min(left.B + right.B, 255),
                             (byte)Math.Min(left.A + right.A, 255));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Color operator -(Color left, Color right)
        {
            return new Color((byte)Math.Max(left.R - right.R, 0),
                             (byte)Math.Max(left.G - right.G, 0),
                             (byte)Math.Max(left.B - right.B, 0),
                             (byte)Math.Max(left.A - right.A, 0));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Color operator *(Color left, Color right)
        {
            return new Color((byte)((int)left.R * right.R / 255),
                             (byte)((int)left.G * right.G / 255),
                             (byte)((int)left.B * right.B / 255),
                             (byte)((int)left.A * right.A / 255));
        }

        /// <summary>
        /// 
        /// </summary>
        public byte R;

        /// <summary>
        /// 
        /// </summary>
        public byte G;

        /// <summary>
        /// 
        /// </summary>
        public byte B;

        /// <summary>
        /// 
        /// </summary>
        public byte A;

        /// <summary>
        /// 
        /// </summary>
        public static readonly Color Black = new Color(0, 0, 0);

        /// <summary>
        /// 
        /// </summary>
        public static readonly Color White = new Color(255, 255, 255);

        /// <summary>
        /// 
        /// </summary>
        public static readonly Color Red = new Color(255, 0, 0);

        /// <summary>
        /// 
        /// </summary>
        public static readonly Color Green = new Color(0, 255, 0);

        /// <summary>
        /// 
        /// </summary>
        public static readonly Color Blue = new Color(0, 0, 255);

        /// <summary>
        /// 
        /// </summary>
        public static readonly Color Yellow = new Color(255, 255, 0);

        /// <summary>
        /// 
        /// </summary>
        public static readonly Color Magenta = new Color(255, 0, 255);

        /// <summary>
        /// 
        /// </summary>
        public static readonly Color Cyan = new Color(0, 255, 255);

        /// <summary>
        /// 
        /// </summary>
        public static readonly Color Transparent = new Color(0, 0, 0, 0);
    }
}