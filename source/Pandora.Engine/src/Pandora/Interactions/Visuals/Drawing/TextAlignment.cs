﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Drawing
{
    /// <summary>
    /// Gibt die Textposition an
    /// </summary>
    public enum TextAlignment
    {
        /// <summary>
        /// Oben - Links
        /// </summary>
        TopLeft,

        /// <summary>
        /// Oben - Mitte
        /// </summary>
        TopCenter,

        /// <summary>
        /// Oben - Rechts
        /// </summary>
        TopRight,

        /// <summary>
        /// Mitte - Links
        /// </summary>
        MiddleLeft,

        /// <summary>
        /// Mitte - Mitte
        /// </summary>
        MiddleCenter,

        /// <summary>
        /// Mitte - Rechts
        /// </summary>
        MiddleRight,

        /// <summary>
        /// Unten - Links
        /// </summary>
        BottomLeft,

        /// <summary>
        /// Unten - Mitte
        /// </summary>
        BottomCenter,

        /// <summary>
        /// Unten - Rechts
        /// </summary>
        BottomRight
    }
}
