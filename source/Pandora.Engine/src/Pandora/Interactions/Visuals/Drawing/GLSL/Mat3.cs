﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Drawing.GLSL
{
    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct Mat3
    {
        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Construct the <see cref="Mat3"/> from its components
        /// </summary>
        /// 
        /// <remarks>
        /// Arguments are in row-major order
        /// </remarks>
        ////////////////////////////////////////////////////////////
        public Mat3(float a00, float a01, float a02,
                    float a10, float a11, float a12,
                    float a20, float a21, float a22)
        {
            fixed (float* m = array)
            {
                m[0] = a00; m[3] = a01; m[6] = a02;
                m[1] = a10; m[4] = a11; m[7] = a12;
                m[2] = a20; m[5] = a21; m[8] = a22;
            }
        }

        ////////////////////////////////////////////////////////////
        /// <summary>
        /// Construct the <see cref="Mat3"/> from a SFML <see cref="Transform"/>
        /// </summary>
        /// <param name="transform">A SFML <see cref="Transform"/></param>
        ////////////////////////////////////////////////////////////
        public Mat3(Transform transform)
        {
            fixed (float* m = array)
            {
                m[0] = transform.m00; m[3] = transform.m01; m[6] = transform.m02;
                m[1] = transform.m10; m[4] = transform.m11; m[7] = transform.m12;
                m[2] = transform.m20; m[5] = transform.m21; m[8] = transform.m22;
            }
        }

        // column-major!
        private fixed float array[3 * 3];
    }
}
