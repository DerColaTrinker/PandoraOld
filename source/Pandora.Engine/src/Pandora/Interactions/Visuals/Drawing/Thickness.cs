﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Drawing
{
    /// <summary>
    /// Stellt eine Rahmendicke dar
    /// </summary>
    public struct Thickness : IEquatable<Thickness>
    {
        private float _left;

        private float _top;

        private float _right;

        private float _bottom;

        /// <summary>
        /// Liefert die Rahmendicke der Linken Seite oder legt sie fest
        /// </summary>
        public float Left
        {
            get
            {
                return this._left;
            }
            set
            {
                this._left = value;
            }
        }

        /// <summary>
        /// Liefert die Rahmendicke der Oberen Seite oder legt sie fest
        /// </summary>
        public float Top
        {
            get
            {
                return this._top;
            }
            set
            {
                this._top = value;
            }
        }

        /// <summary>
        /// Liefert die Rahmendicke der Rechten Seite oder legt sie fest
        /// </summary>
        public float Right
        {
            get
            {
                return this._right;
            }
            set
            {
                this._right = value;
            }
        }

        /// <summary>
        /// Liefert die Rahmendicke der Unteren Seite oder legt sie fest
        /// </summary>
        public float Bottom
        {
            get
            {
                return this._bottom;
            }
            set
            {
                this._bottom = value;
            }
        }

        /// <summary>
        /// Liefert die Größe des Rahmens
        /// </summary>
        internal Vector2f Size
        {
            get
            {
                return new Vector2f(this._left + this._right, this._top + this._bottom);
            }
        }

        /// <summary>
        /// Erstellt eine neue Instanz der Thickness-Klasse
        /// </summary>
        /// <param name="uniformLength"></param>
        public Thickness(float uniformLength)
        {
            this._bottom = uniformLength;
            this._right = uniformLength;
            this._top = uniformLength;
            this._left = uniformLength;
        }

        /// <summary>
        /// Erstellt eine neue Instanz der Thickness-Klasse
        /// </summary>
        /// <param name="left"></param>
        /// <param name="top"></param>
        /// <param name="right"></param>
        /// <param name="bottom"></param>
        public Thickness(float left, float top, float right, float bottom)
        {
            this._left = left;
            this._top = top;
            this._right = right;
            this._bottom = bottom;
        }

        /// <summary>
        /// Vergleicht ein Rahmen-Object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is Thickness)
            {
                Thickness t = (Thickness)obj;
                return this == t;
            }
            return false;
        }

        /// <summary>
        /// Vergleicht ein Rahmen-Objekt
        /// </summary>
        /// <param name="thickness"></param>
        /// <returns></returns>
        public bool Equals(Thickness thickness)
        {
            return this == thickness;
        }

        /// <summary>
        /// Liefert einen Hashcode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this._left.GetHashCode() ^ this._top.GetHashCode() ^ this._right.GetHashCode() ^ this._bottom.GetHashCode();
        }

        /// <summary>
        /// Liefert eine Zeichenfolge die den Rahmenbereich darstellt
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3}", _left, _top, _right, _bottom);
        }

        /// <summary>
        /// Vergleicht zwei Rahmen
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static bool operator ==(Thickness t1, Thickness t2)
        {
            return t1._left == t2._left & t1._top == t2._top & t1._right == t2._right & t1._bottom == t2._bottom;
        }

        /// <summary>
        /// Vergleicht zwei Rahmen auf ungleich
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static bool operator !=(Thickness t1, Thickness t2)
        {
            return !(t1 == t2);
        }
    }
}
