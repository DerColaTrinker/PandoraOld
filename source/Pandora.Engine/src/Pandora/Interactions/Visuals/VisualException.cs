﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#pragma warning disable 1591

namespace Pandora.Interactions.Visuals
{
    [Serializable]
    public class VisualException : InteractionException
    {
        public VisualException()
        { }

        public VisualException(string message)
            : base(message)
        { }

        public VisualException(string message, Exception inner)
            : base(message, inner)
        { }

        protected VisualException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        { }
    }
}

#pragma warning restore 1591