﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Interactions.Visuals.Styles
{
    /// <summary>
    /// Stellt einen Style Trigger Effekt dar
    /// </summary>
    public sealed class StyleTriggerEffect
    {
        internal StyleTriggerEffect(StyleSet styleset, Type effecttype, float time, string value)
        {
            Effecttype = effecttype;
            var valueobject = (object)value;

            // Den Wert holen
            if (value.StartsWith("{") & value.EndsWith("}"))
            {
                // Entrynamen lesen
                var entryname = value.Substring(1, value.Length - 2);
                if (string.IsNullOrEmpty(entryname)) throw new PandoraException(string.Format("Resource key is empty"));

                // Entry suchen
                var entry = (StyleResourceEntry)null;
                if (!styleset.__resources.TryGetValue(entryname, out entry)) throw new PandoraException(string.Format("Style resource key '{0}' not found", entryname));

                valueobject = entry.Value;
            }
            else
            {
                if (!Utility.ConvertHelper(ref  valueobject, TargetType)) throw new InvalidCastException(TargetType.Name);
            }

            // Art der Effect Basisklasse ermitteln
            if (effecttype.BaseType.IsGenericType && effecttype.BaseType.GenericTypeArguments.Length == 2)
            {
                HasTargetType = true;
                TargetType = effecttype.BaseType.GenericTypeArguments[1];
            }

            Time = time;
            Value = valueobject;
        }

        /// <summary>
        /// Liefert den Type des Effekts der ausgelöst werden soll
        /// </summary>
        public Type Effecttype { get; private set; }

        /// <summary>
        /// Liefert die Laufzeit des Effekts
        /// </summary>
        public float Time { get; private set; }

        /// <summary>
        /// Liefert den Wert den der Effekt annehmen soll
        /// </summary>
        public object Value { get; private set; }

        /// <summary>
        /// NICHT BEKANNT
        /// </summary>
        public bool HasTargetType { get; private set; }

        /// <summary>
        /// Liefert den Type des Steuerelements auf das der Effekt angewendet werden soll
        /// </summary>
        public Type TargetType { get; private set; }
    }
}
