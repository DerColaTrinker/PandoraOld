﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Interactions.Visuals.Styles
{
    /// <summary>
    /// Trigger Events die durch den Style aisgelöst werden
    /// </summary>
    public enum TriggerEventTypes
    {
        /// <summary>
        /// Mauszeiger betritt den Bereich
        /// </summary>
        MouseEnter,

        /// <summary>
        /// Mauszeiger verlässt den Bereich
        /// </summary>
        MouseLeave
    }
}
