﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Interactions.Visuals.Styles
{
    /// <summary>
    /// Stellt eine Style-Effekt Trigger dar
    /// </summary>
    public sealed class StyleTrigger
    {
        private HashSet<StyleTriggerEffect> _effects = new HashSet<StyleTriggerEffect>();

        /// <summary>
        /// Liefert den Auslöer
        /// </summary>
        public TriggerEventTypes Event { get; internal set; }

        /// <summary>
        /// Liefert die Gruppe
        /// </summary>
        public string Group { get; internal set; }

        /// <summary>
        /// Liefert das Verhalten der Gruppe
        /// </summary>
        public TriggerGroupModes GroupMode { get; internal set; }

        internal void AddEffect(StyleTriggerEffect styletriggereffect)
        {
            _effects.Add(styletriggereffect);
        }

        internal IEnumerable<StyleTriggerEffect> GetEffects()
        {
            return _effects.AsEnumerable();
        }
    }
}
