﻿using Pandora.Interactions.Visuals.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Pandora.Interactions.Visuals.Styles
{
    /// <summary>
    /// Stellt einen Style dar
    /// </summary>
    public sealed class Style
    {
        private Dictionary<PropertyInfo, object> _propertyvalues = new Dictionary<PropertyInfo, object>();
        private HashSet<StyleTrigger> _triggers = new HashSet<StyleTrigger>();

        /// <summary>
        /// Erstellt eine neue Style-Instaz
        /// </summary>
        /// <param name="controltype"></param>
        /// <param name="controltypename"></param>
        /// <param name="name"></param>
        public Style(Type controltype, string controltypename, string name)
        {
            this.ControlType = controltype;
            this.ControlTypename = controltypename;
            this.Name = name;
        }

        internal void AddProperty(PropertyInfo property, object valueobject)
        {
            var result = _propertyvalues.Keys.Where(m => m.Name == property.Name).FirstOrDefault();
            if (result != null)
            {
                // Es besteht bereits ein Property
                _propertyvalues[result] = valueobject;
            }
            else
            {
                // Neu einfügen
                _propertyvalues.Add(property, valueobject);
            }
        }

        internal void AddTrigger(StyleTrigger trigger)
        {
            _triggers.Add(trigger);
        }

        internal void ApplyStyle(Visual control)
        {
            var controltypename = control.GetType().FullName;
            if (controltypename != ControlType.FullName)
            {
                Logger.Warning("Style control not equal '{0}' -> '{1}'", control.Name, ControlType.Name);
                return;
            }

            foreach (var item in _propertyvalues)
            {
                try
                {
                    item.Key.SetValue(control, item.Value);
                }
                catch (Exception)
                {
                    Logger.Warning("Style property '{0}'->'{1}' invalid cast", item.Key.Name, item.Key.PropertyType);
                    return;
                }
            }
        }

        internal void ApplyAnimations(Control control)
        {
            var controltypename = control.GetType().FullName;
            if (controltypename != ControlType.FullName)
            {
                Logger.Warning("Style control not equal '{0}' -> '{1}'", control.Name, ControlType.Name);
                return;
            }

            foreach (var item in _triggers)
            {
                var trigger = new ControlEffectTrigger(control, item);

                control._controleffects.Add(trigger);
            }
        }

        /// <summary>
        /// Liefert den Type des Controls auf das sich das Style bezieht
        /// </summary>
        public Type ControlType { get; private set; }

        /// <summary>
        /// Liefert den Namen des Steuerelements
        /// </summary>
        public string ControlTypename { get; private set; }

        /// <summary>
        /// Liefert den Namen des Styles
        /// </summary>
        public string Name { get; private set; }
    }
}
