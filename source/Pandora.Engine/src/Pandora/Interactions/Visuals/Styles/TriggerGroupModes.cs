﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Interactions.Visuals.Styles
{
    /// <summary>
    /// Gruppenmodus bei Effekt-Gruppen
    /// </summary>
    public enum TriggerGroupModes
    {
        /// <summary>
        /// Bleiben
        /// </summary>
        Keep = 0,

        /// <summary>
        /// Gruppe anhalten
        /// </summary>
        StopGroup
    }
}
