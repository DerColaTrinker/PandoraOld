﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Interactions.Visuals.Styles
{
    internal sealed class StyleResourceEntry
    {

        public StyleResourceEntry(string name, Type type, object result)
        {
            this.Name = name;
            this.Type = type;
            this.Value = result;
        }

        public string Name { get; private set; }

        public Type Type { get; private set; }

        public object Value { get; private set; }
    }
}
