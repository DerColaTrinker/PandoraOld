﻿using Pandora.Interactions.Visuals.BindingProperties;
using Pandora.Interactions.Visuals.Controls;
using Pandora.Runtime.IO;
using Pandora.Runtime.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Pandora.Interactions.Visuals.Drawing;
using System.Reflection;
using Pandora.Interactions.Visuals.Animations;

namespace Pandora.Interactions.Visuals.Styles
{
    /// <summary>
    /// Stellt einen Satz aus Styles dar
    /// </summary>
    public sealed class StyleSet
    {
        private HashSet<Style> _styles = new HashSet<Style>();
        internal Dictionary<string, StyleResourceEntry> __resources = new Dictionary<string, StyleResourceEntry>();

        #region Factory

        /// <summary>
        /// Beginnt eine Style-Konfiguration zu laden
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static StyleSet LoadStyle(string filename)
        {
            return LoadStyle(PandoraContentStream.Read(filename));
        }

        /// <summary>
        /// Beginnt eine Style-Konfiguration zu laden
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static StyleSet LoadStyle(PandoraContentStream stream)
        {
            var styleset = new StyleSet();
            var xd = new XmlDocument();

            try { xd.Load(stream); }
            catch (Exception ex) { Logger.Exception(ex); }

            #region Resources

            var resourcenodes = xd.SelectNodes("styles/resource");
            foreach (XmlNode resourcenode in resourcenodes)
            {
                var name = resourcenode.Attributes.GetValue("name", string.Empty);
                var type = resourcenode.Attributes.GetValue("type", string.Empty);
                var value = (object)resourcenode.Attributes.GetValue("value", string.Empty);

                if (string.IsNullOrEmpty(name))
                {
                    Logger.Warning("Empty resource entry");
                    continue;
                }


                switch (type.ToLower())
                {
                    case "color":
                        if (!Utility.ConvertHelper(ref value, typeof(Color)))
                        {
                            Logger.Warning("Resource entry '{0}' = '{1}' invalid format", name, value);
                            continue;
                        }
                        styleset.__resources.Add(name, new StyleResourceEntry(name, typeof(Color), value));
                        break;

                    default:
                        Logger.Warning("Unkown resource type '{0}' -> '{1}'", name, type);
                        break;
                }
            }

            #endregion

            #region Styles

            var stylenodes = xd.SelectNodes("styles/style");
            foreach (XmlNode stylenode in stylenodes)
            {
                #region Steuerelement suche

                // Den Type lesen und prüfen ob es diesen auch gibt
                var controltypename = stylenode.Attributes.GetValue("type", string.Empty);
                var controlname = stylenode.Attributes.GetValue("name", string.Empty);

                if (string.IsNullOrEmpty(controltypename)) continue;

                // Suche nach den Typen
                var controltype =
                    (
                    from asm in AppDomain.CurrentDomain.GetAssemblies()
                    from type in asm.GetTypes()
                    where type.FullName.ToLower() == controltypename.ToLower()
                    select type
                    ).FirstOrDefault();

                if (controltype == null)
                {
                    Logger.Warning("Style type '{0}' not found", controltypename);
                    continue;
                }

                var style = new Style(controltype, controltypename, controlname);
                var result1 = styleset._styles.Where(m => (m.ControlType == controltype & m.Name == controlname)).FirstOrDefault();
                var result2 = styleset._styles.Where(m => (m.ControlType == controltype & m.Name == "")).FirstOrDefault();

                #endregion

                #region Property Setter

                // Settings laden
                foreach (XmlNode setnode in stylenode.SelectNodes("set"))
                {
                    // Werte lesen
                    var propertyname = setnode.Attributes.GetValue("property", string.Empty);
                    var value = setnode.Attributes.GetValue("value", string.Empty);
                    if (string.IsNullOrEmpty(propertyname)) continue;

                    // Property suchen
                    var property = controltype.GetProperties().Where(m => m.CanWrite && m.Name.ToLower() == propertyname.ToLower()).FirstOrDefault();
                    if (property == null || !property.CanWrite)
                    {
                        Logger.Warning("Style property '{0}' on '{1}' not found or writable", propertyname, controltype.Name);
                        continue;
                    }

                    // Property Type ermitteln
                    var propertytypecode = Type.GetTypeCode(property.PropertyType);
                    var valueobject = (object)value;

                    // Den Wert holen
                    if (value.StartsWith("{") & value.EndsWith("}"))
                    {
                        // Entrynamen lesen
                        var entryname = value.Substring(1, value.Length - 2);
                        if (string.IsNullOrEmpty(entryname))
                        {
                            Logger.Warning("Style resource key is empty on '{0}' -> '{1}'", propertyname, controltype.Name);
                            continue;
                        }

                        // Entry suchen
                        var entry = (StyleResourceEntry)null;
                        if (!styleset.__resources.TryGetValue(entryname, out entry))
                        {
                            Logger.Warning("Style resource key  '{0}' -> '{1}' not found", entryname, controltype.Name);
                            continue;
                        }

                        // Prüfen ob der Quelltype mit dem Zieltype übereinstimmt
                        if (entry.Type != property.PropertyType)
                        {
                            Logger.Warning("Style resource key  '{0}' -> '{1}' differend type", entryname, propertyname);
                            continue;
                        }

                        valueobject = entry.Value;
                    }
                    else
                    {
                        // In das Zielformat konvertieren
                        if (!Utility.ConvertHelper(ref valueobject, property.PropertyType))
                        {
                            Logger.Warning("Style property '{0}' on '{1}' invalid cast", property.Name, controltype.Name);
                            continue;
                        }
                    }

                    style.AddProperty(property, valueobject);
                }

                #endregion

                #region Trigger

                foreach (XmlNode triggernode in stylenode.SelectNodes("trigger"))
                {
                    var eventtext = triggernode.Attributes.GetValue("event", "");
                    var grouptext = triggernode.Attributes.GetValue("group", "");
                    var groupmodetext = triggernode.Attributes.GetValue("groupmode", "");

                    // Event prüfen
                    if (string.IsNullOrEmpty(eventtext))
                    {
                        Logger.Warning("Style control '' invalid trigger", controltype.Name);
                        continue;
                    }

                    var trigger = new StyleTrigger();

                    try { trigger.Event = (TriggerEventTypes)Enum.Parse(typeof(TriggerEventTypes), eventtext, true); }
                    catch (Exception) { Logger.Warning("Style '{0}' trigger '{1}' invalid event type", controltype.Name, eventtext); continue; }

                    trigger.Group = grouptext.ToLower();

                    if (!string.IsNullOrEmpty(groupmodetext))
                    {
                        try { trigger.GroupMode = (TriggerGroupModes)Enum.Parse(typeof(TriggerGroupModes), groupmodetext); }
                        catch (Exception) { Logger.Warning("Style '{0}' invalid trigger group mode '{1}'", controltype.Name, groupmodetext); }
                    }

                    // Direkter Effekt
                    if (triggernode.FirstChild.Name == "effect")
                    {
                        var effecttypename = triggernode.FirstChild.Attributes.GetValue("type", string.Empty);
                        var effecttime = triggernode.FirstChild.Attributes.GetValue("time", 0F);
                        var value = triggernode.FirstChild.Attributes.GetValue("value", string.Empty);

                        var effecttype = (from asm in AppDomain.CurrentDomain.GetAssemblies()
                                          where asm.FullName.Contains("Pandora")
                                          from t in asm.GetTypes()
                                          let cint = t.GetInterface("IControlEffect`1")
                                          where t.Name.ToLower() == effecttypename.ToLower() && cint != null
                                          select t).FirstOrDefault();

                        if (effecttype == null)
                        {
                            Logger.Warning("Style '{0}' effect not found '{1}'", controltype.Name, effecttypename);
                            continue;
                        }

                        var styletriggereffect = new StyleTriggerEffect(styleset, effecttype, effecttime, value);
                        trigger.AddEffect(styletriggereffect);
                    }

                    style.AddTrigger(trigger);
                }

                #endregion

                styleset._styles.Add(style);
            }

            #endregion

            return styleset;
        }

        #endregion

        internal Style FindStyle(Visual control)
        {
            var controltype = control.GetType();
            var result1 = _styles.Where(m => m.ControlType.FullName == controltype.FullName & (m.Name == control.Name)).FirstOrDefault();
            var result2 = _styles.Where(m => m.ControlType.FullName == controltype.FullName & (string.IsNullOrEmpty(m.Name))).FirstOrDefault();

            if (result1 != null) return result1;

            return result2;
        }
    }
}
