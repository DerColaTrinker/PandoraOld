﻿using Pandora.Interactions.Visuals.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Styles
{
    static class Utility
    {
        internal static bool ConvertHelper(ref  object value, Type targettype)
        {
            var typecode = Type.GetTypeCode(targettype);

            switch (typecode)
            {
                case TypeCode.Object:
                    switch (targettype.FullName)
                    {
                        case "Pandora.Interactions.Visuals.Drawing.Color":          // Color Konverter
                            try
                            {
                                var parts = value.ToString().Split(',');
                                switch (parts.Length)
                                {
                                    case 1:
                                        value = new Color(Convert.ToUInt32(value));
                                        break;
                                    case 3:
                                        value = new Color(Convert.ToByte(parts[0]), Convert.ToByte(parts[1]), Convert.ToByte(parts[2]));
                                        break;
                                    case 4:
                                        value = new Color(Convert.ToByte(parts[0]), Convert.ToByte(parts[1]), Convert.ToByte(parts[2]), Convert.ToByte(parts[3]));
                                        break;
                                    default:
                                        return false;
                                }
                            }
                            catch (Exception)
                            {
                                return false;
                            }
                            break;

                        case "Pandora.Interactions.Visuals.Drawing.Vector2f":
                            try
                            {
                                var parts = value.ToString().Split(',');
                                if (parts.Length != 2) return false;

                                value = new Vector2f(Convert.ToSingle(parts[0]), Convert.ToSingle(parts[1]));
                                break;
                            }
                            catch (Exception)
                            {
                                return false;
                            }

                        case "Pandora.Interactions.Visuals.Drawing.Vector2i":
                            try
                            {
                                var parts = value.ToString().Split(',');
                                if (parts.Length != 2) return false;

                                value = new Vector2f(Convert.ToInt32(parts[0]), Convert.ToInt32(parts[1]));
                                break;
                            }
                            catch (Exception)
                            {
                                return false;
                            }

                        case "Pandora.Interactions.Visuals.Drawing.Vector2u":
                            try
                            {
                                var parts = value.ToString().Split(',');
                                if (parts.Length != 2) return false;

                                value = new Vector2f(Convert.ToUInt32(parts[0]), Convert.ToUInt32(parts[1]));
                                break;
                            }
                            catch (Exception)
                            {
                                return false;
                            }

                        default:
                            return false;
                    }
                    break;

                case TypeCode.DBNull:
                case TypeCode.Empty:
                    throw new InvalidCastException(typecode.ToString());

                default:
                    if (targettype.IsEnum)
                    {
                        value = Enum.Parse(targettype, value.ToString(), true);
                    }
                    else
                    {
                        value = Convert.ChangeType(value, targettype);
                    }
                    break;
            }

            return true;
        }
    }
}
