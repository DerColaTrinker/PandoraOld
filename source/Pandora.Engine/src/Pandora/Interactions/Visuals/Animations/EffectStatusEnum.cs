﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Animations
{
    /// <summary>
    /// Gibt den Status eines Effekts an
    /// </summary>
    public enum EffectStatusEnum
    {
        /// <summary>
        /// Läuft
        /// </summary>
        Running,

        /// <summary>
        /// Gestoppt
        /// </summary>
        Stopped,

        /// <summary>
        /// Angehalten
        /// </summary>
        Pause
    }
}
