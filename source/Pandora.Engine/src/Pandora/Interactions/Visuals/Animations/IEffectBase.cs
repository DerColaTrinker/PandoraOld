﻿using System;

namespace Pandora.Interactions.Visuals.Animations
{
    /// <summary>
    /// 
    /// </summary>
    public interface IEffectBase
    {
        /// <summary>
        /// 
        /// </summary>
        bool IsComplet { get; }

        /// <summary>
        /// 
        /// </summary>
        float Runtime { get; }

        /// <summary>
        /// 
        /// </summary>
        EffectStatusEnum Status { get; set; }
    }
}
