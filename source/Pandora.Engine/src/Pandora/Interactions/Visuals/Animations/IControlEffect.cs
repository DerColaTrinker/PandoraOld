﻿using System;

namespace Pandora.Interactions.Visuals.Animations
{
    interface IControlEffect<TControl>
     where TControl : Pandora.Interactions.Visuals.Controls.IControl
    {
        TControl Control { get; }

        string Group { get; set; }
    }
}
