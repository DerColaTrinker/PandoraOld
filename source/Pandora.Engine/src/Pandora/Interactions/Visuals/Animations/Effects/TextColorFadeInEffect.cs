﻿using Pandora.Interactions.Visuals.BindingProperties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Animations.Effects
{
    /// <summary>
    /// Stellt einen Einbleneffekt dar.
    /// </summary>
    /// <remarks>Das Steuerelement muss die Schnittstelle <see cref="ITextColorProperty"/> implementieren</remarks>
    public class TextColorFadeInEffect : TextColorBlendEffect
    {
        /// <summary>
        /// Erstellt eine neue Instanz der FadeInEffect-Klasse
        /// </summary>
        /// <param name="control"></param>
        /// <param name="time"></param>
        public TextColorFadeInEffect(ITextColorProperty control, float time)
            : base(control, 255, time)
        { }
    }
}
