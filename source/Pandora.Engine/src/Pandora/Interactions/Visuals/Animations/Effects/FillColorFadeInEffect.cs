﻿using Pandora.Interactions.Visuals.BindingProperties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Animations.Effects
{
    /// <summary>
    /// Stellt einen Einbleneffekt dar.
    /// </summary>
    /// <remarks>Das Steuerelement muss die Schnittstelle <see cref="IFillColorProperty"/> implementieren</remarks>
    public class FillColorFadeInEffect : FillColorBlendEffect
    {
        /// <summary>
        /// Erstellt eine neue Instanz der FadeInEffect-Klasse
        /// </summary>
        /// <param name="control"></param>
        /// <param name="time"></param>
        public FillColorFadeInEffect(IFillColorProperty control, float time)
            : base(control, 255, time)
        { }
    }
}
