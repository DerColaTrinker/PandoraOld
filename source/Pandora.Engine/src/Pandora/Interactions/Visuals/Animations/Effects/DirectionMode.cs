﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Interactions.Visuals.Animations.Effects
{
    /// <summary>
    /// Legt die Art der Drehrichtung fest
    /// </summary>
    public enum DirectionMode
    {
        /// <summary>
        /// Direkt den kürzesten Weg
        /// </summary>
        Direct,

        /// <summary>
        /// Immer gegen den Uhrzeigersinn
        /// </summary>
        Left,

        /// <summary>
        /// Immer im Uhrzeigersinn
        /// </summary>
        Right
    }
}
