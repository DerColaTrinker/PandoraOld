﻿using Pandora.Interactions.Visuals.BindingProperties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Animations.Effects
{
    /// <summary>
    /// 
    /// </summary>
    public class RotateEffect : ControlEffect<IRotationProperty, float>
    {
        private float _factor;
        private float _source;
        private DirectionMode _mode;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="target"></param>
        /// <param name="time"></param>
        /// <param name="mode"></param>
        public RotateEffect(IRotationProperty control, float target, DirectionMode mode, float time)
            : base(control, target, time)
        {
            _source = control.Rotation;
            _mode = mode;

            // je nach rotation die korrekte Drehrichtung festelegen
            switch (mode)
            {
                // Der Modus verwendet eine Drehrichtung die sich anhand des Deltas ermittelt
                case DirectionMode.Direct:
                    if (target > control.Rotation)
                        _factor = (target - control.Rotation) / Duration;
                    else
                        _factor = (control.Rotation - target) / Duration;
                    break;

                // Links Drehung
                case DirectionMode.Left:
                    _factor = (target - control.Rotation) / Duration;
                    break;

                // Rechts Drehung
                case DirectionMode.Right:
                    _factor = (control.Rotation - target) / Duration;
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected override void Update(float ms, float s)
        {
            // Position anpassen entsprechend der vergangenen Zeit und dem Faktor
            Control.Rotation += _factor * ms;
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt abgeschlossen wurde
        /// </summary>
        protected override void OnFinish()
        {
            // Das Objekt an die Zielposition lege, da wir auch mit FLOAT nicht genau das Ziel treffen.
            Control.Rotation = TargetValue;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Reset()
        {
            // UFF...
            Control.Rotation = _source;
        }
    }
}
