﻿using Pandora.Interactions.Visuals.BindingProperties;
using Pandora.Interactions.Visuals.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Animations.Effects
{
    /// <summary>
    /// Stellt einen Blendeffekt dar.
    /// </summary>
    /// <remarks>Dieser Effekt kann nur Steuerelemente bedienen der die <see cref="ITextColorProperty"/> schnittstelle implementieren</remarks>
    public class TextColorBlendEffect : ControlEffect<ITextColorProperty, byte>
    {
        private float _cR;
        private float _cG;
        private float _cB;
        private float _cA;
        private float _fA;

        /// <summary>
        /// Erstellt eine neue Instanz der BlendEffect-Klasse
        /// </summary>
        /// <param name="control"></param>
        /// <param name="target"></param>
        /// <param name="time"></param>
        public TextColorBlendEffect(ITextColorProperty control, byte target, float time)
            : base(control, target, time)
        { }

        /// <summary>
        /// Wird beim Start des Effekts aufgerufen
        /// </summary>
        protected override void OnStart()
        {
            ConvertToFloat();
            CreateFactors();

            base.OnStart();
        }

        private void CreateFactors()
        {
            // Faktoren
            _fA = (TargetValue - _cA) / Duration;
        }

        private void ConvertToFloat()
        {
            // Die Farbwerte als FLOAT speichern, da diese besser errechnet werden können
            _cR = (float)Control.TextColor.R;
            _cG = (float)Control.TextColor.G;
            _cB = (float)Control.TextColor.B;
            _cA = (float)Control.TextColor.A;
        }

        /// <summary>
        /// Wird aufgerufen, wenn der Effekt aktualisiert werden muss
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected override void Update(float ms, float s)
        {
            _cA += (_fA * ms);

            Control.TextColor = new Color((byte)_cR, (byte)_cG, (byte)_cB, (byte)_cA);
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt abgeschlossen wurde
        /// </summary>
        protected override void OnFinish()
        {
            Control.TextColor = new Color((byte)Control.TextColor.R, (byte)Control.TextColor.B, (byte)Control.TextColor.G, (byte)TargetValue);
        }

        /// <summary>
        /// Wird aufgerufen, wenn der Effekt zurückgesetzt wird
        /// </summary>
        protected override void Reset()
        {
            ConvertToFloat();
            CreateFactors();
        }
    }
}
