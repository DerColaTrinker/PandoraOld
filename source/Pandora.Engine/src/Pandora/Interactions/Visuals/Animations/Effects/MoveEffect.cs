﻿using Pandora.Interactions.Visuals.BindingProperties;
using Pandora.Interactions.Visuals.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Animations.Effects
{
    /// <summary>
    /// 
    /// </summary>
    public class MoveEffect : ControlEffect<IPositionProperty, Vector2f>
    {
        private Vector2f _factor;
        private Vector2f _source;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="target"></param>
        /// <param name="time"></param>
        public MoveEffect(IPositionProperty control, Vector2f target, float time)
            : base(control, target, time)
        {
            _source = control.Position;
            _factor = new Vector2f((control.Position.X - target.X) / time, (control.Position.Y - target.Y) / time);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected override void Update(float ms, float s)
        {
            // Position anpassen entsprechend der vergangenen Zeit und dem Faktor
            Control.Position += new Vector2f(_factor.X * ms, _factor.Y * ms);
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt abgeschlossen wurde
        /// </summary>
        protected override void OnFinish()
        {
            // Das Objekt an die Zielposition lege, da wir auch mit FLOAT nicht genau das Ziel treffen.
            Control.Position = TargetValue;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Reset()
        {
            // UFF...
            Control.Position = _source;
        }
    }
}
