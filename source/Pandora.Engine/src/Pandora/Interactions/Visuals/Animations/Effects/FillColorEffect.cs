﻿using Pandora.Interactions.Visuals.BindingProperties;
using Pandora.Interactions.Visuals.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Animations.Effects
{
    /// <summary>
    /// Ein Effekt der die Füllfarbe ändert.
    /// </summary>
    /// <remarks> Das Steuerelement muss die <see cref="IFillColorProperty"/> Schnittstelle implementieren</remarks>
    public class FillColorEffect : ControlEffect<IFillColorProperty, Color>
    {
        private float _cR;
        private float _cG;
        private float _cB;
        private float _cA;
        private float _fR;
        private float _fG;
        private float _fB;
        private float _fA;

        /// <summary>
        /// Erstellt eine neue Instnaz der FillColorEffect-Klasse
        /// </summary>
        /// <param name="control"></param>
        /// <param name="target"></param>
        /// <param name="time"></param>
        public FillColorEffect(IFillColorProperty control, Color target, float time)
            : base(control, target, time)
        { }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt startet
        /// </summary>
        protected override void OnStart()
        {
            ConvertToFloat();
            CreateFactors();

            base.OnStart();
        }

        private void CreateFactors()
        {
            // Faktoren
            _fR = ((float)TargetValue.R - _cR) / Duration;
            _fG = ((float)TargetValue.G - _cG) / Duration;
            _fB = ((float)TargetValue.B - _cB) / Duration;
            _fA = ((float)TargetValue.A - _cA) / Duration;
        }

        private void ConvertToFloat()
        {
            // Die Farbwerte als FLOAT speichern, da diese besser errechnet werden können
            _cR = (float)Control.FillColor.R;
            _cG = (float)Control.FillColor.G;
            _cB = (float)Control.FillColor.B;
            _cA = (float)Control.FillColor.A;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected override void Update(float ms, float s)
        {
            _cR += (_fR * ms);
            _cG += (_fG * ms);
            _cB += (_fB * ms);
            _cA += (_fA * ms);

            Control.FillColor = new Color((byte)_cR, (byte)_cG, (byte)_cB, (byte)_cA);
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt abgeschlossen wurde
        /// </summary>
        protected override void OnFinish()
        {
            Control.FillColor = new Color((byte)TargetValue.R, (byte)TargetValue.G, (byte)TargetValue.B, (byte)TargetValue.A);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Reset()
        {
            ConvertToFloat();
            CreateFactors();
        }
    }
}
