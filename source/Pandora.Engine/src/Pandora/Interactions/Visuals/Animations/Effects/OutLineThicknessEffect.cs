﻿using Pandora.Interactions.Visuals.BindingProperties;
using Pandora.Interactions.Visuals.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Animations.Effects
{
    /// <summary>
    /// 
    /// </summary>
    public class OutLineThicknessEffect : ControlEffect<IOutlineThicknessProperty, float>
    {
        private float _time;
        private float _factor;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="target"></param>
        /// <param name="time"></param>
        public OutLineThicknessEffect(IOutlineThicknessProperty control, float target, float time)
            : base(control, target, time)
        {
            _time = time;

            CreateFactors();
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnStart()
        {
            CreateFactors();

            base.OnStart();
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreateFactors()
        {
            _factor = (TargetValue - Control.OutlineThickness) / _time;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected override void Update(float ms, float s)
        {
            Control.OutlineThickness += (_factor * ms);
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt abgeschlossen wurde
        /// </summary>
        protected override void OnFinish()
        {
            Control.OutlineThickness = TargetValue;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Reset()
        {
            CreateFactors();
        }
    }
}
