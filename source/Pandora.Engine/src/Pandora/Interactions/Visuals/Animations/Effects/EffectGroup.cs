﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Animations
{
    /// <summary>
    /// Stellt einen Effekt dar, der mehrere Effekte parallel ausführen kann
    /// </summary>
    public sealed class EffectGroup : EffectBase
    {
        private HashSet<EffectBase> _effects = new HashSet<EffectBase>();

        /// <summary>
        /// Erstellt eine neue Instant der EffectGroup-Klasse
        /// </summary>
        public EffectGroup()
            : base(0)
        { }

        /// <summary>
        /// Fügt ein Effekt hinzu
        /// </summary>
        /// <param name="effect"></param>
        public void Add(EffectBase effect)
        {
            _effects.Add(effect);
            Duration = _effects.Max(m => m.Duration);
        }

        /// <summary>
        /// Fügt eine Auflistung an Effekten hinzu
        /// </summary>
        /// <param name="effects"></param>
        public void AddRange(params EffectBase[] effects)
        {
            foreach (var effect in effects) _effects.Add(effect);
            Duration = _effects.Max(m => m.Duration);
        }

        /// <summary>
        /// Fügt eine Auflistung an Effekten hinzu
        /// </summary>
        /// <param name="effects"></param>
        public void AddRange(IEnumerable<EffectBase> effects)
        {
            foreach (var effect in effects) _effects.Add(effect);
            Duration = _effects.Max(m => m.Duration);
        }

        /// <summary>
        /// Entfernt ein Effekt
        /// </summary>
        /// <param name="effect"></param>
        public void Remove(EffectBase effect)
        {
            _effects.Remove(effect);
            Duration = _effects.Max(m => m.Duration);
        }
  
        /// <summary>
        /// Wird aufgerufen wenn der Effekt aktualisiert wird
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected override void Update(float ms, float s)
        {
            // Erst wenn alle Elemente der Liste beendet sind, wird der Aufruf nicht mehr erfolgen.
            var result = _effects.All(m => { m.InternalUpdate(ms, s); return m.IsComplet; });

            if (result) SetComplet();
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt startet
        /// </summary>
        protected override void OnStart()
        {
            foreach (var effect in _effects) effect.Status = EffectStatusEnum.Running;

            base.OnStart();
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt unterbrochen wird
        /// </summary>
        protected override void OnPause()
        {
            foreach (var effect in _effects) effect.Status = EffectStatusEnum.Pause;

            base.OnPause();
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt angehalten wird
        /// </summary>
        protected override void OnStop()
        {
            foreach (var effect in _effects) effect.Status = EffectStatusEnum.Stopped;

            base.OnStop();
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt zurückgesetzt wird
        /// </summary>
        protected override void Reset()
        {
            foreach (var effect in _effects) effect.InternalReset();
        }
    }
}
