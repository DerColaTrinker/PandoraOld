﻿using Pandora.Interactions.Visuals.BindingProperties;
using Pandora.Interactions.Visuals.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Animations.Effects
{
    /// <summary>
    /// 
    /// </summary>
    public class ScaleEffect : ControlEffect<IScaleProperty, Vector2f>
    {
        private Vector2f _source;
        private Vector2f _factor;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="target"></param>
        /// <param name="time"></param>
        public ScaleEffect(IScaleProperty control, Vector2f target, float time)
            : base(control, target, time)
        {
            _source = control.Scale;
            _factor = new Vector2f((control.Scale.X - target.X) / time, (control.Scale.Y - target.Y) / time);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected override void Update(float ms, float s)
        {
            // Größe anpassen entsprechend der vergangenen Zeit und dem Faktor
            Control.Scale += new Vector2f(_factor.X * ms, _factor.Y * ms);
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt abgeschlossen wurde
        /// </summary>
        protected override void OnFinish()
        {
            // Das Objekt an den Zielwert festlegen, da wir auch mit FLOAT nicht genau das Ziel treffen.
            Control.Scale = TargetValue;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Reset()
        {
            // UFF...
            Control.Scale = _source;
        }
    }
}
