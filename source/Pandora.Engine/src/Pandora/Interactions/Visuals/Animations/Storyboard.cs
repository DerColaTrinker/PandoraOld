﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Animations
{
    /// <summary>
    /// Stellt ein Gruppe von Animationen dar, die Zeitlich gesteuert ablaufen
    /// </summary>
    public sealed class Storyboard : EffectBase
    {
        private static readonly AnimationEntryComparer EntryComparer = new AnimationEntryComparer();
        private List<StoryboardEntry> _entries = new List<StoryboardEntry>();
        private HashSet<StoryboardEntry> _activentries = new HashSet<StoryboardEntry>();
        private int _entryindex = 0;
        private AnimationEntryDirection _entryindexdirection = AnimationEntryDirection.Forward;

        private enum AnimationEntryDirection : int
        {
            Forward = 1,
            Reverse = -1
        }

        private sealed class AnimationEntryComparer : IComparer<StoryboardEntry>
        {
            public int Compare(StoryboardEntry x, StoryboardEntry y)
            {
                if (x.StartTime < y.StartTime) return -1;
                if (x.StartTime > y.StartTime) return 1;
                return 0;
            }
        }

        /// <summary>
        /// Stellt ein Element innerhalb des Storyboards dar
        /// </summary>
        public sealed class StoryboardEntry
        {
            /// <summary>
            /// Erstellt eine neue Instanz der StoryboardEntry-Klasse
            /// </summary>
            /// <param name="starttime"></param>
            /// <param name="effect"></param>
            public StoryboardEntry(float starttime, EffectBase effect)
            {
                StartTime = starttime;
                Effect = effect;
            }

            /// <summary>
            /// Liefert die Startzeit des Effekts
            /// </summary>
            public float StartTime { get; private set; }

            /// <summary>
            /// Liefert die Endzeit des Effekts
            /// </summary>
            public float EndTime { get { return StartTime + Duration; } }

            /// <summary>
            /// Liefert die Laufzeit des Effekts
            /// </summary>
            public float Duration { get { return Effect.Duration; } }

            /// <summary>
            /// Liefert den Effekt
            /// </summary>
            public EffectBase Effect { get; private set; }
        }

        /// <summary>
        /// Erstellt eine neue Klasse der AnimationStoryboard-Klasse
        /// </summary>
        public Storyboard()
            : base(0)
        { }

        #region Entry Management

        /// <summary>
        /// Fügt ein Storyboard-Element hinzu
        /// </summary>
        /// <param name="starttime"></param>
        /// <param name="effect"></param>
        public void Add(float starttime, EffectBase effect)
        {
            // Erstmal ein Entry erstellen
            var entry = new StoryboardEntry(starttime, effect);

            _entries.Add(entry);

            UpdateEntryStats();
        }

        /// <summary>
        /// Fügt eine Liste von Storyboard Elementen hinzu
        /// </summary>
        /// <param name="entries"></param>
        public void AddRange(params StoryboardEntry[] entries)
        {
            AddRange(entries.AsEnumerable());
        }

        private void AddRange(IEnumerable<StoryboardEntry> entries)
        {
            foreach (var entry in entries)
                _entries.Add(entry);

            UpdateEntryStats();
        }

        private void UpdateEntryStats()
        {
            if (_entries.Count == 0) return;

            // Neu sortieren der Startzeit, damit ist es einfacher im späteren Update die nächsten Animationen laufen zu lassen
            _entries.Sort(EntryComparer);

            // Laufzeit berechnen
            Duration = _entries.Last().StartTime + _entries.Last().Duration;
        }

        #endregion

        /// <summary>
        /// Aktualisiert das Storyboard
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected override void Update(float ms, float s)
        {
            // Nach Entries suchen die jetzt dran sind
            for (; _entryindex < _entries.Count; _entryindex += (int)_entryindexdirection)
            {
                var entry = _entries[_entryindex];

                if (CurrentTime >= entry.StartTime)
                {
                    _activentries.Add(entry);
                    entry.Effect.Status = EffectStatusEnum.Running;
                }
                else
                    break;
            }

            _activentries.RemoveWhere(m =>
            {
                m.Effect.InternalUpdate(ms, s);
                return m.Effect.IsComplet;
            }
            );
        }

        /// <summary>
        /// Startet das Storyboard
        /// </summary>
        protected override void OnStart()
        {
            _activentries.Clear();
            switch (_entryindexdirection)
            {
                case AnimationEntryDirection.Forward: _entryindex = 0; break;
                case AnimationEntryDirection.Reverse: _entryindex = _entries.Count - 1; break;

            }
            base.OnStart();
        }

        /// <summary>
        /// Setzt das Storyboard zurück
        /// </summary>
        protected override void Reset()
        {
            _entryindex = 0;
            foreach (var entry in _entries) entry.Effect.InternalReset();
            _activentries.Clear();
        }
    }
}
