﻿using Pandora.Interactions.Visuals.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Animations
{
    /// <summary>
    /// Stellt Basis eines Steuerelement Effekts dar
    /// </summary>
    /// <typeparam name="TControl"></typeparam>
    public abstract class ControlEffect<TControl> : EffectBase, IControlEffect<TControl>
        where TControl : IControl
    {
        /// <summary>
        /// Erstellt eine neue Instanz der ControlEffect-Klasse
        /// </summary>
        /// <param name="control"></param>
        /// <param name="time"></param>
        protected ControlEffect(TControl control, float time)
            : base(time)
        {
            Control = control;
        }

        /// <summary>
        /// Liefert das Steuerelement auf das dieser Effekt angelehnt ist
        /// </summary>
        public TControl Control { get; private set; }
    }

    /// <summary>
    /// Stellt Basis eines Steuerelement Effekts dar
    /// </summary>
    /// <typeparam name="TControl"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public abstract class ControlEffect<TControl, TValue> : EffectBase<TValue>, IControlEffect<TControl>
        where TControl : IControl
    {
        /// <summary>
        /// Erstellt eine neue Instanz der ControlEffect-Klasse
        /// </summary>
        /// <param name="control"></param>
        /// <param name="targetvalue"></param>
        /// <param name="time"></param>
        protected ControlEffect(TControl control, TValue targetvalue, float time)
            : base(targetvalue, time)
        {
            Control = control;
        }

        /// <summary>
        /// Liefert das Steuerelement auf das dieser Effekt angelehnt ist
        /// </summary>
        public TControl Control { get; private set; }
    }
}
