﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Animations
{
    /// <summary>
    /// Ein Type das die Animationszeit darstellt
    /// </summary>
    [StructLayout(LayoutKind.Auto)]
    public struct AnimationTime : IComparable, IComparable<AnimationTime>, IEquatable<AnimationTime>
    {
        private float _value;

        /// <summary>
        /// Erstellt eine neue Struktur mit angegebener Anzahl Millisekunden
        /// </summary>
        /// <param name="ms"></param>
        public AnimationTime(float ms)
        {
            _value = ms;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Addiert zwei Zeiten
        /// </summary>
        /// <param name="d1"></param>
        /// <param name="d2"></param>
        /// <returns></returns>
        public static float operator +(AnimationTime d1, AnimationTime d2)
        {
            return d1._value + d2._value;
        }

        /// <summary>
        /// Subtrahiert zwei Zeiten
        /// </summary>
        /// <param name="d1"></param>
        /// <param name="d2"></param>
        /// <returns></returns>
        public static float operator -(AnimationTime d1, AnimationTime d2)
        {
            return d1._value - d2._value;
        }

        /// <summary>
        /// Vergleicht zwei Zeiten
        /// </summary>
        /// <param name="d1"></param>
        /// <param name="d2"></param>
        /// <returns></returns>
        public static bool operator ==(AnimationTime d1, AnimationTime d2)
        {
            return d1._value == d2._value;
        }

        /// <summary>
        /// Vergleicht zwei unlgeiche Zeiten
        /// </summary>
        /// <param name="d1"></param>
        /// <param name="d2"></param>
        /// <returns></returns>
        public static bool operator !=(AnimationTime d1, AnimationTime d2)
        {
            return d1._value != d2._value;
        }

        /// <summary>
        /// Kleiner
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static bool operator <(AnimationTime t1, AnimationTime t2)
        {
            return t1._value < t2._value;
        }

        /// <summary>
        /// Kleiner gleich
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static bool operator <=(AnimationTime t1, AnimationTime t2)
        {
            return t1._value <= t2._value;
        }

        /// <summary>
        /// Größer
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static bool operator >(AnimationTime t1, AnimationTime t2)
        {
            return t1._value > t2._value;
        }

        /// <summary>
        /// Größer gleich
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static bool operator >=(AnimationTime t1, AnimationTime t2)
        {
            return t1._value >= t2._value;
        }

        /// <summary>
        /// Konvertiert die Zeitangabe in einen Float Datentyp
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static implicit operator float(AnimationTime a)
        {
            return a._value;
        }

        /// <summary>
        /// Konvertiert die Zeitangabe aus einen Float Datentyp
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static implicit operator AnimationTime(float a)
        {
            return new AnimationTime(a);
        }

        /// <summary>
        /// Parst eine Zeitangabe aus einem Formatierten String Stunden:Minuten:Sekunden:Millisekunden
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static AnimationTime Parse(string value)
        {
            if (string.IsNullOrEmpty(value)) throw new ArgumentNullException("value");
            var parts = value.Split(':');

            switch (parts.Length)
            {
                case 1:
                    {
                        var ms = 0F;
                        if (!float.TryParse(parts[0], out ms)) throw new InvalidCastException("Value could not convert");
                        if (ms > 999 | ms < 0) throw new ArgumentOutOfRangeException("ms out of range");

                        return new AnimationTime(ms);
                    }

                case 2:
                    {
                        var s = 0F;
                        if (!float.TryParse(parts[0], out s)) throw new InvalidCastException("Value could not convert");
                        if (s > 59 | s < 0) throw new ArgumentOutOfRangeException("s out of range");

                        var ms = 0F;
                        if (!float.TryParse(parts[1], out ms)) throw new InvalidCastException("Value could not convert");
                        if (ms > 999 | ms < 0) throw new ArgumentOutOfRangeException("ms out of range");

                        return new AnimationTime((s * 1000) + ms);
                    }

                case 3:
                    {
                        var m = 0F;
                        if (!float.TryParse(parts[0], out m)) throw new InvalidCastException("Value could not convert");
                        if (m > 59 | m < 0) throw new ArgumentOutOfRangeException("m out of range");

                        var s = 0F;
                        if (!float.TryParse(parts[1], out s)) throw new InvalidCastException("Value could not convert");
                        if (s > 59 | s < 0) throw new ArgumentOutOfRangeException("s out of range");

                        var ms = 0F;
                        if (!float.TryParse(parts[2], out ms)) throw new InvalidCastException("Value could not convert");
                        if (ms > 999 | ms < 0) throw new ArgumentOutOfRangeException("ms out of range");

                        return new AnimationTime((m * 60000) + (s * 1000) + ms);
                    }

                case 4:
                    {
                        var h = 0F;
                        if (!float.TryParse(parts[0], out h)) throw new InvalidCastException("Value could not convert");
                        if (h < 0) throw new ArgumentOutOfRangeException("h out of range");

                        var m = 0F;
                        if (!float.TryParse(parts[1], out m)) throw new InvalidCastException("Value could not convert");
                        if (m > 59 | m < 0) throw new ArgumentOutOfRangeException("m out of range");

                        var s = 0F;
                        if (!float.TryParse(parts[2], out s)) throw new InvalidCastException("Value could not convert");
                        if (s > 59 | s < 0) throw new ArgumentOutOfRangeException("s out of range");

                        var ms = 0F;
                        if (!float.TryParse(parts[3], out ms)) throw new InvalidCastException("Value could not convert");
                        if (ms > 999 | ms < 0) throw new ArgumentOutOfRangeException("ms out of range");

                        return new AnimationTime((h * 3600000) * (m * 60000) + (s * 1000) + ms);
                    }

                default:
                    throw new InvalidCastException("Value could not convert");
            }
        }

        /// <summary>
        /// Versucht eine Zeichenkette zu parsen
        /// </summary>
        /// <param name="value"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool TryParse(string value, out AnimationTime result)
        {
            result = new AnimationTime();

            try
            {
                result = Parse(value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Vergleicht das angegebene Objekte
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool Equals(Object value)
        {
            if (value is DateTime)
            {
                return _value == ((AnimationTime)value)._value;
            }
            return false;
        }

        /// <summary>
        /// Vergleicht das angegebene Objekt
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int CompareTo(object value)
        {
            if (value == null) return 1;
            if (!(value is AnimationTime))
            {
                throw new ArgumentException("Must be 'AnimationTime'");
            }

            return Compare(this, (AnimationTime)value);
        }

        /// <summary>
        /// Vergleicht zwei Zeiten
        /// </summary>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <returns></returns>
        public static int Compare(AnimationTime t1, AnimationTime t2)
        {
            var ticks1 = t1._value;
            var ticks2 = t2._value;
            if (ticks1 > ticks2) return 1;
            if (ticks1 < ticks2) return -1;
            return 0;
        }

        /// <summary>
        /// Vergleicht eine Zeit
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int CompareTo(AnimationTime value)
        {
            return Compare(this, value);
        }

        /// <summary>
        /// Liefert eine Zeichenkette der Zeit
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var s = (_value / 1000);
            var ms = _value - (s * 1000);
            var m = (s / 60);
            s = s % 60;
            var h = m / 60;
            m = m % 60;

            return string.Format("{0:00}:{1:00}:{2:00}:{3:0000}", h, m, s, ms);
        }

        /// <summary>
        /// Vergleicht die angegebene Zeit
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Equals(AnimationTime value)
        {
            return _value == value._value;
        }
    }
}
