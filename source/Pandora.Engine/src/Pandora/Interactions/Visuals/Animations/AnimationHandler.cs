﻿using Pandora.Interactions.Visuals.Animations.Effects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Interactions.Visuals.Animations
{
    /// <summary>
    /// Ein System, das die Animationen verwaltet.
    /// </summary>
    public sealed class AnimationHandler
    {
        private HashSet<EffectBase> _effects = new HashSet<EffectBase>();

        internal AnimationHandler(IVisualHandler handler)
        {
            Visuals = handler;
        }

        internal bool Initialize()
        {
            Logger.Normal("Animation handler ready");
            return true;
        }

        internal void SystemUpdate(float ms, float s)
        {
            // Damit wir die Effekte direkt aus der Liste löschen können, müssen wir auf ForEach verzichten. Dazu nutzen wir RemoveWhere und ein Predikat.
            _effects.RemoveWhere(effect =>
            {
                if (effect.Status == EffectStatusEnum.Running)
                {
                    // Zuerst das Update innerhalb des Effekts aufrufen
                    effect.InternalUpdate(ms, s);
                }

                // Jetzt entscheiden ob es entfernt werden kann
                if (effect.IsComplet)
                {
                    effect.Status = EffectStatusEnum.Stopped;
                    return true;
                }
                else
                {
                    return false;
                }
            });
        }

        /// <summary>
        /// Liefert Zugriff auf den Interaction Manager
        /// </summary>
        public IVisualHandler Visuals { get; private set; }

        /// <summary>
        /// Startet ein Effekt
        /// </summary>
        /// <param name="effect"></param>
        public void Start(EffectBase effect)
        {
            if (_effects.Add(effect))
            {
                effect.Status = EffectStatusEnum.Running;
                return;
            }
            else
            {
                //_effects.Add(effect);           
                effect.InternalReset();
            }
        }

        /// <summary>
        /// Startet eine Gruppe von Effekten
        /// </summary>
        /// <param name="effects"></param>
        public void Start(IEnumerable<EffectBase> effects)
        {
            foreach (var item in effects) Start(item);
        }

        /// <summary>
        /// Stoppt ein Effekt
        /// </summary>
        /// <param name="effect"></param>
        public void Stop(EffectBase effect)
        {
            // Bei einem Stop, den Effekt aus der Liste nehmen
            effect.Status = EffectStatusEnum.Stopped;

            _effects.Remove(effect);

        }

        /// <summary>
        /// Hält eine Gruppe von Effekten an
        /// </summary>
        /// <param name="effects"></param>
        public void Stop(IEnumerable<EffectBase> effects)
        {
            foreach (var item in effects) Stop(item);
        }

        /// <summary>
        /// Hält einen Effekt an
        /// </summary>
        /// <param name="effect"></param>
        public void Pause(EffectBase effect)
        {
            // Bei einer Pause wird nur ein Flag gesetzt
            effect.Status = EffectStatusEnum.Pause;
        }

        /// <summary>
        /// Hält eine Gruppe von Effekten an
        /// </summary>
        /// <param name="effects"></param>
        public void Pause(IEnumerable<EffectBase> effects)
        {
            foreach (var item in effects) Pause(item);
        }

        /// <summary>
        /// Setzt einen Effekt zurück
        /// </summary>
        /// <param name="effect"></param>
        public void Reset(EffectBase effect)
        {
            effect.InternalReset();
        }

        /// <summary>
        /// Setzt eine Gruppe von Effekten zurück
        /// </summary>
        /// <param name="effects"></param>
        public void Reset(IEnumerable<EffectBase> effects)
        {
            foreach (var item in effects) item.InternalReset();
        }
    }
}
