﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Interactions.Visuals.Animations
{
    /// <summary>
    /// Ein Delegate das aufgerufen wird, wenn sich der Status eines Effekts ändert
    /// </summary>
    /// <param name="effect"></param>
    /// <param name="status"></param>
    public delegate void EffectStatusChangedDelegate(EffectBase effect, EffectStatusEnum status);

    /// <summary>
    /// Stellt die Basisklasse für ein Effekt dar
    /// </summary>
    public abstract class EffectBase
    {
        private EffectStatusEnum _status = EffectStatusEnum.Stopped;

        /// <summary>
        /// Wird aufgerufen wenn der Effektstatus geändert wird
        /// </summary>
        public event EffectStatusChangedDelegate EffectStatusChanged;
        private bool _iscomplet;

        /// <summary>
        /// Erstellt eine neue Instanz der Effect-Klasse
        /// </summary>
        protected EffectBase(float time)
        {
            Duration = time;
        }

        internal void InternalUpdate(float ms, float s)
        {
            CurrentTime += ms;

            // Hier prüfen ob die IstZeit die SollZeit erreicht hat
            if (CurrentTime >= Duration)
            {
                // Flag setzten das der Effekt beendet wurde
                SetComplet();

                // Das Finish soll sicherstellen, das der Effekt den Zielwert erreicht, und nicht durch ein weiteres Update und dessen Berechnung einen Falschen Wert an nimmt.
                // Das verhindert das ggf. Werte außerhalb des index oder negative grafische effekte auftreten.
                OnFinish();
            }
            else
            {
                Update(ms, s);
            }
        }

        internal void InternalReset()
        {
            CurrentTime = 0F;
            IsComplet = false;
        }

        /// <summary>
        /// Gibt an ob der Effekt beendet wurde
        /// </summary>
        public bool IsComplet
        {
            get { return _iscomplet; }
            private set
            {
                _iscomplet = value;

                if (value) _status = EffectStatusEnum.Stopped;
            }
        }

        /// <summary>
        /// Liefert den Status der Animation oder legt ihn fest
        /// </summary>
        public EffectStatusEnum Status
        {
            get { return _status; }
            set
            {
                // Ein Änderung des Effekts nur dann auslösen, wenn auch eine Änderung stattfindet.
                if (_status != value) OnEffectStatusChanged(this, value);

                _status = value;

                switch (_status)
                {
                    case EffectStatusEnum.Stopped:
                        CurrentTime = 0F;
                        OnStop();
                        break;

                    case EffectStatusEnum.Running:
                        OnStart();
                        IsComplet = false;
                        break;

                    case EffectStatusEnum.Pause:
                        OnPause();
                        break;
                }
            }
        }

        /// <summary>
        /// Liefert einen Gruppennamen oder legt ihn fest
        /// </summary>
        public string Group { get; set; }

        /// <summary>
        /// Liefert die Istzeit des Effekts
        /// </summary>
        public float CurrentTime { get; private set; }

        /// <summary>
        /// Liefert die Sollzeit des Effekts
        /// </summary>
        public float Duration { get; internal set; }

        private void OnEffectStatusChanged(EffectBase effect, EffectStatusEnum value)
        {
            if (EffectStatusChanged != null) EffectStatusChanged.Invoke(effect, value);
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt startet
        /// </summary>
        protected virtual void OnStart()
        {
            IsComplet = false;
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt angehalten wird
        /// </summary>
        protected virtual void OnPause()
        {
            // Nichts machen
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt gestoppt wird
        /// </summary>
        protected virtual void OnStop()
        {
            IsComplet = false;
        }

        /// <summary>
        /// Wird aufgerufen, wenn der Effekt die Sollzeit erreicht hat
        /// </summary>
        protected virtual void OnFinish()
        { }

        /// <summary>
        /// Gibt an das die Animation beendet ist
        /// </summary>
        protected virtual void SetComplet()
        {
            IsComplet = true;
        }

        /// <summary>
        /// Wird aufgerufen wenn der Effekt neu berechnet wird
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected abstract void Update(float ms, float s);

        /// <summary>
        /// Wird aufgerufen wenn der Effekt zurückgesetzt werden muss
        /// </summary>
        protected abstract void Reset();
    }

    /// <summary>
    /// Ein Delegate das aufgerufen wird, wenn sich der Status eines Effekts ändert
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class EffectBase<T> : EffectBase
    {
        /// <summary>
        /// Erstellt eine neue Instanz der Effect-Klasse
        /// </summary>
        /// <param name="targetvalue"></param>
        /// <param name="time"></param>
        protected EffectBase(T targetvalue, float time)
            : base(time)
        {
            TargetValue = targetvalue;
        }

        /// <summary>
        /// Liefert den Zielwert
        /// </summary>
        public T TargetValue { get; internal set; }
    }
}
