﻿using Pandora.Interactions.Visuals.Animations;
using Pandora.Interactions.Visuals.Controls;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Styles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Interactions.Visuals
{
    /// <summary>
    /// 
    /// </summary>
    public interface IVisualHandler
    {
        /// <summary>
        /// Liefert die Bildschirmgröße
        /// </summary>
        Vector2u Size { get; }

        /// <summary>
        /// Liefert den Manager für Interaktionen
        /// </summary>
        InteractionManager Manager { get; }

        /// <summary>
        /// Liefert den Handler für Animationen
        /// </summary>
        AnimationHandler Animation { get; }

        /// <summary>
        /// Zeigt eine Szene an
        /// </summary>
        /// <param name="scene"></param>
        void Show(Scene scene);

        /// <summary>
        /// Schließt die Szene
        /// </summary>
        /// <param name="scene"></param>
        void Close(Scene scene);

        /// <summary>
        /// Liefert den verwendeten Style oder legt ihn fest
        /// </summary>
        StyleSet StyleSet { get; set; }
    }
}
