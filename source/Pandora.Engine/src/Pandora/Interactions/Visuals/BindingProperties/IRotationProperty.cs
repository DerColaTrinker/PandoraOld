﻿using Pandora.Interactions.Visuals.Controls;
using System;

namespace Pandora.Interactions.Visuals.BindingProperties
{
    /// <summary>
    /// Schnittstelle die dem Animationssystem Zugang zu den Rotations Eingenschaften erlaubt
    /// </summary>
    public interface IRotationProperty : IControl
    {
        /// <summary>
        /// Liefert die Rotation oder legt sie fest
        /// </summary>
        float Rotation { get; set; }
    }
}
