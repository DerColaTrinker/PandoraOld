﻿using Pandora.Interactions.Visuals.Drawing;
using System;

namespace Pandora.Interactions.Visuals.BindingProperties
{
    /// <summary>
    /// Schnittstelle die dem Animationssystem Zugang zu den Größen Eingenschaften erlaubt
    /// </summary>
    public interface ISizeProperty
    {
        /// <summary>
        /// Liefert die Größe oder legt sie fest
        /// </summary>
        Vector2f Size { get; set; }
    }
}
