﻿using Pandora.Interactions.Visuals.Controls;
using System;

namespace Pandora.Interactions.Visuals.BindingProperties
{
    /// <summary>
    /// Eine Schnittstelle eine Eigenschaft zur größe des Rahmens zur Verfügung stellt
    /// </summary>
    public interface IOutlineThicknessProperty : IControl
    {
        /// <summary>
        /// Liefert die Stiftdicke des Rahmens oder legt sie fest
        /// </summary>
        float OutlineThickness { get; set; }
    }
}
