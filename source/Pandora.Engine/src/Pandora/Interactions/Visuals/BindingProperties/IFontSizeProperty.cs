﻿using Pandora.Interactions.Visuals.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Interactions.Visuals.BindingProperties
{
    /// <summary>
    /// Eine Schnittstelle die das Anpassen von Schriftgröße erlaubt
    /// </summary>
    public interface IFontSizeProperty : IControl
    {
        /// <summary>
        /// Liefert die Schriftgröße oder legt sie fest
        /// </summary>
        uint FontSize { get; set; }
    }
}
