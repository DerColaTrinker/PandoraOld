﻿using Pandora.Interactions.Visuals.Controls;
using Pandora.Interactions.Visuals.Drawing2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Interactions.Visuals.BindingProperties
{
    /// <summary>
    /// Eine Schnittstelle die das Anpassen von Schriftarten erlaubt
    /// </summary>
    public interface IFontProperty : IControl
    {
        /// <summary>
        /// Liefert die Verwendete Schriftart oder legt diese fest
        /// </summary>
        Font Font { get; set; }
    }
}
