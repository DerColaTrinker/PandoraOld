﻿using Pandora.Interactions.Visuals.Controls;
using Pandora.Interactions.Visuals.Drawing;
using System;

namespace Pandora.Interactions.Visuals.BindingProperties
{
    /// <summary>
    /// Schnittstelle die dem Animationssystem Zugang zu den Linienfarb Eingenschaften erlaubt
    /// </summary>
    public interface IOutlineColorProperty : IControl
    {
        /// <summary>
        /// Liefert die Linienfarbe oder legt sie fest
        /// </summary>
        Color OutlineColor { get; set; }
    }
}
