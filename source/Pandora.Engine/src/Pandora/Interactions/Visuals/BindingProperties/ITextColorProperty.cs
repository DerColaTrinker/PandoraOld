﻿using Pandora.Interactions.Visuals.Controls;
using Pandora.Interactions.Visuals.Drawing;
using System;

namespace Pandora.Interactions.Visuals.BindingProperties
{
    /// <summary>
    /// Schnittstelle die dem Animationssystem Zugang zu den Farb Eingenschaften erlaubt
    /// </summary>
    public interface ITextColorProperty : IControl
    {
        /// <summary>
        /// Liefert die Farbe oder legt sie fest
        /// </summary>
        Color TextColor { get; set; }
    }
}
