﻿using Pandora.Interactions.Visuals.Controls;
using Pandora.Interactions.Visuals.Drawing;
using System;

namespace Pandora.Interactions.Visuals.BindingProperties
{
    /// <summary>
    /// Schnittstelle die dem Animationssystem Zugang zu den Skalierungs Eingenschaften erlaubt
    /// </summary>
    public interface IScaleProperty : IControl
    {
        /// <summary>
        /// Liefert die Skalierung oder legt sie fest
        /// </summary>
        Vector2f Scale { get; set; }
    }
}
