﻿using Pandora.Interactions.Visuals.Controls;
using Pandora.Interactions.Visuals.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Interactions.Visuals.BindingProperties
{
    /// <summary>
    /// Eine Schnittstelle die das Anpassen der Textausrichtung ermöglicht
    /// </summary>
    public interface ITextAlignmentProperty : IControl
    {
        /// <summary>
        /// Liefert die Textausrichtung oder legt sie fest
        /// </summary>
        TextAlignment TextAlignment { get; set; }
    }
}
