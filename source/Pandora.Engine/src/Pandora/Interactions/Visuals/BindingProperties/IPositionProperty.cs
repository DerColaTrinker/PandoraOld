﻿using Pandora.Interactions.Visuals.Drawing;
using System;
using Pandora.Interactions.Visuals.Controls;

namespace Pandora.Interactions.Visuals.BindingProperties
{
    /// <summary>
    /// Schnittstelle die dem Animationssystem Zugang zu den Positions Eingenschaften erlaubt
    /// </summary>
    public interface IPositionProperty : IControl
    {
        /// <summary>
        /// Liefert die Position oder legt sie fest
        /// </summary>
        Vector2f Position { get; set; }
    }
}
