﻿using Pandora.Interactions.Visuals.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Interactions.Visuals.BindingProperties
{
    /// <summary>
    /// Eine Schnittstelle die das Anpassen von Schriftarten erlaubt
    /// </summary>
    public interface IFontResourceCode : IControl
    {
        /// <summary>
        /// Liefert den ResourcenCode der Schriftart oder legt sie fest
        /// </summary>
        string FontResourceCode { get; set; }
    }
}
