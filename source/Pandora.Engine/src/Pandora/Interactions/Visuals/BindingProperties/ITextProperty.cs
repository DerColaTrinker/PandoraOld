﻿using Pandora.Interactions.Visuals.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Interactions.Visuals.BindingProperties
{
    /// <summary>
    /// Schnittstelle die ein Text implementiert
    /// </summary>
    public interface ITextProperty : IControl
    {
        /// <summary>
        /// Liefert einen Anzeigetext oder legt ihn fest
        /// </summary>
        string Text { get; set; }
    }
}
