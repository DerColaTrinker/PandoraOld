﻿using Pandora.Interactions.Visuals.Controls;
using Pandora.Interactions.Visuals.Drawing;
using System;

namespace Pandora.Interactions.Visuals.BindingProperties
{
    /// <summary>
    /// Schnittstelle die dem Animationssystem Zugang zu den Bezugspunkt Eingenschaften erlaubt
    /// </summary>
    public interface IOriginProperty : IControl
    {
        /// <summary>
        /// Liefert den Bezugspunkt oder legt ihn fest
        /// </summary>
        Vector2f Origin { get; set; }
    }
}
