﻿using Pandora.Interactions.Visuals.Controls;
using Pandora.Interactions.Visuals.Drawing;
using System;

namespace Pandora.Interactions.Visuals.BindingProperties
{
    /// <summary>
    /// Schnittstelle die dem Animationssystem Zugang zu den Füllfarbe Eingenschaften erlaubt
    /// </summary>
    public interface IFillColorProperty : IControl
    {
        /// <summary>
        /// Liefert die Füllfarbe oder legt sie fest
        /// </summary>
        Color FillColor { get; set; }
    }
}
