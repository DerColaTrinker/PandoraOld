﻿using Pandora.Interactions.Visuals.Controls;
using Pandora.Interactions.Visuals.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#pragma warning disable CS1591

namespace Pandora.Interactions.Visuals.BindingProperties
{
    public interface ITextStyleProperty : IControl
    {
        TextStyle TextStyle { get; set; }
    }
}
