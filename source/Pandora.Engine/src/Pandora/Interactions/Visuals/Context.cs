using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Runtime.ConstrainedExecution;
using Pandora.Interactions.Visuals.Renderer;

namespace Pandora.Interactions.Visuals
{
    internal class Context : CriticalFinalizerObject
    {
        public Context()
        {
            _pointer = UnsafeNativeMethods.sfContext_create();
        }

        ~Context()
        {
            UnsafeNativeMethods.sfContext_destroy(_pointer);
        }

        public bool SetActive(bool active)
        {
            return UnsafeNativeMethods.sfContext_setActive(_pointer, active);
        }

        public ContextSettings Settings
        {
            get { return UnsafeNativeMethods.sfContext_getSettings(_pointer); }
        }

        public static Context Global
        {
            get
            {
                if (ourGlobalContext == null)
                    ourGlobalContext = new Context();

                return ourGlobalContext;
            }
        }

        public override string ToString()
        {
            return "[Context]";
        }

        private static Context ourGlobalContext = null;

        private IntPtr _pointer = IntPtr.Zero;
    }
}
