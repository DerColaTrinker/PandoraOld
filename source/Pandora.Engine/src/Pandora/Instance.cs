﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora
{
    /// <summary>
    /// Stellt eine Instanz der Runtime dar
    /// </summary>
    public abstract class Instance
    {
        internal virtual void InternalSystemUpdate(float ms, float s)
        {
            OnSystemUpdate(ms, s);
        }

        internal virtual void InternalInitialize(PandoraRuntime runtime)
        {
            Runtime = runtime;

            OnInitialize();
        }

        internal virtual void InternalStart()
        {
            OnStart();
        }

        internal virtual void InternalStop()
        {
            OnStop();
        }

        /// <summary>
        /// Wird aufgerufen wenn die Instanz initialisiert wird
        /// </summary>
        protected virtual void OnInitialize()
        { }

        /// <summary>
        /// Wird aufgerufen wenn die Instanz gestartet wird
        /// </summary>
        protected virtual void OnStart()
        { }

        /// <summary>
        /// Wird aufgerufen wenn die Instanz angehalten wird
        /// </summary>
        protected virtual void OnStop()
        { }

        /// <summary>
        /// Wird aufgerufen, wenn ein Frame neugestartet wird
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected virtual void OnSystemUpdate(float ms, float s)
        { }

        /// <summary>
        /// Liefert die Laufzeit
        /// </summary>
        public PandoraRuntime Runtime { get; private set; }

        /// <summary>
        /// Gibt an ob die Spielinstanz initialisiert werden kann ( Standard : wahr )
        /// </summary>
        public virtual bool Initializable { get { return true; } }
    }
}
