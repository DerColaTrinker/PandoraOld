﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora
{
    /// <summary>
    /// Eine Ausnahme die in der Engine auftritt
    /// </summary>
    [Serializable]
    public class PandoraException : Exception
    {
        /// <summary>
        /// Erstellt eine neue Instanz der PandoraRuntimeException-Klasse
        /// </summary>
        public PandoraException()
        { }

        /// <summary>
        /// Erstellt eine neue Instanz der PandoraRuntimeException-Klasse
        /// </summary>
        /// <param name="message"></param>
        public PandoraException(string message)
            : base(message)
        { }

        /// <summary>
        /// Erstellt eine neue Instanz der PandoraRuntimeException-Klasse
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public PandoraException(string message, Exception inner)
            : base(message, inner)
        { }

        /// <summary>
        /// Erstellt eine neue Instanz der PandoraRuntimeException-Klasse
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected PandoraException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        { }
    }
}
