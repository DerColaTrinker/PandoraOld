﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game
{
    /// <summary>
    /// Engine Spiel-Arten
    /// </summary>
    public enum GameModes
    {
        /// <summary>
        /// Der Spielmodus ist nicht festgelegt
        /// </summary>
        Unkown,

        /// <summary>
        /// Einzelspieler
        /// </summary>
        Singleplayer,

        /// <summary>
        /// Mehrspieler Server
        /// </summary>
        MultiplayerServer,

        /// <summary>
        /// Mehrspieler Client
        /// </summary>
        MultiplayerClient
    }
}
