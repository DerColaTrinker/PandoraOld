﻿namespace Pandora.Game
{
    /// <summary>
    /// Gibt an in welchem Status sich das Spielsystem befindet
    /// </summary>
    public enum GameStatus
    {
        /// <summary>
        /// Nicht angegeben
        /// </summary>
        Unkown,

        /// <summary>
        /// Lobby Modus. Hinzufügen von Spielern
        /// </summary>
        Lobby
    }
}