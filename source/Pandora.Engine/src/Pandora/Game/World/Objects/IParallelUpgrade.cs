﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    /// <summary>
    /// Eine Schnittstelle die bei Gebäude und Forschung verwendet werden kann, wenn mehr als ein Paralleler Ausbau stattfiden kann
    /// </summary>
    public interface IParallelUpgrade
    {
        /// <summary>
        /// Liefert die Anzahl der parallel möglichen Ausbauten
        /// </summary>
        int MaxParallelBuildings { get; }
    }
}
