﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pandora.Game.Templates;

namespace Pandora.Game.World.Objects
{
    /// <summary>
    /// Eine Schnittstelle die ein Objekt beschreibt das über Ausbaustufen verfügt
    /// </summary>
    /// <typeparam name="TTemplate"></typeparam>
    /// <typeparam name="TUpgradeTemplate"></typeparam>
    public interface IUpgradableObject<TTemplate, TUpgradeTemplate>
        where TTemplate : UpgradableTemplateBase<TUpgradeTemplate>
        where TUpgradeTemplate : UpgradeTemplateBase<TTemplate>
    {
        /// <summary>
        /// Liefert eine Auflistung der Ausbaustufen
        /// </summary>
        IEnumerable<TUpgradeTemplate> UpgradeTemplates { get; }
    }
}
