﻿using System;

namespace Pandora.Game.World.Objects
{
    /// <summary>
    /// Stellt eine Schnittstelle bereit das Templatesystem implementieren
    /// </summary>
    /// <typeparam name="TTemplate"></typeparam>
    public interface ITemplate<TTemplate>
        where TTemplate : Pandora.Game.Templates.TemplateBase
    {
        /// <summary>
        /// Liefert das Template
        /// </summary>
        TTemplate Template { get; }
    }
}
