﻿using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Handlers
{
    /// <summary>
    /// Delegate das für Ereignisse im RaceHandler verwendet wird
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    /// <param name="race"></param>
    public delegate void RaceDelegate<TRace>(TRace race);

    /// <summary>
    /// Stellt eine Verwaltung für Rassen bereit
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    public class RaceHandler<TRace> : WorldHandlerBase<TRace>
        where TRace : RaceBase
    {
        private HashSet<TRace> _races = new HashSet<TRace>();

        /// <summary>
        /// Wird aufgerufen, wenn eine Rasse hinzugefügt wird
        /// </summary>
        public event RaceDelegate<TRace> RaceAdded;

        /// <summary>
        /// Wird aufgerufen, wenn eine Rasse entfernt wird
        /// </summary>
        public event RaceDelegate<TRace> RaceRemoved;

        /// <summary>
        /// Fügt eine Rasse hinzu
        /// </summary>
        /// <param name="race"></param>
        internal void Add(TRace race)
        {
            if (_races.Add(race))
            {
                Accessor.Add(race);
                OnRaceAdd(race);
            }
        }

        /// <summary>
        /// Entfernt eine Rasse
        /// </summary>
        /// <param name="race"></param>
        internal void Remove(TRace race)
        {
            if (_races.Remove(race))
            {
                Accessor.Remove(race);
                OnRemoveRace(race);
            }
        }

        /// <summary>
        /// Liefert eine Auflistung aller Rassen
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TRace> GetRaces()
        {
            return _races.AsEnumerable();
        }

        /// <summary>
        /// Löst das AddRace-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        protected virtual void OnRaceAdd(TRace race)
        {
            if (RaceAdded != null) RaceAdded.Invoke(race);
        }

        /// <summary>
        /// Löst das RemoveRace-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        protected virtual void OnRemoveRace(TRace race)
        {
            if (RaceRemoved != null) RaceRemoved.Invoke(race);
        }

#pragma warning disable 1591
        public override void Serialize(IWriter writer)
        {
            base.Serialize(writer);

            writer.WriteInt32(_races.Count);
            foreach (var race in _races)
            {
                race.Serialize(writer);
            }
        }

        public override void Deserialize(IReader reader)
        {
            base.Deserialize(reader);

            var racecount = reader.ReadInt32();
            for (int i = 0 ; i < racecount ; i++)
            {
                var race = GameObjectBase.CreateObject<TRace>(Instance, reader);

                _races.Add(race);
            }
        }
#pragma warning restore 1591
    }
}
