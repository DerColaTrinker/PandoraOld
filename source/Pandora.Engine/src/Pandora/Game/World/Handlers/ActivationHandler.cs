﻿using Pandora.Game.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pandora.Runtime.Collections;

namespace Pandora.Game.World.Handlers
{
    /// <summary>
    /// Ein Delegate das im ActivationHandler verwendet wird.
    /// </summary>
    /// <typeparam name="TRace">Der Type der Spielerrasse</typeparam>
    /// <param name="race">Die Rasse die das Ereignis ausgelöst hat</param>
    /// <param name="template">Das Template dessen Bezeichner gespeichert wurde</param>
    public delegate void ActivationDelegate<TRace>(TRace race, TemplateBase template);

    /// <summary>
    /// Eine Verwaltung die verfügbare Template Identifier speichert die als Verfügbar gelten.
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    public class ActivationHandler<TRace> : WorldHandlerBase<TRace>
        where TRace : RaceBase
    {
        private OwnerHashSet<TRace, string> _collections = new OwnerHashSet<TRace, string>();

        /// <summary>
        /// Wird aufgerufen wenn ein neuer Bezeichner hinzugefügt wurde
        /// </summary>
        public event ActivationDelegate<TRace> IdentifierAdded;

        /// <summary>
        /// Wird aufgerufen wenn ein Bezeichner entfernt wurde
        /// </summary>
        public event ActivationDelegate<TRace> IdentifierRemoved;

        /// <summary>
        /// Wird aufgerufen wenn eine neue Rasse hinzugefügt wird
        /// </summary>
        public event RaceDelegate<TRace> RaceAdded;

        /// <summary>
        /// Wird aufgerufen wenn eine Rasse entfernt wird
        /// </summary>
        public event RaceDelegate<TRace> RaceRemoved;

        /// <summary>
        /// Wird aus dem System aufgerufen
        /// </summary>
        /// 
        protected internal override void OnInitialize()
        {
            Races.RaceAdded += delegate(TRace race) { _collections.AddOwner(race); ;OnAddRace(race); };
            Races.RaceRemoved += delegate(TRace race) { _collections.RemoveOwner(race); ;OnRemoveRace(race); };
        }

        /// <summary>
        /// Fügt ein Template der Liste hinzu
        /// </summary>
        /// <param name="race"></param>
        /// <param name="template"></param>
        public void Add(TRace race, TemplateBase template)
        {
            _collections.AddElement(race, template.Identifier);
            OnAddIdentifier(race, template);
        }

        /// <summary>
        /// Entfernt ein Template aus der Liste
        /// </summary>
        /// <param name="race"></param>
        /// <param name="template"></param>
        public void Remove(TRace race, TemplateBase template)
        {
            _collections.RemoveElement(race, template.Identifier);
            OnRemoveIdentifier(race, template);
        }

        /// <summary>
        /// Liefert eine Auflistung aller Templates die durch die Rasse aktiviert sind
        /// </summary>
        /// <typeparam name="TTemplate"></typeparam>
        /// <param name="race"></param>
        /// <returns></returns>
        public IEnumerable<TTemplate> GetAvailableTemplates<TTemplate>(TRace race)
            where TTemplate : TemplateBase
        {
            var templates = Instance.Templates.GetTemplates<TTemplate>();

            foreach (var template in templates)
            {
                if (CheckAvailableTemplate(race, template)) yield return template;
            }
        }

        /// <summary>
        /// Liefert eine Auflistung aller Aktivierungen
        /// </summary>
        /// <param name="race"></param>
        /// <returns></returns>
        public IEnumerable<string> GetActivations(TRace race)
        {
            return _collections.GetElements(race);
        }

        /// <summary>
        /// Gibt an ob für das Template und die Rasse die Bedingungen erfüllt sind
        /// </summary>
        /// <param name="race"></param>
        /// <param name="template"></param>
        /// <returns></returns>
        public bool CheckAvailableTemplate(TRace race, TemplateBase template)
        {
            var elements = _collections.GetElementCollection(race);

            return (!string.IsNullOrEmpty(template.Required1) && elements.Elements.Contains(template.Required1))
                 & (!string.IsNullOrEmpty(template.Required2) && elements.Elements.Contains(template.Required2))
                 & (!string.IsNullOrEmpty(template.Required3) && elements.Elements.Contains(template.Required3))
                 & (!string.IsNullOrEmpty(template.Required4) && elements.Elements.Contains(template.Required4));
        }

        #region Protected Methods

        /// <summary>
        /// Löst das AddIdentifier-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        /// <param name="template"></param>
        protected virtual void OnAddIdentifier(TRace race, TemplateBase template)
        {
            if (IdentifierAdded != null) IdentifierAdded.Invoke(race, template);
        }

        /// <summary>
        /// Löst das RemoveIdentifier-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        /// <param name="template"></param>
        protected virtual void OnRemoveIdentifier(TRace race, TemplateBase template)
        {
            if (IdentifierRemoved != null) IdentifierRemoved.Invoke(race, template);
        }

        /// <summary>
        /// Löst das RemoveRace-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        protected virtual void OnRemoveRace(TRace race)
        {
            if (RaceRemoved != null) RaceRemoved.Invoke(race);
        }

        /// <summary>
        /// Löst das AddRace-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        protected virtual void OnAddRace(TRace race)
        {
            if (RaceAdded != null) RaceAdded.Invoke(race);
        }

        #endregion

#pragma warning disable 1591
        public override void Serialize(Runtime.IO.IWriter writer)
        {
            base.Serialize(writer);

            writer.WriteInt32(_collections.OwnerCount);

            foreach (var race in _collections.GetOwners())
            {
                writer.WriteUInt32(race.ObjectID);

                var elements = _collections.GetElements(race);
                writer.WriteInt32(elements.Count());

                foreach (var element in elements)
                {
                    writer.WriteString(element);
                }
            }
        }

        public override void Deserialize(Runtime.IO.IReader reader)
        {
            base.Deserialize(reader);

            var racecount = reader.ReadInt32();

            for (int raceindex = 0 ; raceindex < racecount ; raceindex++)
            {
                var raceid = reader.ReadUInt32();
                var race = Accessor.GetObject<TRace>(raceid);

                _collections.AddOwner(race);

                var elementcount = reader.ReadInt32();
                for (int elementindex = 0 ; elementindex < elementcount ; elementindex++)
                {
                    _collections.AddElement(race, reader.ReadString());
                }
            }
        }
#pragma warning restore 1591
    }
}
