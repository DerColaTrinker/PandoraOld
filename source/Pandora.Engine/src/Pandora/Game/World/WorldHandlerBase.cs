﻿using Pandora.Game.Globalisation;
using Pandora.Game.Players;
using Pandora.Game.Templates;
using Pandora.Game.World.Handlers.Map;
using Pandora.Game.World.Handlers;
using Pandora.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World
{
    /// <summary>
    /// Basisklasse eines Spielwelt Handler
    /// </summary>
    public abstract class WorldHandlerBase : GameHandlerBase
    {
        /// <summary>
        /// Liefert die Spielwelt, über die der Handler angemeldet wurde
        /// </summary>
        public WorldModificationBase World { get; internal set; }

        /// <summary>
        /// Liefert die Spielkarte
        /// </summary>
        public MapHandler Map { get { return World.Map; } }
    }

    /// <summary>
    /// Basisklasse eines Spielwelt Handler, der auch Zugriff auf die Rassen besitzt
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    public abstract class WorldHandlerBase<TRace> : WorldHandlerBase
        where TRace : RaceBase
    {
        #region Direktzugriff auf die die Welt

        /// <summary>
        /// Liefert die Spielwelt, über die der Handler angemeldet wurde
        /// </summary>
        public new WorldModificationBase<TRace> World { get { return (WorldModificationBase<TRace>)(base.World); } internal set { base.World = value; } }

        /// <summary>
        /// Liefert den Handler für die Rassen
        /// </summary>
        public RaceHandler<TRace> Races { get { return World.Races; } }

        /// <summary>
        /// Liefert den Handler der Spieleraktivierungen
        /// </summary>
        public ActivationHandler<TRace> Activations { get { return World.Activations; } }

        #endregion
    }
}
