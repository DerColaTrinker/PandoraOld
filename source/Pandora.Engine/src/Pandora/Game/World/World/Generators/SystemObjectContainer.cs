﻿using Pandora.Game.World.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.World.Generators
{
    /// <summary>
    /// Stellt einen Kontainer dar der ein System Objekt während der Erstellung enthält
    /// </summary>
    public abstract class SystemObjectContainer
    {
        internal SystemObjectContainer(SystemMapObjectBase instance)
        {
            Instance = instance;
        }

        /// <summary>
        /// Liefert das System Objekt
        /// </summary>
        public SystemMapObjectBase Instance { get; private set; }

        /// <summary>
        /// Liefert die X-Position des Objekts oder legt sie fest
        /// </summary>
        public float PositionX { get; set; }

        /// <summary>
        /// Liefert die Y-Position des Objekts oder legt sie fest
        /// </summary>
        public float PositionY { get; set; }

        /// <summary>
        /// Liefert den Namen des Objekts oder legt ihn fest
        /// </summary>
        public string Name { get; set; }

        internal void Confirm()
        {
            Instance.PositionX = PositionX;
            Instance.PositionY = PositionY;
            Instance.Instance.Accessor.SetName(Instance, Name);
        }
    }

    /// <summary>
    /// Stellt einen Kontainer dar der ein System Objekt während der Erstellung enthält
    /// </summary>
    /// <typeparam name="TSystemObject"></typeparam>
    public sealed class SystemObjectContainer<TSystemObject> : SystemObjectContainer
        where TSystemObject : SystemMapObjectBase
    {
        internal SystemObjectContainer(TSystemObject instance)
            : base(instance)
        { }

        /// <summary>
        /// Liefert das System Objekt
        /// </summary>
        public new TSystemObject Instance { get { return (TSystemObject)base.Instance; } }
    }
}
