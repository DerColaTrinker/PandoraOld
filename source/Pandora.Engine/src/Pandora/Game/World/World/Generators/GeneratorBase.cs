﻿using Pandora.Game.Templates;
using Pandora.Game.World.Objects;
using Pandora.Runtime.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.World.Handlers;

namespace Pandora.Game.World.Generators
{
    /// <summary>
    /// Ein Delegate das vom Spielfeldgenerator verwendet wird
    /// </summary>
    /// <typeparam name="TTemplate"></typeparam>
    /// <typeparam name="TObject"></typeparam>
    /// <param name="index"></param>
    /// <param name="templates"></param>
    /// <param name="ratio"></param>
    /// <returns></returns>
    public delegate bool CreateGalaxyMapObjectBaseDelegate<TTemplate, TObject>(int index, IEnumerable<TTemplate> templates, RatioCounter<TTemplate> ratio)
        where TTemplate : TemplateBase
        where TObject : GalaxyMapObjectBase;

    /// <summary>
    /// Ein Delegate das vom Spielfeldgenerator verwendet wird
    /// </summary>
    /// <typeparam name="TObject"></typeparam>
    /// <param name="index"></param>
    /// <param name="ratio"></param>
    /// <returns></returns>
    public delegate bool CreateGalaxyMapObjectBaseDelegate<TObject>(int index, RatioCounter ratio)
        where TObject : GalaxyMapObjectBase;

    /// <summary>
    /// Ein Delegate das vom Spielfeldgenerator verwendet wird
    /// </summary>
    /// <typeparam name="TTemplate"></typeparam>
    /// <typeparam name="TGalaxyObject"></typeparam>
    /// <typeparam name="TSystemObject"></typeparam>
    /// <param name="index"></param>
    /// <param name="galaxyobject"></param>
    /// <param name="templates"></param>
    /// <param name="ratio"></param>
    /// <returns></returns>
    public delegate bool CreateSystemMapObjectBaseDelegate<TTemplate, TGalaxyObject, TSystemObject>(int index, GalaxyObjectContainer<TGalaxyObject> galaxyobject, IEnumerable<TTemplate> templates, RatioCounter<TTemplate> ratio)
        where TTemplate : TemplateBase
        where TGalaxyObject : GalaxyMapObjectBase
        where TSystemObject : SystemMapObjectBase, ITemplate<TTemplate>;

    /// <summary>
    /// Ein Delegate das vom Spielfeldgenerator verwendet wird
    /// </summary>
    /// <typeparam name="TGalaxyObject"></typeparam>
    /// <typeparam name="TSystemObject"></typeparam>
    /// <param name="index"></param>
    /// <param name="GalaxyMapObjectBase"></param>
    /// <returns></returns>
    public delegate bool CreateSystemMapObjectBaseDelegate<TGalaxyObject, TSystemObject>(int index, GalaxyObjectContainer<TGalaxyObject> GalaxyMapObjectBase)
        where TGalaxyObject : GalaxyMapObjectBase
        where TSystemObject : SystemMapObjectBase;

    /// <summary>
    /// Basisklasse des Spielfeld Generators
    /// </summary>
    public abstract class GeneratorBase
    {
        private GameControllerBase _instance;
        private HashSet<GalaxyObjectContainer> _galaxyobjects = new HashSet<GalaxyObjectContainer>();

        internal bool InternalCreate(GameControllerBase instance, int mapseed)
        {
            _instance = instance;
            Templates = _instance.Templates;

            GalaxyMapSizeX = _instance.GameConfiguration.GetValue("GalaxyMapWidth", 500D);
            GalaxyMapSizeY = _instance.GameConfiguration.GetValue("GalaxyMapHeight", 500D);
            SystemMapSizeX = _instance.GameConfiguration.GetValue("SystemMapWidth", 500D);
            SystemMapSizeY = _instance.GameConfiguration.GetValue("SystemMapHeight", 500D);

            StarDistance = _instance.GameConfiguration.GetValue("StarDistance", 15D);


            // Seed damit ggf. die gleiche Map erstellt werden kann
            RND = new Random(mapseed);

            if (!OnCreateWorld()) return false;

            return true;
        }

        /// <summary>
        /// Wird aufgerufen wenn das System zur Spielwelterzeugung gestartet wird
        /// </summary>
        /// <returns></returns>
        protected abstract bool OnCreateWorld();

        /// <summary>
        /// Beginnt mit der Erstellung der Galaxie Objekten
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <param name="count"></param>
        /// <param name="ratiocount"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        protected bool CreateGalaxyObjects<TObject>(int count, int ratiocount, CreateGalaxyMapObjectBaseDelegate<TObject> target)
            where TObject : GalaxyMapObjectBase
        {
            var ratio = new RatioCounter(ratiocount);

            IsProcessStopped = false;

            Logger.BeginProgress(count, "Creating {0}", typeof(TObject).Name);

            for (int index = 0 ; index < count & !IsProcessStopped ; index++)
            {
                Logger.UpdateProgress(index + 1);

                if (!target.Invoke(index, ratio))
                    return false;
            }

            Logger.EndProgress(IsProcessStopped);

            return true;
        }

        /// <summary>
        /// Beginnt mit der Erstellung der Galaxie Objekten mit Template
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <typeparam name="TTemplate"></typeparam>
        /// <param name="count"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        protected bool CreateGalaxyObjects<TObject, TTemplate>(int count, CreateGalaxyMapObjectBaseDelegate<TTemplate, TObject> target)
            where TTemplate : TemplateBase
            where TObject : GalaxyMapObjectBase
        {
            var template = _instance.Templates.GetTemplates<TTemplate>();
            var ratio = new RatioCounter<TTemplate>(template);

            IsProcessStopped = false;

            Logger.BeginProgress(count, "Creating {0}", typeof(TObject).Name);

            for (int index = 0 ; index < count & !IsProcessStopped ; index++)
            {
                Logger.UpdateProgress(index + 1);

                if (!target.Invoke(index, template, ratio))
                    return false;
            }

            Logger.EndProgress(IsProcessStopped);

            return true;
        }

        /// <summary>
        /// Beginnt mit der Erstellung der System Objekten mit Temolates
        /// </summary>
        /// <typeparam name="TGalaxyObject"></typeparam>
        /// <typeparam name="TSystemObject"></typeparam>
        /// <typeparam name="TTemplate"></typeparam>
        /// <param name="target"></param>
        /// <returns></returns>
        protected bool CreateSystemObjects<TGalaxyObject, TSystemObject, TTemplate>(CreateSystemMapObjectBaseDelegate<TTemplate, TGalaxyObject, TSystemObject> target)
            where TTemplate : TemplateBase
            where TGalaxyObject : GalaxyMapObjectBase
            where TSystemObject : SystemMapObjectBase, ITemplate<TTemplate>
        {
            var template = _instance.Templates.GetTemplates<TTemplate>();
            var ratio = new RatioCounter<TTemplate>(template);
            var items = GetGalaxyObjects<TGalaxyObject>().ToArray();

            IsProcessStopped = false;

            Logger.BeginProgress(items.Length, "Creating {0}", typeof(TSystemObject).Name);

            for (int index = 0 ; index < items.Length & !IsProcessStopped ; index++)
            {
                Logger.UpdateProgress(index + 1);

                if (!target.Invoke(index, items[index], template, ratio))
                    return false;
            }

            Logger.EndProgress(IsProcessStopped);

            return true;
        }

        /// <summary>
        /// Beginnt mit der Erstellung der System Objekten
        /// </summary>
        /// <typeparam name="TGalaxyMapObjectBase"></typeparam>
        /// <typeparam name="TSystemMapObjectBase"></typeparam>
        /// <param name="target"></param>
        /// <returns></returns>
        protected bool CreateSystemObjects<TGalaxyMapObjectBase, TSystemMapObjectBase>(CreateSystemMapObjectBaseDelegate<TGalaxyMapObjectBase, TSystemMapObjectBase> target)
            where TGalaxyMapObjectBase : GalaxyMapObjectBase
            where TSystemMapObjectBase : SystemMapObjectBase
        {
            var items = GetGalaxyObjects<TGalaxyMapObjectBase>().ToArray();

            IsProcessStopped = false;

            Logger.BeginProgress(items.Length, "Creating {0}", typeof(TSystemMapObjectBase));

            for (int index = 0 ; index < items.Length & !IsProcessStopped ; index++)
            {
                Logger.UpdateProgress(index + 1);

                if (!target.Invoke(index, items[index]))
                    return false;
            }

            Logger.EndProgress(IsProcessStopped);

            return true;
        }

        /// <summary>
        /// Ginbt an ob sich ein Galaxie Objekt in der angegebebenen nähe befindet
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        protected bool HasObjectNeighborhood<TObject>(double x, double y, double distance)
            where TObject : GalaxyMapObjectBase
        {
            var x1 = x - distance;
            var x2 = x + distance;
            var y1 = y - distance;
            var y2 = y + distance;

            var result = from o in GetGalaxyObjects<TObject>()
                         where x >= x1 & x <= x2 & y >= y1 & y <= y2                // Sicherstellen das nur Objekte im Rahmenbereich berechnet werden
                         let b = Math.Pow(x - o.PositionX, 2)                       // Fläche b berechnen
                         let c = Math.Pow(y - o.PositionY, 2)                       // Fläche c berechnen
                         let a = Math.Sqrt(b + c)                                   // Länge a aus den addierten Flächen ( Pytagoras )
                         where a <= distance
                         select o;

            return result.Count() > 0;
        }

        /// <summary>
        /// Erstellt eine Galaxie Objekt Instanz
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <typeparam name="TTemplate"></typeparam>
        /// <param name="template"></param>
        /// <returns></returns>
        protected GalaxyObjectContainer<TObject> CreateGalaxyObjectInstance<TObject, TTemplate>(TTemplate template)
            where TTemplate : TemplateBase
            where TObject : GalaxyMapObjectBase<TTemplate>
        {
            var container = new GalaxyObjectContainer<TObject>();
            container.Instance = Accessor.CreateInstance<TObject>();
            Accessor.SetTemplate(container.Instance, template);

            return container;
        }

        /// <summary>
        /// Erstellt eine Galaxie Objekt Instanz
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <returns></returns>
        protected GalaxyObjectContainer<TObject> CreateGalaxyObjectContainer<TObject>()
            where TObject : GalaxyMapObjectBase
        {
            var container = new GalaxyObjectContainer<TObject>();
            container.Instance = Accessor.CreateInstance<TObject>();
            return container;
        }

        /// <summary>
        /// Erstellt ein Container mit Objekt Instanz
        /// </summary>
        /// <typeparam name="TContainer"></typeparam>
        /// <typeparam name="TObject"></typeparam>
        /// <returns></returns>
        protected TContainer CreateGalaxyObjectContainer<TContainer, TObject>()
            where TContainer : GalaxyObjectContainer<TObject>
            where TObject : GalaxyMapObjectBase
        {
            var container = Activator.CreateInstance<TContainer>();
            container.Instance = Accessor.CreateInstance<TObject>();
            return container;
        }

        /// <summary>
        /// Erstellt eine System Objekt Instanz
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <typeparam name="TTemplate"></typeparam>
        /// <param name="template"></param>
        /// <returns></returns>
        protected SystemObjectContainer<TObject> CreateSystemObjectInstance<TObject, TTemplate>(TTemplate template)
            where TTemplate : TemplateBase
            where TObject : SystemMapObjectBase, ITemplate<TTemplate>
        {
            var instance = Accessor.CreateInstance<TObject>();
            Accessor.SetTemplate(instance, template);

            return new SystemObjectContainer<TObject>(instance);
        }

        /// <summary>
        /// Erstellt eine System Objekt Instanz
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <returns></returns>
        protected SystemObjectContainer<TObject> CreateSystemObjectContainer<TObject>()
            where TObject : SystemMapObjectBase
        {
            var instance = Accessor.CreateInstance<TObject>();

            return new SystemObjectContainer<TObject>(instance);
        }

        /// <summary>
        /// Beendet den Prozess
        /// </summary>
        protected void StopProcess()
        {
            IsProcessStopped = true;
        }

        /// <summary>
        /// Liefert die angegebene Zahl in Römischen Ziffern
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        protected string RomanNumber(int number)
        {
            var stringBuilder = new StringBuilder();
            var array = new int[] { 1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000 };
            var array2 = new string[] { "I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M" };

            while (number > 0)
            {
                for (int i = array.Count<int>() - 1 ; i >= 0 ; i--)
                {
                    if (number / array[i] >= 1)
                    {
                        number -= array[i];
                        stringBuilder.Append(array2[i]);
                        break;
                    }
                }
            }

            return stringBuilder.ToString();
        }

        #region Galaxy List

        /// <summary>
        /// Liefert eine Auflistung aller Galaxie Objekte 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<GalaxyObjectContainer> GetGalaxyObjects()
        {
            return _galaxyobjects.AsEnumerable();
        }

        /// <summary>
        /// Liefert eine Auflistung aller Galaxie Objekte 
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <returns></returns>
        public IEnumerable<GalaxyObjectContainer<TObject>> GetGalaxyObjects<TObject>()
            where TObject : GalaxyMapObjectBase
        {
            return _galaxyobjects.OfType<GalaxyObjectContainer<TObject>>();
        }

        /// <summary>
        /// Liefert eine Auflistung der Container
        /// </summary>
        /// <typeparam name="TContainer"></typeparam>
        /// <returns></returns>
        public IEnumerable<TContainer> GetGalaxyObjectContainers<TContainer>()
            where TContainer : GalaxyObjectContainer
        {
            return _galaxyobjects.OfType<TContainer>();
        }

        /// <summary>
        /// Fügt dem Galaxie Objekt hinzu
        /// </summary>
        /// <param name="galaxyobject"></param>
        public void Add(GalaxyObjectContainer galaxyobject)
        {
            _galaxyobjects.Add(galaxyobject);
        }

        #endregion

        /// <summary>
        /// Liefert den Zufallszahlengenerator
        /// </summary>
        public Random RND { get; private set; }

        /// <summary>
        /// Gibt an ob der Prozess angehalten wurde
        /// </summary>
        protected bool IsProcessStopped { get; private set; }

        /// <summary>
        /// Liefert Zugriff auf die Templates
        /// </summary>
        public TemplateManager Templates { get; private set; }

        /// <summary>
        /// Liefert die Spielobjekt-Verwaltung
        /// </summary>
        public ObjectAccessor Accessor { get { return _instance.Accessor; } }

        /// <summary>
        /// Liefert die Breite der Galaxiekarte
        /// </summary>
        public double GalaxyMapSizeX { get; private set; }

        /// <summary>
        /// Liefert die Höhe der Galaxiekarte
        /// </summary>
        public double GalaxyMapSizeY { get; private set; }

        /// <summary>
        /// Liefert die Breite der Systemkarte
        /// </summary>
        public double SystemMapSizeX { get; private set; }

        /// <summary>
        /// Liefert die Höhe der Systemkarte
        /// </summary>
        public double SystemMapSizeY { get; private set; }

        /// <summary>
        /// Liefert den Abstand der Objekte in der Galaxiekarte
        /// </summary>
        public double StarDistance { get; private set; }
    }
}
