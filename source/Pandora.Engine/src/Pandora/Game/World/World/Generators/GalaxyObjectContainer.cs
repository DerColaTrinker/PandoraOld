﻿using Pandora.Game.World.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.World.Generators
{
    /// <summary>
    /// Stellt einen Kontainer dar der Galaxie Objekte während der Erstellung enthält
    /// </summary>
    public abstract class GalaxyObjectContainer
    {
        private HashSet<SystemObjectContainer> _systemobjects = new HashSet<SystemObjectContainer>();

        /// <summary>
        /// Liefert das System Objekt
        /// </summary>
        public GalaxyMapObjectBase Instance { get; internal set; }

        /// <summary>
        /// Liefert die X-Position des Objekts oder legt sie fest
        /// </summary>
        public float PositionX { get; set; }

        /// <summary>
        /// Liefert die Y-Position des Objekts oder legt sie fest
        /// </summary>
        public float PositionY { get; set; }

        /// <summary>
        /// Liefert den Namen des Objekts oder legt ihn fest
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Übernimmt die Standardparameter
        /// </summary>
        protected internal virtual void Confirm()
        {
            Instance.PositionX = PositionX;
            Instance.PositionY = PositionY;
            Instance.Instance.Accessor.SetName(Instance, Name);
        }

        #region SystemObject List

        /// <summary>
        /// Liefert eine AUflistung aller System Objekte innerhalb des Galaxie Objekts
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SystemObjectContainer> GetSystemObjects()
        {
            return _systemobjects.AsEnumerable();
        }

        /// <summary>
        /// Liefert eine AUflistung aller System Objekte innerhalb des Galaxie Objekts
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <returns></returns>
        public IEnumerable<SystemObjectContainer<TObject>> GetSystemObjects<TObject>()
            where TObject : SystemMapObjectBase
        {
            return _systemobjects.OfType<SystemObjectContainer<TObject>>();
        }

        /// <summary>
        /// Fügt dem Galaxie Objekt ein neuen System Objekt hinzu
        /// </summary>
        /// <param name="galaxyobject"></param>
        public void Add(SystemObjectContainer galaxyobject)
        {
            _systemobjects.Add(galaxyobject);
        }

        #endregion
    }

    /// <summary>
    /// Stellt einen Kontainer dar der ein System Objekt während der Erstellung enthält
    /// <typeparam name="TGalaxyObject"></typeparam>
    /// </summary>
    public class GalaxyObjectContainer<TGalaxyObject> : GalaxyObjectContainer
        where TGalaxyObject : GalaxyMapObjectBase
    {
        /// <summary>
        /// Liefert das System Objekt
        /// </summary>
        public new TGalaxyObject Instance { get { return (TGalaxyObject)base.Instance; } internal set { base.Instance = value; } }
    }
}
