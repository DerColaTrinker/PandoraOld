﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Generators
{
    /// <summary>
    /// Stellt eine Struktur bereit mit der es möglich ist, Prozentuale Zufälle zu ermitteln
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class ChanceCounter<T>
    {
        private Dictionary<T, float> _chances = new Dictionary<T, float>();
        private Random _rnd;

        /// <summary>
        /// Erstellt eine neue Instanz der ChanceCounter-Klasse
        /// </summary>
        public ChanceCounter()
        {
            _rnd = new Random();
        }

        /// <summary>
        /// Erstellt eine neue Instanz der ChanceCounter-Klasse
        /// </summary>
        /// <param name="rnd"></param>
        public ChanceCounter(Random rnd)
        {
            _rnd = rnd;
        }

        /// <summary>
        /// Fügt einen neuen Part hinzu
        /// </summary>
        /// <param name="value"></param>
        /// <param name="chance"></param>
        public void Add(T value, float chance)
        {
            _chances[value] = (chance * 1000);
        }

        /// <summary>
        /// Ermittelt eine neue Chance
        /// </summary>
        /// <returns></returns>
        public T GetChance()
        {
            var chances = BuildChances();
            var value = chances.Max(m => m.Item3) * _rnd.NextDouble();

            return chances.Where(m => value >= m.Item2 & value <= m.Item3).Select(m => m.Item1).FirstOrDefault();
        }

        private IEnumerable<Tuple<T, float, float>> BuildChances()
        {
            var startvalue = 0F;
            var endvalue = 0F;

            foreach (var chance in _chances)
            {
                endvalue = startvalue + chance.Value;

                yield return new Tuple<T, float, float>(chance.Key, startvalue, endvalue);

                startvalue = endvalue;
            }
        }
    }
}
