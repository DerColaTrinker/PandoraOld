﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.World.Generators
{
    /// <summary>
    /// Hilfsklasse für die Berechnung einer Verteilung aufgrund der Anzahl
    /// </summary>
    public class RatioCounter : RatioCounter<int>
    {
        /// <summary>
        /// Erstellt eine neue Instanz der RatioCounter-Klasse
        /// </summary>
        /// <param name="count"></param>
        public RatioCounter(int count)
            : base(GetIntegerArray(count))
        { }

        private static IEnumerable<int> GetIntegerArray(int count)
        {
            for (int i = 0 ; i < count ; i++)
            {
                yield return i;
            }
        }
    }

    /// <summary>
    /// Hilfsklasse für die Berechnung einer Verteilung aufgrund der Anzahl
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RatioCounter<T>
    {
        private Dictionary<T, int> _ratio = new Dictionary<T, int>();

        /// <summary>
        /// Erstellt eine neue Instanz der RatioCounter-Klasse
        /// </summary>
        /// <param name="types"></param>
        public RatioCounter(IEnumerable<T> types)
        {
            foreach (var type in types)
            {
                _ratio.Add(type, 0);
            }
        }

        /// <summary>
        /// Liefert die Summe
        /// </summary>
        /// <returns></returns>
        public float Sum()
        {
            return _ratio.Values.Sum();
        }

        /// <summary>
        /// Liefert die Anzahl
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            return _ratio.Count;
        }

        /// <summary>
        /// Liefert den Durchschnitt
        /// </summary>
        /// <returns></returns>
        public float Avg()
        {
            return (float)_ratio.Values.Average();
        }

        /// <summary>
        /// Liefert den Prozentwert des Elements
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public float Ratio(T type)
        {
            var sum = Sum();
            if (sum == 0) return 0F;

            var ratio = _ratio[type] / Sum();

            return ratio;
        }

        /// <summary>
        /// Erhöht die Anzahl des Elements
        /// </summary>
        /// <param name="type"></param>
        public void Increment(T type)
        {
            _ratio[type]++;
        }

        /// <summary>
        /// Verringert die Anzahl des Elements
        /// </summary>
        /// <param name="type"></param>
        public void Decrement(T type)
        {
            _ratio[type]--;
        }

        /// <summary>
        /// Liefert eine Liste aller Elemente
        /// </summary>
        public IEnumerable<T> Types { get { return _ratio.Keys; } }

        /// <summary>
        /// Liefert eine Liste aller Summen der Elemente
        /// </summary>
        public IEnumerable<int> Values { get { return _ratio.Values; } }
    }
}
