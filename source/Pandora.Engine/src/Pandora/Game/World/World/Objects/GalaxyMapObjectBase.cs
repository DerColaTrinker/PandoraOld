﻿using Pandora.Game.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    /// <summary>
    /// Basisklasse eines Galaxie Objects
    /// </summary>
    public abstract class GalaxyMapObjectBase : MapObjectBase
    { }

    /// <summary>
    /// Basisklasse eines Galaxie Objects
    /// </summary>
    /// <typeparam name="TTemplate"></typeparam>
    public abstract class GalaxyMapObjectBase<TTemplate> : GalaxyMapObjectBase, ITemplate<TTemplate>
        where TTemplate : TemplateBase
    {
        /// <summary>
        /// Liefert das Template
        /// </summary>
        public new TTemplate Template { get { return (TTemplate)base.Template; } set { Instance.Accessor.SetTemplate(this, value); } }
    }
}
