﻿using Pandora.Game.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    /// <summary>
    /// Basisklasse eines System Objekts
    /// </summary>
    public abstract class SystemMapObjectBase : MapObjectBase
    {
        /// <summary>
        /// Liefert das Galaxie Objekt an dem es gebunden ist
        /// </summary>
        public GalaxyMapObjectBase GalaxyObjectOwner { get; internal set; }

#pragma warning disable 1591
        public override void Serialize(Runtime.IO.IWriter writer)
        {
            base.Serialize(writer);

            writer.WriteUInt32(GalaxyObjectOwner.ObjectID);
        }

        public override void Deserialize( Runtime.IO.IReader reader)
        {
            base.Deserialize( reader);

            var ownerid = reader.ReadUInt32();

            GalaxyObjectOwner = Instance.Accessor.GetObject<GalaxyMapObjectBase>(ownerid);
        }
#pragma warning restore 1591
    }

    /// <summary>
    /// Basisklasse eines System Objekts
    /// </summary>
    /// <typeparam name="TTemplate"></typeparam>
    public abstract class SystemMapObjectBase<TTemplate> : SystemMapObjectBase, ITemplate<TTemplate>
        where TTemplate : TemplateBase
    {
        /// <summary>
        /// Liefert das Template
        /// </summary>
        public new TTemplate Template { get { return (TTemplate)base.Template; } internal set { Instance.Accessor.SetTemplate(this, value); } }
    }
}
