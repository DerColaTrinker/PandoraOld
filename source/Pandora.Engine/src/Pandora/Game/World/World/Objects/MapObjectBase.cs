﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    /// <summary>
    /// Stellt ein Objekt dar, das durch den MapHandler verwaltet wird
    /// </summary>
    public abstract class MapObjectBase : GameObjectBase
    {
        private float _timer;
        internal float _movetime;
        internal float _fx = 0F;
        internal float _fy = 0F;
        internal float _tx = 0F;
        internal float _ty = 0F;

        /// <summary>
        /// Liefert die X-Koordinate der Position
        /// </summary>
        public float PositionX { get; internal set; }

        /// <summary>
        /// Liefert die Y-Koordinate der Position
        /// </summary>
        public float PositionY { get; internal set; }

        /// <summary>
        /// Gibt an ob das Objekt in Bewegung ist
        /// </summary>
        public bool IsMoving { get; internal set; }

        /// <summary>
        /// Berechnet die aktuelle Position des Objektes wenn eine Bewegung aktiv ist
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        public override void WorldUpdate(float ms, float s)
        {
            if (IsMoving)
            {
                // Zeitsteuerung
                _timer += ms;

                // Prüfen ob die Animation 
                if (_timer >= _movetime)
                {
                    // Position auf das Ziel festlegen
                    PositionX += _tx;
                    PositionY += _ty;

                    // Bewegung anhalten
                    _timer = 0F;
                    IsMoving = false;
                }
                else
                {
                    // Position berechnen
                    PositionX += _fx * ms;
                    PositionY += _fy * ms;
                }
            }
        }

#pragma warning disable 1591
        public override void Serialize(Runtime.IO.IWriter writer)
        {
            base.Serialize(writer);

            // Position und Bewegung speichern
            writer.WriteFloat(PositionX);
            writer.WriteFloat(PositionY);
            writer.WriteFloat(_timer);
            writer.WriteFloat(_movetime);
            writer.WriteFloat(_fx);
            writer.WriteFloat(_fy);
            writer.WriteFloat(_tx);
            writer.WriteFloat(_ty);
        }

        public override void Deserialize(Runtime.IO.IReader reader)
        {
            base.Deserialize(reader);

            PositionX = reader.ReadFloat();
            PositionY = reader.ReadFloat();
            _timer = reader.ReadFloat();
            _movetime = reader.ReadFloat();
            _fx = reader.ReadFloat();
            _fy = reader.ReadFloat();
            _tx = reader.ReadFloat();
            _ty = reader.ReadFloat();
        }
#pragma warning restore 1591
    }
}
