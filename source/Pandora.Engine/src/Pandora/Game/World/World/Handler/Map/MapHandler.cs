﻿using Pandora.Runtime.IO;
using Pandora.Game.World.Generators;
using Pandora.Game.World.Objects;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Runtime.Collections;

namespace Pandora.Game.World.Handlers.Map
{
    /// <summary>
    /// Delegate das im Map Handler verwendet wird
    /// </summary>
    /// <param name="galaxyobject"></param>
    public delegate void GalaxyObjectDelegate(GalaxyMapObjectBase galaxyobject);

    /// <summary>
    /// Delegate das im Map Handler verwendet wird
    /// </summary>
    /// <param name="galaxyobject"></param>
    /// <param name="systemobject"></param>
    public delegate void SystemObjectDelegate(GalaxyMapObjectBase galaxyobject, SystemMapObjectBase systemobject);

    /// <summary>
    /// Handler der die Spielwelt verwaltet und alle darin befindlichen Spielobjekte
    /// </summary>
    public class MapHandler : WorldHandlerBase
    {
        /// <summary>
        /// Wird ausgelöst wenn ein Galaieobjekt eingefügt wird
        /// </summary>
        public event GalaxyObjectDelegate GalaxyObjectAdded;

        /// <summary>
        /// Wird ausgelöst wenn ein Galaxieobjekt entfernt wird
        /// </summary>
        public event GalaxyObjectDelegate GalaxyObjectRemoved;

        /// <summary>
        /// Wird ausgelöst wenn ein Systemobjekt eingefügt wird
        /// </summary>
        public event SystemObjectDelegate SystemObjectAdded;

        /// <summary>
        /// Wird ausgelöst wenn ein Systemobjekt entfernt wird
        /// </summary>
        public event SystemObjectDelegate SystemObjectRemoved;

        /// <summary>
        /// Erstellt eine Spielwelt mit dem angegebenen Generator
        /// </summary>
        /// <typeparam name="TGenerator"></typeparam>
        /// <param name="generator"></param>
        /// <param name="seed"></param>
        /// <returns></returns>
        public bool GenerateWorld<TGenerator>(TGenerator generator, int seed)
            where TGenerator : GeneratorBase
        {
            var result = generator.InternalCreate(Instance, seed);

            if (result)
            {
                // Erstellte Objekte an die Map weiterleiten
                foreach (var galaxyobjectcontainer in generator.GetGalaxyObjects())
                {
                    // Übernehmen
                    galaxyobjectcontainer.Confirm();

                    var galaxyobject = galaxyobjectcontainer.Instance;
                    Add(galaxyobject);

                    // Alle System Objekte übernehmen
                    foreach (var systemobjectcontainer in galaxyobjectcontainer.GetSystemObjects())
                    {
                        // Übernehmen
                        systemobjectcontainer.Confirm();

                        var systemobject = systemobjectcontainer.Instance;
                        Add(galaxyobject, systemobject);
                    }
                }
            }

            return result;
        }

        #region Galaxy Map

        private OwnerHashSet<GalaxyMapObjectBase, SystemMapObjectBase> _map = new OwnerHashSet<GalaxyMapObjectBase, SystemMapObjectBase>();

        /// <summary>
        /// Fügt ein neues Galaxie Objekt hinzu
        /// </summary>
        /// <param name="galaxyobject"></param>
        public void Add(GalaxyMapObjectBase galaxyobject)
        {
            if (_map.ContainsKey(galaxyobject)) return;

            _map.AddOwner(galaxyobject);
            Accessor.Add(galaxyobject);
            OnAddGalaxyObject(galaxyobject);
        }

        /// <summary>
        /// Entfernt ein Galaxie Objekt
        /// </summary>
        /// <param name="galaxyobject"></param>
        public void Remove(GalaxyMapObjectBase galaxyobject)
        {
            if (_map.RemoveOwner(galaxyobject))
            {
                Accessor.Remove(galaxyobject);
                OnRemoveGalaxyObject(galaxyobject);
            }
        }

        /// <summary>
        /// Liefert eine Liste aller Galaxie Objekte
        /// </summary>
        /// <returns></returns>
        public IEnumerable<GalaxyMapObjectBase> GetGalaxyObjects()
        {
            return _map.GetOwners();
        }

        /// <summary>
        /// Liefert eine Liste aller Galaxie Objekte des angegebenen Types
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <returns></returns>
        public IEnumerable<TObject> GetGalaxyObjects<TObject>()
            where TObject : GalaxyMapObjectBase
        {
            return Accessor.GetObjects<TObject>();
        }

        /// <summary>
        /// Liefert ein Galaxie Object
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <param name="objectid"></param>
        /// <returns></returns>
        public TObject GetGalaxyObject<TObject>(uint objectid)
            where TObject : GalaxyMapObjectBase
        {
            return Accessor.GetObject<TObject>(objectid);
        }

        /// <summary>
        /// Liefert die Anzahl der Galaxie Objekte
        /// </summary>
        public int Count { get { return _map.OwnerCount; } }

        /// <summary>
        /// Löst das AddGalaxyObject-Ereignis aus
        /// </summary>
        /// <param name="galaxyobject"></param>
        protected virtual void OnAddGalaxyObject(GalaxyMapObjectBase galaxyobject)
        {
            if (GalaxyObjectAdded != null) GalaxyObjectAdded.Invoke(galaxyobject);
        }

        /// <summary>
        /// Löst das RemoveGalaxyObject-Ereignis aus
        /// </summary>
        /// <param name="galaxyobject"></param>
        protected virtual void OnRemoveGalaxyObject(GalaxyMapObjectBase galaxyobject)
        {
            if (GalaxyObjectRemoved != null) GalaxyObjectRemoved.Invoke(galaxyobject);
        }

        #endregion

        #region System Map

        /// <summary>
        /// Liefert ein System Object
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <param name="objectid"></param>
        /// <returns></returns>
        public TObject GetSystemObject<TObject>(uint objectid)
            where TObject : SystemMapObjectBase
        {
            return Accessor.GetObject<TObject>(objectid);
        }

        /// <summary>
        /// Liefert eine Liste aller System Object des Galaxie Objects
        /// </summary>
        /// <param name="galaxyobject"></param>
        /// <returns></returns>
        public IEnumerable<SystemMapObjectBase> GetSystemObjects(GalaxyMapObjectBase galaxyobject)
        {
            return _map.GetElements(galaxyobject);
        }

        /// <summary>
        /// Liefert eine Liste aller System Object des angegebenen Types
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <param name="galaxyobject"></param>
        /// <returns></returns>
        public IEnumerable<TObject> GetSystemObjects<TObject>(GalaxyMapObjectBase galaxyobject)
            where TObject : SystemMapObjectBase
        {
            return _map.GetElements(galaxyobject).Cast<TObject>().AsEnumerable();
        }

        /// <summary>
        /// Liefert eine Liste aller System Object
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <returns></returns>
        public IEnumerable<TObject> GetSystemObjects<TObject>()
             where TObject : SystemMapObjectBase
        {
            return Accessor.GetObjects<TObject>();
        }

        /// <summary>
        /// Fügt dem System ein neues System Object hinzu
        /// </summary>
        /// <param name="galaxyobject"></param>
        /// <param name="systemobject"></param>
        public void Add(GalaxyMapObjectBase galaxyobject, SystemMapObjectBase systemobject)
        {
            _map.AddElement(galaxyobject, systemobject);
            systemobject.GalaxyObjectOwner = galaxyobject;
            Accessor.Add(systemobject);
            OnAddSystemObject(galaxyobject, systemobject);
        }

        /// <summary>
        /// Entfernt eine System Object
        /// </summary>
        /// <param name="galaxyobject"></param>
        /// <param name="systemobject"></param>
        public void Remove(GalaxyMapObjectBase galaxyobject, SystemMapObjectBase systemobject)
        {
            if (_map.RemoveElement(galaxyobject, systemobject))
            {
                systemobject.GalaxyObjectOwner = null;
                Accessor.Remove(systemobject);
                OnRemoveSystemObject(galaxyobject, systemobject);
            }
        }

        /// <summary>
        /// Wechselt an einem System Objekt die Bezug auf das Galaxie Objekt
        /// </summary>
        /// <typeparam name="TGalaxy"></typeparam>
        /// <typeparam name="TSystem"></typeparam>
        /// <param name="newgalaxyobject"></param>
        /// <param name="systemobject"></param>
        /// <param name="destroy"></param>
        public void ChangeGalaxyObject<TGalaxy, TSystem>(GalaxyMapObjectBase newgalaxyobject, SystemMapObjectBase systemobject, bool destroy)
        {
            var oldgalaxyobject = systemobject.GalaxyObjectOwner;

            // Switch
            if (_map.RemoveElement(oldgalaxyobject, systemobject))
            {
                _map.AddElement(newgalaxyobject, systemobject);
                systemobject.GalaxyObjectOwner = newgalaxyobject;
            }
        }

        /// <summary>
        /// Löst das AddSystemObject-Ereignis aus
        /// </summary>
        /// <param name="galaxyobject"></param>
        /// <param name="systemobject"></param>
        protected virtual void OnAddSystemObject(GalaxyMapObjectBase galaxyobject, SystemMapObjectBase systemobject)
        {
            if (SystemObjectAdded != null) SystemObjectAdded.Invoke(galaxyobject, systemobject);
        }

        /// <summary>
        /// Löst das RemoveSystemObject-Ereignis aus
        /// </summary>
        /// <param name="galaxyobject"></param>
        /// <param name="systemobject"></param>
        protected virtual void OnRemoveSystemObject(GalaxyMapObjectBase galaxyobject, SystemMapObjectBase systemobject)
        {
            if (SystemObjectRemoved != null) SystemObjectRemoved.Invoke(galaxyobject, systemobject);
        }

        #endregion

        #region Movement

        /// <summary>
        /// Startet eine Objekt animation
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="speed"></param>
        public void MoveToBySpeed(MapObjectBase obj, float x, float y, float speed)
        {
            var distance = Convert.ToSingle(Math.Sqrt(((x - obj.PositionX) * 2) + ((y - obj.PositionY) * 2)));
            var time = distance / speed;

            MoveToByTime(obj, x, y, time);
        }

        /// <summary>
        /// Startet eine Objekt animation
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="ms"></param>
        public void MoveToByTime(MapObjectBase obj, float x, float y, float ms)
        {
            obj._movetime = ms;
            obj._fx = (obj.PositionX - x) / ms;
            obj._fy = (obj.PositionY - y) / ms;
            obj._tx = x;
            obj._ty = y;
            obj.IsMoving = true;
        }

        #endregion

#pragma warning disable 1591
        public override void Serialize(IWriter writer)
        {
            var galaxyobjects = GetGalaxyObjects();
            writer.WriteInt32(galaxyobjects.Count());
            foreach (var galaxyobject in galaxyobjects)
            {
                OnSerializeGalaxyObject(writer, galaxyobject);

                var systemobjects = GetSystemObjects(galaxyobject);
                writer.WriteInt32(systemobjects.Count());
                foreach (var systemobject in systemobjects)
                {
                    OnSerializeSystemObject(writer, galaxyobject, systemobject);
                }
            }
        }

        public override void Deserialize(IReader reader)
        {
            var galaxyobjectcount = reader.ReadInt32();
            for (int galaxyobjectindex = 0 ; galaxyobjectindex < galaxyobjectcount ; galaxyobjectindex++)
            {
                var galaxyobject = (GalaxyMapObjectBase)null;
                OnDeserializeGalaxyObject(Instance, reader, out galaxyobject);

                _map.AddOwner(galaxyobject);

                var systemobjectcount = reader.ReadInt32();
                for (int systemobjectindex = 0 ; systemobjectindex < systemobjectcount ; systemobjectindex++)
                {
                    var systemobject = (SystemMapObjectBase)null;
                    OnDeserializeSystemObject(Instance, reader, out systemobject);

                    _map.AddElement(galaxyobject, systemobject);
                }
            }
        }
#pragma warning restore 1591

        /// <summary>
        /// Wird aufgerufen wenn ein System Objekt deserialisiert wird
        /// </summary>
        /// <param name="World"></param>
        /// <param name="reader"></param>
        /// <param name="systemobject"></param>
        protected virtual void OnDeserializeSystemObject(GameControllerBase World, IReader reader, out SystemMapObjectBase systemobject)
        {
            systemobject = (SystemMapObjectBase)GameObjectBase.CreateObject(World, reader);
        }

        /// <summary>
        /// Wird aufgerufen wenn ein Galaxie Objekt deserialisiert wird
        /// </summary>
        /// <param name="World"></param>
        /// <param name="reader"></param>
        /// <param name="galaxyobject"></param>
        protected virtual void OnDeserializeGalaxyObject(GameControllerBase World, IReader reader, out GalaxyMapObjectBase galaxyobject)
        {
            galaxyobject = (GalaxyMapObjectBase)GameObjectBase.CreateObject(World, reader);
        }

        /// <summary>
        /// Wird aufgerufen wenn ein Galaxie Objekt serialisiert wird
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="galaxyobject"></param>
        /// <returns></returns>
        protected virtual bool OnSerializeGalaxyObject(IWriter writer, GalaxyMapObjectBase galaxyobject)
        {
            galaxyobject.Serialize(writer);

            return true;
        }

        /// <summary>
        /// Wird aufgerufen wenn ein System Objekt serialisiert wird
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="galaxyobject"></param>
        /// <param name="systemobject"></param>
        /// <returns></returns>
        protected virtual bool OnSerializeSystemObject(IWriter writer, GalaxyMapObjectBase galaxyobject, SystemMapObjectBase systemobject)
        {
            systemobject.Serialize(writer);

            return true;
        }
    }
}
