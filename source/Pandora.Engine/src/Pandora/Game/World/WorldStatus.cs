﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World
{
    /// <summary>
    /// Beschreibt den Status der Spielwelt
    /// </summary>
    public enum WorldStatus
    {
        /// <summary>
        /// Unbekannt, in der Regel ist das System nicht initialisiert
        /// </summary>
        Unkown,

        /// <summary>
        /// Spielvorbereitungen
        /// </summary>
        Prepare,

        /// <summary>
        /// Spielläuft
        /// </summary>
        Running,

        /// <summary>
        /// Spiel pausiert
        /// </summary>
        Pause
    }
}
