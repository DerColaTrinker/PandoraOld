﻿using Pandora.Game.World.Handlers.Map;
using Pandora.Game.World.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World
{
    /// <summary>
    /// Stellt eine Spielweltmodifikation dar
    /// </summary>
    public abstract class WorldModificationBase : GameModificationBase
    {
#pragma warning disable 1591
        protected override bool OnInitialize()
        {
            Map = new MapHandler();

            RegisterHandler(Map);

            return true;
        }
#pragma warning restore 1591

        /// <summary>
        /// Liefert den Status der Spielwelt
        /// </summary>
        public WorldStatus Status { get; internal set; }

        /// <summary>
        /// Liefert den Objektspeicher der Instanz
        /// </summary>
        public ObjectAccessor Accessor { get { { return Instance.Accessor; } } }

        /// <summary>
        /// Liefert die Spielkarte
        /// </summary>
        public MapHandler Map { get; internal set; }

        /// <summary>
        /// Meldet einen Handler an der Spielinstanz an
        /// </summary>
        /// <param name="handler"></param>
        protected void RegisterHandler(WorldHandlerBase handler)
        {
            Instance.RegisterHandler(handler);

            handler.World = this;
        }

        /// <summary>
        /// Meldet einen Handler an der Spielinstanz an
        /// </summary>
        /// <typeparam name="THandler"></typeparam>
        protected void RegisterHandler<THandler>()
            where THandler : WorldHandlerBase
        {
            var instance = (THandler)null;

            Instance.RegisterHandler<THandler>(out instance);

            instance.World = this;
        }
    }

    /// <summary>
    /// Stellt eine Spielweltmodifikation dar
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    public abstract class WorldModificationBase<TRace> : WorldModificationBase
        where TRace : RaceBase
    {
#pragma warning disable 1591
        protected override bool OnInitialize()
        {
            base.OnInitialize();

            Races = new RaceHandler<TRace>();
            Activations = new ActivationHandler<TRace>();

            RegisterHandler(Races);
            RegisterHandler(Activations);

            return true;
        }
#pragma warning restore 1591

        /// <summary>
        /// Meldet einen Handler an der Spielinstanz an
        /// </summary>
        /// <param name="handler"></param>
        protected void RegisterHandler(WorldHandlerBase<TRace> handler)
        {
            Instance.RegisterHandler(handler);

            handler.World = this;
        }

        /// <summary>
        /// Liefert den Handler der Spielerrassen
        /// </summary>
        public RaceHandler<TRace> Races { get; private set; }

        /// <summary>
        /// Liefert den Handler für die Spieleraktivierungen
        /// </summary>
        public ActivationHandler<TRace> Activations { get; private set; }
    }
}
