﻿using Pandora.Game.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game
{
    /// <summary>
    /// Stellt die Basisklasse einer Spielerrasse dar
    /// </summary>
    public abstract class RaceBase : GameObjectBase
    {
        /// <summary>
        /// Erstellt eine neue Instanz der RaceBase-Klasse
        /// </summary>
        public RaceBase()
        {
        }

        /// <summary>
        /// Liefert den Spieler
        /// </summary>
        public Player Player { get; internal set; }

#pragma warning disable 1591


#pragma warning restore 1591
    }
}
