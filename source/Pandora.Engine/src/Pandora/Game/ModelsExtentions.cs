﻿using System;
using System.Collections.Generic;

namespace Pandora.Game
{
    /// <summary>
    /// Stellt Datenmodel Erweiterungen bereit
    /// </summary>
    public static class ModelsExtentions
    {
        /// <summary>
        /// Liefert eine Auflistung aller Spiel-Objekte
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="game"></param>
        /// <returns></returns>
        public static IEnumerable<TSource> Objects<TSource>(this GameControllerBase game)
            where TSource : GameObjectBase
        {
            return game.Accessor.GetObjects<TSource>();
        }

        /// <summary>
        /// Liefert ein Spiel-Objekt anhand der ID
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="game"></param>
        /// <param name="objectid"></param>
        /// <returns></returns>
        public static TSource Objects<TSource>(this GameControllerBase game, uint objectid)
            where TSource : GameObjectBase
        {
            return game.Accessor.GetObject<TSource>(objectid);
        }

        /// <summary>
        /// Liefert eine Auflistung aller Spiel-Objekte
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="accessor"></param>
        /// <returns></returns>
        public static IEnumerable<TSource> Objects<TSource>(this ObjectAccessor accessor)
            where TSource : GameObjectBase
        {
            return accessor.GetObjects<TSource>();
        }

        /// <summary>
        /// Liefert ein Spiel-Objekt anhand der ID
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="accessor"></param>
        /// <param name="objectid"></param>
        /// <returns></returns>
        public static TSource Objects<TSource>(this ObjectAccessor accessor, uint objectid)
            where TSource : GameObjectBase
        {
            return accessor.GetObject<TSource>(objectid);
        }
    }
}
