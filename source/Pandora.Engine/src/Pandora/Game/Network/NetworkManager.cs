﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;
using Pandora.Game.Network.Modules;

namespace Pandora.Game.Network
{
    internal delegate void ConnectionSendDelegate(Connection connection, PacketOut writer);
    internal delegate void ConnectionReceiveDelegate(PacketIn reader);

    /// <summary>
    /// Ein Delegate das verwendet wird, wenn ein Client die Verbindung unterbricht
    /// </summary>
    /// <param name="connection"></param>
    /// <param name="side"></param>
    public delegate void ClientDisconnectDelegate(Connection connection, ConnectionDisconnectBySide side);

    /// <summary>
    /// Ein Delegate das verwendet wird wenn ein neuer Client Verbunden ist
    /// </summary>
    /// <param name="connection"></param>
    public delegate void ClientConnectDelegate(Connection connection);

    /// <summary>
    /// Stellt ein Engine Modul bereit das die Kommunikation über das netzwerk übernimmt
    /// </summary>
    public sealed class NetworkManager
    {
        /// <summary>
        /// Wird aufgerufen wenn ein Client eine Verbindung herstellt
        /// </summary>
        public event ClientConnectDelegate ClientConnect;

        /// <summary>
        /// Wird aufgerufen wenn ein Client eine Verbindung unterbricht
        /// </summary>
        public event ClientDisconnectDelegate ConnectionDisconnect;

        internal event ConnectionReceiveDelegate ConnectionReceive;
        internal event ConnectionSendDelegate ConnectionSend;

        internal NetworkManager()
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                Logger.Warning("Network is not available");
                CommunicationMode = NetworkStatus.Failed;
            }

            CommunicationMode = NetworkStatus.Stopped;

            Logger.Normal("Network manager initialized");
        }

        #region Kommunikationsystem Steuerung

        private ModuleBase _module;

        /// <summary>
        /// Startet den Host-Betrieb
        /// </summary>
        /// <param name="listenip"></param>
        /// <param name="listenport"></param>
        /// <returns></returns>
        public bool CreateServer(string listenip, int listenport)
        {
            if (CommunicationMode != NetworkStatus.Stopped)
            {
                Logger.Warning("Network system already created");
                return false;
            }

            Module = new ServerModule();
            var result = Module.Start(listenip, listenport);

            if (result)
                CommunicationMode = NetworkStatus.Server;

            return result;
        }

        /// <summary>
        /// Startet den Client-Betrieb
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        /// <returns></returns>
        public bool CreateClient(string ip, int port)
        {
            if (CommunicationMode != NetworkStatus.Stopped)
            {
                Logger.Warning("Network system already created");
                return false;
            }

            Module = new ClientModule();
            var result = Module.Start(ip, port);

            if (result)
                CommunicationMode = NetworkStatus.Client;

            return result;
        }

        /// <summary>
        /// Liefert das verwendete Kommunikationsmodul
        /// </summary>
        public ModuleBase Module
        {
            get { return _module; }
            private set
            {
                // Wenn keine Änderungen sind, direkt verlassen
                if (_module == value) return;

                // Bestehendes Modul abmelden
                if (_module != null) { _module.ClientConnect -= OnConnectionConnect; }

                // Neu setzen
                _module = value;

                // Neues Modul anmelden
                if (value != null) { _module.ClientConnect += OnConnectionConnect; }
            }
        }

        /// <summary>
        /// Liefert den Betriebsmodus
        /// </summary>
        public NetworkStatus CommunicationMode { get; private set; }

        #endregion

        #region Verbindungen Steuern

        private void OnConnectionConnect(Connection connection)
        {
            connection.ConnectionDisconnect += OnConnectionDisconnect;
            connection.ConnectionReceive += OnClientReceive;
            connection.ConnectionSend += OnConnectionSend;

            if (ClientConnect != null) ClientConnect(connection);
        }

        private void OnConnectionDisconnect(Connection connection, ConnectionDisconnectBySide side)
        {
            connection.ConnectionDisconnect -= OnConnectionDisconnect;
            connection.ConnectionReceive -= OnClientReceive;
            connection.ConnectionSend -= OnConnectionSend;

            if (ConnectionDisconnect != null) ConnectionDisconnect(connection, side);
        }

        private void OnClientReceive( PacketIn reader)
        {
            //PushPacketInQueue(client, reader);

            if (ConnectionReceive != null) ConnectionReceive( reader);
        }

        private void OnConnectionSend(Connection connection, PacketOut writer)
        {
            if (ConnectionSend != null) ConnectionSend(connection, writer);
        }

        #endregion
    }
}
