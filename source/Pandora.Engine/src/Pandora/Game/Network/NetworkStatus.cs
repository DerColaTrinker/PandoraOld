﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.Network
{
    /// <summary>
    /// Gibt den Betriebszustand des Modules an
    /// </summary>
    public enum NetworkStatus
    {
        /// <summary>
        /// Angehalten
        /// </summary>
        Stopped,

        /// <summary>
        /// Client betrieb
        /// </summary>
        Client,

        /// <summary>
        /// Server betrieb
        /// </summary>
        Server,

        /// <summary>
        /// Fehler
        /// </summary>
        Failed
    }
}
