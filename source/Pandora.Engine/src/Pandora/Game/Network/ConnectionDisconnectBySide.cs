﻿namespace Pandora.Game.Network
{
    /// <summary>
    /// Gibt an von welcher Gegenstelle die Verbindung getrennt wurde
    /// </summary>
    public enum ConnectionDisconnectBySide
    {
        /// <summary>
        /// Server
        /// </summary>
        Server,

        /// <summary>
        /// Client
        /// </summary>
        Client,

        /// <summary>
        /// Fehler
        /// </summary>
        Error
    }
}