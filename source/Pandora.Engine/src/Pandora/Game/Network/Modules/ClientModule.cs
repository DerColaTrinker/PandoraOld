﻿using Pandora.Runtime.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Network.Modules
{
    /// <summary>
    /// Das Kommunikationsmodul für einen Client
    /// </summary>
    public class ClientModule : ModuleBase
    {
        private IPEndPoint _ripe;

        internal ClientModule()
        {
            Logger.Trace("ClientModule created");
        }

        /// <summary>
        /// Erstellt eine Verbindung zu einem Server. Die Verbindungsdaten werden über die Konfiguration [Network].HostIP und [Network].HostPort abgefragt.
        /// </summary>
        /// <returns></returns>
        public override bool Start(string ip, int port)
        {
            if (!CheckNetwork()) return false;
            if (!CheckRIPE(ip, port)) return false;

            var socket = new Socket(_ripe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                socket.Connect(_ripe);

                Logger.Normal("Client conntected to '{0}'", _ripe.ToString());

                Connection = new Connection(socket);
                OnClientConnect(Connection);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }
        }

        /// <summary>
        /// Trennt die Verbindung zum Server
        /// </summary>
        /// <returns></returns>
        public override bool Stop()
        {
            if (Connection != null) Connection.Disconnect();

            return true;
        }

        private bool CheckNetwork()
        {
            if (!NetworkInterface.GetIsNetworkAvailable())
            {
                Logger.Error("Server network not available");
                return false;
            }

            Logger.BeginState("IPv6 support available");
            if (Socket.OSSupportsIPv6)
                Logger.EndState(LoggerStateResult.Yes);
            else
                Logger.EndState(LoggerStateResult.No);

            return true;
        }

        private bool CheckRIPE(string ipaddress, int port)
        {
            var address = (IPAddress)null;

            if (!IPAddress.TryParse(ipaddress, out address))
            {
                Logger.Error("Server invalid ip address '{0}'", ipaddress);
                return false;
            }

            if (port < 1024 | port > UInt16.MaxValue)
            {
                Logger.Error("Server invalid port '{0}'", port);
                return false;
            }

            _ripe = new IPEndPoint(address, port);
            Logger.Debug("Client remote ip point '{0}'", _ripe.ToString());

            return true;
        }

        /// <summary>
        /// Liefert die Verbindung
        /// </summary>
        public Connection Connection { get; internal set; }
    }
}
