﻿using Pandora.Runtime.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Network.Modules
{
    /// <summary>
    /// Das Kommunikationsmodul für den Server
    /// </summary>
    public sealed class ServerModule : ModuleBase
    {
        private IPEndPoint _lipe;
        private Socket _socket;

        internal ServerModule()
        {
            Logger.Trace("ServerModule created");
        }

        /// <summary>
        /// Startet den Serverbetrieb. Die Verbindungsdaten für den Listener wird über die Konfiguration [Network].ListenIP und [Network].ListenPort geladen
        /// </summary>
        /// <returns></returns>
        public override bool Start(string ip, int port)
        {
            if (!CheckNetwork()) return false;
            if (!CheckLIPE(ip, port)) return false;

            if (_socket != null) return true;
            if (_lipe == null) { Logger.Error("Server not initialised"); return false; }

            _socket = new Socket(_lipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            if (!SocketBinding()) return false;
            if (!SocketListen()) return false;

            Logger.Normal("Server start listening...");

            return true;
        }

        /// <summary>
        /// Beendet den Serverbetrieb, verbundene Clients bleiben erhalten.
        /// </summary>
        /// <returns></returns>
        public override bool Stop()
        {
            if (_socket == null) return false;
            _socket.Close();
            _socket = null;

            Logger.Normal("Server stop listening...");
            return true;
        }

        private bool CheckNetwork()
        {
            if (!NetworkInterface.GetIsNetworkAvailable())
            {
                Logger.Error("Server network not available");
                return false;
            }

            Logger.BeginState("IPv6 support available");
            if (Socket.OSSupportsIPv6)
                Logger.EndState(LoggerStateResult.Yes);
            else
                Logger.EndState(LoggerStateResult.No);

            return true;
        }

        private bool CheckLIPE(string ipaddress, int port)
        {
            var address = (IPAddress)null;

            if (!IPAddress.TryParse(ipaddress, out address))
            {
                Logger.Error("Server invalid listen ip address '{0}'", ipaddress);
                return false;
            }

            if (port < 1024 | port > UInt16.MaxValue)
            {
                Logger.Error("Server invalid listen port '{0}'", port);
                return false;
            }

            _lipe = new IPEndPoint(address, port);
            Logger.Debug("Server listen point '{0}'", _lipe.ToString());

            return true;
        }

        private bool SocketBinding()
        {
            try
            {
                _socket.Bind(_lipe);

                Logger.Debug("Server socket bind to '{0}'", _lipe.ToString());

                // Wenn der Socket eine IPv6 Adresse ist, diese ebenfalls für IPv4 aktivieren
                if (_lipe.AddressFamily == AddressFamily.InterNetworkV6)
                {
                    _socket.SetSocketOption(SocketOptionLevel.IPv6, SocketOptionName.IPv6Only, false);
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }
        }

        private bool SocketListen()
        {
            try
            {
                _socket.Listen(200);
                _socket.BeginAccept(EndAccept, null);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }
        }

        private void EndAccept(IAsyncResult ar)
        {
            if (_socket == null) return;

            var socket = _socket.EndAccept(ar);
            if (socket == null) return;

            Logger.Trace("Server client connecting '{0}'", ((IPEndPoint)socket.RemoteEndPoint).ToString());

            var connection = new Connection(socket);

            OnClientConnect(connection);

            _socket.BeginAccept(EndAccept, null);
        }
    }
}
