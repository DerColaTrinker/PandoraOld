﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Network.Modules
{
    /// <summary>
    /// Stellt eine Basisklasse für die Kommunikation als Server oder Client dar.
    /// </summary>
    public abstract class ModuleBase
    {
        internal event ClientConnectDelegate ClientConnect;

        /// <summary>
        /// Startet den Client- oder Serverbetrieb
        /// </summary>
        /// <remarks>Muss in der abgeleiteten Klasse überschrieben werden.</remarks>
        public abstract bool Start(string ip, int port);

        /// <summary>
        /// Beendet den Client- oder Serverbetrieb
        /// </summary>
        /// <remarks>Muss in der abgeleiteten Klasse überschrieben werden.</remarks>
        public abstract bool Stop();

        /// <summary>
        /// Löst das ClientConnect-Ereignis aus
        /// </summary>
        /// <param name="client"></param>
        protected void OnClientConnect(Connection connection)
        {
            // Event auslösen
            if (ClientConnect != null) ClientConnect(connection);
        }
    }
}
