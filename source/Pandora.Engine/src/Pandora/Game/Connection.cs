﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Pandora.Game.Network;
using Pandora.Game.Players;

namespace Pandora.Game
{
    /// <summary>
    /// Stellt eine Verbindung dar
    /// </summary>
    public sealed class Connection
    {
        private Socket _socket;
        private DateTime _connectiondate;

        internal event ClientDisconnectDelegate ConnectionDisconnect;
        internal event ConnectionReceiveDelegate ConnectionReceive;
        internal event ConnectionSendDelegate ConnectionSend;

        internal Connection(Socket socket) : base()
        {
            _socket = socket;
            _socket.NoDelay = true;
            _connectiondate = DateTime.Now;

            ConnectionStatus = ConnectionStatus.Unkown;

            GUID = Guid.NewGuid().ToString();

            BeginReceive();

            RemoteAddress = ((IPEndPoint)_socket.RemoteEndPoint).ToString();

            Logger.Trace("ServerClientSocket created");
        }

        /// <summary>
        /// Trennt die Verbindung
        /// </summary>
        public void Disconnect()
        {
            if (_socket != null && _socket.Connected)
            {
                _socket.BeginDisconnect(false, EndDisconnect, ConnectionDisconnectBySide.Server);
            }
        }

        /// <summary>
        /// Sendet Daten an den Client
        /// </summary>
        /// <param name="writer"></param>
        public void Send(PacketOut writer)
        {
            if (_socket != null && _socket.Connected)
            {
                _socket.Send(writer.PackageBuffer, writer.Position, SocketFlags.None);
                OnCOnnectionSend(this, writer);
            }
        }

        private void BeginReceive()
        {
            if (_socket != null && _socket.Connected)
            {
                var pin = new PacketIn();
                _socket.BeginReceive(pin._buffer, 0, pin._buffer.Length, SocketFlags.None, EndReceive, pin);
            }
        }

        private void EndReceive(IAsyncResult ar)
        {
            var length = 0;
            var pin = ar.AsyncState as PacketIn;

            if (_socket == null) return;

            try
            {
                length = _socket.EndReceive(ar);
            }
            catch (SocketException ex)
            {
                if (ex.ErrorCode == 10054)
                {
                    OnConnectionDisconnect(this, ConnectionDisconnectBySide.Client);
                }
                else
                {
                    Logger.Exception(ex);
                    OnConnectionDisconnect(this, ConnectionDisconnectBySide.Error);
                }
                return;
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                OnConnectionDisconnect(this, ConnectionDisconnectBySide.Error);
                return;
            }

            Logger.Trace("Receive {0}bytes from '{1}'", length, RemoteAddress);

            if (length > 0)
            {
                // Dem PacketIn mittielen wie groß die eingegangenen Daten sind
                pin.Setup(this, length);

                // Das event auslösen
                OnConnectionReceive(pin);

                // Und das Spiel geht von vorne los
                BeginReceive();
            }
            else
            {
                OnConnectionDisconnect(this, ConnectionDisconnectBySide.Client);
            }
        }

        private void EndDisconnect(IAsyncResult ar)
        {
            _socket.EndDisconnect(ar);

            OnConnectionDisconnect(this, (ConnectionDisconnectBySide)ar.AsyncState);

            _socket.Close();
            _socket = null;
        }

        /// <summary>
        /// Liefert die IP Adresse des Remote-Rechners
        /// </summary>
        public string RemoteAddress { get; private set; }

        /// <summary>
        /// Gibt an ob die Verbingun besteht
        /// </summary>
        public bool IsConnected { get { return _socket != null && _socket.Connected; } }

        /// <summary>
        /// Liefert die GUID der Verbindung
        /// </summary>
        public string GUID { get; private set; }

        /// <summary>
        /// Liefert den Status der Verbindung
        /// </summary>
        public ConnectionStatus ConnectionStatus { get; internal set; }

        /// <summary>
        /// Liefert die Anzahl der Sekunden, wie lange der Spieler verbunden ist
        /// </summary>
        public double ConnectionTime { get { return (DateTime.Now - _connectiondate).TotalSeconds; } }

        internal void OnConnectionDisconnect(Connection client, ConnectionDisconnectBySide side)
        {
            if (ConnectionDisconnect != null) ConnectionDisconnect(client, side);
        }

        internal void OnConnectionReceive(PacketIn pin)
        {
            if (ConnectionReceive != null) ConnectionReceive(pin);
        }

        internal void OnCOnnectionSend(Connection client, PacketOut pout)
        {
            if (ConnectionSend != null) ConnectionSend(client, pout);
        }
    }
}
