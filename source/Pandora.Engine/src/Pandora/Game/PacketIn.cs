﻿using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game
{
    /// <summary>
    /// Stellt ein eingehendes Datenpaket dar
    /// </summary>
    public sealed class PacketIn : Packet, IPacketReader
    {
        private readonly Encoding _encoder = Encoding.UTF8;

        public Connection Connection { get; private set; }

        internal PacketIn()
           : base()
        { }

        /// <summary>
        /// Erstellt eine neue Instnaz der PacketIn-Klasse
        /// </summary>
        /// <param name="pout"></param>
        public PacketIn(PacketOut pout)
        {
            Buffer.BlockCopy(pout.PackageBuffer, 0, PackageBuffer, 0, pout.Length);
            Setup(pout.Length);
        }

        /// <summary>
        /// Liest ein Boolschen Wert
        /// </summary>
        /// <returns></returns>
        public bool ReadBool()
        {
            CheckEndOfBuffer(1);
            var result = BitConverter.ToBoolean(PackageBuffer, Position);
            Position += 1;
            return result;
        }

        /// <summary>
        /// Liest ein Byte
        /// </summary>
        /// <returns></returns>
        public byte ReadByte()
        {
            CheckEndOfLength(1);
            var result = PackageBuffer[Position];
            Position += 1;
            return result;
        }

        /// <summary>
        /// Liest ein unsigniertes Byte
        /// </summary>
        /// <returns></returns>
        public sbyte ReadSByte()
        {
            CheckEndOfLength(1);
            var result = PackageBuffer[Position];
            Position += 1;

            unchecked { return (sbyte)result; }
        }

        /// <summary>
        /// Kopiert das Array
        /// </summary>
        /// <param name="result"></param>
        public void ReadBytes(ref byte[] result)
        {
            CheckEndOfLength(result.Length);
            for (int index = 0; index < result.Length; index++)
                result[index] = PackageBuffer[Position + index];

            Position += result.Length;
        }

        /// <summary>
        /// Lieft eine Anzahl Bytes
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public byte[] ReadBytes(int length)
        {
            CheckEndOfLength(length);

            var result = new byte[length];

            for (int index = 0; index < length; index++)
                result[index] = PackageBuffer[Position + index];

            Position += length;
            return result;
        }

        /// <summary>
        /// Liest eine Int16 Zahl
        /// </summary>
        /// <returns></returns>
        public Int16 ReadInt16()
        {
            CheckEndOfLength(2);
            var result = BitConverter.ToInt16(PackageBuffer, Position);
            Position += 2;
            return result;
        }

        /// <summary>
        /// Liest eine Int32 Zahl
        /// </summary>
        /// <returns></returns>
        public Int32 ReadInt32()
        {
            CheckEndOfLength(4);
            var result = BitConverter.ToInt32(PackageBuffer, Position);
            Position += 4;
            return result;
        }

        /// <summary>
        /// Liest eine Int64 Zahl
        /// </summary>
        /// <returns></returns>
        public Int64 ReadInt64()
        {
            CheckEndOfLength(8);
            var result = BitConverter.ToInt64(PackageBuffer, Position);
            Position += 8;
            return result;
        }

        /// <summary>
        /// Liest eine UInt16 Zahl
        /// </summary>
        /// <returns></returns>
        public UInt16 ReadUInt16()
        {
            CheckEndOfLength(2);
            var result = BitConverter.ToUInt16(PackageBuffer, Position);
            Position += 2;
            return result;
        }

        /// <summary>
        /// Liest eine UInt32 Zahl
        /// </summary>
        /// <returns></returns>
        public UInt32 ReadUInt32()
        {
            CheckEndOfLength(4);
            var result = BitConverter.ToUInt32(PackageBuffer, Position);
            Position += 4;
            return result;
        }

        /// <summary>
        /// Liest eine UInt64 Zahl
        /// </summary>
        /// <returns></returns>
        public UInt64 ReadUInt64()
        {
            CheckEndOfLength(8);
            var result = BitConverter.ToUInt64(PackageBuffer, Position);
            Position += 8;
            return result;
        }

        /// <summary>
        /// Liest eine Double Zahl
        /// </summary>
        /// <returns></returns>
        public Double ReadDouble()
        {
            CheckEndOfLength(8);
            var result = BitConverter.ToDouble(PackageBuffer, Position);
            Position += 8;
            return result;
        }

        /// <summary>
        /// Liest eine Float Zahl
        /// </summary>
        /// <returns></returns>
        public float ReadFloat()
        {
            CheckEndOfLength(4);
            var result = BitConverter.ToSingle(PackageBuffer, Position);
            Position += 4;
            return result;
        }

        /// <summary>
        /// Liest einen String
        /// </summary>
        /// <returns></returns>
        public string ReadString()
        {
            var startpos = Position;
            var endpos = Position;

            // Nach dem Ende des Strings suchen
            while (true)
            {
                if (PackageBuffer[endpos] == 0) break;                                                           // Wenn das null Zeichen gefunden wurde verlassen
                if ((endpos + 1) > Length) throw new IndexOutOfRangeException("String terminator not found");    // Wenn das Ende erreicht wurde ebenfalls Schleife verlassen
                endpos++;                                                                                        // Die Position um 2 erhöhen
            }

            var length = endpos - startpos;                                  // String länge in Bytes die 2 Byte null Zeichen sollten nicht dabei sein
            if (length <= 0) return string.Empty;                            // Nichts zurückgeben, wenn die Länge kleiner gleich 0 ist, war was falsch?!?

            var result = _encoder.GetString(PackageBuffer, startpos, length);

            Position += (length + 1);

            return result;
        }

        /// <summary>
        /// Liest ein Zeichen
        /// </summary>
        /// <returns></returns>
        public char ReadChar()
        {
            var value = ReadInt16();

            return (char)value;
        }

        /// <summary>
        /// Liest ein Datum
        /// </summary>
        /// <returns></returns>
        public DateTime ReadDateTime()
        {
            CheckEndOfLength(8);
            var result = BitConverter.ToInt64(PackageBuffer, Position);
            Position += 8;
            return DateTime.FromBinary(result);
        }

        /// <summary>
        /// Liest ein Wert aus dem Datenpaket das direkt in ein Enum konvertiert wird
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T ReadEnum<T>()
        {
            var type = typeof(T);
            var value = (object)null;

            if (type.IsEnum)
            {
                var basetype = Enum.GetUnderlyingType(type);
                switch (basetype.Name)
                {
                    case "Int16": value = ReadInt16(); break;
                    case "Int32": value = ReadInt32(); break;
                    case "Int64": value = ReadInt64(); break;
                    case "UInt16": value = ReadUInt16(); break;
                    case "UInt32": value = ReadUInt32(); break;
                    case "UInt64": value = ReadUInt64(); break;
                    case "Byte": value = ReadByte(); break;
                    case "SByte": value = ReadSByte(); break;
                    default:
                        break;
                }

                return (T)Enum.Parse(type, value.ToString());
            }
            else
            {
                throw new InvalidCastException("Failed to write binary value.");
            }
        }

        /// <summary>
        /// Liest den OpCode der Nachricht
        /// </summary>
        /// <returns></returns>
        public uint ReadOpCode()
        {
            return ReadUInt32();
        }

        /// <summary>
        /// Legt die Basisdaten fest
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="lenght"></param>
        public new void Setup(Connection connection,int lenght)
        {
            Connection = connection;
            base.Setup(lenght);
        }
    }
}
