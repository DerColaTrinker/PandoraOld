﻿using Pandora.Game.Globalisation;
using Pandora.Game.Network;
using Pandora.Game.OpCodeDelegation;
using Pandora.Game.Players;
using Pandora.Game.Templates;
using Pandora.Runtime.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game
{
    /// <summary>
    /// Stellt eine Instanz eines Spiels dar
    /// </summary>
    public abstract partial class GameControllerBase : Instance
    {
        private Dictionary<string, GameHandlerBase> _handler = new Dictionary<string, GameHandlerBase>();

        #region Handler Controlling

        /// <summary>
        /// Meldet ein Handler an das System an
        /// </summary>
        /// <typeparam name="THandler"></typeparam>
        /// <param name="handler"></param>
        public void RegisterHandler<THandler>(out THandler handler)
            where THandler : GameHandlerBase
        {
            handler = null;

            try { handler = Activator.CreateInstance<THandler>(); }
            catch (Exception ex) { throw new PandoraGameException("Handler instance failed", ex); }

            RegisterHandler(handler);
        }

        /// <summary>
        /// Meldet einen Handler an das System an
        /// </summary>
        /// <param name="handler"></param>
        public void RegisterHandler(GameHandlerBase handler)
        {
            if (handler == null) throw new PandoraGameException("Argument null", new ArgumentNullException("handler"));

            var names = CreateHandlerName(handler.GetType());

            foreach (var name in names)
            {
                _handler.Add(name, handler);
            }

            handler.InternalInitialize(this);

            Logger.Debug("Handler '{0}' added", handler.GetType().Name);
        }

        /// <summary>
        /// Liefert eine Auflistung aller Handler
        /// </summary>
        /// <typeparam name="THandler"></typeparam>
        /// <returns></returns>
        public THandler GetHandler<THandler>()
            where THandler : GameHandlerBase
        {
            var names = CreateHandlerName(typeof(THandler));

            foreach (var name in names)
            {
                if (_handler.ContainsKey(name)) return (THandler)_handler[name];
            }

            return null;
        }

        internal static IEnumerable<string> CreateHandlerName(Type type)
        {
            var types = GetHandlerTypeHistory(type);

            foreach (var t in types)
            {
                if (!t.IsGenericType)
                {
                    yield return string.Format("{0}.{1}", t.Namespace, t.Name);
                }
                else
                {
                    yield return string.Format("{0}.{1}<{2}>", t.Namespace, t.Name.Substring(0, t.Name.IndexOf("`")), string.Join(", ", t.GenericTypeArguments.Select(m => m.Name).ToArray()));
                }
            }


        }

        private static IEnumerable<Type> GetHandlerTypeHistory(Type type)
        {
            var current = type;

            while (true)
            {
                yield return current;

                current = current.BaseType;
                if (current.Name.Contains("HandlerBase")) yield break;
            }
        }

        /// <summary>
        /// Liefert eine Auflistung aller Handler
        /// </summary>
        /// <returns></returns>
        public IEnumerable<GameHandlerBase> GetHandler()
        {
            return _handler.Values.AsEnumerable();
        }

        #endregion     
    }
}
