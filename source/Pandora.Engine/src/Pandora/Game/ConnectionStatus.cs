﻿namespace Pandora.Game
{
    /// <summary>
    /// Gibt den Verbindungsstatus an
    /// </summary>
    public enum ConnectionStatus
    {
        /// <summary>
        /// Nicht bekannt
        /// </summary>
        Unkown,
        Confirmed
    }
}