﻿using Pandora.Game.Network;
using Pandora.Game.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game
{
    /// <summary>
    /// Ein Delegate das in der Player-Klasse verwendet wird
    /// </summary>
    /// <param name="player"></param>
    public delegate void RemovePlayerDelegate(Player player);

    /// <summary>
    /// Stellt einen Spieler dar
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Wird aufgerufen, wenn der Spieler entfernt werden soll
        /// </summary>
        public event RemovePlayerDelegate RemovePlayer;

        internal Player()
        {
            Status = PlayerStatus.Lobby;
        }

        /// <summary>
        /// Liefert den Namen des Spielers
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Liefert den Status des Spielers
        /// </summary>
        public PlayerStatus Status { get; internal set; }
              
        /// <summary>
        /// Liefert die Netzwerkverbindung
        /// </summary>
        public Connection Connection { get; internal set; }

        /// <summary>
        /// Liefert die Identität
        /// </summary>
        public string PlayerIdentity { get; set; }

        /// <summary>
        /// Gibt eine Zeichenfolge zurück, die das aktuelle Objekt darstellt.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0,10} {1,8} {2,20}", Name, Status, PlayerIdentity);
        }

        /// <summary>
        /// Trennt den Spieler vom System
        /// </summary>
        public virtual void Disconnect()
        {
            if (RemovePlayer != null) RemovePlayer.Invoke(this);
        }
    }
}
