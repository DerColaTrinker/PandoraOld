﻿using Pandora.Game.Globalisation;
using Pandora.Game.Network;
using Pandora.Game.OpCodeDelegation;
using Pandora.Game.Players;
using Pandora.Game.Templates;
using Pandora.Runtime.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game
{
    /// <summary>
    /// Stellt eine Instanz eines Spiels dar
    /// </summary>
    public abstract partial class GameControllerBase : Instance
    {
        private List<GameModificationBase> _modifications = new List<GameModificationBase>();

        #region Modification Controlling

        /// <summary>
        /// Liefert eine Liste aller Module des Types
        /// </summary>
        /// <typeparam name="TModules"></typeparam>
        /// <returns></returns>
        public IEnumerable<TModules> GetModification<TModules>()
            where TModules : GameModificationBase
        {
            return _modifications.OfType<TModules>().AsEnumerable();
        }

        internal void InternalCreated()
        {
            GameMode = GameModes.Unkown;
            GameStatus = GameStatus.Unkown;
            OnGameCreated();
        }

        /// <summary>
        /// Löst das GameCreated-Ereignis aus
        /// </summary>
        protected virtual void OnGameCreated()
        {
            if (GameCreated != null) GameCreated.Invoke(this);
        }

        /// <summary>
        /// Liefert alle Module
        /// </summary>
        /// <returns></returns>
        public IEnumerable<GameModificationBase> GetModifications()
        {
            return _modifications.AsEnumerable();
        }

        /// <summary>
        /// Meldet ein Runtime Modul an
        /// </summary>
        /// <typeparam name="TModule"></typeparam>
        public void RegisterModification<TModule>()
            where TModule : GameModificationBase
        {
            var instance = (TModule)null;

            try { instance = Activator.CreateInstance<TModule>(); }
            catch (Exception ex) { throw new PandoraException("Modification instance failed", ex); }

            RegisterModification(instance);
        }

        /// <summary>
        /// Meldet ein Runtime Modul an
        /// </summary>
        /// <param name="type"></param>
        public void RegisterModification(Type type)
        {
            var instance = (GameModificationBase)null;

            if (!type.IsSubclassOf(typeof(GameModificationBase))) throw new InvalidCastException(string.Format("Type {0} could not cast in GameModificationBase or is not inherits", type.Name));

            try { instance = (GameModificationBase)Activator.CreateInstance(type); }
            catch (Exception ex) { throw new PandoraException("Modification instance failed", ex); }

            RegisterModification(instance);
        }

        /// <summary>
        /// Meldet ein Runtime Modul an
        /// </summary>
        /// <param name="module"></param>
        public void RegisterModification(GameModificationBase module)
        {
            if (module == null) throw new ArgumentNullException("module");

            var type = module.GetType();
            module.Directory = Path.GetDirectoryName(type.Assembly.Location);

            _modifications.Add(module);

            module.IntenralInitialize(this);

            Logger.Debug("Modification '{0}' added", module.GetType().Name);
        }

        #endregion 
    }
}
