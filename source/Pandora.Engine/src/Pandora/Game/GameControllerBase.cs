﻿using Pandora.Game.Globalisation;
using Pandora.Game.Network;
using Pandora.Game.Network.Modules;
using Pandora.Game.OpCodeDelegation;
using Pandora.Game.Players;
using Pandora.Game.Templates;
using Pandora.Runtime.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game
{
    /// <summary>
    /// Ein Delegate das in der GameInstance verwendet wird
    /// </summary>
    /// <param name="instance"></param>
    public delegate void GameInstanceDelegate(GameControllerBase instance);

    /// <summary>
    /// Stellt eine Instanz eines Spiels dar
    /// </summary>
    public abstract partial class GameControllerBase : Instance
    {
        /// <summary>
        /// Wird ausgelöst wenn ein Spiel gestartet wurde
        /// </summary>
        public event GameInstanceDelegate GameCreated;

        public event GameInstanceDelegate ServerCreated;

        /// <summary>
        /// Erstellt eine neue Instanz der GameInstance-Klasse
        /// </summary>
        public GameControllerBase()
        {
            Communications = new NetworkManager();
            Accessor = new ObjectAccessor(this);
            OpCodes = new OpCodeHandler();
            Players = new PlayerManager();
            Templates = new TemplateManager();
            Translations = new TranslationManager();

            GameStatus = GameStatus.Unkown;

            // Den PlayerManager als Handler anmelden :)
            RegisterHandler(Players);

            // Events umleiten
            Communications.ClientConnect += InternalAddConnection;
            Communications.ConnectionDisconnect += InternalRemoveConnection;
            Communications.ConnectionReceive += PushPacketInQueue;
        }

        #region Properties

        /// <summary>
        /// Liefert die Netzwerk-Kommunikationssysteme
        /// </summary>
        public NetworkManager Communications { get; private set; }

        /// <summary>
        /// Liefert die Spielkonfiguration
        /// </summary>
        public ConfigurationSection GameConfiguration { get; set; }

        /// <summary>
        /// Liefert den Spiel-Modus
        /// </summary>
        public GameModes GameMode { get; internal set; }

        /// <summary>
        /// Liefert den Handler für die RPC Aufrufe
        /// </summary>
        public OpCodeHandler OpCodes { get; private set; }

        /// <summary>
        /// Liefert die Objektdatenbank
        /// </summary>
        public ObjectAccessor Accessor { get; private set; }

        /// <summary>
        /// Liefert die Templatedatenbank
        /// </summary>
        public TemplateManager Templates { get; private set; }

        /// <summary>
        /// Liefert die Übersetzungsdatenbank
        /// </summary>
        public TranslationManager Translations { get; private set; }

        /// <summary>
        /// Liefert den Handler für die Spielerverwaltung
        /// </summary>
        public PlayerManager Players { get; private set; }

        /// <summary>
        /// Liefert den Spielstatus
        /// </summary>
        public GameStatus GameStatus { get; internal set; }

        /// <summary>
        /// Liefert die Version des Game-Controllers
        /// </summary>
        public abstract Version GameControllerVersion { get; }

        #endregion

        #region GameController

        public bool CreateGameServer()
        {
            if (!Communications.CreateServer(GameConfiguration.GetValue("listenip", "0.0.0.0"), GameConfiguration.GetValue<int>("listenport", 32000))) return false;

            GameMode = GameModes.MultiplayerServer;
            GameStatus = GameStatus.Lobby;

            OnServerCreated();

            return true;
        }

        public bool CreateGameClient()
        {
            if (!Communications.CreateClient(GameConfiguration.GetValue("hostip", "0.0.0.0"), GameConfiguration.GetValue<int>("hostport", 32000))) return false;

            GameMode = GameModes.MultiplayerServer;
            GameStatus = GameStatus.Lobby;

            // Wellcome senden
            var writer = new PacketOut(GameControllerOpCodes.CMSG_GAMECONTROLLER_REGISTER);
            writer.WriteInt32(Runtime.Version.Major);
            writer.WriteInt32(Runtime.Version.Minor);
            writer.WriteInt32(Runtime.Version.Build);
            writer.WriteInt32(Runtime.Version.Revision);
            writer.WriteInt32(GameControllerVersion.Major);
            writer.WriteInt32(GameControllerVersion.Minor);
            writer.WriteInt32(GameControllerVersion.Build);
            writer.WriteInt32(GameControllerVersion.Revision);

            writer.WriteInt32(Templates.Checksums.Count);
            foreach (var item in Templates.Checksums.GetChecksums())
            {
                writer.WriteString(item);
            }

            writer.WriteInt32(_modifications.Count);
            foreach (var modification in _modifications)
            {
                writer.WriteString(modification.Name);
                writer.WriteInt32(modification.Version.Major);
                writer.WriteInt32(modification.Version.Minor);
                writer.WriteInt32(modification.Version.Build);
                writer.WriteInt32(modification.Version.Revision);

                writer.WriteInt32(modification.Templates.Checksums.Count);
                foreach (var item in modification.Templates.Checksums.GetChecksums())
                {
                    writer.WriteString(item);
                }
            }

            Broadcast(writer);

            return true;
        }

        protected virtual void OnServerCreated()
        {
            if (ServerCreated != null) ServerCreated.Invoke(this);
        }

        internal override void InternalInitialize(PandoraRuntime runtime)
        {
            base.InternalInitialize(runtime);

            GameConfiguration = Runtime.Configurations.GetOrCreateSection("Game", Pandora.Runtime.Configuration.ConfigurationSectionOptions.NotDeletable | Pandora.Runtime.Configuration.ConfigurationSectionOptions.NotRenameable);
        }

        internal override void InternalStart()
        {
            base.InternalStart();

            foreach (var handler in _handler.Values) { handler.OnStart(); }
            foreach (var modification in _modifications) { modification.OnStart(); }
        }

        internal override void InternalStop()
        {
            base.InternalStop();

            foreach (var handler in _handler.Values) { handler.OnStop(); }
            foreach (var modification in _modifications) { modification.OnStop(); }
        }

        internal override void InternalSystemUpdate(float ms, float s)
        {
            foreach (var handler in _handler.Values) { handler.OnSystemUpdate(ms, s); }
            foreach (var modification in _modifications) { modification.OnSystemUpdate(ms, s); }

            DispatchMessage();

            base.InternalSystemUpdate(ms, s);
        }

        #endregion
    }
}
