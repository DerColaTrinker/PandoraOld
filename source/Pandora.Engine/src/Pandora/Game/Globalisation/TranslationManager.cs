﻿using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Pandora.Runtime.Extensions;
using Pandora.Game.Templates;
using System.Globalization;
using System.Diagnostics.CodeAnalysis;

namespace Pandora.Game.Globalisation
{
    /// <summary>
    /// Delegate das verwendet wird, wenn eine die Übersetzungsdatenbank geändert wird
    /// </summary>
    /// <param name="dictionary"></param>
    public delegate void TranslationChangedDelegate(TranslationDictionary dictionary);

    /// <summary>
    /// Verwaltet die Übersetzungen für die unterschiedlichen Sprachen
    /// </summary>
    public sealed class TranslationManager
    {
        private Dictionary<string, TranslationDictionary> _dictionaries = new Dictionary<string, TranslationDictionary>(StringComparer.InvariantCultureIgnoreCase);
        private HashSet<string> _identifiers = new HashSet<string>();

        /// <summary>
        /// Wird aufgerufen, wenn eine andere Übersetzungsdatenbank aktiviert wird
        /// </summary>
        public event TranslationChangedDelegate TranslationChanged;

        /// <summary>
        /// Übernimmt die Spracheinstellungen in den Templates
        /// </summary>
        /// <param name="manager"></param>
        public void ApplyToTemplate(TemplateManager manager)
        {
            if (CurrentDictionary == null)
            {
                Logger.Warning("No language set");
                return;
            }

            Logger.Debug("Set language '{0}' to tempaltes", CurrentDictionary.LanguageCode);

            foreach (var template in manager.GetTemplates())
            {
                if (ContainsIdentifier(template.Identifier))
                {
                    var entry = GetEntry(template.Identifier);
                    template.Name = entry.Name;
                    template.Description = entry.Description;

                    Logger.Trace("Template identifier '{0}' found", template.Identifier);
                }
                else
                {
                    Logger.Warning("Template identifier '{0}' not found", template.Identifier);
                }
            }
        }

        #region Dictionaries

        /// <summary>
        /// Erstellt eine neue Übersetzungsdatenbank
        /// </summary>
        /// <param name="langcode"></param>
        /// <returns></returns>
        public bool AddDictionary(string langcode)
        {
            if (ContainsLanguage(langcode))
            {
                Logger.Error("Translation dictionary '{0}' already exists", langcode);
                return false;
            }

            var dictionary = new TranslationDictionary(langcode);

            //CurrentDictionary = dictionary;

            // Alle Einträge übernehmen
            _identifiers.All(m => { dictionary.Add(m, string.Empty, string.Empty); return true; });

            _dictionaries.Add(langcode, dictionary);

            Logger.Debug("Translation dictionary '{0}' added", langcode);
            return true;
        }

        /// <summary>
        /// Entfernt eine bestehende Übersetzungsdatenbank
        /// </summary>
        /// <param name="langcode"></param>
        /// <returns></returns>
        public bool RemoveDictionary(string langcode)
        {
            if (!ContainsLanguage(langcode))
            {
                Logger.Error("Translation dictionary '{0}' not found", langcode);
                return false;
            }

            _dictionaries.Remove(langcode);

            if (CurrentDictionary.LanguageCode == langcode) CurrentDictionary = _dictionaries.Values.FirstOrDefault();

            Logger.Debug("Translation dictionary '{0}' removed", langcode);
            return true;
        }

        /// <summary>
        /// Gibt an ob eine Übersetzung vorliegt
        /// </summary>
        /// <param name="langcode"></param>
        /// <returns></returns>
        public bool ContainsLanguage(string langcode)
        {
            return _dictionaries.ContainsKey(langcode);
        }

        /// <summary>
        /// Liefert alle Übersetzungsdatenbanken
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TranslationDictionary> GetDictionaries()
        {
            return _dictionaries.Values;
        }

        /// <summary>
        /// Liefert eine Übersetzungsdatenbank
        /// </summary>
        /// <param name="langcode"></param>
        /// <returns></returns>
        public TranslationDictionary GetDictionary(string langcode)
        {
            if (ContainsLanguage(langcode))
            {
                return _dictionaries[langcode];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Liefert die aktuell verwendete Übersetzungsdatenbank
        /// </summary>
        public TranslationDictionary CurrentDictionary { get; private set; }

        /// <summary>
        /// Aktiviert eine Übersetzungsdatenbank
        /// </summary>
        /// <param name="langcode"></param>
        /// <returns></returns>
        public bool SelectTranslation(string langcode)
        {
            if (!_dictionaries.ContainsKey(langcode))
                return false;

            return SelectTranslation(_dictionaries[langcode]);
        }

        /// <summary>
        /// Gibt an, das die Standardsprache verwendet wird
        /// </summary>
        /// <returns></returns>
        public bool SelectDefaultTranslation()
        {
            var sysculture = CultureInfo.CurrentCulture;
            var lang = sysculture.TwoLetterISOLanguageName;

            if (_dictionaries.ContainsKey(lang))
                SelectTranslation(lang);
            else
                SelectTranslation(_dictionaries.Keys.First());

            return true;
        }

        /// <summary>
        /// Aktiviert eine Übersetzungsdatenbank
        /// </summary>
        /// <param name="dictionary"></param>
        /// <returns></returns>
        public bool SelectTranslation(TranslationDictionary dictionary)
        {
            CurrentDictionary = dictionary;

            Logger.Normal("Translation dictionary changed to '{0}'", dictionary.LanguageCode);

            if (TranslationChanged != null) TranslationChanged.Invoke(dictionary);

            return true;
        }

        #endregion

        #region Entries

        /// <summary>
        /// Fügt der aktuellen Übersetzungsdatenbank ein Eintrag hinzu
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public bool AddEntry(string identifier, string name, string description)
        {
            if (_identifiers.Add(identifier))
            {
                foreach (var d in _dictionaries.Values) { d.Add(identifier, string.Empty, string.Empty); }
            }

            if (CurrentDictionary != null) CurrentDictionary.Update(identifier, name, description);

            Logger.Trace("Translation identifier '{0}' created", identifier);

            return true;
        }

        /// <summary>
        /// Fügt der aktuellen Übersetzungsdatenbank ein Eintrag hinzu
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public bool AddEntry(string identifier)
        {
            return AddEntry(identifier, string.Empty, string.Empty);
        }

        /// <summary>
        /// Fügt einer Übersetzungsdatenbank ein Eintrag hinzu
        /// </summary>
        /// <param name="langcode"></param>
        /// <param name="identifier"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public bool AddEntry(string langcode, string identifier, string name, string description)
        {
            if (_identifiers.Add(identifier))
            {
                foreach (var d in _dictionaries.Values) { d.Add(identifier, string.Empty, string.Empty); }
            }

            var dictionary = GetDictionary(langcode);
            dictionary.Update(identifier, name, description);

            Logger.Trace("Translation identifier '{0}' created", identifier);

            return true;
        }

        /// <summary>
        /// Entfernt einen Eintrag
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public bool RemoveEntry(string identifier)
        {
            if (_identifiers.Remove(identifier))
            {
                foreach (var d in _dictionaries.Values) { d.Remove(identifier); }

                Logger.Trace("Translation identifier '{0}' removed", identifier);
                return true;
            }
            else
            {
                Logger.Warning("Translation entry identifier '{0}' not found", identifier);
                return false;
            }
        }

        /// <summary>
        /// Benennt einen Eintrag um
        /// </summary>
        /// <param name="oldidentifier"></param>
        /// <param name="newidentifier"></param>
        /// <returns></returns>
        public bool Rename(string oldidentifier, string newidentifier)
        {
            if (_identifiers.Contains(newidentifier))
            {
                Logger.Error("Translation entry rename '{0}' already exists", newidentifier);
                return false;
            }

            if (!_identifiers.Contains(oldidentifier))
            {
                Logger.Error("Translation entry rename '{0}' not found", oldidentifier);
                return false;
            }

            // Identifier austauschen
            _identifiers.Remove(oldidentifier);
            _identifiers.Add(newidentifier);

            foreach (var d in _dictionaries.Values)
            {
                d.Rename(oldidentifier, newidentifier);
            }

            Logger.Debug("Translation entry '{0}' -> '{1}' renamed", oldidentifier, newidentifier);
            return false;
        }

        /// <summary>
        /// Gibt an ob ein Bezeichner vergeben ist
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public bool ContainsIdentifier(string identifier)
        {
            return _identifiers.Contains(identifier);
        }

        /// <summary>
        /// Liefert eine Auflistung aller Bezeichner
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetIdentifiers()
        {
            return _identifiers.ToArray();
        }

        /// <summary>
        /// Liefert alle Einträge der aktuellen Übersetzungsdatenbank
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TranslationEntry> GetCurrentTranslationEntries()
        {
            return CurrentDictionary.GetEntries();
        }

        /// <summary>
        /// Liefert einen Eitnrag der aktuellen Übersetzungsdatenbank
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public TranslationEntry GetCurrentTranslationEntry(string identifier)
        {
            if (!ContainsIdentifier(identifier))
            {
                Logger.Warning("Translation entry identifier '{0}' not found", identifier);
                return null;
            }

            return CurrentDictionary.GetEntry(identifier);
        }

        /// <summary>
        /// Liefert einen EIntrag aus der aktuellen Übersetzungsdatenbank
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public TranslationEntry GetEntry(string identifier)
        {
            if (_identifiers.Contains(identifier))
                return CurrentDictionary.GetEntry(identifier);
            else
                return null;
        }

        #endregion

        #region Loading File

        /// <summary>
        /// Liest eine Sprachdatenbank aus einer XML Datei
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool Load(string filename)
        {
            return Load(new FileInfo(filename));
        }

        /// <summary>
        /// Liest eine Sprachdatenbank aus einer XML Datei
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public bool Load(FileInfo file)
        {
            return Load(PandoraContentStream.Read(file));
        }

        /// <summary>
        /// Liest eine Sprachdatenbank aus einer XML Datei
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public bool Load(PandoraContentStream stream)
        {
            if (stream.ContentType == PandoraContentStreamTypes.Source)
            {
                if (!stream.File.Exists)
                {
                    Logger.Error("Translation file '{0}' not found", stream.File.Name);
                    return false;
                }
            }

            var xd = new XmlDocument();
            xd.Load(stream);

            foreach (XmlNode dnode in xd.SelectNodes("pandora/translations/dictionary"))
            {
                var langcode = dnode.Attributes.GetValue("langcode", "");
                if (string.IsNullOrEmpty(langcode)) continue;
                if (ContainsLanguage(langcode)) { Logger.Warning("Translation language '{0}' already exists", langcode); continue; }
                if (!AddDictionary(langcode)) continue;

                foreach (XmlNode inode in dnode.SelectNodes("entry"))
                {
                    var identifier = inode.Attributes.GetValue("identifier", "");
                    var name = inode.Attributes.GetValue("name", string.Empty);
                    var description = inode.Attributes.GetValue("description", string.Empty);

                    AddEntry(langcode, identifier, name, description);
                }
            }

            Logger.Normal("Translation file '{0}' loaded", stream.File.Name);

            if (CurrentDictionary == null) SelectDefaultTranslation();

            return true;
        }

        #endregion
    }
}
