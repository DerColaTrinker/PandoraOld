﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.Globalisation
{
    /// <summary>
    /// Stellt einen Eintrag der Übersetzung dar
    /// </summary>
    public sealed class TranslationEntry
    {
        internal TranslationEntry(string identifier, string name, string description)
        {

            Identifier = identifier;
            Name = name;
            Description = description;
        }

        /// <summary>
        /// Liefert den Bezeichner dieses Eintrags
        /// </summary>
        public string Identifier { get; internal set; }

        /// <summary>
        /// Liefert den Namen oder legt ihn fest
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Liefert die Beschreibung des Eintrags
        /// </summary>
        public string Description { get; set; }
    }
}
