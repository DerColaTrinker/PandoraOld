﻿using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game
{
    /// <summary>
    /// Diese Basisklasse stellt Datenpaket dar
    /// </summary>
    public abstract class Packet : IPacket
    {
        internal byte[] _buffer = new byte[4096];

        internal Packet()
        { }

        /// <summary>
        /// Ermöglicht direkten Zugriff auf das Byte Buffer Array
        /// </summary>
        protected internal byte[] PackageBuffer { get { return _buffer; } }

        /// <summary>
        /// Liefert die aktuelle Position im Datenpaket
        /// </summary>
        public virtual int Position { get; internal set; }

        /// <summary>
        /// Liefert die Länge des Datenpaket
        /// </summary>
        public virtual int Length { get; internal set; }

        internal void CheckEndOfLength(int length)
        {
            if ((Position) >= Length) throw new IndexOutOfRangeException("Packet");
        }

        internal void CheckEndOfBuffer(int length)
        {
            if ((Position + length) >= PackageBuffer.Length) throw new IndexOutOfRangeException("PackageBuffer");
        }

        internal void Setup(int length)
        {
            Position = 0;
            Length = length;
        }

        internal byte[] ToArray()
        {
            return _buffer;
        }
    }
}
