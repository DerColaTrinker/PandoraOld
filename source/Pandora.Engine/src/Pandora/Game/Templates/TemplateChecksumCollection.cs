﻿using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.Templates
{
    /// <summary>
    /// Stellt eine Auflistung aller Checksummen dar
    /// </summary>
    public sealed class TemplateChecksumCollection
    {
        private HashSet<string> _checksums = new HashSet<string>();

        internal bool Add(PandoraContentStream stream)
        {
            return _checksums.Add(string.Format("{0}_{1}", stream.File.FullName, stream.HashCode));
        }

        /// <summary>
        /// Liefert eine Auflistung aller Checksummen
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetChecksums()
        {
            return _checksums.AsEnumerable();
        }

        /// <summary>
        /// Liefert die Anzahl der Checksummen
        /// </summary>
        public int Count { get { return _checksums.Count; } }
    }
}
