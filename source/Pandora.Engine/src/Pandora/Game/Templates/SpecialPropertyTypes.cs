﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates
{
    /// <summary>
    /// Liste der Spezielfelder
    /// </summary>
    public enum SpecialPropertyTypes : int
    {
        /// <summary>
        /// Standard
        /// </summary>
        Default,

        /// <summary>
        /// Verhältnis
        /// </summary>
        Ratio
    }
}
