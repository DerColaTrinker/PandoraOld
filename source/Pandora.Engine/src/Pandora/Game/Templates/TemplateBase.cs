﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates
{
    /// <summary>
    /// Basisklasse eines Tempaltes
    /// </summary>
    public abstract class TemplateBase
    {
        private string _name;
        /// <summary>
        /// Erstellt eine neue Instanz der TemplateBase Klasse
        /// </summary>
        protected TemplateBase()
            : this(Guid.NewGuid().ToString())
        { }

        /// <summary>
        /// Erstellt eine neue Instanz der TemplateBase Klasse
        /// </summary>
        /// <param name="identifier"></param>
        public TemplateBase(string identifier)
        {
            Identifier = identifier;

            _name = string.Empty;
            Description = string.Empty;
        }

        /// <summary>
        /// Liefert den Bezeichner
        /// </summary>
        [TemplateProperty(InternalTemplatePropertyTypes.Identifier)]
        public string Identifier { get; internal set; }

        /// <summary>
        /// Liefert den Resource Code oder legt ihn fest
        /// </summary>
        [TemplateProperty(InternalTemplatePropertyTypes.ResourceCode)]
        public string ResourceCode { get; set; }

        /// <summary>
        /// Liefert den Namen oder legt ihn fest
        /// </summary>
        public string Name { get { return string.IsNullOrEmpty(_name) ? Identifier : _name; } set { _name = value; } }

        /// <summary>
        /// Liefert die Beschreibung oder legt sie fest
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Liefert den Bezeichner der ersten Voraussetzung oder legt ihn fest
        /// </summary>
        [TemplateProperty(InternalTemplatePropertyTypes.Required, SortOrder = 0)]
        public string Required1 { get; set; }

        /// <summary>
        /// Liefert den Bezeichner der zweiter Voraussetzung oder legt ihn fest
        /// </summary>
        [TemplateProperty(InternalTemplatePropertyTypes.Required, SortOrder = 1)]
        public string Required2 { get; set; }

        /// <summary>
        /// Liefert den Bezeichner der dritter Voraussetzung oder legt ihn fest
        /// </summary>
        [TemplateProperty(InternalTemplatePropertyTypes.Required, SortOrder = 2)]
        public string Required3 { get; set; }

        /// <summary>
        /// Liefert den Bezeichner der vierter Voraussetzung oder legt ihn fest
        /// </summary>
        [TemplateProperty(InternalTemplatePropertyTypes.Required, SortOrder = 3)]
        public string Required4 { get; set; }

        /// <summary>
        /// Wird auf gerufen, wenn das Template validiert wird
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<string> OnValidate()
        {
            yield break;
        }
    }
}
