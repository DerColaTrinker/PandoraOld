﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates
{
    /// <summary>
    /// Eine Ausnahme die im TemplateManager ausgelöst wird
    /// </summary>
    [Serializable]
    public class TemplateException : PandoraException
    {
        /// <summary>
        /// Erstellt eine neue Instanz der TemplateException-Klasse
        /// </summary>
        public TemplateException()
        { }

        /// <summary>
        /// Erstellt eine neue Instanz der TemplateException-Klasse
        /// </summary>
        /// <param name="message"></param>
        public TemplateException(string message)
            : base(message)
        { }

        /// <summary>
        /// Erstellt eine neue Instanz der TemplateException-Klasse
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public TemplateException(string message, Exception inner)
            : base(message, inner)
        { }

        /// <summary>
        /// Erstellt eine neue Instanz der TemplateException-Klasse
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected TemplateException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        { }
    }
}
