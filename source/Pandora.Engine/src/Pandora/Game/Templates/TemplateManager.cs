﻿using Pandora.Game.Templates.Bindings;
using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Pandora.Runtime.Extensions;

namespace Pandora.Game.Templates
{
    /// <summary>
    /// Ein Delegate das im TemplateManager verwendet wird
    /// </summary>
    public delegate void TemplateManagerDelegate();

    /// <summary>
    /// Ein Delegate das ausgelöst wird, wenn ein Template umbenannt wird
    /// </summary>
    /// <param name="oldidentifier"></param>
    /// <param name="newidentifier"></param>
    public delegate void IdentifierRenameDelegate(string oldidentifier, string newidentifier);

    /// <summary>
    /// Eine Manager der Template-Vorlagen verwaltet
    /// </summary>
    public sealed class TemplateManager
    {
        private Dictionary<string, TemplateBase> _templates = new Dictionary<string, TemplateBase>();

        /// <summary>
        /// Wird ausgelöst wenn ein Template geladen wurde
        /// </summary>
        public event TemplateManagerDelegate TemplateLoaded;

        /// <summary>
        /// Wird ausgelst wenn ein Template umbenannt wird
        /// </summary>
        public event IdentifierRenameDelegate TemplateRenamed;

        /// <summary>
        /// Erstellt eine neue Instanz der TemplateMananger-Klasse
        /// </summary>
        public TemplateManager()
        {
            Logger.Trace("TemplateManager created");

            Checksums = new TemplateChecksumCollection();
            Bindings = new TemplateBindingHandler();
        }

        /// <summary>
        /// Fügt ein neues Template hinzu
        /// </summary>
        /// <typeparam name="TTemplate"></typeparam>
        /// <param name="template"></param>
        public void Add<TTemplate>(TTemplate template)
            where TTemplate : TemplateBase
        {
            if (ContainsIdentifier(template.Identifier))
            {
                Logger.Warning("Template '{0}' alerady exists", template.Identifier);
                return;
            }

            _templates.Add(template.Identifier, template);
            Logger.Debug("Template '{0}' added", template.Identifier);
        }

        /// <summary>
        /// Entfernt eine Template
        /// </summary>
        /// <typeparam name="TTemplate"></typeparam>
        /// <param name="template"></param>
        public void Remove<TTemplate>(TTemplate template)
            where TTemplate : TemplateBase
        {
            if (!ContainsIdentifier(template.Identifier))
            {
                Logger.Warning("Template '{0}' not found", template.Identifier);
                return;
            }

            _templates.Remove(template.Identifier);

            // Alle Verbindungen in den Voraussetzungen ebenfalls entfernen
            foreach (var item in _templates.Values)
            {
                if (!string.IsNullOrEmpty(item.Required1) && item.Required1 == template.Identifier) item.Required1 = string.Empty;
                if (!string.IsNullOrEmpty(item.Required2) && item.Required2 == template.Identifier) item.Required2 = string.Empty;
                if (!string.IsNullOrEmpty(item.Required3) && item.Required3 == template.Identifier) item.Required3 = string.Empty;
                if (!string.IsNullOrEmpty(item.Required4) && item.Required4 == template.Identifier) item.Required4 = string.Empty;
            }
        }

        /// <summary>
        /// Benennt eine Template um
        /// </summary>
        /// <param name="template"></param>
        /// <param name="newidentifier"></param>
        public void Rename(TemplateBase template, string newidentifier)
        {
            // Wenn es sich um einen Key handelt muss dieser zu erst aus der Templateliste entfernt und wieder neu eingefügt werden
            var oldidentifier = template.Identifier;

            if (string.IsNullOrEmpty(newidentifier)) return;

            foreach (var rtemplte in _templates.Values)
            {
                if (!string.IsNullOrEmpty(rtemplte.Required1) && rtemplte.Required1 == oldidentifier) { rtemplte.Required1 = newidentifier; Logger.Trace("Rename Request 1 '{0}' -> '{1}'.", rtemplte.Identifier, newidentifier); }
                if (!string.IsNullOrEmpty(rtemplte.Required2) && rtemplte.Required2 == oldidentifier) { rtemplte.Required2 = newidentifier; Logger.Trace("Rename Request 2 '{0}' -> '{1}'.", rtemplte.Identifier, newidentifier); }
                if (!string.IsNullOrEmpty(rtemplte.Required3) && rtemplte.Required3 == oldidentifier) { rtemplte.Required3 = newidentifier; Logger.Trace("Rename Request 3 '{0}' -> '{1}'.", rtemplte.Identifier, newidentifier); }
                if (!string.IsNullOrEmpty(rtemplte.Required4) && rtemplte.Required4 == oldidentifier) { rtemplte.Required4 = newidentifier; Logger.Trace("Rename Request 4 '{0}' -> '{1}'.", rtemplte.Identifier, newidentifier); }
            }

            _templates.Remove(template.Identifier);
            template.Identifier = newidentifier;
            _templates.Add(template.Identifier, template);

            if (TemplateRenamed != null) TemplateRenamed.Invoke(oldidentifier, newidentifier);
        }

        /// <summary>
        /// Entfernt alle Templates
        /// </summary>
        public void Clear()
        {
            _templates.Clear();
        }

        /// <summary>
        /// Liefert alle Templates des angegebenen Types
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IEnumerable<T> GetTemplates<T>() where T : TemplateBase
        {
            return _templates.Values.OfType<T>();
        }

        /// <summary>
        /// Liefert alle Templates des angegebenen Types
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<TemplateBase> GetTemplates(Type type)
        {
            return _templates.Values.Where(m => m.GetType() == type);
        }

        /// <summary>
        /// Liefert eine Liste aller Bezeichenr die als Voraussetzung festgelegt sind
        /// </summary>
        /// <param name="template"></param>
        /// <returns></returns>
        public IEnumerable<TemplateBase> GetRequestTemplates(TemplateBase template)
        {
            if (template.Required1 != null && ContainsIdentifier(template.Required1)) yield return _templates[template.Required1];
            if (template.Required1 != null && ContainsIdentifier(template.Required1)) yield return _templates[template.Required1];
            if (template.Required1 != null && ContainsIdentifier(template.Required1)) yield return _templates[template.Required1];
            if (template.Required1 != null && ContainsIdentifier(template.Required1)) yield return _templates[template.Required1];
        }

        /// <summary>
        /// Liefert eine Auflistung aller Templates
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TemplateBase> GetTemplates()
        {
            return _templates.Values;
        }

        /// <summary>
        /// Liefert ein Template
        /// </summary>
        /// <typeparam name="TTemplate"></typeparam>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public TTemplate GetTemplate<TTemplate>(string identifier)
          where TTemplate : TemplateBase
        {
            var result = (TemplateBase)null;

            if (_templates.TryGetValue(identifier, out result))
            {
                try
                {
                    return (TTemplate)result;
                }
                catch (Exception ex)
                {
                    throw new TemplateException(string.Format("Template '{0}' could not convert in '{1}'", identifier, typeof(TTemplate).Name), ex);
                }
            }
            else
            {
                throw new TemplateException(string.Format("Template '{0}' Guid '{1}' not found", typeof(TTemplate).Name, identifier));
            }
        }

        /// <summary>
        /// Liefert ein Template
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        internal TemplateBase GetTemplate(string identifier)
        {
            var result = (TemplateBase)null;

            if (_templates.TryGetValue(identifier, out result))
            {
                return result;
            }
            else
            {
                throw new TemplateException(string.Format("Template identifier '{0}' not found", identifier));
            }
        }

        /// <summary>
        /// Liefert eine Auflistung aller Bezeichner
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetTemplateIdentifiers()
        {
            return _templates.Keys.ToArray();
        }

        /// <summary>
        /// Gibt an ob der Bezeichner besteht
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public bool ContainsIdentifier(string identifier)
        {
            return _templates.ContainsKey(identifier);
        }

        /// <summary>
        /// Gibt an ob das Template besteht
        /// </summary>
        /// <typeparam name="TTemplate"></typeparam>
        /// <returns></returns>
        public bool HasTemplates<TTemplate>()
             where TTemplate : TemplateBase
        {
            return GetTemplates<TTemplate>().Count() > 0;
        }

        /// <summary>
        /// Liefert die Anzahl der Templates
        /// </summary>
        public int Count { get { return _templates.Count; } }

        /// <summary>
        /// Liefert eine Auflistung aller Checksummen
        /// </summary>
        public TemplateChecksumCollection Checksums { get; private set; }

        /// <summary>
        /// Liefert eine Auflistung aller Template Typen
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Type> GetTemplateTypes()
        {
            return _templates.Values.Select(m => m.GetType()).Distinct();
        }

        /// <summary>
        /// Liefert die Template Bindungen
        /// </summary>
        public TemplateBindingHandler Bindings { get; private set; }

        #region Load & Save

        /// <summary>
        /// Speichert die aktuellen Templates in einer Datei
        /// </summary>
        /// <param name="filename"></param>
        public void Save(string filename)
        {
            Save(new FileInfo(filename));
        }

        /// <summary>
        /// Speichert die aktuellen Templates in einer Datei
        /// </summary>
        /// <param name="file"></param>
        public void Save(FileInfo file)
        {
            using (var stream = file.Open(FileMode.Create, FileAccess.Write))
            {
                var writer = XmlWriter.Create(stream);

                writer.WriteStartDocument();
                writer.WriteComment("Create Date : " + DateTime.Now);

                writer.WriteStartElement("templates");

                #region Assemblies

                var assemblies = Bindings.GetTemplateBindings().Select(m => m.Assembly).Distinct();
                foreach (var asm in assemblies)
                {
                    var codebase = new FileInfo(asm.Location);

                    writer.WriteStartElement("assembly");
                    writer.WriteAttributeString("filename", codebase.Name);
                    writer.WriteAttributeString("fullname", asm.GetName().Name);
                    writer.WriteAttributeString("version", asm.GetName().Version.ToString());
                    writer.WriteEndElement();
                }
                writer.Flush();

                #endregion

                #region Typen und Templates

                var bindings = Bindings.GetTemplateBindings();
                foreach (var binding in bindings)
                {
                    writer.WriteStartElement("binding");
                    writer.WriteAttributeString("assembly", binding.Assembly.GetName().Name);
                    writer.WriteAttributeString("type", binding.TemplateType.FullName);

                    #region Templates
                    var properties = binding.GetPropertyBindings();
                    var templates = GetTemplates(binding.TemplateType);

                    foreach (var template in templates)
                    {
                        writer.WriteStartElement("template");
                        //writer.WriteAttributeString("type", binding.TemplateType.FullName);

                        foreach (var property in properties)
                        {
                            var value = property.GetValue(template);
                            writer.WriteAttributeString(property.Name, value == null ? string.Empty : value.ToString());
                        }
                        writer.Flush();

                        writer.WriteEndElement();
                    }
                    writer.Flush();

                    #endregion

                    writer.WriteEndElement();
                }

                #endregion

                writer.WriteEndElement();

                writer.WriteEndDocument();

                writer.Flush();

                writer.Close();
                stream.Close();
            }
        }

        /// <summary>
        /// Läd die Templates aus einer Datei
        /// </summary>
        /// <param name="filename"></param>
        public void Load(string filename)
        {
            Load(PandoraContentStream.Read(filename, true));
        }

        /// <summary>
        /// Läd die Templates aus einer Datei
        /// </summary>
        /// <param name="file"></param>
        public void Load(FileInfo file)
        {
            Load(PandoraContentStream.Read(file, true));
        }

        /// <summary>
        /// Läd die Templates aus einer Datei
        /// </summary>
        /// <param name="stream"></param>
        public void Load(PandoraContentStream stream)
        {
            Logger.Normal("Template file loading '{0}'", stream.File.Name);

            if (!Checksums.Add(stream))
            {
                Logger.Warning("Template file '{0}' already loaded", stream.File.Name);
            }

            var xd = new XmlDocument();
            xd.Load(stream);

            #region Assemblies

            var assemblynodes = xd.SelectNodes("templates/assembly");
            foreach (XmlNode assemblynode in assemblynodes)
            {
                var filename = assemblynode.Attributes.GetValue("filename", string.Empty);
                var name = assemblynode.Attributes.GetValue("fullname", string.Empty);
                var version = assemblynode.Attributes.GetValue("version", string.Empty);
                var result = AppDomain.CurrentDomain.GetAssemblies().Where(asm => asm.GetName().Name == name).FirstOrDefault();

                if (result == null) { throw new TemplateException(string.Format("Assembly '{0}' not found", name)); }

                foreach (XmlNode bindingnode in xd.SelectNodes("templates/binding"))
                {
                    var typename = bindingnode.Attributes.GetValue("type", string.Empty);
                    if (string.IsNullOrEmpty(typename)) continue;

                    var binding = Bindings.GetBinding(typename);

                    if (binding == null) { Logger.Warning("Binding type {0} not found", typename); continue; }

                    foreach (XmlNode templatenode in bindingnode.SelectNodes("template"))
                    {
                        var template = (TemplateBase)Activator.CreateInstance(binding.TemplateType, new object[] { string.Empty });

                        foreach (var property in binding.GetPropertyBindings())
                        {
                            var value = templatenode.Attributes.GetValue(property.Name, string.Empty);

                            try
                            {
                                if (property.Property.PropertyType.IsEnum)
                                {
                                    property.SetValue(template, Enum.Parse(property.Property.PropertyType, value));
                                }
                                else
                                {
                                    var convertvalue = Convert.ChangeType(value, property.Property.PropertyType, System.Globalization.CultureInfo.InvariantCulture);
                                    property.SetValue(template, convertvalue);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Warning("Could not set value for '{0}'", property.Name);
                                Logger.Warning(ex.Message);
                            }
                        }

                        Add(template);
                    }
                }
            }

            stream.Close();

            #endregion

            if (TemplateLoaded != null) TemplateLoaded.Invoke();
        }

        #endregion

        /// <summary>
        /// Liefert die Ausbau-Templates des angegebenen Haupt-Templates
        /// </summary>
        /// <typeparam name="TTemplate"></typeparam>
        /// <typeparam name="TUpgradeTemplate"></typeparam>
        /// <param name="template"></param>
        /// <returns></returns>
        public IEnumerable<TUpgradeTemplate> GetUpgradeTemplates<TTemplate, TUpgradeTemplate>(TTemplate template)
            where TTemplate : UpgradableTemplateBase<TUpgradeTemplate>
            where TUpgradeTemplate : UpgradeTemplateBase<TTemplate>
        {
            return _templates.Values.OfType<TUpgradeTemplate>().Where(m => m.BaseTemplateIdentifier == template.Identifier).AsEnumerable();
        }
    }
}
