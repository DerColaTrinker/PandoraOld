﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates
{
    /// <summary>
    /// Stellt den Typ der Eigenschaften-Bindung dar
    /// </summary>
    public enum InternalTemplatePropertyTypes : int
    {
        /// <summary>
        /// Bezeichner
        /// </summary>
        Identifier,

        /// <summary>
        /// Resourcen Code
        /// </summary>
        ResourceCode,

        /// <summary>
        /// Voraussetzungs Bezeichner
        /// </summary>
        Required,

        /// <summary>
        /// Templatevorlage die als Basis gilt
        /// </summary>
        UpgradableTemplateBase,

        /// <summary>
        /// Ausbausstufe
        /// </summary>
        UpgradeLevel,

        /// <summary>
        /// Standard
        /// </summary>
        Default
    }
}
