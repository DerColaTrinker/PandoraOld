﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates
{
    /// <summary>
    /// Stellt eine Konfiguration der Displayfarbe der Spalte in der Tabellenansicht dar
    /// </summary>
    public enum ColumnColors
    {
        /// <summary>
        /// Standard
        /// </summary>
        Default = 0,

        /// <summary>
        /// Systemfarbe
        /// </summary>
        System,

        /// <summary>
        /// Erfordert
        /// </summary>
        Request,

        /// <summary>
        /// Schwarz
        /// </summary>
        Black,

        /// <summary>
        /// Wert 1
        /// </summary>
        Color1,

        /// <summary>
        /// Wert 2
        /// </summary>
        Color2,

        /// <summary>
        /// Wert 3
        /// </summary>
        Color3,

        /// <summary>
        /// Wert 4
        /// </summary>
        Color4,

        /// <summary>
        /// Wert 5
        /// </summary>
        Color5,

        /// <summary>
        /// Wert 6
        /// </summary>
        Color6,

        /// <summary>
        /// Wert 7
        /// </summary>
        Color7,

        /// <summary>
        /// Wert 8
        /// </summary>
        Color8,

        /// <summary>
        /// Wert 9
        /// </summary>
        Color9
    }
}
