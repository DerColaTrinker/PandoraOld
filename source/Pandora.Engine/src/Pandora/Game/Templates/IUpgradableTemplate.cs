﻿using System;

namespace Pandora.Game.Templates
{
    /// <summary>
    /// Stellt eine Schnittstelle bereit der die Ausbaustufe bei Spielstart bekannt gibt
    /// </summary>
    public interface IUpgradableTemplate
    {
        /// <summary>
        /// Liefert die Ausbaustufe bei Spielstart
        /// </summary>
        int LevelOnGameStart { get; set; }
    }
}
