﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates
{
    /// <summary>
    /// Ein Attribute, das eine Templatevorlage darstellt
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class TemplateAttribute : Attribute
    {
        /// <summary>
        /// Erstellt eine neue Instanz der TemplateAttribute-Klasse
        /// </summary>
        /// <param name="description"></param>
        public TemplateAttribute(string description)
        {
            Description = description;
        }

        /// <summary>
        /// Liefert die Beschreibung des Feldes
        /// </summary>
        public string Description { get; private set; }
    }
}
