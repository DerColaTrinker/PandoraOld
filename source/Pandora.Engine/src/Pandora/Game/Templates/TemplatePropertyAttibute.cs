﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates
{
    /// <summary>
    /// Ein Attribut das eine Template-Eigenschaft konfiguriert
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class TemplatePropertyAttribute : Attribute
    {
        internal TemplatePropertyAttribute(InternalTemplatePropertyTypes type)
        {
            TemplatePropertyType = type;
            Name = string.Empty;

            switch (type)
            {
                case InternalTemplatePropertyTypes.Identifier: ColumnColor = ColumnColors.System; break;
                case InternalTemplatePropertyTypes.ResourceCode: ColumnColor = ColumnColors.Black; break;
                case InternalTemplatePropertyTypes.Required: ColumnColor = ColumnColors.Request; break;
                case InternalTemplatePropertyTypes.UpgradeLevel: ColumnColor = ColumnColors.Black; break;
                default: ColumnColor = ColumnColors.Default; break;
            }
        }

        internal TemplatePropertyAttribute(InternalTemplatePropertyTypes type, string name)
        {
            TemplatePropertyType = type;
            Name = name;

            switch (type)
            {
                case InternalTemplatePropertyTypes.Identifier: ColumnColor = ColumnColors.System; break;
                case InternalTemplatePropertyTypes.ResourceCode: ColumnColor = ColumnColors.Black; break;
                case InternalTemplatePropertyTypes.Required: ColumnColor = ColumnColors.Request; break;
                case InternalTemplatePropertyTypes.UpgradeLevel: ColumnColor = ColumnColors.Black; break;
                default: ColumnColor = ColumnColors.Default; break;
            }
        }

        /// <summary>
        /// Erstellt eine neue Instanz der TemplatePropertyAttribute-Klasse
        /// </summary>
        public TemplatePropertyAttribute()
            : this(InternalTemplatePropertyTypes.Default)
        {
            SpecialPropertyType = SpecialPropertyTypes.Default;
        }

        /// <summary>
        /// Erstellt eine neue Instanz der TemplatePropertyAttribute-Klasse
        /// </summary>
        /// <param name="type"></param>
        public TemplatePropertyAttribute(SpecialPropertyTypes type)
            : this(InternalTemplatePropertyTypes.Default)
        {
            SpecialPropertyType = type;
        }

        /// <summary>
        /// Erstellt eine neue Instanz der TemplatePropertyAttribute-Klasse
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        public TemplatePropertyAttribute(SpecialPropertyTypes type, string name)
            : this(InternalTemplatePropertyTypes.Default, name)
        {
            SpecialPropertyType = type;
        }

        /// <summary>
        /// Liefert den Template Eigenschaft Typ
        /// </summary>
        public InternalTemplatePropertyTypes TemplatePropertyType { get; private set; }

        /// <summary>
        /// Liefert den Spezialfeld Typ
        /// </summary>
        public SpecialPropertyTypes SpecialPropertyType { get; private set; }

        /// <summary>
        /// Liefert die Sortierung oder legt sie fest
        /// </summary>
        public int SortOrder { get; set; }

        /// <summary>
        /// Liefert die Spaltenfarbe oder legt sie fest
        /// </summary>
        public ColumnColors ColumnColor { get; set; }

        /// <summary>
        /// Liefert den Namen
        /// </summary>
        public string Name { get; private set; }
    }
}
