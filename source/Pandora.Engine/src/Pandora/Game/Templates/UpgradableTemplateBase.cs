﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates
{
    /// <summary>
    /// Stellt ein Template dar das Ausgebaut werden kann
    /// </summary>
    public abstract class UpgradableTemplateBase : TemplateBase, IUpgradableTemplate
    {
        internal UpgradableTemplateBase(string identifier)
            : base(identifier)
        { }

        /// <summary>
        /// Liefert den Type der Ausbau-Templates
        /// </summary>
        public UpgradeTemplateBase UpgradeTemplate { get; internal set; }

        /// <summary>
        /// Liefert den Type des Ausbau Templates
        /// </summary>
        public Type UpgradeTemplateType { get; internal set; }

        /// <summary>
        /// Liefert eine Ausbaustufe die bei Spielstart direkt gebaut werden soll
        /// </summary>
        [TemplateProperty(InternalTemplatePropertyTypes.UpgradeLevel, SortOrder = 4)]
        public int LevelOnGameStart { get; set; }
    }

    /// <summary>
    /// Stellt ein Template dar das Ausgebaut werden kann
    /// </summary>
    /// <typeparam name="TUpgradeTemplate"></typeparam>
    public abstract class UpgradableTemplateBase<TUpgradeTemplate> : UpgradableTemplateBase
        where TUpgradeTemplate : UpgradeTemplateBase
    {
        /// <summary>
        /// Erstellt eine neue Instanz der UpgradableTemplateBase-Klasse
        /// </summary>
        /// <param name="identifier"></param>
        public UpgradableTemplateBase(string identifier)
            : base(identifier)
        {
            UpgradeTemplateType = typeof(TUpgradeTemplate);
        }

        /// <summary>
        /// Liefert den Type des Ausbau-Templates
        /// </summary>
        public new TUpgradeTemplate UpgradeTemplate { get { return (TUpgradeTemplate)base.UpgradeTemplate; } internal set { base.UpgradeTemplate = value; } }
    }
}
