﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.Templates
{
    /// <summary>
    /// Stellt eine Templatevorlage dar, die als Upgrade fungiert
    /// </summary>
    public abstract class UpgradeTemplateBase : TemplateBase
    {
        internal UpgradeTemplateBase(string identifier)
            : base(identifier)
        { }

        /// <summary>
        /// Liefert den Bezeichenr der Basis-Template-Vorlage
        /// </summary>
        public string BaseTemplateIdentifier { get; set; }

        internal Type UpgradableTemplateType { get; set; }

        /// <summary>
        /// Liefert das Tempalte der Basis
        /// </summary>
        public UpgradableTemplateBase UpgradableTemplate { get; internal set; }

        /// <summary>
        /// Liefert die Ausbaustufe
        /// </summary>
        [TemplateProperty(InternalTemplatePropertyTypes.UpgradeLevel)]
        public int UpgradeLevel { get; internal set; }
    }

    /// <summary>
    /// Stellt eine Templatevorlage dar, die als Upgrade fungiert
    /// </summary>
    /// <typeparam name="TUpgradableTemplate"></typeparam>
    public abstract class UpgradeTemplateBase<TUpgradableTemplate> : UpgradeTemplateBase
        where TUpgradableTemplate : UpgradableTemplateBase
    {
        /// <summary>
        /// Erstellt eine neue Instanz der UpgradeTemplateBase-Klasse
        /// </summary>
        /// <param name="identifier"></param>
        public UpgradeTemplateBase(string identifier)
            : base(identifier)
        {
            UpgradableTemplateType = typeof(TUpgradableTemplate);
        }

        /// <summary>
        /// Liefert das Tempalte der Basis
        /// </summary>
        public new TUpgradableTemplate UpgradableTemplate { get { return (TUpgradableTemplate)base.UpgradableTemplate; } internal set { base.UpgradableTemplate = value; } }
    }
}
