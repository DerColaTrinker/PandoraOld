﻿using Pandora.Runtime.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates.Bindings
{
    /// <summary>
    /// Verwaltet Konfigurierbare Eigenschaften einer Templatevorlage
    /// </summary>
    public sealed class TemplateBindingHandler
    {
        private Dictionary<Type, TemplateBinding> _templatebindings = new Dictionary<Type, TemplateBinding>();

        internal TemplateBindingHandler()
        {

        }

        /// <summary>
        /// Untersucht den angegebenen Type ob der ein Template ist und erstellt eine Bindung
        /// </summary>
        /// <param name="type"></param>
        public void ScanClass(Type type)
        {
            if (_templatebindings.ContainsKey(type)) return;

            if (type.IsClass & type.IsSubclassOf(typeof(TemplateBase)))
            {
                var templateattribute = AttributeUtility.GetAttribute<TemplateAttribute>(type);
                if (templateattribute != null)
                {
                    var binding = new TemplateBinding(type.Assembly, type, templateattribute);
                    _templatebindings.Add(type, binding);
                }
            }
        }

        /// <summary>
        /// Untersucht eine Assembly nach einer Templatevorlage und erstellt eine Bindung
        /// </summary>
        /// <param name="asm"></param>
        public void ScanAssembly(Assembly asm)
        {
            // Suche innerhalb der Assembly nach allen Klassen die das TemplateAttribute besitzen
            var templatetypes = from type in asm.GetTypes()
                                where type.IsClass & type.IsSubclassOf(typeof(TemplateBase))                            // Alle Template Vorlagen lassen sich von TemplateBase ableiten
                                let templateattribute = AttributeUtility.GetAttribute<TemplateAttribute>(type)          // Suche nach dem Attribut das eine Klasse zu einem Template macht
                                where templateattribute != null
                                select new TemplateBinding(asm, type, templateattribute);

            foreach (var item in templatetypes)
            {
                if (_templatebindings.ContainsKey(item.TemplateType)) continue;
                _templatebindings.Add(item.TemplateType, item);
            }
        }

        /// <summary>
        /// Untersucht alle geladenen Assemblies auf Templatevorlagen und erstellt die Bindung
        /// </summary>
        public void ScanAssembly()
        {
            foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
            {
                ScanAssembly(asm);
            }
        }

        /// <summary>
        /// Liefert eine Auflistung aller Template-Bindungen
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TemplateBinding> GetTemplateBindings()
        {
            return _templatebindings.Values.AsEnumerable();
        }

        /// <summary>
        /// Liefert die Template-Bindung des angegebenen Types
        /// </summary>
        /// <param name="typename"></param>
        /// <returns></returns>
        public TemplateBinding GetBinding(string typename)
        {
            return _templatebindings.Values.Where(m => m.TemplateType.FullName == typename).FirstOrDefault();
        }
    }
}
