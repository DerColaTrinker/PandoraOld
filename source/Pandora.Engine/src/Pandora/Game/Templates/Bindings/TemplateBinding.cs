﻿using Pandora.Runtime.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Pandora.Game.Templates.Bindings
{
    /// <summary>
    /// Stellt eine Template Bindung dar
    /// </summary>
    public sealed class TemplateBinding
    {
        private HashSet<TemplateBindingProperty> _bindingproperties;

        internal TemplateBinding(System.Reflection.Assembly asm, Type type, TemplateAttribute templateattribute)
        {
            this.Assembly = asm;
            this.TemplateType = type;
            this.Description = templateattribute.Description;

            IsUpgradable = type.IsSubclassOf(typeof(UpgradableTemplateBase));
            IsUpgrade = type.IsSubclassOf(typeof(UpgradeTemplateBase));

            ScanProperties();
        }

        private void ScanProperties()
        {
            // Suche nach allen Eigenschaften die das TemplatePropertyAttribut besitzen.
            var properties = from property in TemplateType.GetProperties()
                             let propertyattribute = AttributeUtility.GetAttribute<TemplatePropertyAttribute>(property)
                             where propertyattribute != null
                             orderby propertyattribute.TemplatePropertyType, propertyattribute.SortOrder
                             select new TemplateBindingProperty(this, property, propertyattribute);

            _bindingproperties = new HashSet<TemplateBindingProperty>(properties);
        }

        /// <summary>
        /// Liefert das Assembly
        /// </summary>
        public Assembly Assembly { get; set; }

        /// <summary>
        /// Liefert den Type
        /// </summary>
        public Type TemplateType { get; set; }

        /// <summary>
        /// Liefert die Beschreibung
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gibt an ob das Template Ausbaustufen besitzt
        /// </summary>
        public bool IsUpgradable { get; set; }

        /// <summary>
        /// Gibt an ob die Vorlage eine Ausbaustufe ist
        /// </summary>
        public bool IsUpgrade { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Description;
        }

        /// <summary>
        /// Liefert eine Auflistung aller Vorlagen-Eigenschaften Bindungen
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TemplateBindingProperty> GetPropertyBindings()
        {
            return _bindingproperties.AsEnumerable();
        }
    }
}
