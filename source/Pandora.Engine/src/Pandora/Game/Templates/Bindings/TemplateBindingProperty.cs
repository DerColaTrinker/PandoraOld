﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Pandora.Game.Templates.Bindings
{
    /// <summary>
    /// Stellt eine Template-Eigenschaft-Bindung dar
    /// </summary>
    public sealed class TemplateBindingProperty
    {
        internal TemplateBindingProperty(TemplateBinding templateBinding, PropertyInfo property, TemplatePropertyAttribute propertyattribute)
        {
            TemplateBinding = templateBinding;
            Property = property;
            PropertyAttribute = propertyattribute;

            if (!string.IsNullOrEmpty(PropertyAttribute.Name))
                Name = PropertyAttribute.Name;  // Alternative Bezeichnung
            else
                Name = property.Name;
        }

        /// <summary>
        /// Liefert die Template-Bindung
        /// </summary>
        public TemplateBinding TemplateBinding { get; private set; }

        /// <summary>
        /// Liefert die Eingenschaft
        /// </summary>
        public PropertyInfo Property { get; private set; }

        /// <summary>
        /// Liefert das Attibute der Eigenschaft
        /// </summary>
        public TemplatePropertyAttribute PropertyAttribute { get; private set; }

        /// <summary>
        /// Liefert den Namen
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// Ruft den Wert der Eigenschaft ab
        /// </summary>
        /// <param name="template"></param>
        /// <returns></returns>
        public object GetValue(TemplateBase template)
        {
            return Property.GetValue(template);
        }

        /// <summary>
        /// Speichert den Wert in die Eigenschaft
        /// </summary>
        /// <param name="template"></param>
        /// <param name="value"></param>
        public void SetValue(TemplateBase template, object value)
        {
            Property.SetValue(template, value);
        }
    }
}
