﻿using Pandora.Game.Globalisation;
using Pandora.Game.Network;
using Pandora.Game.OpCodeDelegation;
using Pandora.Game.Players;
using Pandora.Game.Templates;
using Pandora.Runtime.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game
{
    /// <summary>
    /// Stellt eine Instanz eines Spiels dar
    /// </summary>
    public abstract partial class GameControllerBase : Instance
    {
        private Dictionary<string, Connection> _connections = new Dictionary<string, Connection>();
        private Queue<PacketIn> _incommingpackets = new Queue<PacketIn>(200);

        private void InternalRemoveConnection(Connection connection, ConnectionDisconnectBySide side)
        {
            lock (_connections)
            {
                Logger.Normal("Client disconnect '{0}'", connection.RemoteAddress);
                _connections.Remove(connection.GUID);

                connection.ConnectionDisconnect -= InternalRemoveConnection;
                connection.ConnectionReceive -= PushPacketInQueue;
            }
        }

        private void InternalAddConnection(Connection connection)
        {
            lock (_connections)
            {
                Logger.Normal("Client connect '{0}'", connection.RemoteAddress);
                _connections.Add(connection.GUID, connection);

                connection.ConnectionDisconnect += InternalRemoveConnection;
                connection.ConnectionReceive += PushPacketInQueue;
            }
        }

        internal void PushPacketInQueue(PacketIn reader)
        {
            lock (_incommingpackets)
            {
                _incommingpackets.Enqueue(reader);
            }
        }

        /// <summary>
        /// Verarbeitet ein eingegangenes Paket
        /// </summary>
        public void DispatchMessage()
        {
            lock (_incommingpackets)
            {
                if (_incommingpackets.Count == 0) return;

                OpCodes.InvokePacket(_incommingpackets.Dequeue());
            }
        }

        /// <summary>
        /// Sendet ein Paket an den Spieler
        /// </summary>
        /// <param name="player"></param>
        /// <param name="pout"></param>
        public void Send(Player player, PacketOut pout)
        {
            //TODO: Hier ggf. prüfen ob es ein Solo Spiel ist.
            player.Connection.Send(pout);
        }

        /// <summary>
        /// Sendet ein Paket an alle Spieler
        /// </summary>
        /// <param name="pout"></param>
        public void Broadcast(PacketOut pout)
        {
            foreach (var connection in _connections.Values)
            {
                connection.Send(pout);
            }
        }

        #region OPCODES

        private void Handle_CMSG_GAMECONTROLLER_REGISTER(PacketIn reader)
        {
            var writer = new PacketOut(GameControllerOpCodes.SMSG_GAMECONTROLLER_REGISTER);
            var rtversion = new Version(reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32());
            var gcversion = new Version(reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32());

            if (rtversion.Major == Runtime.Version.Major & rtversion.Minor == Runtime.Version.Minor)
            {
                if (gcversion == GameControllerVersion)
                {
                    var tplchecksumsA = new List<string>();
                    var tplcountA = reader.ReadInt32();
                    for (int t = 0; t < tplcountA; t++)
                    {
                        tplchecksumsA.Add(reader.ReadString());
                    }

                    var tplresultA = tplchecksumsA.Except(Templates.Checksums.GetChecksums());
                    if (tplresultA.Count() == 0)
                    {

                    }
                    else
                    {
                        writer.WriteEnum<GameControllerRegisterResult>(GameControllerRegisterResult.InvalidTemplates);
                    }

                    var modcount = reader.ReadInt32();
                    var checkcounter = 0;
                    for (int i = 0; i < modcount; i++)
                    {
                        var modname = reader.ReadString();
                        var mod = GetModifications().Where(m => m.Name == modname & m.Version.Major == reader.ReadInt32() & m.Version.Minor == reader.ReadInt32() & m.Version.Build == reader.ReadInt32() & m.Version.Revision == reader.ReadInt32()).FirstOrDefault();

                        if (mod != null)
                        {
                            var tplchecksumsB = new List<string>();
                            var tplcountB = reader.ReadInt32();
                            for (int n = 0; n < tplcountB; n++)
                            {
                                tplchecksumsB.Add(reader.ReadString());
                            }

                            var tplresultB = tplchecksumsB.Except(mod.Templates.Checksums.GetChecksums());
                            if (tplresultB.Count() == 0)
                            {
                                checkcounter++;
                            }
                            else
                            {
                                writer.WriteEnum<GameControllerRegisterResult>(GameControllerRegisterResult.InvalidModificationTemplates);
                                writer.WriteString(modname);
                            }
                        }
                        else
                        {
                            writer.WriteEnum<GameControllerRegisterResult>(GameControllerRegisterResult.UnkownModification);
                            writer.WriteString(modname);
                        }
                    }

                    if(checkcounter == _modifications.Count)
                    {
                        writer.WriteEnum<GameControllerRegisterResult>(GameControllerRegisterResult.Confirmed);
                    }
                    else
                    {
                        writer.WriteEnum<GameControllerRegisterResult>(GameControllerRegisterResult.InvalidModificationCount);
                    }
                }
                else
                {
                    writer.WriteEnum<GameControllerRegisterResult>(GameControllerRegisterResult.InvalidGameControllerVersion);
                }
            }
            else
            {
                writer.WriteEnum<GameControllerRegisterResult>(GameControllerRegisterResult.InvalidRuntimeVersion);
            }

            reader.Connection.Send(writer);
        }

        private void Handle_SMSG_GAMECONTROLLER_REGISTER(PacketIn reader)
        {

        }

        #endregion
    }
}
