﻿using Pandora.Game.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game
{
    /// <summary>
    /// Ein Delegate das im ObjectAccessor verwendet wird
    /// </summary>
    /// <param name="worldobject"></param>
    public delegate void ObjectAccessorDelegate(GameObjectBase worldobject);

    /// <summary>
    /// Ein Speicher für alle Spielobjekte die in der Spielwelt verwendet werden
    /// </summary>
    public sealed class ObjectAccessor
    {
        private Dictionary<uint, GameObjectBase> _objects = new Dictionary<uint, GameObjectBase>();
        private GameControllerBase _world;

        /// <summary>
        /// Wird aufgerufen wenn ein Spielobjekt eingefügt wird
        /// </summary>
        public event ObjectAccessorDelegate WorldObjectAdded;

        /// <summary>
        /// Wird aufgerufen wenn ein Spielobjekt entfernt wird
        /// </summary>
        public event ObjectAccessorDelegate WorldObjectRemoved;

        /// <summary>
        /// Wird aufgerufen wenn ein Spielobjekt umbenannt wird
        /// </summary>
        public event ObjectAccessorDelegate WorldObjectRenamed;

        /// <summary>
        /// Wird aufgerufen wenn dem Spielobjekt ein neues Template zugewiesen wird
        /// </summary>
        public event ObjectAccessorDelegate WorldObjectChangeTemplate;

        internal ObjectAccessor(GameControllerBase instance)
        {

            _world = instance;
            Logger.Debug("Object Accessor created");
        }

        /// <summary>
        /// Legt den Template des Objekts fest
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="template"></param>
        public void SetTemplate(GameObjectBase obj, TemplateBase template)
        {
            obj.Template = template;

            if (WorldObjectChangeTemplate != null) WorldObjectChangeTemplate.Invoke(obj);
        }

        /// <summary>
        /// Legt den Namen des Objekts fest
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="name"></param>
        public void SetName(GameObjectBase obj, string name)
        {
            obj.Name = name;

            if (WorldObjectRenamed != null) WorldObjectRenamed.Invoke(obj);
        }

        /// <summary>
        /// Fügt dem Speicher ein neues Objekt hinzu
        /// </summary>
        /// <param name="obj"></param>
        public void Add(GameObjectBase obj)
        {
            Logger.Trace("Add object [{0,-4}]:{1}", obj.ObjectID, obj.Name);

            if (obj.ObjectID == 0)
            {
                obj.ObjectID = GetNextObjectID();
            }
            else
            {
                if (_objects.ContainsKey(obj.ObjectID) && _objects[obj.ObjectID] != null)
                    throw new ArgumentException(string.Format("ObjectID '{0}' already exists", obj.ObjectID));
            }

            obj.Instance = _world;
            _objects[obj.ObjectID] = obj;
            obj.OnLoad();

            if (WorldObjectAdded != null) WorldObjectAdded.Invoke(obj);
        }

        /// <summary>
        /// Fügt ein neuen Objekt besonders schnell hinzu
        /// </summary>
        /// <param name="obj"></param>
        public void AddQuick(GameObjectBase obj)
        {
            if (obj.ObjectID == 0)
            {
                obj.ObjectID = _objects.Keys.Max() + 1;
            }
            else
            {
                if (_objects.ContainsKey(obj.ObjectID)) throw new PandoraException(string.Format("Object ID '{0}' already exists", obj.ObjectID));
            }

            Add(obj);
        }

        /// <summary>
        /// Entfernt ein Objekt aus dem Speicher
        /// </summary>
        /// <param name="obj"></param>
        public void Remove(GameObjectBase obj)
        {
            if (_objects.ContainsKey(obj.ObjectID))
            {
                Logger.Trace("Remove object [{0,-4}]:{1}", obj.ObjectID, obj.Name);
                _objects[obj.ObjectID] = null;

                obj.ObjectID = 0;

                if (WorldObjectRemoved != null) WorldObjectRemoved.Invoke(obj);
            }
            else
            {
                Logger.Trace("Remove object not found [{0,-4}]:{1}", obj.ObjectID, obj.Name);
            }
        }

        internal uint GetNextObjectID()
        {
            if (_objects.Count == 0)
            {
                // Start bei 1
                return (uint)1;
            }
            else
            {
                var result = from r in _objects
                             where r.Value == null
                             select r.Key;

                if (result.Count() == 0)
                {
                    // Keine freie ObjectID mehr vorhanden
                    return _objects.Keys.Max() + 1;
                }
                else
                {
                    // Nächste freie ObjectID zurückliefern
                    return result.FirstOrDefault();
                }
            }
        }

        /// <summary>
        /// Liefert eine Auflistung aller Spielobjekte
        /// </summary>
        /// <returns></returns>
        public IEnumerable<GameObjectBase> GetObjects()
        {
            return _objects.Values.AsEnumerable();
        }

        /// <summary>
        /// Liefert eine Auflistung aller Spielobjekte des angegebenen Types
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <returns></returns>
        public IEnumerable<TObject> GetObjects<TObject>()
            where TObject : GameObjectBase
        {
            return _objects.Values.OfType<TObject>();
        }

        /// <summary>
        /// Liefert ein Spielobjekt anhand der Objekt ID
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <param name="objectid"></param>
        /// <returns></returns>
        public TObject GetObject<TObject>(uint objectid)
            where TObject : GameObjectBase
        {
            return (TObject)_objects[objectid];
        }

        /// <summary>
        /// Liefert ein Objekt
        /// </summary>
        /// <param name="objectid"></param>
        /// <returns></returns>
        public GameObjectBase GetObject(uint objectid)
        {
            return _objects[objectid];
        }

        /// <summary>
        /// Erstellt eine neue Instanz des Objekts
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <returns></returns>
        public TObject CreateInstance<TObject>()
            where TObject : GameObjectBase
        {
            var instance = (TObject)Activator.CreateInstance(typeof(TObject), true);
            instance.Instance = _world;

            return instance;
        }
    }
}