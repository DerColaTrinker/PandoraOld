﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora
{
    /// <summary>
    /// Eine Ausnahme die in der Engine auftritt
    /// </summary>
    [Serializable]
    public class PandoraGameException : PandoraException
    {
        /// <summary>
        /// Erstellt eine neue Instanz der PandoraRuntimeException-Klasse
        /// </summary>
        public PandoraGameException()
        { }

        /// <summary>
        /// Erstellt eine neue Instanz der PandoraRuntimeException-Klasse
        /// </summary>
        /// <param name="message"></param>
        public PandoraGameException(string message)
            : base(message)
        { }

        /// <summary>
        /// Erstellt eine neue Instanz der PandoraRuntimeException-Klasse
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public PandoraGameException(string message, Exception inner)
            : base(message, inner)
        { }

        /// <summary>
        /// Erstellt eine neue Instanz der PandoraRuntimeException-Klasse
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected PandoraGameException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        { }
    }
}
