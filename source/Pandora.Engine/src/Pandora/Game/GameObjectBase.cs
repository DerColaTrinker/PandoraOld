﻿using Pandora.Game.Templates;
using Pandora.Runtime.Collections;
using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game
{
    /// <summary>
    /// Stellt ein Spielobejkt dar
    /// </summary>
    public abstract class GameObjectBase : IGameObject, IOwnerCollection, ISerializable
    {
        private string _name = string.Empty;
        private TemplateBase _template;

        /// <summary>
        /// Erstellt eine neue Instnaz der WorldObjectBase-Klasse
        /// </summary>
        protected internal GameObjectBase()
        { }

        /// <summary>
        /// Liefert den Type-Namen des Objekts
        /// </summary>
        public string ObjectTypename { get { return GetType().FullName; } }

        /// <summary>
        /// Liefert eine eindeutige ID des Objekts
        /// </summary>
        public uint ObjectID { get; internal set; }

        /// <summary>
        /// Liefert die Rasse an die das Objekt gebunden ist
        /// </summary>
        public RaceBase OwnerRace { get; protected internal set; }

        /// <summary>
        /// Liefert den Namen des Objekts
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                if (Instance != null && ObjectID > 0) Instance.Accessor.SetName(this, value);
            }
        }

        /// <summary>
        /// Liefert das Template des Objekts
        /// </summary>
        public TemplateBase Template
        {
            get { return _template; }
            set
            {
                _template = value;
                if (Instance != null && ObjectID > 0) Instance.Accessor.SetTemplate(this, value);
            }
        }

        /// <summary>
        /// Gibt an ob das Objekt ein Template besitzt
        /// </summary>
        public bool HasTemplate { get { return Template != null; } }

        /// <summary>
        /// Liefert den World Manager
        /// </summary>
        public GameControllerBase Instance { get; internal set; }

        /// <summary>
        /// Wird aufgerufen wenn das Objekt am ObjectAccessor angemeldet wird
        /// </summary>
        protected internal virtual void OnLoad()
        {
            // Nichts machen
        }

        /// <summary>
        /// Wird aufgerufen wenn das WorldUpate ausgeführt wird
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        public virtual void WorldUpdate(float ms, float s)
        {
            // Nichts machen
        }

        #region Serialisierung

        /// <summary>
        /// Serialisiert das Objekt in den Stream
        /// </summary>
        /// <param name="writer"></param>
        public virtual void Serialize(IWriter writer)
        {
            writer.WriteString(ObjectTypename);
            writer.WriteUInt32(ObjectID);
            writer.WriteString(Name);

            //UNDONE: Löst beim speichern einen Fehler aus
            // writer.WriteUInt32(OwnerRace.ObjectID);

            writer.WriteBool(HasTemplate);
            if (HasTemplate) writer.WriteString(Template.Identifier);
        }

        /// <summary>
        /// Deserialisiert das Objekt aus dem Stream
        /// </summary>
        /// <param name="reader"></param>
        public virtual void Deserialize(IReader reader)
        {
            ObjectID = reader.ReadUInt32();
            Name = reader.ReadString();

            OwnerRace = Instance.Accessor.GetObject<RaceBase>(reader.ReadUInt32());

            var hastemplate = reader.ReadBool();
            if (hastemplate)
            {
                var identifier = reader.ReadString();
                Template = Instance.Templates.GetTemplate(identifier);
            }
        }

        /// <summary>
        /// Erstellt ein neues Objekt anhand des Streams
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <param name="instance"></param>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static TObject CreateObject<TObject>(GameControllerBase instance, IReader reader)
            where TObject : GameObjectBase
        {
            var typename = reader.ReadString();
            var type = FindType(typename).FirstOrDefault();

            if (type == null) throw new ArgumentException(string.Format("Object-Type '{0}' unkown", typename));

            var objectinstance = (TObject)null;

            try
            {
                objectinstance = (TObject)Activator.CreateInstance(type);
            }
            catch (Exception ex)
            {
                throw new PandoraException(string.Format("Could not create object '{0}'", typename), ex);
            }

            objectinstance.Deserialize(reader);

            instance.Accessor.Add(objectinstance);

            return objectinstance;
        }

        /// <summary>
        /// Erstellt ein neues Objekt anhand des Streams
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static object CreateObject(GameControllerBase instance, IReader reader)
        {
            var typename = reader.ReadString();
            var type = FindType(typename).FirstOrDefault();

            if (type == null) throw new ArgumentException(string.Format("Object-Type '{0}' unkown", typename));

            var objectinstance = (GameObjectBase)null;

            try
            {
                objectinstance = (GameObjectBase)Activator.CreateInstance(type);
            }
            catch (Exception ex)
            {
                throw new PandoraException(string.Format("Could not create object '{0}'", typename), ex);
            }

            objectinstance.Instance = instance;
            objectinstance.Deserialize(reader);

            instance.Accessor.Add(objectinstance);

            return objectinstance;
        }

        private static IEnumerable<Type> FindType(string typename)
        {
            return from asm in AppDomain.CurrentDomain.GetAssemblies()
                   from t in asm.GetTypes()
                   where t.FullName == typename
                   select t;
        }

        #endregion
    }
}
