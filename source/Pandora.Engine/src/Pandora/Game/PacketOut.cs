﻿using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game
{
    /// <summary>
    /// Stellt ein ausgehendes Datenpaket dar
    /// </summary>
    public sealed class PacketOut : Packet, IPacketWriter
    {
        private readonly Encoding __encoder = Encoding.UTF8;

        /// <summary>
        /// Erstellt eine neue Instanz der PacketOut-Klasse
        /// </summary>
        internal PacketOut() : base()
        { }

        internal PacketOut(GameControllerOpCodes opcode) :base()
        {
            WriteUInt32((uint)opcode);
        }

        /// <summary>
        /// Erstellt ein neues Ausgehendes Datenpaket
        /// </summary>
        /// <param name="opcode"></param>
        public PacketOut(uint opcode)
          : base()
        {
            WriteUInt32(opcode);
        }

        /// <summary>
        /// Schreibt einen Boolschen Wert
        /// </summary>
        /// <param name="value"></param>
        public void WriteBool(bool value)
        {
            CheckEndOfBuffer(1);
            var bytes = BitConverter.GetBytes(value);
            PackageBuffer[Position + 0] = bytes[0];
            Position += 1;
        }

        /// <summary>
        /// Schreibt ein Byte
        /// </summary>
        /// <param name="value"></param>
        public void WriteByte(byte value)
        {
            CheckEndOfBuffer(1);
            PackageBuffer[Position + 0] = value;
            Position += 1;
        }

        /// <summary>
        /// Schreibt ein unsigniertes Byte
        /// </summary>
        /// <param name="value"></param>
        public void WriteSByte(sbyte value)
        {
            CheckEndOfBuffer(1);
            var bytes = BitConverter.GetBytes(value);
            PackageBuffer[Position + 0] = bytes[0];
            Position += 1;
        }

        /// <summary>
        /// Schreibt ein Byte-Array
        /// </summary>
        /// <param name="value"></param>
        public void WriteBytes(byte[] value)
        {
            CheckEndOfBuffer(value.Length);

            for (int index = 0; index < value.Length; index++)
                PackageBuffer[Position + index] = value[index];

            Position += value.Length;
        }

        /// <summary>
        /// Schreibt eine Int16 Zahl
        /// </summary>
        /// <param name="value"></param>
        public void WriteInt16(Int16 value)
        {
            CheckEndOfBuffer(2);
            var bytes = BitConverter.GetBytes(value);
            PackageBuffer[Position + 0] = bytes[0];
            PackageBuffer[Position + 1] = bytes[1];
            Position += 2;
        }

        /// <summary>
        /// Schreibt eine Int32 Zahl
        /// </summary>
        /// <param name="value"></param>
        public void WriteInt32(Int32 value)
        {
            CheckEndOfBuffer(4);
            var bytes = BitConverter.GetBytes(value);
            PackageBuffer[Position + 0] = bytes[0];
            PackageBuffer[Position + 1] = bytes[1];
            PackageBuffer[Position + 2] = bytes[2];
            PackageBuffer[Position + 3] = bytes[3];
            Position += 4;
        }

        /// <summary>
        /// Schreibt eine Int64 Zahl
        /// </summary>
        /// <param name="value"></param>
        public void WriteInt64(Int64 value)
        {
            CheckEndOfBuffer(8);
            var bytes = BitConverter.GetBytes(value);
            PackageBuffer[Position + 0] = bytes[0];
            PackageBuffer[Position + 1] = bytes[1];
            PackageBuffer[Position + 2] = bytes[2];
            PackageBuffer[Position + 3] = bytes[3];
            PackageBuffer[Position + 4] = bytes[4];
            PackageBuffer[Position + 5] = bytes[5];
            PackageBuffer[Position + 6] = bytes[6];
            PackageBuffer[Position + 7] = bytes[7];
            Position += 8;
        }

        /// <summary>
        /// Schreibt eine UInt16 Zahl
        /// </summary>
        /// <param name="value"></param>
        public void WriteUInt16(UInt16 value)
        {
            CheckEndOfBuffer(2);
            var bytes = BitConverter.GetBytes(value);
            PackageBuffer[Position + 0] = bytes[0];
            PackageBuffer[Position + 1] = bytes[1];
            Position += 2;
        }

        /// <summary>
        /// Schreibt eine UInt32 Zahl
        /// </summary>
        /// <param name="value"></param>
        public void WriteUInt32(UInt32 value)
        {
            CheckEndOfBuffer(4);
            var bytes = BitConverter.GetBytes(value);
            PackageBuffer[Position + 0] = bytes[0];
            PackageBuffer[Position + 1] = bytes[1];
            PackageBuffer[Position + 2] = bytes[2];
            PackageBuffer[Position + 3] = bytes[3];
            Position += 4;
        }

        /// <summary>
        /// Schreibt eine UInt64 Zahl
        /// </summary>
        /// <param name="value"></param>
        public void WriteUInt64(UInt64 value)
        {
            CheckEndOfBuffer(8);
            var bytes = BitConverter.GetBytes(value);
            PackageBuffer[Position + 0] = bytes[0];
            PackageBuffer[Position + 1] = bytes[1];
            PackageBuffer[Position + 2] = bytes[2];
            PackageBuffer[Position + 3] = bytes[3];
            PackageBuffer[Position + 4] = bytes[4];
            PackageBuffer[Position + 5] = bytes[5];
            PackageBuffer[Position + 6] = bytes[6];
            PackageBuffer[Position + 7] = bytes[7];
            Position += 8;
        }

        /// <summary>
        /// Schreibt eine Double Zahl
        /// </summary>
        /// <param name="value"></param>
        public void WriteDouble(double value)
        {
            CheckEndOfBuffer(8);
            var bytes = BitConverter.GetBytes(value);
            PackageBuffer[Position + 0] = bytes[0];
            PackageBuffer[Position + 1] = bytes[1];
            PackageBuffer[Position + 2] = bytes[2];
            PackageBuffer[Position + 3] = bytes[3];
            PackageBuffer[Position + 4] = bytes[4];
            PackageBuffer[Position + 5] = bytes[5];
            PackageBuffer[Position + 6] = bytes[6];
            PackageBuffer[Position + 7] = bytes[7];
            Position += 8;
        }

        /// <summary>
        /// Schreibt eine Float Zahl
        /// </summary>
        /// <param name="value"></param>
        public void WriteFloat(float value)
        {
            CheckEndOfBuffer(4);
            var bytes = BitConverter.GetBytes(value);
            PackageBuffer[Position + 0] = bytes[0];
            PackageBuffer[Position + 1] = bytes[1];
            PackageBuffer[Position + 2] = bytes[2];
            PackageBuffer[Position + 3] = bytes[3];
            Position += 4;
        }

        /// <summary>
        /// Schreibt einen String
        /// </summary>
        /// <param name="value"></param>
        public void WriteString(string value)
        {
            var bytes = __encoder.GetBytes(value);

            CheckEndOfBuffer(bytes.Length);

            for (int index = 0; index < bytes.Length; index++)
                PackageBuffer[Position + index] = bytes[index];

            Position += value.Length;

            WriteByte(0);   // Terminatorzeichen
        }

        /// <summary>
        /// Schreibt ein Zeichen
        /// </summary>
        /// <param name="value"></param>
        public void WriteChar(char value)
        {
            var charvalue = (Int16)value;

            WriteInt16(charvalue);
        }

        /// <summary>
        /// Schreibt ein Datum
        /// </summary>
        /// <param name="value"></param>
        public void WriteDateTime(DateTime value)
        {
            CheckEndOfBuffer(8);
            var bytes = BitConverter.GetBytes(value.ToBinary());
            PackageBuffer[Position + 0] = bytes[0];
            PackageBuffer[Position + 1] = bytes[1];
            PackageBuffer[Position + 2] = bytes[2];
            PackageBuffer[Position + 3] = bytes[3];
            PackageBuffer[Position + 4] = bytes[4];
            PackageBuffer[Position + 5] = bytes[5];
            PackageBuffer[Position + 6] = bytes[6];
            PackageBuffer[Position + 7] = bytes[7];
            Position += 8;
        }

        /// <summary>
        /// Schreibt den Wert eines Enums in das Datenpaket
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        public void WriteEnum<T>(T value)
        {
            var type = typeof(T);
            if (type.IsEnum)
            {
                var basetype = Enum.GetUnderlyingType(type);
                switch (basetype.Name)
                {
                    case "Int16": WriteInt16((short)(object)value); break;
                    case "Int32": WriteInt32((int)(object)value); break;
                    case "Int64": WriteInt64((long)(object)value); break;
                    case "UInt16": WriteUInt16((ushort)(object)value); break;
                    case "UInt32": WriteUInt32((uint)(object)value); break;
                    case "UInt64": WriteUInt64((ulong)(object)value); break;
                    case "Byte": WriteByte((byte)(object)value); break;
                    case "SByte": WriteSByte((sbyte)(object)value); break;
                    default:
                        break;
                }
            }
            else
            {
                throw new InvalidCastException("Failed to write binary value.");
            }
        }

        /// <summary>
        /// Liefert die Position des Datenpakets
        /// </summary>
        public override int Position
        {
            get
            {
                return base.Position;
            }
            internal set
            {
                base.Position = value;
                base.Length = value;
            }
        }
    }
}
