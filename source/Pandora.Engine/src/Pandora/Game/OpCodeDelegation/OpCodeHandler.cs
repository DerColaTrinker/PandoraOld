﻿using Pandora.Game.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.OpCodeDelegation
{
    /// <summary>
    /// Liste mit RPC Zielen
    /// </summary>
    public sealed class OpCodeHandler
    {
        private Dictionary<uint, OpCodeContainer> _targets = new Dictionary<uint, OpCodeContainer>();

        internal OpCodeHandler()
        { }

        /// <summary>
        /// Meldet ein OpCode am System an
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="status"></param>
        /// <param name="target"></param>
        public void Register(uint opcode, PlayerStatus status, OpCodeTargetDelegate target)
        {
            _targets[opcode] = new OpCodeContainer(opcode, status, target);
        }

        /// <summary>
        /// Meldet einen OpCode an
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="status"></param>
        /// <param name="target"></param>
        public void Register(GameControllerOpCodes opcode, PlayerStatus status, OpCodeTargetDelegate target)
        {
            Register((uint)opcode, status, target);
        }

        internal void InvokePacket( PacketIn reader)
        {
            var target = (OpCodeContainer)null;
            var opcode = reader.ReadUInt32();

            if (_targets.TryGetValue(opcode, out target))
            {
                target.Target.Invoke( reader);
            }
            else
            {
                Logger.Warning("OpCode unkown opcode '{0}' from '{1}'", opcode, reader.Connection.RemoteAddress);
                reader.Connection.Disconnect();
            }
        }
    }
}
