﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.OpCodeDelegation
{
    /// <summary>
    /// Ein Delegate das für RPC Aufrufe verwendet wird
    /// </summary>
    /// <param name="reader"></param>
    public delegate void OpCodeTargetDelegate(PacketIn reader);
}
