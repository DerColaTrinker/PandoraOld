﻿using Pandora.Game.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.OpCodeDelegation
{
    class OpCodeContainer
    {
        public OpCodeContainer(uint opcode, PlayerStatus status, OpCodeTargetDelegate target)
        {
            OpCode = opcode;
            PlayerStatus = status;
            Target = target;
        }

        public uint OpCode { get; private set; }

        public OpCodeTargetDelegate Target { get; private set; }

        public PlayerStatus PlayerStatus { get; private set; }
    }
}
