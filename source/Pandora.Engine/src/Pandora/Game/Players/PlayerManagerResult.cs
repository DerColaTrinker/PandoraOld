﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.Players
{
    /// <summary>
    /// Ergebnisse als Serverantworten 
    /// </summary>
    public enum PlayerManagerResult : byte
    {
        /// <summary>
        /// Zu viele Spieler
        /// </summary>
        ToManyPlayer,

        /// <summary>
        /// Ungültige Identität
        /// </summary>
        InvalidIdentity,

        /// <summary>
        /// Ungültige Runtime Version
        /// </summary>
        InvalidRuntimeVersion,

        /// <summary>
        /// Ungültige Modifikationen
        /// </summary>        
        InvalidModifications,

        /// <summary>
        /// Spielername wird bereits verwendet
        /// </summary>
        NameAlreadyExists,

        /// <summary>
        /// Erfolgreich
        /// </summary>
        Success,

        /// <summary>
        /// Ungültiger Spielstatus
        /// </summary>
        InvalidGameStatus
    }
}
