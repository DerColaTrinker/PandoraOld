﻿using Pandora.Game.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Players
{
    /// <summary>
    /// Ein Delegate das im PlayerManager verwendet wird
    /// </summary>
    /// <param name="player"></param>
    /// <param name="result"></param>
    public delegate void PlayerManagerDelegate(Player player, PlayerRegisterResult result);

    /// <summary>
    /// Stellt eine Verwaltung für Spieler dar
    /// </summary>
    public sealed class PlayerManager : GameHandlerBase
    {
        /// <summary>
        /// Wird aufgerufen wenn ein Ergebnis der Spieleranmeldung vorliegt
        /// </summary>
        public event PlayerManagerDelegate PlayerManagerEvent;

        private HashSet<Player> _players = new HashSet<Player>();

        internal PlayerManager()
        {

        }

        internal override void InternalInitialize(GameControllerBase instance)
        {
            base.InternalInitialize(instance);
        }

        /// <summary>
        /// Erstellt einen neuen Spieler
        /// </summary>
        /// <param name="name">Spielername</param>
        /// <returns>Gibt eine Instanz der Player-Klasse zurück</returns>
        public void CreatePlayer(string name)
        {
            //TODO: Spieler erzeugen
        }

        void OnPlayerEvent(Player player, PlayerRegisterResult status)
        {
            if (PlayerManagerEvent != null) PlayerManagerEvent.Invoke(player, status);
        }

        /// <summary>
        /// Liefert die Anzahl der Maximalen Spieler
        /// </summary>
        public int MaxPlayers { get { return Instance.GameConfiguration.GetValue("MaxPlayers", 8); } }

        /// <summary>
        /// Liefert den lokalen Spieler
        /// </summary>
        public Player LocalPlayer { get; private set; }


        private void InternalRemoveConnection(Player player)
        {
            if (_players.Remove(player))
            {
                //TODO: Ein Spieler hat das Spiel verlassen... AI übernimmt oder Spielerdaten aus der Welt löschen
                Logger.Debug("Player disconnect '{0}'", player.Name);
            }
        }

        /// <summary>
        /// Liefert eine Auflistung aller Spieler
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Player> GetPlayers()
        {
            return _players.AsEnumerable();
        }

        /// <summary>
        /// Liefert eine Liste aller Spieler die den angegebenen Status besitzen
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public IEnumerable<Player> GetPlayers(PlayerStatus status)
        {
            return _players.Where(m => m.Status == status);
        }
    }
}
