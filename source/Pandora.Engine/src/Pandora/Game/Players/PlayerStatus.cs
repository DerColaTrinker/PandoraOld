﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.Players
{
    /// <summary>
    /// Spielerstatus
    /// </summary>
    public enum PlayerStatus
    {
        /// <summary>
        /// Nicht gesetzt
        /// </summary>
        Unkown,

        /// <summary>
        /// Lobby
        /// </summary>
        Lobby
    }
}
