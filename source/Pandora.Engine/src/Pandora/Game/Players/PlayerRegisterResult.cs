﻿#pragma warning disable CS1591

namespace Pandora.Game.Players
{
    public enum PlayerRegisterResult
    {
        Success,
        TooManyPlayers,
        PlayernameAlreadyExists,
        InvalidGameStatus,
        InvalidRuntimeVersion,
        InvalidGameControllerVersion
    }
}