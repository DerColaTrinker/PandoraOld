﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#pragma warning disable CS1591 

namespace Pandora.Game
{
    public enum GameControllerOpCodes : uint
    {
        CMSG_GAMECONTROLLER_REGISTER,
        SMSG_GAMECONTROLLER_REGISTER
    }
}
