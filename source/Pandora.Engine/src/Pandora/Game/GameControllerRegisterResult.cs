﻿namespace Pandora.Game
{
    internal enum GameControllerRegisterResult
    {
        InvalidTemplates,
        InvalidModificationTemplates,
        UnkownModification,
        InvalidGameControllerVersion,
        InvalidRuntimeVersion,
        InvalidModificationCount,
        Confirmed
    }
}