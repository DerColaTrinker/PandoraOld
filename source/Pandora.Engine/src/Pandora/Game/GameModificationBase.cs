﻿using Pandora.Game.Globalisation;
using Pandora.Game.Templates;
using Pandora.Interactions;
using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game
{
    /// <summary>
    /// Wird verwendet um eine Systemaktualisierung auszulösen
    /// </summary>
    /// <param name="ms"></param>
    /// <param name="s"></param>
    public delegate void SystemUpdateDelegate(float ms, float s);

    /// <summary>
    /// Delegate das bei der Initialisierung verwendet wird
    /// </summary>
    public delegate void ModificationInitializeDelegate(ref bool result);

    /// <summary>
    /// Delegate das bei start des Modul auslöst
    /// </summary>
    public delegate void ModuleStartDelegate();

    /// <summary>
    /// Stellt ein Engine Runtime Modul dar
    /// </summary>
    public abstract class GameModificationBase
    {
        /// <summary>
        /// Wird aufgerufen wenn eine Systemaktualisierung stattfindet
        /// </summary>
        public event SystemUpdateDelegate SystemUpdate;

        /// <summary>
        /// Wird aufgerufen, wenn das Module initialisiert wird
        /// </summary>
        public event ModificationInitializeDelegate Initialized;

        /// <summary>
        /// Wird aufgerufen wenn das Spielsystem gestartet wird
        /// </summary>
        public event ModuleStartDelegate Started;

        /// <summary>
        /// Wird aufgerufen wenn das Spielsystem angehalten wird
        /// </summary>
        public event ModuleStartDelegate Stopped;

        /// <summary>
        /// Erstellt eine neue Instanz der PandoraEngineModule-Klasse
        /// </summary>
        public GameModificationBase()
        { }

        internal bool IntenralInitialize(GameControllerBase instance)
        {
            Instance = instance;

            var result = OnInitialize();

            if (result) IsInitialized = true;

            return result;
        }

        /// <summary>
        /// Wird aufgerufen wenn das Spielsystem gestartet wurde
        /// </summary>
        protected internal virtual void OnStart()
        {
            if (Started != null) Started.Invoke();
        }

        /// <summary>
        /// Wird aufgerufen wenn das Spielsystem angehalten wurde
        /// </summary>
        protected internal virtual void OnStop()
        {
            if (Stopped != null) Stopped.Invoke();
        }

        /// <summary>
        /// Wird aufgerufen wenn ein Savegame erstellt wird
        /// </summary>
        /// <param name="writer"></param>
        protected internal virtual void OnSave(IWriter writer)
        {

        }

        /// <summary>
        /// Wird aufgerufen wenn ein Savegame geladen wird
        /// </summary>
        /// <param name="reader"></param>
        protected internal virtual void OnLoad(IReader reader)
        {

        }
        
        /// <summary>
        /// Löst das erstellen der Spielwelt aus
        /// </summary>
        /// <returns></returns>
        public virtual bool CreateWorld()
        {
            return OnCreateWorld();
        }

        /// <summary>
        /// Wird aufgerufen wenn die Spielwelt erstellt werden muss
        /// </summary>
        /// <returns></returns>
        protected virtual bool OnCreateWorld()
        {
            return false;
        }

        /// <summary>
        /// Wird aufgerufen wenn die Engine das Modul initialisiert
        /// </summary>
        protected virtual bool OnInitialize()
        {
            var result = true;

            if (Initialized != null) Initialized.Invoke(ref result);

            return result;
        }

        /// <summary>
        /// Aktualisiert das Spielsystem
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected internal virtual void OnSystemUpdate(float ms, float s)
        {
            if (SystemUpdate != null) SystemUpdate.Invoke(ms, s);
        }

        /// <summary>
        /// Meldet einen Handler an der Spielinstanz an
        /// </summary>
        /// <param name="handler"></param>
        protected void RegisterHandler(GameHandlerBase handler)
        {
            Instance.RegisterHandler(handler);
        }

        /// <summary>
        /// Gibt an ob das Module initialisiert wurde
        /// </summary>
        public bool IsInitialized { get; private set; }

        /// <summary>
        /// Liefert die Engine auf dem das Modul ausgeführt wird
        /// </summary>
        public GameControllerBase Instance { get; private set; }

        /// <summary>
        /// Liefert die Templatedatenbank
        /// </summary>
        public TemplateManager Templates { get { return Instance.Templates; } }

        /// <summary>
        /// Liefert die Übersetzungsdatenbank
        /// </summary>
        public TranslationManager Translations { get { return Instance.Translations; } }

        /// <summary>
        /// Liefert das Interaction-System
        /// </summary>
        public InteractionManager Interaction { get { return Instance.Runtime.Interaction; } }

        /// <summary>
        /// Liefert das Verzeichnis in dem die Modifikation gefunden wurde
        /// </summary>
        public string Directory { get; internal set; }

        /// <summary>
        /// Liefert den Namen der Modifikation
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// Liefert die Version der Modifikation
        /// </summary>
        public abstract Version Version { get; }
    }
}
