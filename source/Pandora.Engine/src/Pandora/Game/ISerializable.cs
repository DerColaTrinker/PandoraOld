﻿using Pandora.Runtime.IO;

namespace Pandora.Game
{
    /// <summary>
    /// Schnittstelle die das Serialisieren von Objekten unterstützt
    /// </summary>
    public interface ISerializable
    {
        /// <summary>
        /// Wird aufgerufen wenn ein Objekt deserialisiert werden muss
        /// </summary>
        /// <param name="reader"></param>
        void Deserialize(IReader reader);

        /// <summary>
        /// Wird aufgerufen wenn ein Objekt serialisiert werden muss
        /// </summary>
        /// <param name="writer"></param>
        void Serialize(IWriter writer);
    }
}