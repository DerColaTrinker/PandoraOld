﻿using Pandora.Game.Network;
using Pandora.Game.Globalisation;
using Pandora.Game.Players;
using Pandora.Game.Templates;
using Pandora.Interactions;
using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.OpCodeDelegation;

namespace Pandora.Game
{
    /// <summary>
    /// Ein Delegate das im Handler verwendet wird
    /// </summary>
    /// <param name="handler"></param>
    public delegate void EngineHandlerDelegate(GameHandlerBase handler);

    /// <summary>
    /// Stellt die Basis eines Handlers bereit
    /// </summary>
    public abstract class GameHandlerBase : ISerializable
    {
        /// <summary>
        /// Wird ausgelöst wenn der Handler geladen wurde
        /// </summary>
        public event EngineHandlerDelegate Initialized;

        /// <summary>
        /// Wird ausgelöst wenn das Spielsystem gestartet wird
        /// </summary>
        public event EngineHandlerDelegate Started;

        /// <summary>
        /// Wird ausgelöst wenn das Spielsytem angehalten wird
        /// </summary>
        public event EngineHandlerDelegate Stopped;

        /// <summary>
        /// Wird ausgelöst wenn das Spielsystem aktualisiert wird
        /// </summary>
        public event SystemUpdateDelegate SystemUpdate;

        /// <summary>
        /// Liefert den angegebenen Handler
        /// </summary>
        /// <typeparam name="THandler"></typeparam>
        /// <returns></returns>
        protected THandler GetHandler<THandler>()
            where THandler : GameHandlerBase
        {
            return Instance.GetHandler<THandler>();
        }

        internal virtual void InternalInitialize(GameControllerBase instance)
        {
            Instance = instance;

            OnInitialize();
        }

        internal virtual void InternalStart()
        {
            OnStart();
        }

        /// <summary>
        /// Löst das Load-Ereignis aus
        /// </summary>
        protected internal virtual void OnInitialize()
        {
            if (Initialized != null) Initialized.Invoke(this);
        }

        /// <summary>
        /// Löst das Started-Ereignis aus
        /// </summary>
        protected internal virtual void OnStart()
        {
            if (Started != null) Started.Invoke(this);
        }

        /// <summary>
        /// Löst das Stopped-Ereignis aus
        /// </summary>
        protected internal virtual void OnStop()
        {
            if (Stopped != null) Stopped.Invoke(this);
        }

        /// <summary>
        /// Löst das SystemUpdate-Ereignis aus
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        protected internal virtual void OnSystemUpdate(float ms, float s)
        {
            if (SystemUpdate != null) SystemUpdate.Invoke(ms, s);
        }

        /// <summary>
        /// Interne Funktion die das entfernen von System Handlers verhindern soll
        /// </summary>
        internal virtual bool NotRemovable { get { return false; } }

        /// <summary>
        /// Liefert die Spielwelt
        /// </summary>
        public virtual GameControllerBase Instance { get; private set; }

        #region Direktzugriff auf die Runtime

        /// <summary>
        /// Liefert die Runtime
        /// </summary>
        public PandoraRuntime Runtime { get { return Instance.Runtime; } }

        /// <summary>
        /// Liefert das Interaction-Subsystem
        /// </summary>
        public InteractionManager Interaction { get { return Instance.Runtime.Interaction; } }

        #endregion

        #region Direktzugriffe auf die Spiel-Instanz

        /// <summary>
        /// Liefert die Objektdatenbank
        /// </summary>
        public ObjectAccessor Accessor { get { return Instance.Accessor; } }

        /// <summary>
        /// Liefert die Templatedatenbank
        /// </summary>
        public TemplateManager Templates { get { return Instance.Templates; } }

        /// <summary>
        /// Liefert die Übersetzungsdatenbank
        /// </summary>
        public TranslationManager Translations { get { return Instance.Translations; } }

        /// <summary>
        /// Liefert den Handler für die Spielerverwaltung
        /// </summary>
        public PlayerManager Players { get { return Instance.Players; } }

        /// <summary>
        /// Liefert einen Handler der ObCodes verwaltet
        /// </summary>
        public OpCodeHandler OpCodes { get { return Instance.OpCodes; } }

        #endregion

        #region Senden

        /// <summary>
        /// Sendet ein Paket an den Spieler
        /// </summary>
        /// <param name="player"></param>
        /// <param name="pout"></param>
        public void Send(Player player, PacketOut pout)
        {
            Instance.Send(player, pout);
        }

        /// <summary>
        /// Sendet ein Paket an alle Spieler
        /// </summary>
        /// <param name="pout"></param>
        public void Broadcast(PacketOut pout)
        {
            Instance.Broadcast(pout);
        }

        #endregion

        #region Serialisieren

        /// <summary>
        /// Beginnt mit dem serialisieren des Handlers
        /// </summary>
        /// <param name="writer"></param>
        public virtual void Serialize(IWriter writer)
        { }

        /// <summary>
        /// Beginnt mit dem deserialisieren des Handlers
        /// </summary>
        /// <param name="reader"></param>
        public virtual void Deserialize(IReader reader)
        { }

        #endregion
    }
}