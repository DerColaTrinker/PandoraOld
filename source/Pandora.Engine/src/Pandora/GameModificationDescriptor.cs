﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Pandora.Runtime.Extensions;
using System.Reflection;
using Pandora.Game;

namespace Pandora
{
    /// <summary>
    /// Stellt eine Beschreibung einer Modifikation dar
    /// </summary>
    public sealed class GameModificationDescriptor
    {
        /// <summary>
        /// Liefert den Namen der Modifikation
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Liefert den Autor der Modifikation
        /// </summary>
        public string Author { get; internal set; }

        /// <summary>
        /// Liefert die Webseite der Modifikation
        /// </summary>
        public string Website { get; internal set; }

        /// <summary>
        /// Liefert den Pfad der Modul Datei
        /// </summary>
        public string AssemblyFilename { get; internal set; }

        /// <summary>
        /// Liefert eine FileInfo Instanz zur Modul Datei
        /// </summary>
        public FileInfo AssemblyFile { get; internal set; }

        /// <summary>
        /// Liefert eine FileInfo Instanz zur Konfigurationsdatei
        /// </summary>
        public FileInfo DescriptionFile { get; internal set; }

        /// <summary>
        /// Liefert den Basispfad der Modifikation
        /// </summary>
        public string ModificationPath { get; internal set; }

        internal XmlDocument XmlDocument { get; set; }

        internal XmlNode ModificationNode { get; set; }

        /// <summary>
        /// Liefert die Version der Modifikation
        /// </summary>
        public Version Version { get; internal set; }

        /// <summary>
        /// Liefert den Modul Dateinamen
        /// </summary>
        public string AssemblyName { get; internal set; }

        internal bool SetupXMLInformation(FileInfo file)
        {
            DescriptionFile = file;
            ModificationPath = file.Directory.FullName;

            if (file != null)
            {
                XmlDocument = new XmlDocument();
                try
                {
                    XmlDocument.Load(file.OpenRead());
                }
                catch (Exception ex)
                {
                    Logger.Warning("Modification description '...{0}\\{1}.xml' invalid XML format", file.Directory.Name, file.Name);
                    Logger.Warning(ex.Message);
                    return false;
                }

                ModificationNode = XmlDocument.SelectSingleNode("modification");
                if (ModificationNode == null)
                {
                    Logger.Warning("Modification description '...{0}\\{1}.xml' invalid XML format", file.Directory.Name, file.Name);
                    return false;
                }

                Name = ModificationNode.Attributes.GetValue("name", string.Empty);
                Author = ModificationNode.Attributes.GetValue("author", string.Empty);
                Website = ModificationNode.Attributes.GetValue("website", string.Empty);
                AssemblyFilename = ModificationNode.Attributes.GetValue("assemblyfile", string.Empty);

                // Name und Assemblyfile sind wichtig
                if (string.IsNullOrEmpty(Name))
                {
                    Logger.Warning("Modification name '...{0}\\{1}.xml' not set", file.Directory.Name, file.Name);
                    return false;
                }

                if (string.IsNullOrEmpty(AssemblyFilename))
                {
                    Logger.Warning("Modification assemblyfile '...{0}\\{1}.xml' not set", file.Directory.Name, file.Name);
                    return false;
                }

                AssemblyFile = new FileInfo(Path.Combine(file.Directory.FullName, AssemblyFilename));

                return true;
            }

            Logger.Warning("Modification file '...{0}\\{1}.xml' not found", file.Directory.Name, file.Name);

            return false;
        }

        internal bool SetupAssemblyInformation()
        {
            if (!AssemblyFile.Exists)
            {
                Logger.Warning("Modification '{1}' Assemblyfile '{0}' not found", AssemblyFile.Name, Name);
                return false;
            }

            var refasm = (Assembly)null;
            try
            {
                refasm = Assembly.ReflectionOnlyLoadFrom(AssemblyFile.FullName);
            }
            catch (Exception)
            {
                Logger.Warning("Modification '{1}' Assemblyfile '{0}' loading failed", AssemblyFile.Name, Name);
                return false;
            }

            Version = refasm.GetName().Version;
            AssemblyName = refasm.FullName;
            RefenreceAssembly = refasm;

            return true;
        }

        internal Assembly RefenreceAssembly { get; private set; }

        /// <summary>
        /// Liefert die Modifikations-Type Module
        /// </summary>
        [Obsolete]
        public Type[] GameModifications { get; internal set; }

        /// <summary>
        /// Liefert die Spiel-Handler Module
        /// </summary>
        [Obsolete]
        public Type[] GameHandlers { get; internal set; }
    }
}
