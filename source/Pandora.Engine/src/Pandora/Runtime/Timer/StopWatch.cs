﻿using Pandora.Runtime.SFML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime.Timer
{
    /// <summary>
    /// Stellt eine Stopuhr für die Zeitmessung dar
    /// </summary>
    public sealed class StopWatch : PointerObject
    {
        /// <summary>
        /// Erstellt eine neue Instanz der StopWatch-Klasse
        /// </summary>
        public StopWatch()
        {
            InternalPointer = UnsafeNativeMethods.sfClock_create();
        }

        /// <summary>
        /// Setzt die Zeitmessung zurück
        /// </summary>
        public void Reset()
        {
            UnsafeNativeMethods.sfClock_restart(Pointer);
        }

        /// <summary>
        /// Liefert ein Frame mit den genauen Zeitinformationen
        /// </summary>
        /// <returns></returns>
        public Frame GetElapsedTime()
        {
            return UnsafeNativeMethods.sfClock_getElapsedTime(Pointer);
        }

        /// <summary>
        /// Zerstört das Object und gibt eden Pointer an das System zurück
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Destroy(bool disposing)
        {
            UnsafeNativeMethods.sfClock_destroy(Pointer);

            base.Destroy(disposing);
        }
    }
}