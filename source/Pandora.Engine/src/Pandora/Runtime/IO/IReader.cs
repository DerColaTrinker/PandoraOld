﻿using System;

namespace Pandora.Runtime.IO
{
    /// <summary>
    /// Ein eingehendes Datenpaket
    /// </summary>
    public interface IReader
    {
        /// <summary>
        /// Liest ein Boolschen Wert
        /// </summary>
        /// <returns></returns>
        bool ReadBool();

        /// <summary>
        /// Liest ein Byte
        /// </summary>
        /// <returns></returns>
        byte ReadByte();

        /// <summary>
        /// Liest ein unsigniertes Byte
        /// </summary>
        /// <returns></returns>
        sbyte ReadSByte();

        /// <summary>
        /// Kopiert das Array
        /// </summary>
        /// <param name="result"></param>
        void ReadBytes(ref byte[] result);

        /// <summary>
        /// Lieft eine Anzahl Bytes
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        byte[] ReadBytes(int length);

        /// <summary>
        /// Liest eine Int16 Zahl
        /// </summary>
        /// <returns></returns>
        Int16 ReadInt16();

        /// <summary>
        /// Liest eine Int32 Zahl
        /// </summary>
        /// <returns></returns>
        Int32 ReadInt32();

        /// <summary>
        /// Liest eine Int64 Zahl
        /// </summary>
        /// <returns></returns>
        Int64 ReadInt64();

        /// <summary>
        /// Liest eine UInt16 Zahl
        /// </summary>
        /// <returns></returns>
        UInt16 ReadUInt16();

        /// <summary>
        /// Liest eine UInt32 Zahl
        /// </summary>
        /// <returns></returns>
        UInt32 ReadUInt32();

        /// <summary>
        /// Liest eine UInt64 Zahl
        /// </summary>
        /// <returns></returns>
        UInt64 ReadUInt64();

        /// <summary>
        /// Liest eine Double Zahl
        /// </summary>
        /// <returns></returns>
        Double ReadDouble();

        /// <summary>
        /// Liest eine Float Zahl
        /// </summary>
        /// <returns></returns>
        float ReadFloat();

        /// <summary>
        /// Liest einen String
        /// </summary>
        /// <returns></returns>
        string ReadString();

        /// <summary>
        /// Liest ein Zeichen
        /// </summary>
        /// <returns></returns>
        char ReadChar();

        /// <summary>
        /// Liest ein Datum
        /// </summary>
        /// <returns></returns>
        DateTime ReadDateTime();

        /// <summary>
        /// Liest ein Wert aus dem Datenpaket das direkt in ein Enum konvertiert wird
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T ReadEnum<T>();
    }
}
