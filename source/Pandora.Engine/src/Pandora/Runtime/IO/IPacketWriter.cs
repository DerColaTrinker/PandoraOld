﻿using System;

namespace Pandora.Runtime.IO
{
    /// <summary>
    /// Stellt ein ausgehendes Datenpaket dar
    /// </summary>
    public interface IPacketWriter : IPacket, IWriter
    {
    
    }
}
