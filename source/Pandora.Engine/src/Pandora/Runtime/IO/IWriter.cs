﻿using System;

namespace Pandora.Runtime.IO
{
    /// <summary>
    /// Stellt ein ausgehendes Datenpaket dar
    /// </summary>
    public interface IWriter 
    {
        /// <summary>
        /// Schreibt einen Boolschen Wert
        /// </summary>
        /// <param name="value"></param>
        void WriteBool(bool value);

        /// <summary>
        /// Schreibt ein Byte
        /// </summary>
        /// <param name="value"></param>
        void WriteByte(byte value);

        /// <summary>
        /// Schreibt ein unsigniertes Byte
        /// </summary>
        /// <param name="value"></param>
        void WriteSByte(sbyte value);

        /// <summary>
        /// Schreibt ein Byte-Array
        /// </summary>
        /// <param name="value"></param>
        void WriteBytes(byte[] value);

        /// <summary>
        /// Schreibt eine Int16 Zahl
        /// </summary>
        /// <param name="value"></param>
        void WriteInt16(Int16 value);

        /// <summary>
        /// Schreibt eine Int32 Zahl
        /// </summary>
        /// <param name="value"></param>
        void WriteInt32(Int32 value);

        /// <summary>
        /// Schreibt eine Int64 Zahl
        /// </summary>
        /// <param name="value"></param>
        void WriteInt64(Int64 value);

        /// <summary>
        /// Schreibt eine UInt16 Zahl
        /// </summary>
        /// <param name="value"></param>
        void WriteUInt16(UInt16 value);

        /// <summary>
        /// Schreibt eine UInt32 Zahl
        /// </summary>
        /// <param name="value"></param>
        void WriteUInt32(UInt32 value);

        /// <summary>
        /// Schreibt eine UInt64 Zahl
        /// </summary>
        /// <param name="value"></param>
        void WriteUInt64(UInt64 value);

        /// <summary>
        /// Schreibt eine Double Zahl
        /// </summary>
        /// <param name="value"></param>
        void WriteDouble(double value);

        /// <summary>
        /// Schreibt eine Float Zahl
        /// </summary>
        /// <param name="value"></param>
        void WriteFloat(float value);

        /// <summary>
        /// Schreibt einen String
        /// </summary>
        /// <param name="value"></param>
        void WriteString(string value);

        /// <summary>
        /// Schreibt ein Zeichen
        /// </summary>
        /// <param name="value"></param>
        void WriteChar(char value);

        /// <summary>
        /// Schreibt ein Datum
        /// </summary>
        /// <param name="value"></param>
        void WriteDateTime(DateTime value);

        /// <summary>
        /// Schreibt den Wert eines Enums in das Datenpaket
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        void WriteEnum<T>(T value);
    }
}
