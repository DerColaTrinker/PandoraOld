﻿using System;

namespace Pandora.Runtime.IO
{
    /// <summary>
    /// Ein eingehendes Datenpaket
    /// </summary>
    public interface IPacketReader : IReader
    {
        /// <summary>
        /// Liefert den OpCode
        /// </summary>
        /// <returns></returns>
        uint ReadOpCode();
    }
}
