﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#pragma warning disable 1591

namespace Pandora.Runtime.IO
{
    public sealed class StreamBinaryWriter : IWriter
    {
        private static readonly Encoding __encoder = Encoding.UTF8;

        private byte[] _buffer = new byte[16];

        public StreamBinaryWriter(Stream stream)
        {
            BaseStream = stream;
        }

        private void WriteBuffer(int length)
        {
            BaseStream.Write(_buffer, 0, length);
        }

        public Stream BaseStream { get; private set; }

        #region IWriter Member

        public void WriteBool(bool value)
        {
            _buffer[0] = (byte)(value ? 1 : 0);
            BaseStream.Write(_buffer, 0, 1);
        }

        public void WriteByte(byte value)
        {
            _buffer[0] = value;
            BaseStream.Write(_buffer, 0, 1);
        }

        public void WriteSByte(sbyte value)
        {
            _buffer[0] = (byte)value;
            BaseStream.Write(_buffer, 0, 1);
        }

        public void WriteBytes(byte[] value)
        {
            BaseStream.Write(value, 0, value.Length);
        }

        public void WriteInt16(short value)
        {
            _buffer[0] = (byte)value;
            _buffer[1] = (byte)(value >> 8);
            BaseStream.Write(this._buffer, 0, 2);
        }

        public void WriteInt32(int value)
        {
            _buffer[0] = (byte)value;
            _buffer[1] = (byte)(value >> 8);
            _buffer[2] = (byte)(value >> 16);
            _buffer[3] = (byte)(value >> 24);
            BaseStream.Write(this._buffer, 0, 4);
        }

        public void WriteInt64(long value)
        {
            _buffer[0] = (byte)value;
            _buffer[1] = (byte)(value >> 8);
            _buffer[2] = (byte)(value >> 16);
            _buffer[3] = (byte)(value >> 24);
            _buffer[4] = (byte)(value >> 32);
            _buffer[5] = (byte)(value >> 40);
            _buffer[6] = (byte)(value >> 48);
            _buffer[7] = (byte)(value >> 56);
            BaseStream.Write(this._buffer, 0, 8);
        }

        public void WriteUInt16(ushort value)
        {
            _buffer[0] = (byte)value;
            _buffer[1] = (byte)(value >> 8);
            BaseStream.Write(this._buffer, 0, 2);
        }

        public void WriteUInt32(uint value)
        {
            _buffer[0] = (byte)value;
            _buffer[1] = (byte)(value >> 8);
            _buffer[2] = (byte)(value >> 16);
            _buffer[3] = (byte)(value >> 24);
            BaseStream.Write(this._buffer, 0, 4);
        }

        public void WriteUInt64(ulong value)
        {
            _buffer[0] = (byte)value;
            _buffer[1] = (byte)(value >> 8);
            _buffer[2] = (byte)(value >> 16);
            _buffer[3] = (byte)(value >> 24);
            _buffer[4] = (byte)(value >> 32);
            _buffer[5] = (byte)(value >> 40);
            _buffer[6] = (byte)(value >> 48);
            _buffer[7] = (byte)(value >> 56);
            BaseStream.Write(this._buffer, 0, 8);
        }

        public unsafe void WriteDouble(double value)
        {
            var num = (ulong)(*(long*)(&value));
            _buffer[0] = (byte)num;
            _buffer[1] = (byte)(num >> 8);
            _buffer[2] = (byte)(num >> 16);
            _buffer[3] = (byte)(num >> 24);
            _buffer[4] = (byte)(num >> 32);
            _buffer[5] = (byte)(num >> 40);
            _buffer[6] = (byte)(num >> 48);
            _buffer[7] = (byte)(num >> 56);
            BaseStream.Write(this._buffer, 0, 8);
        }

        public unsafe void WriteFloat(float value)
        {
            var num = (ulong)(*(long*)(&value));
            _buffer[0] = (byte)num;
            _buffer[1] = (byte)(num >> 8);
            _buffer[2] = (byte)(num >> 16);
            _buffer[3] = (byte)(num >> 24);
            BaseStream.Write(this._buffer, 0, 4);
        }

        public void WriteString(string value)
        {
            WriteInt32(value.Length);
            if (value.Length == 0) return;

            WriteBytes(__encoder.GetBytes(value));
        }

        public void WriteChar(char value)
        {
            var charvalue = (Int16)value;

            WriteInt16(charvalue);
        }

        public void WriteDateTime(DateTime value)
        {
            WriteInt64(value.ToBinary());
        }

        public void WriteEnum<T>(T value)
        {
            var type = typeof(T);
            if (type.IsEnum)
            {
                var basetype = Enum.GetUnderlyingType(type);
                switch (basetype.Name)
                {
                    case "Int16": WriteInt16((short)(object)value); break;
                    case "Int32": WriteInt32((int)(object)value); break;
                    case "Int64": WriteInt64((long)(object)value); break;
                    case "UInt16": WriteUInt16((ushort)(object)value); break;
                    case "UInt32": WriteUInt32((uint)(object)value); break;
                    case "UInt64": WriteUInt64((ulong)(object)value); break;
                    case "Byte": WriteByte((byte)(object)value); break;
                    case "SByte": WriteSByte((sbyte)(object)value); break;
                    default: throw new InvalidCastException(string.Format("Enum {0} could not cast to {1}", type.Name, basetype.Name));
                }

            }
            else
            {
                throw new InvalidCastException("Failed to write binary value.");
            }
        }

        #endregion
    }
}
