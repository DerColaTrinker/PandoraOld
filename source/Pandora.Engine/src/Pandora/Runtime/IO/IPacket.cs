﻿using System;

namespace Pandora.Runtime.IO
{
    /// <summary>
    /// Schittstelle eines Datenpakets
    /// </summary>
    public interface IPacket
    {
        /// <summary>
        /// Liefert die Länge des Datenpakets
        /// </summary>
        int Length { get; }

        /// <summary>
        /// Liefert die Position für Lese- und Schreiboperationen
        /// </summary>
        int Position { get; }
    }
}
