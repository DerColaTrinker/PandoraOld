﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#pragma warning disable 1591

namespace Pandora.Runtime.Logging.Targets
{
    public class ConsoleLoggerTarget : LoggerTarget
    {
        public ConsoleLoggerTarget()
        {
            TraceColor = ConsoleColor.DarkGray;
            DebugColor = ConsoleColor.Blue;
            NormalColor = ConsoleColor.White;
            WarningColor = ConsoleColor.Yellow;
            ErrorColor = ConsoleColor.Red;
            ExceptionColor = ConsoleColor.Magenta;

            Configuration = new ConsoleLoggerTargetConfiguration(this);
        }

        protected override void OnBeginProgress(float count, string message, object[] args)
        {
            // Nix machen
        }

        protected override void OnBeginState(string message, object[] args)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("{0,-70}", string.Format(message, args));
        }

        protected override void OnEndProgress(float count, string message, bool abort)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("{0,-40} [{1,-20}] {2:000.0}% ", message, new string('=', 20), 100F);

            if (abort)
            {
                Console.Write("[");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("ERROR");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("] ");
            }
            else
            {
                Console.Write("[");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("OK");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("]");
            }
        }

        protected override void OnEndState(LoggerStateResult type)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("[");
            switch (type)
            {
                case LoggerStateResult.Abort:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    break;

                case LoggerStateResult.Yes:
                case LoggerStateResult.Success:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;

                case LoggerStateResult.No:
                case LoggerStateResult.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;

                case LoggerStateResult.Done:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;

                case LoggerStateResult.Warning:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
            }

            Console.Write(type.ToString().ToUpper());
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("]");
        }

        protected override void OnLogException(Exception ex, string membername, int linenumber, string file)
        {
            Console.ForegroundColor = ExceptionColor;

            Console.WriteLine("EXCEPTION : {0}", ex.GetType().Name);
            Console.WriteLine("{0} :  {1}", membername, ex.Message);
            Console.WriteLine("{0,-4} : {1}", linenumber, file);
        }

        protected override void OnLogMessage(LoggerMessageType type, string message, object[] args)
        {
            switch (type)
            {
                case LoggerMessageType.Trace: Console.ForegroundColor = TraceColor; break;
                case LoggerMessageType.Debug: Console.ForegroundColor = DebugColor; break;
                case LoggerMessageType.Normal: Console.ForegroundColor = NormalColor; break;
                case LoggerMessageType.Warning: Console.ForegroundColor = WarningColor; break;
                case LoggerMessageType.Error: Console.ForegroundColor = ErrorColor; break;
            }

            Console.WriteLine(message, args);
        }

        protected override void OnUpdateProgress(float count, string message, float percent)
        {
            var chars = new string('=', (int)(20F * percent));

            if (message.Length > 40) message = string.Format("{0}...", message.Substring(0, 37));

            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("{0,-40} [{1,-20}] {2:000.0}%\r", message, chars, percent * 100F);
        }

        public ConsoleColor DebugColor { get; set; }

        public ConsoleColor ErrorColor { get; set; }

        public ConsoleColor ExceptionColor { get; set; }

        public ConsoleColor NormalColor { get; set; }

        public ConsoleColor TraceColor { get; set; }

        public ConsoleColor WarningColor { get; set; }

        public ConsoleLoggerTargetConfiguration Configuration { get; private set; }
    }
}
