﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime.SFML
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct IdentificationMarshalData
    {
        public IntPtr Name;

        public uint VendorId;
        public uint ProductId;
    }
}
