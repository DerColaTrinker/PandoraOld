﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime.Configuration
{
    /// <summary>
    /// Stellt eine Konfigurationssystem bereit
    /// </summary>
    public sealed class ConfigurationManager : IConfigurationNotification
    {
        private Dictionary<string, ConfigurationSection> _sections = new Dictionary<string, ConfigurationSection>(StringComparer.InvariantCultureIgnoreCase);

        /// <summary>
        /// Wird ausgelöst wenn ein Wert geändert wurde
        /// </summary>
        public event ConfigurationEntryChangedDelegate EntryChanged;

        /// <summary>
        /// Wird ausgelöst wenn ein Wert sich ändert
        /// </summary>
        public event ConfigurationEntryChangingDelegate EntryChanging;

        /// <summary>
        /// Fügt eine neue Sektion hinzu
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ConfigurationSection Add(string name)
        {
            if (_sections.ContainsKey(name)) return null;

            var section = new ConfigurationSection(name);
            Add(section);
            return section;
        }

        /// <summary>
        /// Fügt eine neue Sektion hinzu
        /// </summary>
        /// <param name="section"></param>
        public void Add(ConfigurationSection section)
        {
            if (_sections.ContainsKey(section.Name)) return;

            _sections[section.Name] = section;

            section.EntryChanging += EntryPropertyChanging;
            section.EntryChanged += EntryPropertyChanged;
        }

        /// <summary>
        /// Entfernt eine Sektion
        /// </summary>
        /// <param name="name"></param>
        public void Remove(string name)
        {
            var result = (ConfigurationSection)null;

            if (_sections.TryGetValue(name, out result))
                Remove(result);
        }

        /// <summary>
        /// Entfernt eine Sektion
        /// </summary>
        /// <param name="section"></param>
        public void Remove(ConfigurationSection section)
        {
            _sections.Remove(section.Name);

            if (section.Options.HasFlag(ConfigurationSectionOptions.NotDeletable))
                return;

            section.EntryChanging -= EntryPropertyChanging;
            section.EntryChanged -= EntryPropertyChanged;
        }

        /// <summary>
        /// Liefert die angegebene Sektion oder erstellt sie neu
        /// </summary>
        /// <param name="sectioname"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public ConfigurationSection GetOrCreateSection(string sectioname, ConfigurationSectionOptions options)
        {
            var section = (ConfigurationSection)null;

            if (_sections.TryGetValue(sectioname, out section))
            {
                return section;
            }
            else
            {
                section = new ConfigurationSection(sectioname, options);
                Add(section);

                return section;
            }
        }

        /// <summary>
        /// Liefert eine Sektion
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ConfigurationSection GetSection(string name)
        {
            var result = (ConfigurationSection)null;

            _sections.TryGetValue(name, out result);

            return result;
        }

        /// <summary>
        /// Liefert eine Auflistung der Sektionen
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ConfigurationSection> GetSections()
        {
            return _sections.Values.ToArray();
        }

        /// <summary>
        /// Liefert eine Auflistung der Sektionsnamen
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetSectionNames()
        {
            return _sections.Keys.ToArray();
        }

        /// <summary>
        /// Benennt eine Sektion um
        /// </summary>
        /// <param name="section"></param>
        /// <param name="newname"></param>
        public void Rename(ConfigurationSection section, string newname)
        {
            if (section.Options.HasFlag(ConfigurationSectionOptions.NotRenameable)) throw new ArgumentException("Sectionname not changeable", "newname");

            if (_sections.ContainsKey(newname)) throw new ArgumentException("Section name already exists", "newname");

            Remove(section);
            section.Name = newname;
            Add(section);
        }

        /// <summary>
        /// Liefert die Anzahl der Sektionen
        /// </summary>
        public int Count { get { return _sections.Count; } }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sectionname"></param>
        /// <returns></returns>
        public bool ContainsSection(string sectionname)
        {
            return _sections.ContainsKey(sectionname);
        }

        /// <summary>
        /// Liefert einen Wert
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sectionname"></param>
        /// <param name="entryname"></param>
        /// <returns></returns>
        public T GetValue<T>(string sectionname, string entryname)
        {
            return GetValue(sectionname, entryname, default(T));
        }

        /// <summary>
        /// Liefert einen Wert
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sectionname"></param>
        /// <param name="entryname"></param>
        /// <param name="defaultvalue"></param>
        /// <returns></returns>
        public T GetValue<T>(string sectionname, string entryname, T defaultvalue)
        {
            var section = (ConfigurationSection)null;

            if (_sections.TryGetValue(sectionname, out section))
            {
                return section.GetValue<T>(entryname, defaultvalue);
            }
            else
            {
                return defaultvalue;
            }
        }

        /// <summary>
        /// Legt einen Wert fest
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sectionname"></param>
        /// <param name="entryname"></param>
        /// <param name="value"></param>
        public void SetValue<T>(string sectionname, string entryname, T value)
        {
            var section = (ConfigurationSection)null;

            if (!_sections.TryGetValue(sectionname, out section))
            {
                section = new ConfigurationSection(sectionname);
                Add(section);
            }

            section.SetValue<T>(entryname, value);
        }

        void EntryPropertyChanged(ConfigurationEntry entry, object value)
        {
            if (EntryChanged != null) EntryChanged.Invoke(entry, value);
        }

        void EntryPropertyChanging(ConfigurationEntry entry, object oldvalue, object newvalue)
        {
            if (EntryChanging != null) EntryChanging.Invoke(entry, oldvalue, newvalue);
        }
    }
}
