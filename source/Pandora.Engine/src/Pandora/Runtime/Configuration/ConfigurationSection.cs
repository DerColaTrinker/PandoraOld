﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime.Configuration
{
    /// <summary>
    /// Stellt eine Sektion der Konfiguration dar
    /// </summary>
    public sealed class ConfigurationSection : IConfigurationNotification
    {
        private Dictionary<string, ConfigurationEntry> _entries = new Dictionary<string, ConfigurationEntry>(StringComparer.InvariantCultureIgnoreCase);

        /// <summary>
        /// Wird ausgelöst wenn ein Wert geändert wurde
        /// </summary>
        public event ConfigurationEntryChangedDelegate EntryChanged;

        /// <summary>
        /// Wird ausgelöst wenn ein Wert sich ändert
        /// </summary>
        public event ConfigurationEntryChangingDelegate EntryChanging;

        /// <summary>
        /// Erstellt eine neue Instanz der ConfigurationSection-Klasse
        /// </summary>
        /// <param name="name"></param>
        public ConfigurationSection(string name):this(name, ConfigurationSectionOptions.None)
        {        }

        /// <summary>
        /// Erstellt eine neue Instanz der ConfigurationSection-Klasse
        /// </summary>
        /// <param name="name"></param>
        /// <param name="options"></param>
        public ConfigurationSection(string name, ConfigurationSectionOptions options)
        {
            Options = options;
            Name = name;
        }

        /// <summary>
        /// Liefert den Namen der Sektion
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Liefrt die Optionen
        /// </summary>
        public ConfigurationSectionOptions Options { get; internal set; }

        /// <summary>
        /// Fügt der Sektion ein neues Element hinzu
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void Add(string name, object value)
        {
            Add(name, ConfigurationFlags.Normal, value);
        }

        /// <summary>
        /// Fügt der Sektion ein neues Element hinzu
        /// </summary>
        /// <param name="name"></param>
        /// <param name="flags"></param>
        /// <param name="value"></param>
        public void Add(string name, ConfigurationFlags flags, object value)
        {
            Add(new ConfigurationEntry(name, flags, value));
        }

        /// <summary>
        /// Fügt der Sektion ein neues Element hinzu
        /// </summary>
        /// <param name="entry"></param>
        public void Add(ConfigurationEntry entry)
        {
            if (_entries.ContainsKey(entry.Name)) return;

            _entries[entry.Name] = entry;

            entry.EntryChanging += EntryPropertyChanging;
            entry.EntryChanged += EntryPropertyChanged;
        }

        /// <summary>
        /// Entfernt ein Element aus der Sektion
        /// </summary>
        /// <param name="name"></param>
        public void Remove(string name)
        {
            var result = (ConfigurationEntry)null;

            if (_entries.TryGetValue(name, out result))
                Remove(result);
        }

        /// <summary>
        /// Entfernt ein Element aus der Sektion
        /// </summary>
        /// <param name="entry"></param>
        public void Remove(ConfigurationEntry entry)
        {
            _entries.Remove(entry.Name);

            entry.EntryChanging -= EntryPropertyChanging;
            entry.EntryChanged -= EntryPropertyChanged;
        }

        /// <summary>
        /// Liefert ein Element
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ConfigurationEntry GetEntry(string name)
        {
            var result = (ConfigurationEntry)null;

            _entries.TryGetValue(name, out result);

            return result;
        }

        /// <summary>
        /// Liefert eine Auflistung aller Elemente
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ConfigurationEntry> GetEntries()
        {
            return _entries.Values.ToArray();
        }

        /// <summary>
        /// Liefert eine Auflistung aller Elementnamen
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetEntryNames()
        {
            return _entries.Keys.ToArray();
        }

        /// <summary>
        /// Benennt die Sektion um
        /// </summary>
        /// <param name="entry"></param>
        /// <param name="newname"></param>
        public void Rename(ConfigurationEntry entry, string newname)
        {
            if (_entries.ContainsKey(newname)) throw new ArgumentException("Entry name already exists", "newname");

            Remove(entry);
            entry.Name = newname;
            Add(entry);
        }

        /// <summary>
        /// Liefert die Anzahl der Elemente
        /// </summary>
        public int Count { get { return _entries.Count; } }

        /// <summary>
        /// Liefert den Wert eines Elements
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entryname"></param>
        /// <returns></returns>
        public T GetValue<T>(string entryname)
        {
            return GetValue(entryname, default(T));
        }

        /// <summary>
        /// Liefert den Wert eines Elements
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entryname"></param>
        /// <param name="defaultvalue"></param>
        /// <returns></returns>
        public T GetValue<T>(string entryname, T defaultvalue)
        {
            var entry = (ConfigurationEntry)null;

            if (_entries.TryGetValue(entryname, out entry))
            {
                return entry.GetValue<T>(defaultvalue);
            }
            else
            {
                return defaultvalue;
            }
        }

        /// <summary>
        /// Legt den Wert eines Elements fest
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entryname"></param>
        /// <param name="value"></param>
        public void SetValue<T>(string entryname, T value)
        {
            var entry = (ConfigurationEntry)null;

            if (!_entries.TryGetValue(entryname, out entry))
            {
                entry = new ConfigurationEntry(entryname, ConfigurationFlags.Normal, value);
                Add(entry);
                return;
            }

            entry.SetValue<T>(value);
        }

        /// <summary>
        /// Liefert den Namen der Sektion
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("Name:{0}", Name);
        }

        void EntryPropertyChanged(ConfigurationEntry entry, object value)
        {
            if (EntryChanged != null) EntryChanged.Invoke(entry, value);
        }

        void EntryPropertyChanging(ConfigurationEntry entry, object oldvalue, object newvalue)
        {
            if (EntryChanging != null) EntryChanging.Invoke(entry, oldvalue, newvalue);
        }
    }
}
