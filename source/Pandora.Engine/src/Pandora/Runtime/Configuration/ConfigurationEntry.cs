﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime.Configuration
{
    /// <summary>
    /// Stellt ein Konfigurationseintrag dar
    /// </summary>
    public sealed class ConfigurationEntry : ConfigurationNotificationBase
    {
        /// <summary>
        /// Erstellt eine neue Instnaz der ConfigurationEntry-Klasse
        /// </summary>
        /// <param name="name"></param>
        /// <param name="flags"></param>
        /// <param name="value"></param>
        public ConfigurationEntry(string name, ConfigurationFlags flags, object value)
        {
            this.Name = name;
            this.Flags = flags;
            this.Value = value;
        }

        /// <summary>
        /// Liefert den Namen
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Liefert die Optionen
        /// </summary>
        public ConfigurationFlags Flags { get; private set; }

        /// <summary>
        /// Liefert den Wert
        /// </summary>
        public object Value { get; private set; }

        /// <summary>
        /// Gibt ab ob der Wert nur gelesen werden kann
        /// </summary>
        public bool IsReadonly { get { return (Flags & ConfigurationFlags.Readonly) == ConfigurationFlags.Readonly; } }

        /// <summary>
        /// Gibt an ob es sich um ein Internen eintrag handelt
        /// </summary>
        public bool IsInternal { get { return (Flags & ConfigurationFlags.Internal) == ConfigurationFlags.Internal; } }

        /// <summary>
        /// Liefert den Wert
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetValue<T>()
        {
            return GetValue<T>(default(T));
        }

        /// <summary>
        /// Liefert den Wert
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="defaultvalue"></param>
        /// <returns></returns>
        public T GetValue<T>(T defaultvalue)
        {
            try
            {
                return (T)Convert.ChangeType(Value, typeof(T));
            }
            catch (Exception)
            {
                return defaultvalue;
            }
        }

        /// <summary>
        /// Legt den Wert fest
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        public void SetValue<T>(T value)
        {
            if (IsReadonly) throw new InvalidOperationException("Entry is readonly");

            OnConfigurationChanging(this, Value, value);
            Value = (object)value;
            OnConfigurationChanged(this, value);
        }

        /// <summary>
        /// Gibt den Namen und den Wert aus
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("Name:{0}, Value:{1}", Name, Value);
        }
    }
}
