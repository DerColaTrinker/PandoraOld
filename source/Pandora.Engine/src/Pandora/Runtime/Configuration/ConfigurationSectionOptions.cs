﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Runtime.Configuration
{
    /// <summary>
    /// Stellt Optionen der Sektion bereit
    /// </summary>
    [Flags]
    public enum ConfigurationSectionOptions
    {
        /// <summary>
        /// Keine Optionen
        /// </summary>
        None,

        /// <summary>
        /// Nicht löschbar
        /// </summary>
        NotDeletable,

        /// <summary>
        /// Nicht umbenennbar
        /// </summary>
        NotRenameable
    }
}
