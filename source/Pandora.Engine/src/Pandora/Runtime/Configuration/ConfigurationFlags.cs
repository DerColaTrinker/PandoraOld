﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Runtime.Configuration
{
    /// <summary>
    /// Elementoptionen
    /// </summary>
    [Flags]
    public enum ConfigurationFlags
    {
        /// <summary>
        /// Normal
        /// </summary>
        Normal = 0,
        
        /// <summary>
        /// Nur lesen
        /// </summary>
        Readonly = 1,

        /// <summary>
        /// Intern
        /// </summary>
        Internal = 2
    }
}
