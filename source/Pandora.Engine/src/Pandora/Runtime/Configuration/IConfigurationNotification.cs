﻿namespace Pandora.Runtime.Configuration
{
    /// <summary>
    /// Stellt eine Schnittstelle bereit die Änderungen an der Kofiguration mitteilt
    /// </summary>
    public interface IConfigurationNotification
    {
        /// <summary>
        /// Wird ausgelöst wenn ein Wert geändert wurde
        /// </summary>
        event ConfigurationEntryChangedDelegate EntryChanged;

        /// <summary>
        /// Wird ausgelöst wenn ein Wert sich ändert
        /// </summary>
        event ConfigurationEntryChangingDelegate EntryChanging;
    }
}
