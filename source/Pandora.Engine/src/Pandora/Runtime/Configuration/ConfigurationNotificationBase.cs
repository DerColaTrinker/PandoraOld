﻿namespace Pandora.Runtime.Configuration
{
    /// <summary>
    /// Delegate das verwendet wird, wenn ein Wert geändert wird
    /// </summary>
    /// <param name="entry"></param>
    /// <param name="oldvalue"></param>
    /// <param name="newvalue"></param>
    public delegate void ConfigurationEntryChangingDelegate(ConfigurationEntry entry, object oldvalue, object newvalue);

    /// <summary>
    /// Delegate das verwendet wird, wenn ein Wert geändert wurde
    /// </summary>
    /// <param name="entry"></param>
    /// <param name="value"></param>
    public delegate void ConfigurationEntryChangedDelegate(ConfigurationEntry entry, object value);

    /// <summary>
    /// Stellt eine Basisfunktion der Änderungsüberwachung dar
    /// </summary>
    public abstract class ConfigurationNotificationBase : IConfigurationNotification
    {

        /// <summary>
        /// Wird ausgelöst wenn ein Wert geändert wurde
        /// </summary>
        public event ConfigurationEntryChangedDelegate EntryChanged;

        /// <summary>
        /// Wird ausgelöst wenn ein Wert sich ändert
        /// </summary>
        public event ConfigurationEntryChangingDelegate EntryChanging;

        /// <summary>
        /// Löst das Ereignis <see cref="EntryChanging"/> aus.
        /// </summary>
        /// <param name="entry"></param>
        /// <param name="oldvalue"></param>
        /// <param name="newvalue"></param>
        protected virtual void OnConfigurationChanging(ConfigurationEntry entry, object oldvalue, object newvalue)
        {
            if (EntryChanging != null) EntryChanging.Invoke(entry, oldvalue, newvalue);
        }

        /// <summary>
        /// Löst das Ereignis <see cref="EntryChanged"/> aus.
        /// </summary>
        /// <param name="entry"></param>
        /// <param name="value"></param>
        protected virtual void OnConfigurationChanged(ConfigurationEntry entry, object value)
        {
            if (EntryChanged != null) EntryChanged.Invoke(entry, value);
        }
    }
}
