﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Runtime.Collections
{
    /// <summary>
    /// Markiert eine Klasse als Owner Element für die OwnerHashSet- oder OwnerList-Klasse
    /// </summary>
    public interface IOwnerCollection
    {
    }
}
