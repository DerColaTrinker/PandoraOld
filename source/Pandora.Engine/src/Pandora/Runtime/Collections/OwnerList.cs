﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Runtime.Collections
{
    /// <summary>
    /// Eine Liste die über mit Beisztern und Elementen arbeiten kann
    /// </summary>
    /// <typeparam name="TOwner"></typeparam>
    /// <typeparam name="TElement"></typeparam>
    public class OwnerList<TOwner, TElement>
        where TOwner : IOwnerCollection
        where TElement : class
    {
        #region Neasted Class

        /// <summary>
        /// Stellt eine Liste der Elemente dar
        /// </summary>
        public sealed class ElementCollection
        {
            /// <summary>
            /// Erstellt eine neue Instanz der OwnerCollection.ElementCollection-Klasse
            /// </summary>
            /// <param name="owner"></param>
            public ElementCollection(TOwner owner)
            {
                Owner = owner;
                Elements = new List<TElement>();
            }

            /// <summary>
            /// Liefert den Besitzer dieses Elements
            /// </summary>
            public TOwner Owner { get; private set; }

            internal List<TElement> Elements { get; private set; }

            /// <summary>
            /// Liefert eine Auflistung aller Elemente
            /// </summary>
            /// <returns></returns>
            public IEnumerable<TElement> GetElements()
            {
                return Elements.AsEnumerable();
            }

            /// <summary>
            /// Liefert die Anzahl der Elemente
            /// </summary>
            public int Count { get { return Elements.Count; } }

            /// <summary>
            /// Liefert ein Element aus der Liste anhand des Index
            /// </summary>
            /// <param name="index"></param>
            /// <returns></returns>
            public TElement ElementAt(int index)
            {
                return Elements[index];
            }
        }

        #endregion

        private Dictionary<TOwner, ElementCollection> _owners = new Dictionary<TOwner, ElementCollection>();

        #region Owner Management

        /// <summary>
        /// Fügt einen Besitzer hinzu
        /// </summary>
        /// <param name="owner"></param>
        public void AddOwner(TOwner owner)
        {
            _owners[owner] = new ElementCollection(owner);
        }

        /// <summary>
        /// Entfernt einen Besitzer
        /// </summary>
        /// <param name="owner"></param>
        /// <returns></returns>
        public bool RemoveOwner(TOwner owner)
        {
            return _owners.Remove(owner);
        }

        /// <summary>
        /// Liefert eine Auflistung aller Besitzer
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TOwner> GetOwners()
        {
            return _owners.Keys.AsEnumerable();
        }

        /// <summary>
        /// Gibt an ob der Besitzer bereits vorhanden ist
        /// </summary>
        /// <param name="owner"></param>
        /// <returns></returns>
        public bool ContainsKey(TOwner owner)
        {
            return _owners.ContainsKey(owner);
        }

        /// <summary>
        /// Liefert die Anzahl der Besitzer
        /// </summary>
        public int OwnerCount { get { return _owners.Count; } }

        #endregion

        #region Element Management

        /// <summary>
        /// Fügt einem Besitzer ein Element hinzu
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="element"></param>
        public void AddElement(TOwner owner, TElement element)
        {
            if (!_owners.ContainsKey(owner))
                AddOwner(owner);

            _owners[owner].Elements.Add(element);
        }

        /// <summary>
        /// Fügt dem Besitzer eine Auflistung an Elementen hinzu
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="elements"></param>
        public void AddElementRange(TOwner owner, IEnumerable<TElement> elements)
        {
            elements.Any(m => { AddElement(owner, m); return true; });
        }

        /// <summary>
        /// Entfernt ein Element
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public bool RemoveElement(TElement element)
        {
            var result = (from o in _owners.Values from e in o.Elements where e == element select o.Owner).FirstOrDefault();

            if (result == null) return false;

            return RemoveElement(result, element);
        }

        /// <summary>
        /// Entfernt ein Element
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="element"></param>
        /// <returns></returns>
        public bool RemoveElement(TOwner owner, TElement element)
        {
            var result = _owners[owner].Elements.Remove(element);

            if (result)
            {
                if (AutoDeleteOwnerOnEmptyElements && GetElementCount(owner) == 0)
                    RemoveOwner(owner);
            }

            return result;
        }

        /// <summary>
        /// Liefert eine Auflistung aller Elemente eines Besitzers
        /// </summary>
        /// <param name="owner"></param>
        /// <returns></returns>
        public IEnumerable<TElement> GetElements(TOwner owner)
        {
            return _owners[owner].Elements.AsEnumerable();
        }

        /// <summary>
        /// Liefert eine Auflistung aller Elemente
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TElement> GetElements()
        {
            return from o in _owners.Values from e in o.Elements select e;
        }

        /// <summary>
        /// Liefert die Instanz der ElementCollection-Klasse des Besitzers
        /// </summary>
        /// <param name="owner"></param>
        /// <returns></returns>
        public ElementCollection GetElementCollection(TOwner owner)
        {
            return _owners[owner];
        }

        /// <summary>
        /// Liefert eine Auflistung der ElementCollection Instanzen
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ElementCollection> GetElementCollections()
        {
            return _owners.Values;
        }

        /// <summary>
        /// Liefert die Anzahl der Elemente
        /// </summary>
        /// <param name="owner"></param>
        /// <returns></returns>
        public int GetElementCount(TOwner owner)
        {
            return _owners[owner].Count;
        }

        #endregion

        /// <summary>
        /// Gibt an ob die Besitzer automatisch entfernt werden, wenn die Elemente Auflistung leer ist oder legt es fest. ( Standard : false )
        /// </summary>
        public bool AutoDeleteOwnerOnEmptyElements { get; set; }
    }
}
