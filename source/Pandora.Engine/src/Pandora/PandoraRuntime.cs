﻿using Pandora.Runtime.Configuration;
using Pandora.Runtime.Timer;
using Pandora.Interactions;
using Pandora.Runtime.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.Network;
using System.IO;
using Pandora.Game;
using System.Xml;
using System.Reflection;
using Pandora.Interactions.Visuals.Controls;

namespace Pandora
{
    /// <summary>
    /// Die Basis Runtime
    /// </summary>
    public sealed class PandoraRuntime
    {
        private HashSet<Instance> _instaces = new HashSet<Instance>();
        private HashSet<GameModificationDescriptor> _moddescriptions = new HashSet<GameModificationDescriptor>();
        private List<Type> _loadedcontroller = new List<Type>();
        private List<Type> _loadedmodtypes = new List<Type>();
        private List<Type> _loadedhandlertypes = new List<Type>();

        internal static bool __disablevisuals;

        /// <summary>
        /// Erstellt eine neue Instanz der PandoraEngine-Klasse
        /// </summary>
        public PandoraRuntime()
        {
            CreateConfiguration();

            LoadIdentifier();

            LoadModifications();

            LoadConfiguration();

            // Das Interaction-Subsystem wird zwar instanziert aber nicht initialisiert
            Interaction = new InteractionManager();
            __disablevisuals = Configurations.GetValue("Engine", "DisableVisuals", false);
            if (!__disablevisuals)
            {
                AddInstance(Interaction);
            }

            Logger.Normal("Pandora engine initialized");

            Assembly.LoadFrom("Pandora.Framework.UI.dll");
        }

        private void LoadConfiguration()
        {
            var xd = new XmlDocument();
            xd.Load("engine.config");

            // Konfiguration übernehmen
            var setnodes = xd.SelectNodes("engine/configuration/set");
            foreach (XmlNode setnode in setnodes)
            {
                var section = setnode.Attributes.GetValue("section", string.Empty);
                var entry = setnode.Attributes.GetValue("entry", string.Empty);
                var value = setnode.Attributes.GetValue("value", string.Empty);

                if (Configurations.ContainsSection(section))
                    Configurations.SetValue(section, entry, value);
            }

            // Modifikationen laden
            var modnodes = xd.SelectNodes("engine/modifications/add");
            foreach (XmlNode modsubnode in modnodes)
            {
                var name = modsubnode.Attributes.GetValue("name", string.Empty);
                var mod = _moddescriptions.Where(m => m.Name.ToLower() == name.ToLower()).FirstOrDefault();
                if (mod == null)
                {
                    Logger.Warning("Modification '{0}' not found", name);
                }

                var asm = Assembly.LoadFrom(mod.AssemblyFile.FullName);
                Logger.Normal("Modification '{0}' loaded", mod.Name);

                // Suche nach den SpielModificationen
                var types = asm.GetTypes();

                Logger.BeginProgress(types.Length, "Scan assembly '{0}'", asm.GetName().Name);
                for (int i = 0; i < types.Length; i++)
                {
                    Logger.UpdateProgress(i + 1);

                    var type = types[i];

                    if (type.IsSubclassOf(typeof(GameModificationBase))) _loadedmodtypes.Add(type);
                    if (type.IsSubclassOf(typeof(GameHandlerBase))) _loadedhandlertypes.Add(type);
                    if (type.IsSubclassOf(typeof(GameControllerBase))) _loadedcontroller.Add(type);
                }
                Logger.EndProgress(false);

                foreach (var item in _loadedcontroller) Logger.Trace("Found game controller : {0}", item.Name);
                foreach (var item in _loadedmodtypes) Logger.Trace("Found modification : {0}", item.Name);
                foreach (var item in _loadedhandlertypes) Logger.Trace("Found handler : {0}", item.Name);
            }
        }

        private void LoadModifications()
        {
            var modificationpath = Path.Combine(Environment.CurrentDirectory, "mods");
            var stack = new Stack<DirectoryInfo>();

            stack.Push(new DirectoryInfo(modificationpath));

            while (stack.Count > 0)
            {
                var current = stack.Pop();

                // Unterverzeichnisse pushen
                foreach (var item in current.GetDirectories()) { stack.Push(item); }

                var files = current.GetFiles();
                var file = files.Where(m => m.Name.ToLower() == "modification.xml").FirstOrDefault();

                if (file != null)
                {
                    var modificationdescription = new GameModificationDescriptor();

                    if (!modificationdescription.SetupXMLInformation(file)) continue;
                    if (!modificationdescription.SetupAssemblyInformation()) continue;

                    Logger.Normal("Found modification '{0}' V{1}", modificationdescription.Name, modificationdescription.Version);
                    _moddescriptions.Add(modificationdescription);
                }
            }
        }

        private void LoadIdentifier()
        {
#if DEBUG
            Identity = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
#else
            if (File.Exists("identity.dat"))
            {
                using (var fs = new StreamReader("identity.dat", Encoding.ASCII))
                {
                    Identity = fs.ReadToEnd();
                }

                if (string.IsNullOrEmpty(Identity))
                    CreateNewIdentityFile();
            }
            else
            {
                CreateNewIdentityFile();
            }
#endif

            Logger.Debug("Installation identity : {0}", Identity);
        }

        private void CreateNewIdentityFile()
        {
            Identity = Convert.ToBase64String(Guid.NewGuid().ToByteArray());

            using (var fs = new StreamWriter("identity.dat", false, Encoding.ASCII))
            {
                fs.Write(Identity);
                fs.Flush();
            }
        }

        private void CreateConfiguration()
        {
            Configurations = new ConfigurationManager();
            RuntimeConfiguration = new ConfigurationSection("Engine", ConfigurationSectionOptions.NotDeletable | ConfigurationSectionOptions.NotRenameable);

            Configurations.Add(RuntimeConfiguration);
        }

        /// <summary>
        /// Liefert die Engine-Konfiguration
        /// </summary>
        public ConfigurationSection RuntimeConfiguration { get; private set; }

        /// <summary>
        /// Liefert alle Konfigurationen
        /// </summary>
        public ConfigurationManager Configurations { get; private set; }

        /// <summary>
        /// Liefert eine Liste der Modifikationen
        /// </summary>
        public IEnumerable<GameModificationDescriptor> Modifications { get { return _moddescriptions.AsEnumerable(); } }

        /// <summary>
        /// Liefert eine eindeutige Identifikation der Runtime
        /// </summary>
        public string Identity { get; private set; }

        /// <summary>
        /// Liefert die Aktuelle Version der Spielengine
        /// </summary>
        public Version Version { get { return System.Reflection.Assembly.GetEntryAssembly().GetName().Version; } }

        /// <summary>
        /// Liefert das Interaction-Subsystem
        /// </summary>
        public InteractionManager Interaction { get; private set; }

        private void AddInstance(Instance instance)
        {
            if (instance.Initializable)
            {
                if (_instaces.Add(instance))
                {
                    instance.InternalInitialize(this);
                    Logger.Debug("Instance addet");
                }
            }
        }

        /// <summary>
        /// Startet ein neues Singleplayer-Spiel
        /// </summary>
        /// <returns></returns>
        public GameControllerBase CreateGameController()
        {
            Logger.Normal("Creating game controller");

            var controllername = RuntimeConfiguration.GetValue("GameController", "PandoraGameController");
            var controllertype = _loadedcontroller.Where(m => m.FullName == controllername).FirstOrDefault();
            if (controllertype == null) throw new PandoraException(string.Format("Game controller not found '{0}'", controllername));

            var instance = (GameControllerBase)Activator.CreateInstance(controllertype);

            // Der Runtime hinzufügen
            AddInstance(instance);

            // Laden der Modifikationen
            foreach (var item in _loadedmodtypes) instance.RegisterModification(item);

            // Das erstellen intern auslösen
            instance.InternalCreated();

            // Den Gamecontroller zurückgeben.
            return instance;
        }

        private Type GetGameControllerType()
        {
            var result = from t in _loadedmodtypes
                         where t.IsSubclassOf(typeof(GameControllerBase))
                         select t;

            if (result.Count() > 0)
                throw new PandoraGameException("Multiple game controller");

            if (result.Count() == 0)
                throw new PandoraGameException("Game controller not found");

            return result.FirstOrDefault();
        }

        #region Runtime Controlling

        /// <summary>
        /// Startet die Runtime
        /// </summary>
        public void Start(Scene scene)
        {
            if (Running) return;

            LoadModificationConfiguration();

            foreach (var instance in _instaces) { instance.InternalStart(); }

            if (scene != null) Interaction.Visuals.Show(scene);

            RuntimeLoop();
        }

        private void LoadModificationConfiguration()
        {
            // Lade die Konfiguration der Modificationen
            foreach (var mod in _moddescriptions)
            {
                var setnodes = mod.XmlDocument.SelectNodes("modification/configuration/set");
                foreach (XmlNode setnode in setnodes)
                {
                    var section = setnode.Attributes.GetValue("section", string.Empty);
                    var entry = setnode.Attributes.GetValue("entry", string.Empty);
                    var value = setnode.Attributes.GetValue("value", string.Empty);

                    if (Configurations.ContainsSection(section))
                        Configurations.SetValue(section, entry, value);
                }
            }
        }

        /// <summary>
        /// Hält die Runtime an
        /// </summary>
        public void Stop()
        {
            if (!Running) return;

            Running = false;
        }

        /// <summary>
        /// Gibt an ob die Runtime ausgeführt wird
        /// </summary>
        public bool Running { get; private set; }

        /// <summary>
        /// Löst einen manuellen Frame aus
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="s"></param>
        public void CallSystemUpdate(float ms, float s)
        {
            if (Running) return;

            InternalCallSystemUpdate(ms, s);
        }

        private void RuntimeLoop()
        {
            var timer = new StopWatch();
            var ms = 1F;
            var s = ms * 1000F;

            Running = true;

            while (Running)
            {
                timer.Reset();

                InternalCallSystemUpdate(ms, s);

                var frame = timer.GetElapsedTime();
                ms = frame.AsMilliseconds();
                s = ms * 1000F;
            }

            Running = false;

            foreach (var instance in _instaces) { instance.InternalStop(); }
        }

        private void InternalCallSystemUpdate(float ms, float s)
        {
            _instaces.RemoveWhere(instance => { instance.InternalSystemUpdate(ms, s); return false; });
            //Interaction.InternalSystemUpdate(ms, s);
        }

        #endregion

        /// <summary>
        /// Liefert den Namen des Stammverzeichnisses
        /// </summary>
        public string RootPath { get { return Environment.CurrentDirectory; } }
    }
}
