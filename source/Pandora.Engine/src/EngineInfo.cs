﻿
#if ALPHA && BETA
    #error ALPHA und BETA Status kann nicht gleichzeitig gesetzte werden
#endif

#pragma warning disable 1591

using System;
using System.Reflection;

public static class EngineInfo
{
	static EngineInfo()
	{
		__CompileDateTime = "30.05.2017 08:54:55";
		__AssemblyVersion = Assembly.GetEntryAssembly().GetName().Version.ToString();
		
#if ALPHA
		__AssemblyFullVersion = String.Format("{0} ALPHA", __AssemblyVersion);
#elif BETA
		__AssemblyFullVersion = String.Format("{0} BETA", __AssemblyVersion);
#else
		__AssemblyFullVersion = String.Format("{0}", __AssemblyVersion);
#endif
	}

	public static readonly string __CompileDateTime;
	public static readonly string __AssemblyVersion;
	public static readonly string __AssemblyFullVersion;
}