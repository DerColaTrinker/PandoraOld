﻿using Pandora.Game.Templates.Bindings;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Tools.TemplateEditor.Controls
{
    class IdentifierColumn : PropertyTextBoxColumn
    {
        public IdentifierColumn(TemplateBindingProperty property)
            : base(property)
        {
            HeaderText = "Identifier";
            DefaultCellStyle.BackColor = Color.DarkRed;
            DefaultCellStyle.ForeColor = Color.White;
            ValueType = typeof(string);
        }
    }
}
