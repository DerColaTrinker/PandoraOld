﻿namespace Pandora.Tools.TemplateEditor.Controls
{
    partial class LoggerViewer
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnClearLogger = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.pgbStatus = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.rtfLogger = new System.Windows.Forms.RichTextBox();
            this.lblStatus = new System.Windows.Forms.ToolStripLabel();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnClearLogger,
            this.toolStripSeparator1,
            this.pgbStatus,
            this.toolStripStatusLabel1,
            this.lblStatus});
            this.toolStrip1.Location = new System.Drawing.Point(0, 281);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(507, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnClearLogger
            // 
            this.btnClearLogger.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClearLogger.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClearLogger.Name = "btnClearLogger";
            this.btnClearLogger.Size = new System.Drawing.Size(23, 22);
            this.btnClearLogger.Text = "Protokol löschen";
            this.btnClearLogger.Click += new System.EventHandler(this.btnClearLogger_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // pgbStatus
            // 
            this.pgbStatus.AutoSize = false;
            this.pgbStatus.BackColor = System.Drawing.SystemColors.Control;
            this.pgbStatus.ForeColor = System.Drawing.Color.Lime;
            this.pgbStatus.Maximum = 10000;
            this.pgbStatus.Name = "pgbStatus";
            this.pgbStatus.Size = new System.Drawing.Size(300, 18);
            this.pgbStatus.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 20);
            // 
            // rtfLogger
            // 
            this.rtfLogger.BackColor = System.Drawing.Color.Black;
            this.rtfLogger.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtfLogger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtfLogger.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtfLogger.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.rtfLogger.Location = new System.Drawing.Point(0, 0);
            this.rtfLogger.Name = "rtfLogger";
            this.rtfLogger.ReadOnly = true;
            this.rtfLogger.Size = new System.Drawing.Size(507, 306);
            this.rtfLogger.TabIndex = 1;
            this.rtfLogger.Text = "";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 22);
            // 
            // LoggerViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.rtfLogger);
            this.Name = "LoggerViewer";
            this.Size = new System.Drawing.Size(507, 306);
            this.Load += new System.EventHandler(this.LoggerViewer_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.RichTextBox rtfLogger;
        private System.Windows.Forms.ToolStripButton btnClearLogger;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripProgressBar pgbStatus;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripLabel lblStatus;
    }
}
