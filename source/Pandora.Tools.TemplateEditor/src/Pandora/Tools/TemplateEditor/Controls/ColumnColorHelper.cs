﻿//using Pandora.Runtime.Game.Templates.Bindings;
//using System.Drawing;
//using System.Windows.Forms;

//namespace Pandora.Tools.Portal.UI.Controls.TemplateGridView
//{
//    class ColumnColorHelper
//    {
//        public static void SetColor(TemplatePropertyBinding propertybinding, DataGridViewColumn column)
//        {
//            switch (propertybinding.ColumnColor)
//            {
//                case GridViewColumnColors.Default:
//                    column.DefaultCellStyle.BackColor = Color.White;
//                    column.DefaultCellStyle.ForeColor = Color.Black;
//                    break;
//                case GridViewColumnColors.System:
//                    column.DefaultCellStyle.BackColor = Color.FromArgb(128, 0, 0);
//                    column.DefaultCellStyle.ForeColor = Color.White;
//                    break;
//                case GridViewColumnColors.Request:
//                    column.DefaultCellStyle.BackColor = Color.FromArgb(80, 0, 0);
//                    column.DefaultCellStyle.ForeColor = Color.White;
//                    break;
//                case GridViewColumnColors.Color1:
//                    column.DefaultCellStyle.BackColor = Color.FromArgb(251, 213, 181);
//                    column.DefaultCellStyle.ForeColor = Color.Black;
//                    break;
//                case GridViewColumnColors.Color2:
//                    column.DefaultCellStyle.BackColor = Color.FromArgb(183, 221, 232);
//                    column.DefaultCellStyle.ForeColor = Color.Black;
//                    break;
//                case GridViewColumnColors.Color3:
//                    column.DefaultCellStyle.BackColor = Color.FromArgb(204, 193, 217);
//                    column.DefaultCellStyle.ForeColor = Color.Black;
//                    break;
//                case GridViewColumnColors.Color4:
//                    column.DefaultCellStyle.BackColor = Color.FromArgb(215, 227, 188);
//                    column.DefaultCellStyle.ForeColor = Color.Black;
//                    break;
//                case GridViewColumnColors.Color5:
//                    column.DefaultCellStyle.BackColor = Color.FromArgb(229, 185, 183);
//                    column.DefaultCellStyle.ForeColor = Color.Black;
//                    break;
//                case GridViewColumnColors.Color6:
//                    column.DefaultCellStyle.BackColor = Color.FromArgb(184, 204, 228);
//                    column.DefaultCellStyle.ForeColor = Color.Black;
//                    break;
//                case GridViewColumnColors.Color7:
//                    column.DefaultCellStyle.BackColor = Color.FromArgb(141, 179, 226);
//                    column.DefaultCellStyle.ForeColor = Color.Black;
//                    break;
//                case GridViewColumnColors.Color8:
//                    column.DefaultCellStyle.BackColor = Color.FromArgb(196, 189, 151);
//                    column.DefaultCellStyle.ForeColor = Color.Black;
//                    break;
//                case GridViewColumnColors.Color9:
//                    column.DefaultCellStyle.BackColor = Color.FromArgb(192, 192, 192);
//                    column.DefaultCellStyle.ForeColor = Color.Black;
//                    break;
//                case GridViewColumnColors.Black:
//                    column.DefaultCellStyle.BackColor = Color.Black;
//                    column.DefaultCellStyle.ForeColor = Color.White;
//                    break;
//                default:
//                    break;
//            }
//        }
//    }
//}
