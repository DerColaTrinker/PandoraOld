﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Pandora.Runtime.Logging;
using Pandora.Runtime.Logging.Targets;

namespace Pandora.Tools.TemplateEditor.Controls
{
    public partial class LoggerViewer : UserControl
    {
        public LoggerViewer()
        {
            InitializeComponent();
        }

        private void LoggerViewer_Load(object sender, EventArgs e)
        {
            LoggerManager.IsEnabled = true;
            LoggerManager.MinLevel = LoggerMessageType.Trace;
            LoggerManager.MaxLevel = LoggerMessageType.Error;

            LoggerManager.Add("delegate", new DelegateLoggerTarget() { IsEnabled = true, MinLevel = LoggerMessageType.Trace, MaxLevel = LoggerMessageType.Error });
            var logger = LoggerManager.GetLogger<DelegateLoggerTarget>("delegate");

            logger.LoggerMessage += logger_LoggerMessage;
            logger.LoggerException += logger_LoggerException;
            logger.LoggerBeginProgress += logger_LoggerBeginProgress;
            logger.LoggerUpdateProgress += logger_LoggerUpdateProgress;
            logger.LoggerEndProgress += logger_LoggerEndProgress;
            logger.LoggerBeginState += logger_LoggerBeginState;
            logger.LoggerEndState += logger_LoggerEndState;
        }

        private void logger_LoggerMessage(LoggerMessageType type, string message)
        {
            var color = Color.White;

            switch (type)
            {
                case LoggerMessageType.Trace: color = Color.Silver; break;
                case LoggerMessageType.Debug: color = Color.DarkBlue; break;
                case LoggerMessageType.Normal: color = Color.White; break;
                case LoggerMessageType.Warning: color = Color.Yellow; break;
                case LoggerMessageType.Error: color = Color.Red; break;
                default: break;
            }

            rtfLogger.SelectionColor = color;
            rtfLogger.SelectedText = DateTime.Now.ToLongTimeString() + " : " + message + '\n';
            rtfLogger.SelectionLength = 0;
            rtfLogger.ScrollToCaret();
        }

        private void logger_LoggerException(Exception ex, string membername, int linenumber, string file)
        {
            var color = Color.Magenta;

            rtfLogger.SelectionColor = color;
            rtfLogger.SelectedText = DateTime.Now.ToLongTimeString() + " : " + ex.Message + '\n';
            rtfLogger.SelectedText = string.Format("Member {0}, Line{1}, File {2}", membername, linenumber, file);
            rtfLogger.SelectedText = ex.StackTrace;
            rtfLogger.SelectionLength = 0;
            rtfLogger.ScrollToCaret();
        }

        private void logger_LoggerBeginProgress(float count, string message)
        {
            rtfLogger.SelectionColor = Color.Black;
            rtfLogger.SelectedText = DateTime.Now.ToLongTimeString() + " : Begin progress : " + message + '\n';
            lblStatus.Text = message;
            rtfLogger.ScrollToCaret();
            pgbStatus.Value = 0;
        }

        private void logger_LoggerUpdateProgress(float percent)
        {
            pgbStatus.Value = (int)(percent * 10000);
        }

        private void logger_LoggerEndProgress(bool abort)
        {
            rtfLogger.SelectionColor = Color.Black;
            rtfLogger.SelectedText = DateTime.Now.ToLongTimeString() + " : End progress" + '\n';
            lblStatus.Text = "";
            rtfLogger.ScrollToCaret();
            pgbStatus.Value = 0;
        }

        private void logger_LoggerBeginState(string message)
        {
            var color = Color.White;

            rtfLogger.SelectionColor = color;
            rtfLogger.SelectedText = DateTime.Now.ToLongTimeString() + " : " + message + " -> ";
            rtfLogger.SelectionLength = 0;
            rtfLogger.ScrollToCaret();
        }

        private void logger_LoggerEndState(LoggerStateResult result)
        {
            var color = Color.White;

            switch (result)
            {
                case LoggerStateResult.Abort:
                case LoggerStateResult.Error:
                    color = Color.Red;
                    break;

                case LoggerStateResult.Warning:
                case LoggerStateResult.No:
                    color = Color.Yellow;
                    break;

                case LoggerStateResult.Success:
                case LoggerStateResult.Done:
                case LoggerStateResult.Yes:
                    color = Color.Green;
                    break;
            }

            rtfLogger.SelectionColor = color;
            rtfLogger.SelectedText = result.ToString() + "\n";
            rtfLogger.SelectionLength = 0;
            rtfLogger.ScrollToCaret();
        }

        //void logger_LoggerEndProgress(bool cancel)
        //{
        //    rtfLogger.SelectionColor = Color.Black;
        //    rtfLogger.SelectedText = DateTime.Now.ToLongTimeString() + " : End progress" + '\n';
        //    lblStatus.Text = "";
        //    rtfLogger.ScrollToCaret();
        //    pgbStatus.Value = 0;
        //}

        //void logger_LoggerUpdateProgress(string message, int count, float percent)
        //{
        //    pgbStatus.Value = (int)(percent * 10000);
        //}

        //void logger_LoggerBeginProgress(string message, int count)
        //{
        //    rtfLogger.SelectionColor = Color.Black;
        //    rtfLogger.SelectedText = DateTime.Now.ToLongTimeString() + " : Begin progress : " + message + '\n';
        //    lblStatus.Text = message;
        //    rtfLogger.ScrollToCaret();
        //    pgbStatus.Value = 0;
        //}

        //void logger_LoggerFatal(string message)
        //{
        //    rtfLogger.SelectionColor = Color.Magenta;
        //    rtfLogger.SelectedText = DateTime.Now.ToLongTimeString() + " : " + message + '\n';
        //    rtfLogger.SelectionLength = 0;
        //    rtfLogger.ScrollToCaret();
        //}

        //void logger_LoggerMessage(MessageType type, string message)
        //{
        //    var color = Color.White;

        //    switch (type)
        //    {
        //        case MessageType.Trace: color = Color.Silver; break;
        //        case MessageType.Debug: color = Color.DarkBlue; break;
        //        case MessageType.Normal: color = Color.Black; break;
        //        case MessageType.Warning: color = Color.DarkOrange; break;
        //        case MessageType.Error: color = Color.Red; break;
        //        default: break;
        //    }

        //    rtfLogger.SelectionColor = color;
        //    rtfLogger.SelectedText = DateTime.Now.ToLongTimeString() + " : " + message + '\n';
        //    rtfLogger.SelectionLength = 0;
        //    rtfLogger.ScrollToCaret();
        //}

        private void btnClearLogger_Click(object sender, EventArgs e)
        {
            rtfLogger.Clear();
        }
    }
}
