﻿using Pandora.Game.Templates.Bindings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Tools.TemplateEditor.Controls
{
    class PropertyEnumColumn : PropertyComboBoxColumn
    {
        public PropertyEnumColumn(TemplateBindingProperty property)
            : base(property)
        {
            HeaderText = property.Name;
            ValueType = typeof(string);

            Items.AddRange(Enum.GetNames(property.Property.PropertyType));
        }
    }
}
