﻿using Pandora.Game.Templates.Bindings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pandora.Tools.TemplateEditor.Controls
{
    class PropertyTextBoxColumn : DataGridViewTextBoxColumn, IPropertyColumn
    {
        public PropertyTextBoxColumn(TemplateBindingProperty property)
        {
            PropertyBinding = property;
        }

        public TemplateBindingProperty PropertyBinding { get; private set; }
    }
}
