﻿using Pandora.Game.Templates.Bindings;

namespace Pandora.Tools.TemplateEditor.Controls
{
    interface IPropertyColumn
    {
       TemplateBindingProperty PropertyBinding { get; }
    }
}
