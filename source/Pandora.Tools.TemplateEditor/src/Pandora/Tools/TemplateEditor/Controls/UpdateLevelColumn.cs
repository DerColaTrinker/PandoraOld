﻿using Pandora.Game.Templates.Bindings;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Tools.TemplateEditor.Controls
{
    class UpdateLevelColumn : PropertyTextBoxColumn
    {
        public UpdateLevelColumn(TemplateBindingProperty property)
            : base(property)
        {
            HeaderText = "UpgradeLevel";
            DefaultCellStyle.BackColor = Color.Black;
            DefaultCellStyle.ForeColor = Color.White;
            ValueType = typeof(int);
        }
    }
}
