﻿using Pandora.Game.Templates.Bindings;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Tools.TemplateEditor.Controls
{
    class ResourceCodeColumn : PropertyTextBoxColumn
    {
        public ResourceCodeColumn(TemplateBindingProperty property)
            : base(property)
        {
            HeaderText = "ResourceCode";
            DefaultCellStyle.BackColor = Color.Black;
            DefaultCellStyle.ForeColor = Color.White;
            //DefaultCellStyle.Font = new Font(DefaultCellStyle.Font, FontStyle.Bold);
            ValueType = typeof(string);
        }
    }
}
