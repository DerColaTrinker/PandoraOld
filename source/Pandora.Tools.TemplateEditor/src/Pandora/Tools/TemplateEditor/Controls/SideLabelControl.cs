﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Pandora.Tools.TemplateEditor.Controls
{
    public class SideLabelControl : Control
    {
        private Color _colora = Color.Gray;
        private Color _colorb = Color.Blue;
        private float _angle = 0F;
        private LinearGradientBrush _backgroundbrush;

        private StringFormat _stringformat;
        private string _text = string.Empty;
        private SolidBrush _textcolor;
        private bool _vertical = true;

        public SideLabelControl()
        {
            Size = new Size(50, 50);

            _stringformat = new StringFormat();
            _stringformat.FormatFlags = StringFormatFlags.DirectionVertical;
            ForeColor = Color.Black;
        }

        #region Eigenschaften

        public override Color BackColor
        {
            get { return base.BackColor; }
            set { base.BackColor = value; Invalidate(); }
        }

        public override Color ForeColor
        {
            get { return base.ForeColor; }
            set { base.ForeColor = value; _textcolor = new SolidBrush(base.ForeColor); Invalidate(); }
        }

        public override string Text
        {
            get
            {
                return _text;
            }
            set
            {
                _text = value;
                Invalidate();
            }
        }

        public Color ColorA
        {
            get { return _colora; }
            set
            {
                _colora = value;
                _backgroundbrush = new LinearGradientBrush(new Rectangle(0, 0, Width, Height), _colora, _colorb, _angle);
                Invalidate();
            }
        }

        public Color ColorB
        {
            get { return _colorb; }
            set
            {
                _colorb = value;
                _backgroundbrush = new LinearGradientBrush(new Rectangle(0, 0, Width, Height), _colora, _colorb, _angle);
                Invalidate();
            }
        }

        public float Angle
        {
            get { return _angle; }
            set
            {
                _angle = value;
                _backgroundbrush = new LinearGradientBrush(new Rectangle(0, 0, Width, Height), _colora, _colorb, _angle);
                Invalidate();
            }
        }

        public bool VerticalText
        {
            get { return _vertical; }
            set { _vertical = value; _stringformat.FormatFlags = value ? StringFormatFlags.DirectionVertical : (StringFormatFlags)0; Invalidate(); }
        }

        public StringAlignment Alignment
        {
            get { return _stringformat.Alignment; }
            set { _stringformat.Alignment = value; Invalidate(); }
        }

        public StringAlignment LineAlignment
        {
            get { return _stringformat.LineAlignment; }
            set { _stringformat.LineAlignment = value; Invalidate(); }
        }

        #endregion

        #region Überschriebene Events

        protected override void OnResize(EventArgs e)
        {
            _backgroundbrush = new LinearGradientBrush(new Rectangle(0, 0, Width, Height), _colora, _colorb, _angle);

            base.OnResize(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            // Hintergrund zeichnen
            e.Graphics.FillRectangle(_backgroundbrush, e.ClipRectangle);
            e.Graphics.Flush();

            // e.Graphics.RotateTransform(_textangle);
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            e.Graphics.DrawString(_text, this.Font, _textcolor, e.ClipRectangle, _stringformat);
            e.Graphics.Flush();
        }

        #endregion
    }
}
