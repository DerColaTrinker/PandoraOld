﻿using Pandora.Game.Templates;
using Pandora.Game.Templates.Bindings;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pandora.Tools.TemplateEditor.Controls
{
    class TemplateRow : DataGridViewRow
    {
        public TemplateRow(TemplateBase template, TemplateBinding binding, IEnumerator columns)
        {
            Template = template;
            Binding = binding;

            while (columns.MoveNext())
            {
                var cell = (DataGridViewCell)null;
                var col = columns.Current;

                if (columns.Current is PropertyTextBoxColumn)
                {
                    var column = columns.Current as PropertyTextBoxColumn;
                    cell = new DataGridViewTextBoxCell();
                    cell.ValueType = column.ValueType;
                    cell.Value = column.PropertyBinding.GetValue(template);
                }

                if (columns.Current is PropertyComboBoxColumn)
                {
                    var column = columns.Current as PropertyComboBoxColumn;
                    cell = new DataGridViewComboBoxCell();

                    if (column.PropertyBinding.Property.PropertyType.IsEnum)
                        cell.ValueType = typeof(string);
                    else
                        cell.ValueType = column.ValueType;

                    foreach (var item in column.Items)
                    {
                        ((DataGridViewComboBoxCell)cell).Items.Add(item);
                    }

                    ((DataGridViewComboBoxCell)cell).Value = column.PropertyBinding.GetValue(template);
                }

                if (columns.Current is PropertyCheckBoxColumn)
                {
                    var column = columns.Current as PropertyCheckBoxColumn;
                    cell = new DataGridViewCheckBoxCell();
                    cell.ValueType = column.ValueType;
                    cell.Value = column.PropertyBinding.GetValue(template);
                }

                Cells.Add(cell);
            }
        }

        public TemplateBase Template { get; private set; }

        public TemplateBinding Binding { get; private set; }
    }
}
