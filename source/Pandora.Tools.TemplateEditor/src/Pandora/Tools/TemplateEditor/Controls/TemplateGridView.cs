﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Pandora.Game.Templates.Bindings;
using Pandora.Game.Templates;

namespace Pandora.Tools.TemplateEditor.Controls
{
    public partial class TemplateGridView : UserControl
    {
        private TemplateManager _manager;
        private TemplateBinding _binding;

        public TemplateGridView()
        {
            InitializeComponent();
        }

        public TemplateManager Manager
        {
            get { return _manager; }
            set
            {
                _manager = value;
                UpdateSelectors();
            }
        }

        private void UpdateSelectors()
        {
            ddlTemplates.Items.Clear();

            if (_manager == null) return;

            foreach (var item in _manager.Bindings.GetTemplateBindings())
            {
                ddlTemplates.Items.Add(item);
            }
        }

        private void UpdateGridView()
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();

            if (_binding == null) return;

            #region Columns

            foreach (var propertybinding in _binding.GetPropertyBindings())
            {
                var column = (DataGridViewColumn)null;

                switch (propertybinding.PropertyAttribute.TemplatePropertyType)
                {
                    case Pandora.Game.Templates.InternalTemplatePropertyTypes.Identifier:
                        column = new IdentifierColumn(propertybinding);
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        column.FillWeight = 80;
                        break;

                    case Pandora.Game.Templates.InternalTemplatePropertyTypes.ResourceCode:
                        column = new ResourceCodeColumn(propertybinding);
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        column.FillWeight = 10;
                        break;

                    case Pandora.Game.Templates.InternalTemplatePropertyTypes.Required:
                        column = new RequiredColumn(propertybinding, _manager);
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        column.FillWeight = 10;
                        break;

                    case Pandora.Game.Templates.InternalTemplatePropertyTypes.UpgradeLevel:
                        column = new UpdateLevelColumn(propertybinding);
                        column.Width = 50;
                        break;

                    case InternalTemplatePropertyTypes.UpgradableTemplateBase:
                        column = new UpgradableTemplateColumn(_manager, propertybinding);
                        column.Width = 60;
                        break;

                    case Pandora.Game.Templates.InternalTemplatePropertyTypes.Default:
                        {
                            switch (propertybinding.PropertyAttribute.SpecialPropertyType)
                            {
                                case Pandora.Game.Templates.SpecialPropertyTypes.Default:
                                    if (propertybinding.Property.PropertyType.IsEnum)
                                    {
                                        column = new PropertyEnumColumn(propertybinding);
                                    }
                                    else
                                    {
                                        if (propertybinding.Property.PropertyType == typeof(bool))
                                        {
                                            column = new PropertyCheckBoxColumn(propertybinding);
                                            column.ValueType = typeof(bool);
                                        }
                                        else
                                        {
                                            column = new PropertyTextBoxColumn(propertybinding);
                                            column.ValueType = propertybinding.Property.PropertyType;
                                        }
                                    }
                                    break;

                                case Pandora.Game.Templates.SpecialPropertyTypes.Ratio:
                                    column = new PropertyTextBoxColumn(propertybinding);
                                    column.ValueType = propertybinding.Property.PropertyType;
                                    column.DefaultCellStyle.Format = "N2";
                                    column.DefaultCellStyle.NullValue = 0F;
                                    break;
                            }

                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        }
                        break;
                }

                column.HeaderText = propertybinding.Name;
                column.MinimumWidth = 60;

                dataGridView1.Columns.Add(column);
            }

            #endregion

            #region Rows

            var templates = _manager.GetTemplates(_binding.TemplateType);
            foreach (var template in templates)
            {
                var row = new TemplateRow(template, _binding, dataGridView1.Columns.GetEnumerator());

                dataGridView1.Rows.Add(row);
            }

            #endregion
        }

        private void ddlTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            _binding = (TemplateBinding)ddlTemplates.SelectedItem;

            UpdateGridView();

            toolStripButton1.Enabled = _binding != null;
            toolStripButton2.Enabled = _binding != null;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            var identifier = Guid.NewGuid().ToString();
            var template = (TemplateBase)Activator.CreateInstance(_binding.TemplateType, new object[] { identifier });
            var row = new TemplateRow(template, _binding, dataGridView1.Columns.GetEnumerator());

            dataGridView1.Rows.Add(row);

            _manager.Add(template);
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            var row = dataGridView1.Rows[e.RowIndex] as TemplateRow;
            var column = dataGridView1.Columns[e.ColumnIndex];
            var cell = row.Cells[e.ColumnIndex];

            var template = row.Template;
            var property = ((IPropertyColumn)column).PropertyBinding;

            switch (property.PropertyAttribute.TemplatePropertyType)
            {
                case InternalTemplatePropertyTypes.Identifier:
                    // Hier muss das Ändern über dem Manager erfolgen, da auch die Bezüge direkt mit umbenannt werden
                    _manager.Rename(template, cell.Value.ToString());
                    break;

                case InternalTemplatePropertyTypes.ResourceCode: break;
                case InternalTemplatePropertyTypes.Required: break;
                case InternalTemplatePropertyTypes.UpgradeLevel: break;
                case InternalTemplatePropertyTypes.Default:
                    if (property.Property.PropertyType.IsEnum)
                    {
                        var value = cell.Value.ToString();

                        property.SetValue(template, Enum.Parse(property.Property.PropertyType, value));
                    }
                    else
                    {
                        property.SetValue(template, cell.Value);
                    }

                    break;
            }
        }
    }
}
