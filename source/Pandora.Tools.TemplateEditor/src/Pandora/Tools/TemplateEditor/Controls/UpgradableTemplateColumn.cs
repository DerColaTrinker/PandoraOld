﻿using Pandora.Game.Templates;
using Pandora.Game.Templates.Bindings;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Tools.TemplateEditor.Controls
{
    class UpgradableTemplateColumn : PropertyComboBoxColumn
    {
        public UpgradableTemplateColumn(TemplateManager _manager, TemplateBindingProperty property)
            : base(property)
        {
            var templatetype = base.PropertyBinding.TemplateBinding.TemplateType.BaseType.GenericTypeArguments[0];

            Items.AddRange(_manager.GetTemplates(templatetype).Select(m => m.Identifier).ToArray());

            DefaultCellStyle.BackColor = Color.Black;
            DefaultCellStyle.ForeColor = Color.White;
            ValueType = typeof(string);
        }
    }
}
