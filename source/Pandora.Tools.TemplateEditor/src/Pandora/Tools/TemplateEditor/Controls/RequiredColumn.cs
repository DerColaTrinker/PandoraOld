﻿using Pandora.Game.Templates;
using Pandora.Game.Templates.Bindings;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Tools.TemplateEditor.Controls
{
    class RequiredColumn : PropertyComboBoxColumn
    {
        public RequiredColumn(TemplateBindingProperty property, TemplateManager manager)
            : base(property)
        {
            HeaderText = property.Name;
            DefaultCellStyle.BackColor = Color.DarkGray;
            DefaultCellStyle.ForeColor = Color.White;
            ValueType = typeof(string);

            Manager = manager;

            Items.AddRange(Manager.GetTemplates().Select(m => m.Identifier).ToArray());
        }

        public TemplateManager Manager { get; private set; }
    }
}
