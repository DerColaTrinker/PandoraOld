﻿using System.Xml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Pandora.Game.Templates;
using System.IO;
using Pandora.Runtime.IO;
using System.Reflection;

namespace Pandora.Tools.TemplateEditor.Dialogs
{
    public partial class MainWindow : Form
    {
        private TemplateManager _manager = new TemplateManager();
        private FileInfo _currentfile = null;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            templateGridView1.Manager = _manager;
        }

        private void mnuFileLoadGame_Click(object sender, EventArgs e)
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.Title = "Open Game file";
                dialog.CheckFileExists = true;
                dialog.Filter = "Pandora game file (*.dll)|*.dll|All files (*.*)|*.*";


                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    //try
                    //{
                        var asm = Assembly.LoadFrom(dialog.FileName);
                        _manager.Bindings.ScanAssembly(asm);

                        templateGridView1.Manager = _manager;
                    //}
                    //catch (Exception ex)
                    //{
                    //    MessageBox.Show(string.Format("Error : {0}\n{1}\n{2}", ex.Message, ex.InnerException == null ? string.Empty : ex.InnerException.Message, ex.StackTrace), ex.GetType().Name, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //}
                }
            }
        }

        private void mnuFileSaveAs_Click(object sender, EventArgs e)
        {
            using (var dialog = new SaveFileDialog())
            {
                dialog.Title = "Save as";
                dialog.Filter = "Pandora Template File (*.pptf)|*.pptf|All files (*.*)|*.*";
                dialog.CheckPathExists = true;

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    _currentfile = new FileInfo(dialog.FileName);

                    _manager.Save(_currentfile);
                }
            }
        }

        private void mnuFileSave_Click(object sender, EventArgs e)
        {
            if (_currentfile == null)
                mnuFileSaveAs.PerformClick();

            _manager.Save(_currentfile);
        }

        private void mnuFileOpen_Click(object sender, EventArgs e)
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.Title = "Open";
                dialog.Filter = "Pandora Template File (*.pptf)|*.pptf|All files (*.*)|*.*";
                dialog.CheckFileExists = true;

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    _currentfile = new FileInfo(dialog.FileName);

                    _manager.Load(_currentfile);
                }
            }

            templateGridView1.Manager = _manager;
        }

        private void mnuFileNew_Click(object sender, EventArgs e)
        {
            _manager = new TemplateManager();

            templateGridView1.Manager = _manager;
        }
    }
}
