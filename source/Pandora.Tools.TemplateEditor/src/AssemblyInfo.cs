﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Pandora Template Editor")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Projekt-Pandora.de")]
[assembly: AssemblyProduct("Pandora Template Editor")]
[assembly: AssemblyCopyright("Copyright © Projekt-Pandora.de 2016-2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("fe04d792-9990-462b-8f2d-ca050d049c6b")]

[assembly: AssemblyVersion("0.0.2.0")]
[assembly: AssemblyFileVersion("0.0.2.0")]
