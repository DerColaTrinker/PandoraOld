﻿using Pandora.Game.World.Objects;
using Pandora.Game.World.Objects.Fleet;
using Pandora.Game.World.Objects.Ship;
using Pandora.Runtime.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Handlers.Fleet
{
    /// <summary>
    /// Ein Delegate das im FleetShipHandler verwendet wird
    /// </summary>
    /// <typeparam name="TFleet"></typeparam>
    /// <typeparam name="TShip"></typeparam>
    /// <param name="fleet"></param>
    /// <param name="ship"></param>
    public delegate void FleetShipHandlerDelegate<TFleet, TShip>(TFleet fleet, TShip ship);

    /// <summary>
    /// Ein Handler der Schiffe innerhalb einer Flotte verwaltet
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    /// <typeparam name="TFleet"></typeparam>
    /// <typeparam name="TShip"></typeparam>
    public class FleetShipHandler<TRace, TFleet, TShip> : FleetHandler<TRace, TFleet>
        where TRace : RaceBase
        where TFleet : FleetBase
        where TShip : ShipBase
    {
        private OwnerHashSet<TFleet, TShip> _collections = new OwnerHashSet<TFleet, TShip>();
        private FleetHandler<TRace, TFleet> _fleethandler;

        /// <summary>
        /// Wird aufgerufen wenn ein Schiff in die Flotte eingefügt wird
        /// </summary>
        public event FleetShipHandlerDelegate<TFleet, TShip> ShipAdded;

        /// <summary>
        /// Wird aufgerufen wenn ein Schiff aus der Flotte entfernt wird
        /// </summary>
        public event FleetShipHandlerDelegate<TFleet, TShip> ShipRemoved;

#pragma warning disable 1591
        protected override void OnStart()
        {
            _fleethandler = GetHandler<FleetHandler<TRace, TFleet>>();
        }

        protected override void OnFleetAdded(TRace race, TFleet fleet)
        {
            _collections.AddOwner(fleet);
            base.OnFleetAdded(race, fleet);
        }

        protected override void OnFleetRemoved(TRace race, TFleet fleet)
        {
            _collections.RemoveOwner(fleet);
            base.OnFleetRemoved(race, fleet);
        }
#pragma warning restore 1591

        /// <summary>
        /// Fügt ein Schiff der Flotte hinzu
        /// </summary>
        /// <param name="fleet"></param>
        /// <param name="ship"></param>
        public void AddShip(TFleet fleet, TShip ship)
        {
            if (_collections.AddElement(fleet, ship))
                OnShipAdded(fleet, ship);
        }

        /// <summary>
        /// Entfernt eine Schiff aus der Flotte
        /// </summary>
        /// <param name="fleet"></param>
        /// <param name="ship"></param>
        public void RemoveShip(TFleet fleet, TShip ship)
        {
            if (_collections.RemoveElement(fleet, ship))
                OnShipRemoved(fleet, ship);
        }

        /// <summary>
        /// Löst das ShipAdded-Ereignis aus
        /// </summary>
        /// <param name="fleet"></param>
        /// <param name="ship"></param>
        protected virtual void OnShipAdded(TFleet fleet, TShip ship)
        {
            if (ShipAdded != null) ShipAdded.Invoke(fleet, ship);
        }

        /// <summary>
        /// Löst das ShipRemoved-Ereignis aus
        /// </summary>
        /// <param name="fleet"></param>
        /// <param name="ship"></param>
        protected virtual void OnShipRemoved(TFleet fleet, TShip ship)
        {
            if (ShipRemoved != null) ShipRemoved.Invoke(fleet, ship);
        }       
    }
}
