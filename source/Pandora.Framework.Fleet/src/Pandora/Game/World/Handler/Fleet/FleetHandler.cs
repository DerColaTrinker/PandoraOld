﻿using Pandora.Game.World.Objects;
using Pandora.Game.World.Objects.Fleet;
using Pandora.Runtime.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Handlers.Fleet
{
    /// <summary>
    /// Ein Delegate der im FleetHandler verwendet wird
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    /// <typeparam name="TFleet"></typeparam>
    /// <param name="race"></param>
    /// <param name="fleet"></param>
    public delegate void FleetHandlerDelegate<TRace, TFleet>(TRace race, TFleet fleet);

    /// <summary>
    /// Ein Handler der Spielerflotten verwaltet
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    /// <typeparam name="TFleet"></typeparam>
    public class FleetHandler<TRace, TFleet> : WorldHandlerBase<TRace>
        where TRace : RaceBase
        where TFleet : FleetBase
    {
        private OwnerHashSet<TRace, TFleet> _collections = new OwnerHashSet<TRace, TFleet>();

        /// <summary>
        /// Wird aufgerufen wenn eine Flotte eingefügt wird
        /// </summary>
        public event FleetHandlerDelegate<TRace, TFleet> FleetAdded;

        /// <summary>
        /// Wird aufgerufen wenn eine Flotte entfernt wird
        /// </summary>
        public event FleetHandlerDelegate<TRace, TFleet> FleetRemoved;

#pragma warning disable 1591
        protected override void OnStart()
        {
            Races.RaceAdded += delegate(TRace race) { _collections.AddOwner(race); };
            Races.RaceRemoved += delegate(TRace race) { _collections.RemoveOwner(race); };

            base.OnStart();
        }
#pragma warning restore 1591

        /// <summary>
        /// Fügt eine neue Flotte hinzu
        /// </summary>
        /// <param name="race"></param>
        /// <param name="fleet"></param>
        public void AddFleet(TRace race, TFleet fleet)
        {
            if (_collections.AddElement(race, fleet))
                OnFleetAdded(race, fleet);
        }

        /// <summary>
        /// Entfernt eine Flotte
        /// </summary>
        /// <param name="race"></param>
        /// <param name="fleet"></param>
        public void RemoveFleet(TRace race, TFleet fleet)
        {
            if (_collections.RemoveElement(race, fleet))
                OnFleetRemoved(race, fleet);
        }

        /// <summary>
        /// Liefert eine Auflistung aller Flotten
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TFleet> GetFleets()
        {
            return _collections.GetElements();
        }

        /// <summary>
        /// Liefert eine Auflistung aller Flotten einer Rasse
        /// </summary>
        /// <param name="race"></param>
        /// <returns></returns>
        public IEnumerable<TFleet> GetFleets(TRace race)
        {
            return _collections.GetElements(race);
        }

        /// <summary>
        /// Löst das FleetAdded-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        /// <param name="fleet"></param>
        protected virtual void OnFleetAdded(TRace race, TFleet fleet)
        {
            if (FleetAdded != null) FleetAdded.Invoke(race, fleet);
        }

        /// <summary>
        /// Löst das FleetRemoved-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        /// <param name="fleet"></param>
        protected virtual void OnFleetRemoved(TRace race, TFleet fleet)
        {
            if (FleetRemoved != null) FleetRemoved.Invoke(race, fleet);
        }
    }
}
