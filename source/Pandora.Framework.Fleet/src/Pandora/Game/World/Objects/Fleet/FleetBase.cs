﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects.Fleet
{
    /// <summary>
    /// Stellt eine Flotte dar
    /// </summary>
    public abstract class FleetBase : SystemMapObjectBase
    { }

    /// <summary>
    /// Stellt eine Flotte dar
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    public abstract class FleetBase<TRace> : FleetBase
        where TRace : RaceBase
    { }
}
