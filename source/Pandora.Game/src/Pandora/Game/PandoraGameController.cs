﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game
{
    public class PandoraGameController : GameControllerBase
    {
        public override Version GameControllerVersion { get { return System.Reflection.Assembly.GetEntryAssembly().GetName().Version; } }

        protected override void OnGameCreated()
        {
            //TODO: Menü zum erstellen der Spielwelt anzeigen

            base.OnGameCreated();
        }
    }
}
