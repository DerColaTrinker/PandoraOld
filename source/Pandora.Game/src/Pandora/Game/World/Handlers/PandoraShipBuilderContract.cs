﻿using Pandora.Game.World.Handlers.Ship;
using Pandora.Game.World.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Handlers
{
    public class PandoraShipBuilderContract : ShipBuilderContractBase<PlanetColony, FleetShipDesign>
    {
        protected override void Begin()
        {
            CurrentConstructionPoints = 0F;
            RequiredConstructionPoints = base.ShipDesign.RequiredConstructionPoints;

            base.Begin();
        }

        public override void WorldUpdate(float ms, float s)
        {
            CurrentConstructionPoints += Location.Construction * s;

            if (CurrentConstructionPoints > RequiredConstructionPoints) Finish();
        }

        public float CurrentConstructionPoints { get; private set; }

        public float RequiredConstructionPoints { get; private set; }

        public float Percent { get { return (CurrentConstructionPoints / RequiredConstructionPoints) * 100; } }
    }
}
