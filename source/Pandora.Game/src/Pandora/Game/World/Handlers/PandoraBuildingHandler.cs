﻿using Pandora.Game.World.Handlers.Building;
using Pandora.Game.World.Handlers.Colony;
using Pandora.Game.World.Objects;
using Pandora.Game.Templates.Buildings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.World.Handlers
{
    public sealed class PandoraBuildingHandler : BuildingHandler<PlanetColony, PlanetColonyBuilding>
    {
        private IEnumerable<BuildingTemplate> _templates;
        private HashSet<PlanetColonyBuilding> _upgradebuildings = new HashSet<PlanetColonyBuilding>();

        protected override void OnInitialize()
        {
            // Die Gebäude verfügen über ein Haupttemplate und die entsprechenden Upgrade-Templates.
            // Damit die Liste nicht bei jeder neuen Kolonie geladen werden müssen, werden sie hier einmal zwischen gespeichert.
            _templates = Templates.GetTemplates<BuildingTemplate>();
        } 
    }
}
