﻿using Pandora.Game.World.Handlers.Component;
using Pandora.Game.World.Objects;
using Pandora.Runtime.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Pandora.Game.World.Handlers
{
    public class PandoraShipComponentDescriptor : ComponentDescriptorHandler<Race, ShipComponentDescriptor>
    {
        protected override void OnCreateComponentDescriptors(Race race, OwnerHashSet<Race, ShipComponentDescriptor>.ElementCollection collection)
        {

        }
    }
}
