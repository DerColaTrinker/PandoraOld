﻿using Pandora.Game.World.Handlers.Colony;
using Pandora.Game.World.Objects;
using Pandora.Game.Templates.Buildings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Handlers
{
    public sealed class PandoraColonyHandler : ColonyHandler<Race, Planet, PlanetColony>
    {
        private PandoraBuildingHandler _buildings;

        private HashSet<PlanetColonyBuilding> _upgradebuildings = new HashSet<PlanetColonyBuilding>();

        protected override void OnInitialize()
        {
            // Den Gebäude-Handler laden. Der weiß auf welcher Kolonie welche Gebäude errichtet wurden
            _buildings = GetHandler<PandoraBuildingHandler>();
        }

        public override void Invalidate(PlanetColony colony)
        {
            // Dieser Aufruf berechnet eine Kolonie neu. Dies ist der Fall wenn sich die Werte der Kolonie ändern. z.B. ein Gebäude Ausgebaut wurde
            // Aus Performancegründen wird das nur bei Änderung gemacht, und nicht permanent.

            // Eine Liste aller Gebäude dieser Kolonie holen
            var buildings = _buildings.GetBuildings(colony);

            // Die Werte müssen zurückgesetzt werden, damit die FOREACH-Schleife die Summen korrekt addieren kann
            colony.PopulationMax.Clear();
            colony.PopulationGrowth.Clear();
            colony.Construction.Clear();
            colony.Research.Clear();
            colony.Espionage.Clear();

            colony.MoneyIn.Clear();
            colony.MoneyOut.Clear();

            /// Schleife die alle Gebäude der Kolonie durchläuft
            foreach (var building in buildings)
            {
                // Ein Gebäude ignorieren, das keine Ausbaustufe besitzt
                if (!building.IsBuild) continue;

                // Population
                colony.PopulationMax.Fixed += building.CurrentUpgradeTemplate.MaxPopulationFixed;
                colony.PopulationMax.Percent += building.CurrentUpgradeTemplate.MaxPopulationPercent;
                colony.PopulationGrowth.Fixed += building.CurrentUpgradeTemplate.PopulationGrowthFixed;
                colony.PopulationGrowth.Percent += building.CurrentUpgradeTemplate.PopulationGrowthPercent;

                // Construction
                colony.Construction.Fixed += building.CurrentUpgradeTemplate.ConstructionPointsFixed;
                colony.Construction.Percent += building.CurrentUpgradeTemplate.ConstructionPointsPercent;
                colony.Construction.PerPopulation += building.CurrentUpgradeTemplate.ConstructionPointsPerPopulation;

                // Research
                colony.Research.Fixed += building.CurrentUpgradeTemplate.ResearchPointsFixed;
                colony.Research.Percent += building.CurrentUpgradeTemplate.ResearchPointsPercent;
                colony.Research.PerPopulation += building.CurrentUpgradeTemplate.ResearchPointsPerPopulation;

                // Espionage
                colony.Espionage.Fixed += building.CurrentUpgradeTemplate.EspionagePointsFixed;
                colony.Espionage.Percent += building.CurrentUpgradeTemplate.EspionagePointsPercent;
                colony.Espionage.PerPopulation += building.CurrentUpgradeTemplate.EspionagePointsPerPopulation;

                // Einkommen
                colony.MoneyIn.Fixed += building.CurrentUpgradeTemplate.MoneyIncomingFixed;
                colony.MoneyIn.Percent += building.CurrentUpgradeTemplate.MoneyIncomingPercent;
                colony.MoneyIn.PerPopulation += building.CurrentUpgradeTemplate.MoneyIncomingPerPopulation;

                // Ausgaben
                colony.MoneyOut.Fixed += building.CurrentUpgradeTemplate.MoneyOutgoingFixed;
                colony.MoneyOut.Percent += building.CurrentUpgradeTemplate.MoneyOutgoingPercent;
                colony.MoneyOut.PerPopulation += building.CurrentUpgradeTemplate.MoneyOutgoingPerPopulation;
            }

            // Nachberechnen der Population
            colony.PopulationMax.Calculate();
            colony.PopulationGrowth.Calculate();

            // Nachberechnen der 3 Hauptwerte inkl. aktuelle Population
            colony.Construction.Calculate(colony.Population);
            colony.Research.Calculate(colony.Population);
            colony.Espionage.Calculate(colony.Population);

            // Ein- und Ausgaben inkl. aktuelle Population
            colony.MoneyIn.Calculate(colony.Population);
            colony.MoneyOut.Calculate(colony.Population);
        }

        protected override void OnRaceAdd(Race race)
        {
            // Startplaneten suchen, der üppig bestückt ist
            var target = Map.GetSystemObjects<Planet>()
                         .Where(planet => !planet.HasColoy)
                         .Where(planet => planet.Temperature >= race.MinTemperature & planet.Temperature <= race.MaxTemperature)
                //.Where(planet => planet.BasePopulation > 10 & planet.BaseSize > 10 & planet.Wealth > 20)
                         .OrderByDescending(planet => planet.Wealth)
                         .OrderByDescending(planet => planet.BaseSize)
                         .OrderByDescending(planet => planet.BasePopulation)
                         .FirstOrDefault();

            // Den Planeten mit der Rasse kolonisieren und angeben, das es sich ein Spieleinstieg handelt
            Colonize(race, target, ColonizeType.RaceAdding);
        }
    }
}

