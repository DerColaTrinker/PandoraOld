﻿using Pandora.Game.World.Objects.Building;
using Pandora.Game.Templates.Buildings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Runtime.IO;

namespace Pandora.Game.World.Objects
{
    public class PlanetColonyBuilding : BuildingBase<PlanetColony, BuildingTemplate, BuildingUpgradeTemplate>
    {
        public float RequiredConstructionPoints { get; set; }

        public float CurrentConstructionPoints { get; set; }

        public override void Serialize(IWriter writer)
        {
            base.Serialize(writer);

            writer.WriteFloat(RequiredConstructionPoints);
            writer.WriteFloat(CurrentConstructionPoints);
        }

        public override void Deserialize(IReader reader)
        {
            base.Deserialize(reader);

            RequiredConstructionPoints = reader.ReadFloat();
            CurrentConstructionPoints = reader.ReadFloat();
        }
    }
}
