﻿using Pandora.Game.Templates;
using Pandora.Game.Templates.MapObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    public class Planet : ColonizableSystemObjectBase<PlanetTemplate, PlanetColony>
    {
        public StarOrbitTemplates PlanetOrbit { get; set; }

        public int BasePopulation { get; set; }

        public int BaseSize { get; set; }

        public int Wealth { get; set; }

        public float Temperature { get; set; }

        public float StarOrbitAngle { get; set; }

        public float Distance { get; set; }

        public double Hullarea { get { return 4 * Math.PI * Math.Pow(Distance, 2); } }

        public double Solarconst { get; set; }

        public override void Serialize(Runtime.IO.IWriter writer)
        {
            base.Serialize(writer);

            writer.WriteString(PlanetOrbit.Identifier);
            writer.WriteInt32(BasePopulation);
            writer.WriteInt32(BaseSize);
            writer.WriteInt32(Wealth);
            writer.WriteFloat(Temperature);
            writer.WriteFloat(StarOrbitAngle);
            writer.WriteFloat(Distance);
            writer.WriteDouble(Solarconst);
        }

        public override void Deserialize(Runtime.IO.IReader reader)
        {
            base.Deserialize(reader);

            PlanetOrbit = Instance.Templates.GetTemplate<StarOrbitTemplates>(reader.ReadString());
            BasePopulation = reader.ReadInt32();
            BaseSize = reader.ReadInt32();
            Wealth = reader.ReadInt32();
            Temperature = reader.ReadFloat();
            StarOrbitAngle = reader.ReadFloat();
            Distance = reader.ReadFloat();
            Solarconst = reader.ReadDouble();
        }




    }
}
