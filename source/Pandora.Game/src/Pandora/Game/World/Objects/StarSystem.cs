﻿using Pandora.Game.Templates.MapObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    public sealed class StarSystem : GalaxyMapObjectBase<StarTemplate>
    {
        public StarSystem()
        {
            Wormholes = new HashSet<SystemWormhole>();
        }

        public float MassFactor { get; set; }

        public override void Serialize(Runtime.IO.IWriter writer)
        {
            base.Serialize(writer);

            writer.WriteFloat(MassFactor);
        }

        public override void Deserialize(Runtime.IO.IReader reader)
        {
            base.Deserialize(reader);

            MassFactor = reader.ReadFloat();
        }

        public HashSet<SystemWormhole> Wormholes { get; private set; }

        public double RadiationEnergy { get; set; }
    }
}
