﻿using Pandora.Game.World.Objects.Component;
using Pandora.Game.Templates.ShipComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    public class ShipComponent< TTemplate> : ComponentBase, IShipComponent
        where TTemplate : ShipComponentTemplate
    {
        public new TTemplate Template { get { return (TTemplate)base.Template; } set { base.Template = value; } }
    }
}
