﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    public class SystemWormhole : SystemMapObjectBase
    {
        public StarSystem SystemA { get; set; }

        public StarSystem SystemB { get; set; }

        public override void Serialize(Runtime.IO.IWriter writer)
        {
            base.Serialize(writer);

            writer.WriteUInt32(SystemA.ObjectID);
            writer.WriteUInt32(SystemB.ObjectID);
        }

        public override void Deserialize(Runtime.IO.IReader reader)
        {
            base.Deserialize(reader);

            var systemaid = reader.ReadUInt32();
            var systembid = reader.ReadUInt32();

            SystemA = Instance.Accessor.GetObject<StarSystem>(systemaid);
            SystemB = Instance.Accessor.GetObject<StarSystem>(systembid);
        }
    }
}
