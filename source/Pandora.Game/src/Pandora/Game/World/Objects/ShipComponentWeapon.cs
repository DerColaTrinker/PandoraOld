﻿using Pandora.Game.Templates.ShipComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Runtime.IO;

namespace Pandora.Game.World.Objects
{
    public class ShipComponentWeapon : ShipComponent<ShipComponentWeaponTemplate>
    {
        public override void Deserialize(IReader reader)
        {
            base.Deserialize(reader);
        }

        public override void Serialize(IWriter writer)
        {
            base.Serialize(writer);
        }
    }
}
