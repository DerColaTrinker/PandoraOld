﻿using Pandora.Game.World.Objects.Building;
using Pandora.Game.World.Objects.Colony;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    public sealed class PlanetColony : ColonyBase<Race, Planet>, IShipBuilderLocation, IBuildingConstructionLocation
    {
        public PlanetColony()
        {
            PopulationMax = new ComplexValue();
            PopulationGrowth = new ComplexValue();

            Construction = new ComplexValue();
            Research = new ComplexValue();
            Espionage = new ComplexValue();

            MoneyIn = new ComplexValue();
            MoneyOut = new ComplexValue();
        }

        public ComplexValue PopulationMax { get; private set; }
        public ComplexValue PopulationGrowth { get; private set; }
        public float Population { get; internal set; }

        public ComplexValue Construction { get; private set; }
        public ComplexValue Research { get; private set; }
        public ComplexValue Espionage { get; private set; }

        public ComplexValue MoneyIn { get; private set; }
        public ComplexValue MoneyOut { get; private set; }
        public float MoneyBalance { get { return MoneyIn - MoneyOut; } }

        public int CurrentBuildingUpgrades { get; set; }

        public override void Serialize(Runtime.IO.IWriter writer)
        {
            base.Serialize(writer);

            PopulationMax.Serialize(writer);
            PopulationGrowth.Serialize(writer);
            writer.WriteFloat(Population);

            Construction.Serialize(writer);
            Research.Serialize(writer);
            Espionage.Serialize(writer);

            MoneyIn.Serialize(writer);
            MoneyOut.Serialize(writer);
            writer.WriteFloat(MoneyBalance);

            writer.WriteInt32(CurrentBuildingUpgrades);
        }

        public override void Deserialize(Runtime.IO.IReader reader)
        {
            base.Deserialize(reader);

            PopulationMax.Deserialize(reader);
            PopulationGrowth.Deserialize(reader);
            Population = reader.ReadFloat();

            Construction.Deserialize(reader);
            Research.Deserialize(reader);
            Espionage.Deserialize(reader);

            MoneyIn.Deserialize(reader);
            MoneyOut.Deserialize(reader);
            reader.ReadFloat();  // Summy, wird direkt berechnet.

            CurrentBuildingUpgrades = reader.ReadInt32();
        }

        #region IShipBuilderLocation Member

        public int MaxShipBuilderQueue { get; set; }

        public int ParallelShipUpdates { get; set; }

        #endregion
    }
}
