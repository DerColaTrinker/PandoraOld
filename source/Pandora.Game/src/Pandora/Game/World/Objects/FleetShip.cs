﻿using Pandora.Game.World.Objects.Component;
using Pandora.Game.World.Objects.Ship;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    public class FleetShip : ShipBase<Race, FleetShipDesign>, IComponentContainer
    {
    }
}
