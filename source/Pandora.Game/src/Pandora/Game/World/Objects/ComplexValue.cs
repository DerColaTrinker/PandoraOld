﻿using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    public sealed class ComplexValue : ISerializable
    {
        public float Fixed { get; set; }
        public float Percent { get; set; }
        public float PerPopulation { get; set; }
        public float Population { get; set; }

        public float Result { get; private set; }

        public void Clear()
        {
            Fixed = 0F;
            Percent = 0F;
            PerPopulation = 0F;
            Result = 0F;
        }

        public void Calculate()
        {
            Result = Fixed + (PerPopulation * Population);
            Result += Fixed * Percent;
        }

        internal void Calculate(float population)
        {
            Population = population;
            Calculate();
        }

        public static implicit operator float(ComplexValue a)
        {
            return a.Result;
        }

        public void Serialize(IWriter writer)
        {
            writer.WriteFloat(Fixed);
            writer.WriteFloat(Percent);
            writer.WriteFloat(PerPopulation);
            writer.WriteFloat(Population);
        }

        public void Deserialize(IReader reader)
        {
            Fixed = reader.ReadFloat();
            Percent = reader.ReadFloat();
            PerPopulation = reader.ReadFloat();
            Population = reader.ReadFloat();
        }
    }
}
