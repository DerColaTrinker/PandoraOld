﻿using Pandora.Game.Templates.MapObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    public sealed class Star : SystemMapObjectBase<StarTemplate>
    {
        public override void Serialize(Runtime.IO.IWriter writer)
        {
            base.Serialize(writer);

            writer.WriteFloat(MassFactor);
            writer.WriteFloat(Temperature);
            writer.WriteFloat(MassFactor);
        }

        public override void Deserialize(Runtime.IO.IReader reader)
        {
            base.Deserialize(reader);

            MassFactor = reader.ReadFloat();
            Temperature = reader.ReadFloat();
            MassFactor = reader.ReadFloat();
        }

        public float Radius { get; set; }

        public double Hullarea { get { return 4 * Math.PI * Math.Pow(Radius, 2); } }

        public float Temperature { get; set; }

        public float MassFactor { get; set; }

        public double RadiationEnergy { get; set; }
    }
}
