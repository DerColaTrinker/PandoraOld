﻿using Pandora.Game.Templates.ShipComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Runtime.IO;

namespace Pandora.Game.World.Objects
{
    public class ShipComponentShield : ShipComponent<ShipComponentShieldTemplate>
    {
        public override void WorldUpdate(float ms, float s)
        {
            if (HitPoints < MaxHitPoints)
            {
                HitPoints += RegenerationPerSecond * s;

                if (HitPoints > MaxHitPoints) HitPoints = MaxHitPoints;
            }
        }

        public float HitPoints { get; set; }

        public float MaxHitPoints { get { return Template.MaxHitPoints; } }

        public float RegenerationPerSecond { get { return Template.RegenerationPerSecond; } }

        public override void Serialize(IWriter writer)
        {
            base.Serialize(writer);

            writer.WriteFloat(HitPoints);
        }

        public override void Deserialize(IReader reader)
        {
            base.Deserialize(reader);

            HitPoints = reader.ReadFloat();
        }
    }
}
