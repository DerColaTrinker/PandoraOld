﻿using Pandora.Game.World.Scenes;
using Pandora.Game.World.Handlers;
using Pandora.Game.World.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game.World.Generators;
using System.IO;
using Pandora.Runtime.IO;

namespace Pandora.Game.World
{
    public sealed class PandoraWorld : WorldModificationBase<Race>
    {
        public PandoraWorld()
        { }

        protected override bool OnInitialize()
        {
            Buildings = new PandoraBuildingHandler();
            Colonies = new PandoraColonyHandler();
            ShipComponentDescriptor = new PandoraShipComponentDescriptor();

            // Anmelden, dann wird das WorldUpdate automatisch ausgelöst
            RegisterHandler(Buildings);
            RegisterHandler(Colonies);
            RegisterHandler(ShipComponentDescriptor);

            // Übersetzungen laden
            Translations.Load(Path.Combine(Directory, "Data", "de.language"));

            // Templates laden
            Templates.Load(Path.Combine(Directory, "Data", "basetemplates.pptf"));

            return true;
        }

        protected override bool OnCreateWorld()
        {
            return Map.GenerateWorld<WorldGenerator>(new WorldGenerator(), Environment.TickCount);
        }

        protected override void OnSave(IWriter writer)
        {
            Map.Serialize(writer);
            Colonies.Serialize(writer);
            Buildings.Serialize(writer);
        }

        protected override void OnLoad(IReader reader)
        {
            Map.Deserialize(reader);
            Colonies.Deserialize(reader);
            Buildings.Deserialize(reader);
        }

        public PandoraColonyHandler Colonies { get; private set; }

        public PandoraBuildingHandler Buildings { get; private set; }

        public PandoraShipComponentDescriptor ShipComponentDescriptor { get; private set; }

        public List<StarSystem> FindRoute(StarSystem start, StarSystem target)
        {
            if (start == null || target == null) return null;

            var openList = new Stack<PathNode>();
            var closedList = new List<StarSystem>();
            var targetnode = new PathNode(target);

            Logger.Debug("Path Finding {0} -> {1}", start.Name, target.Name);
            var result = new List<StarSystem>();

            lock (openList)
            {
                openList.Push(new PathNode(start));
                while (openList.Count != 0)
                {
                    var pathNode = openList.Peek();
                    pathNode.Scans++;
                    if (pathNode == targetnode)
                    {
                        List<StarSystem> list = (from m in openList where m.Scans > 0 select m.System).ToList<StarSystem>();

                        Logger.Debug("Path found in {0} routes", list.Count);
                        result = list;
                        return result;
                    }

                    var foundtargets = (from wormhole in pathNode.System.Wormholes
                                        where !closedList.Contains(wormhole.SystemB)
                                        where (from node in openList where node.System == wormhole.SystemB select 1).Sum() == 0
                                        select wormhole.SystemB).ToList<StarSystem>();

                    if (foundtargets.Count<StarSystem>() > 0)
                    {
                        foundtargets.ForEach(current =>
                        {
                            openList.Push(new PathNode(current));
                        });
                    }

                    PathNode pathNode2 = openList.Pop();

                    closedList.Add(pathNode2.System);
                }
                Logger.Error("Route to {0} not found", target.Name);
                result = null;
            }

            return result;
        }

        public override string Name
        {
            get { return "Pandora default"; }
        }

        public override Version Version { get { return System.Reflection.Assembly.GetEntryAssembly().GetName().Version; } }
    }
}
