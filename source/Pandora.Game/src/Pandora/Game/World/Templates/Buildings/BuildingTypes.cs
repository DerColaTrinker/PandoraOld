﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.Templates.Buildings
{
    public enum BuildingTypes
    {
        NotSpecified,
        Construction,
        Population,
        Research,
        Espionage,
        Finance,
        Shipyard,
        Weapon
    }
}
