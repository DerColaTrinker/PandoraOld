﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates.Buildings
{
    [Template("Gebäude - Ausbaustufen")]
    public sealed class BuildingUpgradeTemplate : UpgradeTemplateBase<BuildingTemplate>
    {
        public BuildingUpgradeTemplate(string identifier)
            : base(identifier)
        { }

        #region Required

        [TemplateProperty(ColumnColor = ColumnColors.Color1)]
        public float RequiredConstructionPoints { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color1)]
        public float RequiredMoney { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color1)]
        public int RequiredBuildSize { get; set; }

        #endregion

        #region Population

        [TemplateProperty(ColumnColor = ColumnColors.Color2)]
        public float MaxPopulationFixed { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color2)]
        public float MaxPopulationPercent { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color2)]
        public float PopulationGrowthFixed { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color2)]
        public float PopulationGrowthPercent { get; set; }

        #endregion

        #region Construction-Points

        [TemplateProperty(ColumnColor = ColumnColors.Color3)]
        public float ConstructionPointsFixed { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color3)]
        public float ConstructionPointsPercent { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color3)]
        public float ConstructionPointsPerPopulation { get; set; }

        #endregion

        #region Research-Points

        [TemplateProperty(ColumnColor = ColumnColors.Color4)]
        public float ResearchPointsFixed { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color4)]
        public float ResearchPointsPercent { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color4)]
        public float ResearchPointsPerPopulation { get; set; }

        #endregion

        #region Espionage-Points

        [TemplateProperty(ColumnColor = ColumnColors.Color5)]
        public float EspionagePointsFixed { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color5)]
        public float EspionagePointsPercent { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color5)]
        public float EspionagePointsPerPopulation { get; set; }

        #endregion

        #region Money Incoming

        [TemplateProperty(ColumnColor = ColumnColors.Color6)]
        public float MoneyIncomingFixed { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color6)]
        public float MoneyIncomingPercent { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color6)]
        public float MoneyIncomingPerPopulation { get; set; }

        #endregion

        #region Money Outgoing

        [TemplateProperty(ColumnColor = ColumnColors.Color7)]
        public float MoneyOutgoingFixed { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color7)]
        public float MoneyOutgoingPercent { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color7)]
        public float MoneyOutgoingPerPopulation { get; set; }

        #endregion

        #region Absorbshield

        [TemplateProperty(ColumnColor = ColumnColors.Color8)]
        public float AbsorbShieldHitPoints { get; private set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color8)]
        public float AbsorbShieldRegenerate { get; private set; }

        #endregion

        #region Reduceshield

        [TemplateProperty(ColumnColor = ColumnColors.Color9)]
        public float ReduceShieldHitPoints { get; private set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color9)]
        public float ReduceShieldRegenerate { get; private set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color9)]
        public float ReduceShieldValuePercent { get; private set; }

        #endregion

        #region Armor & HitPoints

        [TemplateProperty(ColumnColor = ColumnColors.Color1)]
        public float Armor { get; private set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color1)]
        public float MaxHitPoints { get; private set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color1)]
        public float HitPointsRegenerate { get; private set; }

        #endregion

        #region Scanner

        [TemplateProperty(ColumnColor = ColumnColors.Color2)]
        public float ScannerRange { get; private set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color2)]
        public int ScannerTargets { get; private set; }

        #endregion

        #region Damage

        [TemplateProperty(ColumnColor = ColumnColors.Color3)]
        public float DamageAttackSpeed { get; private set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color3)]
        public int DamageMininmal { get; private set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color3)]
        public int DamageMaximal { get; private set; }

        #endregion
    }
}
