﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates.Buildings
{
    [Template("Gebäude")]
    public sealed class BuildingTemplate : UpgradableTemplateBase<BuildingUpgradeTemplate>
    {
        public BuildingTemplate(string identifier)
            : base(identifier)
        { }

        [TemplateProperty(ColumnColor = ColumnColors.Color1)]
        public BuildingTypes BuildingType { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color1)]
        public BuildingPositions BuildingPlace { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color3)]
        public bool Buildable { get; set; }
    }
}
