﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates.MapObjects
{
    [Template("Planet")]
    public sealed class PlanetTemplate : TemplateBase
    {
        public PlanetTemplate(string identifier)
            : base(identifier)
        { }

        [TemplateProperty(SpecialPropertyTypes.Ratio, ColumnColor = ColumnColors.Color9)]
        public float Ratio { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color1)]
        public int MinPopulation { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color1)]
        public int MaxPopulation { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color2)]
        public int MinWealth { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color2)]
        public int MaxWealth { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color3)]
        public int MinSize { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color3)]
        public int MaxSize { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color5)]
        public int MinDistance { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color5)]
        public int MaxRadius { get; set; }
    }
}