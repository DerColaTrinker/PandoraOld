﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates.MapObjects
{
    [Template("Planeten Orbit")]
    public sealed class StarOrbitTemplates : TemplateBase
    {
        public StarOrbitTemplates(string identifier)
            : base(identifier)
        { }

        [TemplateProperty(ColumnColor = ColumnColors.Color4)]
        public int MinDistance { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color4)]
        public int MaxDistace { get; set; }
    }
}
