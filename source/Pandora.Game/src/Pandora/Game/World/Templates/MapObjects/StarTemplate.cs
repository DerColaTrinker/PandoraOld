﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates.MapObjects
{
    [Template("Stern")]
    public sealed class StarTemplate : TemplateBase
    {
        public StarTemplate(string identifier)
            : base(identifier)
        { }

        [TemplateProperty(SpecialPropertyTypes.Ratio)]
        public float Ratio { get; set; }

        [TemplateProperty()]
        public int MinTemperature { get; set; }

        [TemplateProperty()]
        public int MaxTemperature { get; set; }

        [TemplateProperty()]
        public int MinRadius { get; set; }

        [TemplateProperty()]
        public int MaxRadius { get; set; }

        [TemplateProperty()]
        public float MassFactorMin { get; set; }

        [TemplateProperty()]
        public float MassFactorMax { get; set; }
    }
}
