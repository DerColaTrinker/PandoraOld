﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates.ShipComponents
{
    public abstract class ShipComponentTemplate : TemplateBase
    {
        public ShipComponentTemplate()
            : base()
        { }

        public ShipComponentTemplate(string identifier)
            : base(identifier)
        { }

        [TemplateProperty(ColumnColor = ColumnColors.Color9)]
        public ShipComponentType ComponentType { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color9)]
        public ShipComponentSlot ComponentSlot { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color8)]
        public float RequiredMoney { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color8)]
        public float RequiredConstructionPoints { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color8)]
        public float MoneyCosts { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color8)]
        public float CargoSpace { get; set; }
    }
}
