﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates.ShipComponents
{
    [Template("Schiffkomponente - Schilde")]
    public class ShipComponentShieldTemplate : ShipComponentTemplate
    {
        public ShipComponentShieldTemplate()
            : base()
        { }

        public ShipComponentShieldTemplate(string identifier)
            : base(identifier)
        { }

        [TemplateProperty(ColumnColor = ColumnColors.Color1)]
        public ShipComponentShieldType ShieldType { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color2)]
        public float MaxHitPoints { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color2)]
        public float Value { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color2)]
        public float RegenerationPerSecond { get; set; }
    }
}
