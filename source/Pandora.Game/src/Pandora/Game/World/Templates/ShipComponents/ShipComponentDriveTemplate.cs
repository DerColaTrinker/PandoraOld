﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates.ShipComponents
{
    [Template("Schiffkomponente - Antrieb")]
    public class ShipComponentDriveTemplate : ShipComponentTemplate
    {
        public ShipComponentDriveTemplate()
            : base()
        { }

        public ShipComponentDriveTemplate(string identifier)
            : base(identifier)
        { }

        [TemplateProperty(ColumnColor = ColumnColors.Color1)]
        public float FlySpeed { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color2)]
        public int Jumps { get; set; }
    }
}
