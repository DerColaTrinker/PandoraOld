﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates.ShipComponents
{
    [Template("Schiffkomponente - Rüstung")]
    public class ShipComponentArmorTemplate : ShipComponentTemplate
    {
        public ShipComponentArmorTemplate()
            : base()
        { }

        public ShipComponentArmorTemplate(string identifier)
            : base(identifier)
        { }

        [TemplateProperty(ColumnColor = ColumnColors.Color1)]
        public float BonusHitPoints { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color1)]
        public float Value { get; set; }
    }
}
