﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.Templates.ShipComponents
{
    [Template("Schiffkomponente - Waffen")]
    public class ShipComponentWeaponTemplate : ShipComponentTemplate
    {
        public ShipComponentWeaponTemplate()
            : base()
        { }

        public ShipComponentWeaponTemplate(string identifier)
            : base(identifier)
        { }

        [TemplateProperty(ColumnColor = ColumnColors.Color1)]
        public float MinDamage { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color1)]
        public float MaxDamage { get; set; }

        [TemplateProperty(ColumnColor = ColumnColors.Color2)]
        public float Attackspeed { get; set; }
    }
}
