﻿using Pandora.Game.World.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.World
{
    public sealed class Race : RaceBase
    {
        public int MinTemperature { get; set; }

        public int MaxTemperature { get; set; }

        public float Money { get; set; }

        public override void Serialize(Runtime.IO.IWriter writer)
        {
            base.Serialize(writer);

            writer.WriteInt32(MinTemperature);
            writer.WriteInt32(MaxTemperature);
            writer.WriteFloat(Money);
        }

        public override void Deserialize(Runtime.IO.IReader reader)
        {
            base.Deserialize(reader);

            MinTemperature = reader.ReadInt32();
            MinTemperature = reader.ReadInt32();
            Money = reader.ReadFloat();
        }
    }
}
