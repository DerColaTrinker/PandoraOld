﻿using Pandora.Game.World.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Generators
{
    public class GalaxyWormholeContainer : GalaxyObjectContainer<GalaxyWormhole>
    {
         public GalaxyObjectContainer<StarSystem> SystemA { get; set; }

        public GalaxyObjectContainer<StarSystem> SystemB { get; set; }

        protected override void Confirm()
        {
            base.Confirm();

            Instance.SystemA = SystemA.Instance;
            Instance.SystemB = SystemB.Instance;
        }
    }
}
