﻿using Pandora.Game.World.Objects;
using Pandora.Game.Templates.MapObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Generators
{
    public sealed class WorldGenerator : GeneratorBase
    {
        private IEnumerable<StarOrbitTemplates> _orbits;

        // Stefan-Boltzmann-Konstanten
        private readonly double __sigma = 5.670367D * Math.Pow(10, -8);
        private readonly double __ae = 149597870700D;

        protected override bool OnCreateWorld()
        {
            // Das erstellen verhindern, wenn keine Templates vorhanden sind
            if (!Templates.HasTemplates<StarOrbitTemplates>()) { Logger.Error("StarOrbit Templates not found"); return false; }
            if (!Templates.HasTemplates<StarTemplate>()) { Logger.Error("Star Templates not found"); return false; }
            if (!Templates.HasTemplates<PlanetTemplate>()) { Logger.Error("Planet Templates not found"); return false; }

            _orbits = Templates.GetTemplates<StarOrbitTemplates>();

            if (!CreateGalaxyObjects<StarSystem, StarTemplate>(350, GenerateStarsystems)) return false;
            if (!CreateSystemObjects<StarSystem, Planet, PlanetTemplate>(GeneratePlanets)) return false;
            if (!GeneratesWormholes()) return false;

            return true;
        }

        private bool GeneratePlanets(int index, GalaxyObjectContainer<StarSystem> galaxyobject, IEnumerable<PlanetTemplate> templates, RatioCounter<PlanetTemplate> ratio)
        {
            var planettemplate = (PlanetTemplate)null;
            var orbittemplate = (StarOrbitTemplates)null;
            var failcounter = 0;

            var planetcount = RND.Next(1, _orbits.Count());

            for (int planetindex = 0; planetindex < planetcount; planetindex++)
            {
                // Zuerst den Planetentyp auslosen
                while (true)
                {
                    var planettypeindex = RND.Next(ratio.Count());
                    planettemplate = templates.Skip(planettypeindex).Take(1).First();

                    if (ratio.Ratio(planettemplate) < planettemplate.Ratio)
                    {
                        ratio.Increment(planettemplate);
                        break;
                    }

                    failcounter++;
                    if (failcounter > 200)
                    {
                        // Einfach nehmen...
                        ratio.Increment(planettemplate);
                        break;
                    }
                }

                failcounter = 0;

                //TODO: Einen Orbit suchen :)
                //// Jetzt einen Passenden Orbit suchen
                //while (true)
                //{
                //    temperature = RND.Next(planettemplate.MinTemperature, planettemplate.MaxTemperature);

                //    var orbits = _orbits.Where(m => temperature >= m.MinTemperature & temperature <= m.MaxTemperature);

                //    if (orbits.Count() == 0)
                //    {
                //        failcounter++;
                //        if (failcounter > 200)
                //        {
                //            // Einfach nehmen...
                //            orbits = _orbits;
                //            break;
                //        }
                //    }

                //    foreach (var orbit in orbits)
                //    {
                //        if (galaxyobject.GetSystemObjects<Planet>().Where(m => m.Instance.PlanetOrbit != null && m.Instance.PlanetOrbit.Identifier != orbit.Identifier).Count() == 0)
                //        {
                //            orbittemplate = orbit;
                //            break;
                //        }
                //    }

                //    if (orbittemplate != null) break;
                //}

                var planet = CreateSystemObjectInstance<Planet, PlanetTemplate>(planettemplate);
                planet.Instance.PlanetOrbit = orbittemplate;
                planet.Instance.Distance = (float)(RND.Next(orbittemplate.MinDistance, orbittemplate.MaxDistace) * __ae);
                planet.Instance.Solarconst = galaxyobject.Instance.RadiationEnergy / planet.Instance.Hullarea;
                planet.Instance.Temperature = (float)Math.Sqrt(Math.Sqrt(planet.Instance.Solarconst / (4 * __sigma)));
                planet.Instance.BasePopulation = RND.Next(planettemplate.MinPopulation, planettemplate.MaxPopulation);
                planet.Instance.BaseSize = RND.Next(planettemplate.MinSize, planettemplate.MaxSize);
                planet.Instance.Wealth = RND.Next(planettemplate.MinWealth, planettemplate.MaxWealth);
                //planet.Instance.Temperature = temperature;
                planet.Instance.StarOrbitAngle = RND.Next(0, 359);

                // Die Position wird im SystemUpdate berechnet
                planet.PositionX = 0;
                planet.PositionY = 0;

                planet.Name = galaxyobject.Name + ' ' + RomanNumber(planetindex + 1);

                // Planet hinzufügen
                galaxyobject.Add(planet);
            }

            return true;
        }

        private bool GenerateStarsystems(int index, IEnumerable<StarTemplate> templates, RatioCounter<StarTemplate> ratio)
        {
            // Vorbereitungen
            var x = 0D;
            var y = 0D;
            var startype = (StarTemplate)null;
            var failcounter = 0;

            // Position ermitteln. Wenn ein Stern in der nähe ist, eine neue Position suchen
            while (true)
            {
                x = RND.NextDouble() * GalaxyMapSizeX;
                y = RND.NextDouble() * GalaxyMapSizeY;

                if (!HasObjectNeighborhood<StarSystem>(x, y, StarDistance)) break;

                failcounter++;
                if (failcounter > 200)
                {
                    StopProcess();
                    Logger.Warning("No more space for more stars.");
                    return true;
                }
            }

            failcounter = 0;

            // Sterntyp ermitteln. Zufallsbasierend allerdings abhängig von der Angabe wie oft ein Sterntyp vorkommen darf.
            while (true)
            {
                var startypeindex = RND.Next(ratio.Count());
                startype = templates.Skip(startypeindex).Take(1).First();

                if (ratio.Ratio(startype) < startype.Ratio)
                {
                    ratio.Increment(startype);
                    break;
                }

                failcounter++;
                if (failcounter > 200)
                {
                    // Einfach nehmen...
                    ratio.Increment(startype);
                    break;
                }
            }

            // Werte berechnen
            var massfactor = FloatRND(startype.MassFactorMin, startype.MassFactorMax);

            var star = CreateSystemObjectInstance<Star, StarTemplate>(startype);
            star.PositionX = (float)SystemMapSizeX / 2F;
            star.PositionY = (float)SystemMapSizeY / 2F;
            star.Instance.MassFactor = massfactor;
            star.Instance.Radius = (float)RND.Next(startype.MinRadius, startype.MaxRadius);
            star.Instance.Temperature = (float)RND.Next(startype.MinTemperature, startype.MaxTemperature);
            star.Instance.RadiationEnergy = star.Instance.Hullarea * Math.Pow(star.Instance.Temperature, 4) * __sigma;
            star.Name = "Stern " + (index + 1);

            // Objekte werden in Containern erstellt, damit sie in der Engine korrekt eingerichtet werden können.
            var system = CreateGalaxyObjectInstance<StarSystem, StarTemplate>(startype);
            system.PositionX = (float)x;
            system.PositionY = (float)y;
            system.Instance.MassFactor = massfactor;
            system.Instance.RadiationEnergy = star.Instance.Hullarea * Math.Pow(star.Instance.Temperature, 4) * __sigma;
            system.Name = "System " + (index + 1);

            Add(system);
            system.Add(star);

            return true;
        }

        private bool GeneratesWormholes()
        {
            var waystack = new Stack<GalaxyObjectContainer<StarSystem>>();
            var starcount = GetGalaxyObjects<StarSystem>().Count();
            var waychance = new RatioCounter<int>(new int[] { 1, 2, 3 });
            var nodecountratio = new float[] { 0.8F, 0.15F, 0.05F };

            Logger.BeginProgress(GetGalaxyObjects<StarSystem>().Count(), "Create Wormholes");

            // Startsystem einfügen
            waystack.Push(GetGalaxyObjects<StarSystem>().First());

            while (waystack.Count > 0 & starcount > 1)
            {
                starcount--;

                //Logger.UpdateProgress(starcount - openstack.Count);
                var sourcesystem = waystack.Pop();
                var nearsystems = (from system in GetGalaxyObjects<StarSystem>()
                                   where system != sourcesystem & system.GetSystemObjects<SystemWormhole>().Count() == 0
                                   let distance = Distance(sourcesystem, system)
                                   orderby distance
                                   select system).Take(3).ToArray();     // Den berechnungsprozess durch AsParallel() etwas beschleunigen


                // Wenn keine Sterne mehr gefunden werden ist das nun mal eben so. also verlassen
                if (nearsystems.Length == 0) break;

                // Anzahl der Verzweigungen ermitteln
                var nodecount = 0;
                var failcounter = 0;
                while (true)
                {
                    nodecount = RND.Next(1, 3);
                    if (nodecount > nearsystems.Length) nodecount = nearsystems.Length;
                    if (waychance.Ratio(nodecount) < nodecountratio[nodecount - 1]) { waychance.Increment(nodecount); break; }

                    failcounter++;
                    if (failcounter > 200)
                    {
                        // Einfach nehmen...
                        waychance.Increment(nodecount);
                        break;
                    }
                }

                for (int index = 0; index < nodecount; index++)
                {
                    var targetsystem = nearsystems[index];

                    // Bei einer Wurmloch überschneidung, diese Verbindung nicht erstellen
                    var intersects = Intersect(targetsystem, sourcesystem);
                    if (intersects) continue;

                    // Systeme puschen                   
                    waystack.Push(targetsystem);

                    // Das Wurmloch für die Galaxyseite erzeigen
                    var gowormhole = CreateGalaxyObjectContainer<GalaxyWormholeContainer, GalaxyWormhole>();
                    gowormhole.SystemA = sourcesystem;
                    gowormhole.SystemB = targetsystem;
                    Add(gowormhole);

                    // Die Wurmlöcher in der Systemmap erstellen
                    var so1wormhole = CreateSystemObjectContainer<SystemWormhole>();
                    var so2wormhole = CreateSystemObjectContainer<SystemWormhole>();
                    so1wormhole.Instance.SystemA = sourcesystem.Instance as StarSystem;
                    so1wormhole.Instance.SystemB = targetsystem.Instance as StarSystem;
                    so2wormhole.Instance.SystemB = sourcesystem.Instance as StarSystem;
                    so2wormhole.Instance.SystemA = targetsystem.Instance as StarSystem;
                    sourcesystem.Add(so1wormhole);
                    targetsystem.Add(so2wormhole);
                }

                Logger.UpdateProgress(GetGalaxyObjects<StarSystem>().Count() - starcount);
            }

            Logger.EndProgress(false);
            return true;
        }

        private float Distance(GalaxyObjectContainer currentsystem, GalaxyObjectContainer<StarSystem> system)
        {
            var b = Math.Pow(currentsystem.PositionX - system.PositionX, 2);
            var c = Math.Pow(currentsystem.PositionY - system.PositionY, 2);
            var a = Math.Sqrt(b + c);
            return (float)a;
        }

        private bool Intersect(GalaxyObjectContainer a, GalaxyObjectContainer b)
        {
            foreach (var wormhole in GetGalaxyObjectContainers<GalaxyWormholeContainer>())
            {
                if (this.DoLinesIntersect(a, b, wormhole.SystemA, wormhole.SystemB))
                {
                    return true;
                }
            }

            return false;
        }

        private bool DoLinesIntersect(GalaxyObjectContainer ss1, GalaxyObjectContainer ss2, GalaxyObjectContainer ss3, GalaxyObjectContainer ss4)
        {
            return !(PointsEqual(ss1, ss3) | PointsEqual(ss1, ss4) | PointsEqual(ss2, ss3) | PointsEqual(ss2, ss4)) && (CounterClockwiseTest(ss1, ss2, ss3) * CounterClockwiseTest(ss1, ss2, ss4) <= 0 & CounterClockwiseTest(ss3, ss4, ss1) * CounterClockwiseTest(ss3, ss4, ss2) <= 0);
        }

        private bool PointsEqual(GalaxyObjectContainer s1, GalaxyObjectContainer s2)
        {
            return s1.PositionX == s2.PositionX & s1.PositionY == s2.PositionY;
        }

        private int CounterClockwiseTest(GalaxyObjectContainer ss1, GalaxyObjectContainer ss2, GalaxyObjectContainer ss3)
        {
            var x1 = ss2.PositionX - ss1.PositionX;
            var x2 = ss3.PositionX - ss1.PositionX;
            var y1 = ss2.PositionY - ss1.PositionY;
            var y2 = ss3.PositionY - ss1.PositionY;

            if (x1 * y2 > y1 * x2) return 1;

            return -1;
        }

        private float FloatRND(float min, float max)
        {
            while (true)
            {
                var value = (float)RND.NextDouble() * 100F;
                if (value >= min & value <= max) return value;
            }
        }
    }
}
