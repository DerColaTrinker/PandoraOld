﻿using Pandora.Game.World.Objects;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World
{
    [DebuggerDisplay("System={System.Name}, Scans={Scans}")]
    internal class PathNode
    {
        private StarSystem _system;

        public StarSystem System
        {
            get
            {
                return this._system;
            }
        }

        public int Scans
        {
            get;
            set;
        }

        public PathNode(StarSystem start)
        {
            this._system = start;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            PathNode pathNode = obj as PathNode;
            return pathNode != null && this.System == pathNode.System;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool operator ==(PathNode a, PathNode b)
        {
            return object.ReferenceEquals(a, b) || (a != null && b != null && a.System == b.System);
        }

        public static bool operator !=(PathNode a, PathNode b)
        {
            return !(a == b);
        }
    }
}
