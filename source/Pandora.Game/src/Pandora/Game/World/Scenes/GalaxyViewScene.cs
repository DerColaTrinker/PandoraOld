﻿using Pandora.Interactions.Visuals.Controls;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Drawing2D;
using Pandora.Interactions.Visuals.Renderer;
using Pandora.Game.World.Objects;
using Pandora.Game.Templates.MapObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Pandora.Game.World.Scenes
{
    public class GalaxyViewScene : Scene
    {
        private PandoraWorld world;
        private Dictionary<string, RectShape> _starsrect = new Dictionary<string, RectShape>();

        private HashSet<StarSystem> _systems;
        private HashSet<GalaxyWormhole> _wormholes;
        private Line _line;

        public GalaxyViewScene(PandoraWorld world)
        {
            this.world = world;
        }

        protected override void OnLoad()
        {
            BackgroundColor = Color.Black;
            this.Viewport = new Viewport(new Vector2f(500, 500), Visuals.Size);

            // Cachedaten laden
            foreach (var template in world.Templates.GetTemplates<StarTemplate>())
            {
                var filename = Path.Combine(world.Directory, "data", "gfx", string.Format("{0}.{1}", template.ResourceCode, "png"));

                if (!File.Exists(filename)) throw new FileNotFoundException("File not found", filename);

                var texture = Cache.Textures.GetOrLoad(template.ResourceCode, filename);
                var rect = new RectShape(texture.Size.ToVector2f());
                rect.Texture = texture;
                rect.Origin = new Vector2f(rect.Size.X / 2F, rect.Size.Y / 2F);
                rect.Texture.Smooth = true;

                _starsrect.Add(template.Identifier, rect);
            }

            _systems = new HashSet<StarSystem>(world.Accessor.GetObjects<StarSystem>());
            _wormholes = new HashSet<GalaxyWormhole>(world.Accessor.GetObjects<GalaxyWormhole>());
            _line = new Line();

            base.OnLoad();
        }

        protected override void OnRenderUpdate(IRenderTarget target)
        {
            base.OnRenderUpdate(target);

            // Wurmlöcher zeichnen

            foreach (var wormhole in _wormholes)
            {
                _line.Position1 = Viewport.MapToScreenCoords(new Vector2f(wormhole.SystemA.PositionX, wormhole.SystemA.PositionY));
                _line.Position2 = Viewport.MapToScreenCoords(new Vector2f(wormhole.SystemB.PositionX, wormhole.SystemB.PositionY));

                target.DrawVertexArray(_line);
            }

            foreach (var system in _systems)
            {
                if (Viewport.MapVisible(system.PositionX, system.PositionY))
                {
                    var rect = _starsrect[system.Template.Identifier];
                    rect.Position = this.Viewport.MapToScreenCoords(system.PositionX, system.PositionY);
                    rect.Scale = new Vector2f(system.MassFactor, system.MassFactor);
                    rect.Draw(target);
                }
            }
        }

        public Viewport Viewport { get; private set; }
    }
}
