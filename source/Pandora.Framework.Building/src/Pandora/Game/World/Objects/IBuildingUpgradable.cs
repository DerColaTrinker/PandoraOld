﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.World.Objects
{
    /// <summary>
    /// Eine Schnittstelle die ein Gebäude markiert, das ausgebaut werden kann
    /// </summary>
    public interface IBuildingUpgradable
    {
    }
}
