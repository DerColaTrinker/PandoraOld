﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.World.Objects
{
    /// <summary>
    /// Eine Schnittstelle die angibt, das an diesem Ort Gebäude errichtet werden können
    /// </summary>
    public interface IBuildingConstructionLocation : IGameObject
    {
    }

    /// <summary>
    /// Eine Schnittstelle die angibt, das an diesem Ort Gebäude errichtet und ausgebaut werden können
    /// </summary>
    public interface IBuildingUpgradeConstructionLocation : IBuildingConstructionLocation
    {
        /// <summary>
        /// Gibt die Anzahl der Gebäudeausbauaufträge in der Warteschlange an
        /// </summary>
        int MaxConstructionQueue { get; set; }

        /// <summary>
        /// Gibt die Anzahl der parallel arbeiten Gebäudeausbauaufträge an
        /// </summary>
        int ParallelConstructions { get; set; }
    }
}
