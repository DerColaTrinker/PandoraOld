﻿using Pandora.Game.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects.Building
{
    /// <summary>
    /// Stellt ein Gebäude dar, das an einem Ort gebunden ist
    /// </summary>
    /// <typeparam name="TLocation"></typeparam>
    public abstract class BuildingBase<TLocation> : GameObjectBase
        where TLocation : GameObjectBase, IBuildingConstructionLocation
    {
        /// <summary>
        /// Liefert den Ort an dem das Gebäude errichtet wurde
        /// </summary>
        public TLocation Location { get; internal set; }

        /// <summary>
        /// Gibt an ob das Gebäude gebaut ist
        /// </summary>
        public bool IsBuild { get; internal set; }

        /// <summary>
        /// Gibt an ob das Gebäude gebaut werden kann
        /// </summary>
        public bool IsBuildable { get; internal set; }

        /// <summary>
        /// Gibt an ob das Gebäude ausgebaut werden kann
        /// </summary>
        public bool IsUpgradeable { get { return UpgradeTemplates != null && UpgradeTemplates.Count() > 0; } }

        internal IEnumerable<TemplateBase> UpgradeTemplates { get; set; }

        internal int CurrentLevel { get; set; }

        internal TemplateBase CurrentUpgradeTemplate { get; set; }
    }

    /// <summary>
    /// Stellt ein Gebäude dar, das an einem Ort gebunden ist
    /// </summary>
    /// <typeparam name="TLocation"></typeparam>
    /// <typeparam name="TTemplate"></typeparam>
    public abstract class BuildingBase<TLocation, TTemplate> : BuildingBase<TLocation>
        where TLocation : GameObjectBase, IBuildingConstructionLocation
        where TTemplate : TemplateBase
    {
        /// <summary>
        /// Liefert das Template
        /// </summary>
        public new TTemplate Template { get { return (TTemplate)base.Template; } set { base.Template = Template; } }
    }

    /// <summary>
    /// Stellt ein Gebäude dar, das an einem Ort gebunden ist
    /// </summary>
    /// <typeparam name="TLocation"></typeparam>
    /// <typeparam name="TTemplate"></typeparam>
    /// <typeparam name="TUpgrade"></typeparam>
    public abstract class BuildingBase<TLocation, TTemplate, TUpgrade> : BuildingBase<TLocation, TTemplate>, IBuildingUpgradable
        where TLocation : GameObjectBase, IBuildingConstructionLocation
        where TTemplate : UpgradableTemplateBase<TUpgrade>
        where TUpgrade : UpgradeTemplateBase<TTemplate>
    {
        /// <summary>
        /// Liefert den Type des Ausbau-Templates
        /// </summary>
        public Type UpgrateTemplateType { get { return typeof(TUpgrade); } }

        /// <summary>
        /// Liefert eine Liste aller Templates die Ausgebaut werden können
        /// </summary>
        public new IEnumerable<TUpgrade> UpgradeTemplates { get { return (IEnumerable<TUpgrade>)base.UpgradeTemplates; } internal set { base.UpgradeTemplates = (IEnumerable<TemplateBase>)value; } }

        /// <summary>
        /// Liefert die aktuelle Ausbaustufe
        /// </summary>
        public new int CurrentLevel { get { return base.CurrentLevel; } internal set { base.CurrentLevel = value; } }

        /// <summary>
        /// Liefert das aktuelle Ausbau-Template
        /// </summary>
        public new TUpgrade CurrentUpgradeTemplate { get { return (TUpgrade)base.CurrentUpgradeTemplate; } internal set { base.CurrentUpgradeTemplate = value; } }

        /// <summary>
        /// Gibt an ob das Gebäude gebaut ist
        /// </summary>
        public new bool IsBuild { get { return CurrentLevel > 0; } }
    }
}
