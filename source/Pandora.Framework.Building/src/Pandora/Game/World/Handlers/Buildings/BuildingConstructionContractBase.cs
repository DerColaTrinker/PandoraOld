﻿using Pandora.Game.World.Objects;
using Pandora.Game.World.Objects.Building;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.World.Handlers.Building
{
    /// <summary>
    /// Eine Basisklasse die einen Gebäudeausbauauftrag darstellt
    /// </summary>
    /// <typeparam name="TLocation"></typeparam>
    /// <typeparam name="TBuilding"></typeparam>
    public class BuildingConstructionContractBase<TLocation, TBuilding> : GameObjectBase
        where TLocation : GameObjectBase, IBuildingUpgradeConstructionLocation
        where TBuilding : BuildingBase<TLocation>, IBuildingUpgradable
    {
        /// <summary>
        /// Liefert den Ort wo das Schiff gebaut wird
        /// </summary>
        public TLocation Location { get; internal set; }

        /// <summary>
        /// Liefert das Design das gebaut wird
        /// </summary>
        public TBuilding Building { get; internal set; }

        /// <summary>
        /// Liefert den Status 
        /// </summary>
        public BuildingConstructionStatus Status { get; internal set; }

        /// <summary>
        /// Legt den Status auf ShipBuildStatus.InProcess fest und beginnt somit die Produktion
        /// </summary>
        protected internal virtual void Begin()
        {
            Status = BuildingConstructionStatus.InProcess;
        }

        /// <summary>
        /// Gibt der Engine bekannt, das der Bau abgeschlossen wurde
        /// </summary>
        public void Finish()
        {
            Status = BuildingConstructionStatus.Finished;
        }

        /// <summary>
        /// Gibt der Engine bekannt, das der Bau abgebrochen wurde
        /// </summary>
        public void Cancel()
        {
            Status = BuildingConstructionStatus.Canceled;
        }
    }
}
