﻿using Pandora.Game.World.Objects;
using Pandora.Game.World.Objects.Building;
using Pandora.Runtime.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Handlers.Building
{
    /// <summary>
    /// Ein Delegate das im BuildingUpgradeHandler verwendet wird
    /// </summary>
    /// <typeparam name="TContract"></typeparam>
    /// <param name="contract"></param>
    public delegate void BuildingUpgradeHandlerContractDelegate<TContract>(TContract contract);

    /// <summary>
    /// Ein Delegate das im BuildingUpgradeHandler verwendet wird
    /// </summary>
    /// <typeparam name="TContract"></typeparam>
    /// <typeparam name="TBuilding"></typeparam>
    /// <param name="contract"></param>
    /// <param name="building"></param>
    public delegate void BuildingUpgradeHandlerShipDelegate<TContract, TBuilding>(TContract contract, TBuilding building);

    /// <summary>
    /// Verwaltet Gebäudeausbauaufträge
    /// </summary>
    /// <typeparam name="TLocation"></typeparam>
    /// <typeparam name="TContract"></typeparam>
    /// <typeparam name="TBuilding"></typeparam>
    public class BuildingUpgradeHandler<TLocation, TContract, TBuilding> : WorldHandlerBase
        where TLocation : GameObjectBase, IBuildingUpgradeConstructionLocation                 // Sicherstellen das der Standort auch anzeigen kann, wie die Bauoptionen aussehen
        where TContract : BuildingConstructionContractBase<TLocation, TBuilding>                // Der Ausbauvertrag
        where TBuilding : BuildingBase<TLocation>, IBuildingUpgradable                          // Sicherstellen das es auch ein Ausbaubares Gebäude ist
    {
        private OwnerList<TLocation, TContract> _collection = new OwnerList<TLocation, TContract>();

        /// <summary>
        /// Wird aufgerufen wenn ein Bauauftrag erteilt wurde
        /// </summary>
        public event BuildingUpgradeHandlerContractDelegate<TContract> ContractAdded;

        /// <summary>
        /// Wird aufgerufen wenn ein Bauauftrag abgeschlossen wurde
        /// </summary>
        public event BuildingUpgradeHandlerContractDelegate<TContract> ContractFinished;

        /// <summary>
        /// Wird aufgerufen wenn ein Bauauftrag abgebrochen wurde
        /// </summary>
        public event BuildingUpgradeHandlerContractDelegate<TContract> ContractCanceled;

        /// <summary>
        /// Begint ein Bauauftrag
        /// </summary>
        /// <param name="location"></param>
        /// <param name="building"></param>
        public void BeginUpgrade(TLocation location, TBuilding building)
        {
            // Prüfen ob ein Ausbau noch erlaubt ist
            if (_collection.ContainsKey(location))
            {
                if (_collection.GetElementCount(location) >= location.MaxConstructionQueue) return;
            }

            // Bauvertrag erstellen und das Prepare auslösen
            var contract = (TContract)Activator.CreateInstance(typeof(TContract));
            contract.Building = building;
            contract.Location = location;
            OnContractAdded(contract);
        }

        /// <summary>
        /// Löst das ContractAdded-Ereignis aus
        /// </summary>
        /// <param name="contract"></param>
        protected virtual void OnContractAdded(TContract contract)
        {
            if (ContractAdded != null) ContractAdded.Invoke(contract);
        }

        /// <summary>
        /// Löst das ContractFinished-Ereignis aus
        /// </summary>
        /// <param name="contract"></param>
        protected virtual void OnContractFinished(TContract contract)
        {
            if (ContractFinished != null) ContractFinished.Invoke(contract);
        }

        /// <summary>
        /// Löst das ContractCanceled-Ereignis aus
        /// </summary>
        /// <param name="contract"></param>
        protected virtual void OnContractCanceled(TContract contract)
        {
            if (ContractCanceled != null) ContractCanceled.Invoke(contract);
        }

#pragma warning disable 1591
        protected override void OnSystemUpdate(float ms, float s)
        {
            foreach (var collection in _collection.GetElementCollections())
            {
                var location = collection.Owner;
                var max = collection.Count > location.MaxConstructionQueue ? location.MaxConstructionQueue : collection.Count;

                for (int index = 0 ; index < max ; index++)
                {
                    var contract = collection.ElementAt(index);
                    switch (contract.Status)
                    {
                        case BuildingConstructionStatus.Pending:
                            contract.Begin();
                            break;

                        case BuildingConstructionStatus.InProcess:
                            contract.WorldUpdate(ms, s);
                            break;

                        case BuildingConstructionStatus.Finished:
                            _collection.RemoveElement(location, contract);
                            UpdateUpgradeLevel(contract);
                            OnContractFinished(contract);
                            break;

                        case BuildingConstructionStatus.Canceled:
                            _collection.RemoveElement(location, contract);
                            OnContractCanceled(contract);
                            break;
                    }
                }
            }
        }
#pragma warning restore 1591

        private void UpdateUpgradeLevel(TContract contract)
        {
            var building = contract.Building;

            building.CurrentLevel++;
            building.CurrentUpgradeTemplate = building.UpgradeTemplates.ElementAt(building.CurrentLevel - 1);
        }
    }
}
