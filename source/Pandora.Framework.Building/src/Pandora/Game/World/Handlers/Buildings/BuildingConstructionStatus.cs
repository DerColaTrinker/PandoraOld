﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pandora.Game.World.Handlers.Building
{
    /// <summary>
    /// Status eines Gebäudeausbauauftrags
    /// </summary>
    public enum BuildingConstructionStatus
    {
        /// <summary>
        /// Ausstehend
        /// </summary>
        Pending,

        /// <summary>
        /// Inbearbeitung
        /// </summary>
        InProcess,

        /// <summary>
        /// Abgeschlossen
        /// </summary>
        Finished,

        /// <summary>
        /// Abgebrochen
        /// </summary>
        Canceled
    }
}
