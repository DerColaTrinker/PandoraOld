﻿using Pandora.Game.World.Objects;
using Pandora.Game.World.Objects.Building;
using Pandora.Runtime.Collections;
using Pandora.Runtime.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Handlers.Building
{
    /// <summary>
    /// Ein Delegate das im BuildingHandler verwendet wird
    /// </summary>
    /// <typeparam name="TLocation"></typeparam>
    /// <param name="location"></param>
    public delegate void BuildingLocationDelegate<TLocation>(TLocation location);

    /// <summary>
    /// Ein Delegate das im BuildingHandler verwendet wird
    /// </summary>
    /// <typeparam name="TLocation"></typeparam>
    /// <typeparam name="TBuilding"></typeparam>
    /// <param name="location"></param>
    /// <param name="building"></param>
    public delegate void BuildingHandlerDelegate<TLocation, TBuilding>(TLocation location, TBuilding building);

    /// <summary>
    /// Verwaltet die Geäude eines Ortes
    /// </summary>
    /// <typeparam name="TLocation"></typeparam>
    /// <typeparam name="TBuilding"></typeparam>
    public class BuildingHandler<TLocation, TBuilding> : WorldHandlerBase
        where TLocation : GameObjectBase, IBuildingConstructionLocation
        where TBuilding : BuildingBase<TLocation>
    {
        private OwnerHashSet<TLocation, TBuilding> _collection = new OwnerHashSet<TLocation, TBuilding>();

        /// <summary>
        /// Wird aufgerufen wenn ein neuer Standort eingefügt wird
        /// </summary>
        public event BuildingLocationDelegate<TLocation> LocationAdded;

        /// <summary>
        /// Wird aufgerufen wenn ein Standort entfernt wird
        /// </summary>
        public event BuildingLocationDelegate<TLocation> LocationRemoved;

        /// <summary>
        /// Wird aufgerufen wenn ein neues Gebäude errichtet wird
        /// </summary>
        public event BuildingHandlerDelegate<TLocation, TBuilding> BuildingAdded;

        /// <summary>
        /// Wird aufgerufen wenn ein Gebäude entfernt wird
        /// </summary>
        public event BuildingHandlerDelegate<TLocation, TBuilding> BuildingRemoved;

#pragma warning disable 1591

        protected override void OnStart()
        {
            // Handler suchen
            Accessor.WorldObjectAdded += delegate(GameObjectBase obj) { if (obj is TLocation) { _collection.AddOwner((TLocation)obj); OnLocationAdd((TLocation)obj); } };

            base.OnStart();
        }

        public override void Serialize(IWriter writer)
        {
            var locations = _collection.GetOwners();
            writer.WriteInt32(locations.Count());
            foreach (var location in locations)
            {
                writer.WriteUInt32(location.ObjectID);

                var buildings = GetBuildings(location);
                writer.WriteInt32(buildings.Count());
                foreach (var building in buildings)
                {
                    OnSerializeShip(writer, location, building);
                }
            }
        }

        public override void Deserialize(IReader reader)
        {
            var locationcount = reader.ReadInt32();
            for (int locationindex = 0 ; locationindex < locationcount ; locationindex++)
            {
                var location = Accessor.GetObject<TLocation>(reader.ReadUInt32());
                _collection.AddOwner(location);

                var colonycount = reader.ReadInt32();
                for (int colonyindex = 0 ; colonyindex < colonycount ; colonyindex++)
                {
                    var building = (TBuilding)null;
                    OnDeserializeShip(reader, location, out building);
                    _collection.AddElement(location, building);
                }
            }
        }

#pragma warning restore

        /// <summary>
        /// Fügt ein Schiff hinzu
        /// </summary>
        /// <param name="location"></param>
        /// <param name="building"></param>
        public void AddBuilding(TLocation location, TBuilding building)
        {
            building.Location = location;

            _collection.AddElement(location, building);
            Accessor.Add(building);
            OnBuildingAdd(location, building);
        }

        /// <summary>
        /// Entfernt ein Schiff
        /// </summary>
        /// <param name="location"></param>
        /// <param name="building"></param>
        public void RemoveBuilding(TLocation location, TBuilding building)
        {
            building.Location = null;

            _collection.RemoveElement(location, building);
            Accessor.Remove(building);
            OnBuildingRemove(location, building);
        }

        /// <summary>
        /// Liefert eine Auflistung aller Schiffe
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TBuilding> GetBuildings()
        {
            return _collection.GetElements().AsEnumerable();
        }

        /// <summary>
        /// Liefert eine Auflistung aller Schiffe einer Rasse
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public IEnumerable<TBuilding> GetBuildings(TLocation location)
        {
            return _collection.GetElements(location).AsEnumerable();
        }

        /// <summary>
        /// Liefert ein Schiff
        /// </summary>
        /// <param name="objectid"></param>
        /// <returns></returns>
        public TBuilding GetBuilding(uint objectid)
        {
            return Accessor.GetObject<TBuilding>(objectid);
        }

        /// <summary>
        /// Löst das AddRace-Ereignis aus
        /// </summary>
        /// <param name="location"></param>
        protected virtual void OnLocationAdd(TLocation location)
        {
            if (LocationAdded != null) LocationAdded.Invoke(location);
        }

        /// <summary>
        /// Löst das RemoveRace-Ereignis aus
        /// </summary>
        /// <param name="location"></param>
        protected virtual void OnRaceRemove(TLocation location)
        {
            if (LocationRemoved != null) LocationRemoved.Invoke(location);
        }

        /// <summary>
        /// Löst das ShipAdd-Ereignis aus
        /// </summary>
        /// <param name="location"></param>
        /// <param name="building"></param>
        protected virtual void OnBuildingAdd(TLocation location, TBuilding building)
        {
            if (BuildingAdded != null) BuildingAdded.Invoke(location, building);
        }

        /// <summary>
        /// Löst das ShipRemove-Ereignis aus
        /// </summary>
        /// <param name="location"></param>
        /// <param name="building"></param>
        protected virtual void OnBuildingRemove(TLocation location, TBuilding building)
        {
            if (BuildingRemoved != null) BuildingRemoved.Invoke(location, building);
        }

        /// <summary>
        /// Wird aufgerufen wenn das Schiff serialisiert wird
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="location"></param>
        /// <param name="building"></param>
        protected virtual void OnSerializeShip(IWriter writer, TLocation location, TBuilding building)
        {
            building.Serialize(writer);
        }

        /// <summary>
        /// Wird aufgerufen wenn das Schiff deserialisiert wird
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="location"></param>
        /// <param name="building"></param>
        protected virtual void OnDeserializeShip(IReader reader, TLocation location, out TBuilding building)
        {
            building = GameObjectBase.CreateObject<TBuilding>(Instance, reader);
        }

    }
}
