﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Pandora Framework - Building")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Projekt-Pandora.de")]
[assembly: AssemblyProduct("Pandora Framework - Building")]
[assembly: AssemblyCopyright("Copyright © Projekt-Pandora.de 2016-2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("f81d5e6c-1e51-481f-b4d6-f85ec2690544")]

[assembly: AssemblyVersion("0.0.2.0")]
[assembly: AssemblyFileVersion("0.0.2.0")]
