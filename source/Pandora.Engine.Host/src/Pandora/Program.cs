﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Pandora.Runtime.Extensions;
using Pandora.Game;
using Pandora.Runtime.Logging;
using Pandora.Runtime.Logging.Targets;
using System.IO;
using System.Threading;
using Pandora.UI.Scenes;
using Pandora.Interactions.Visuals.Styles;

namespace Pandora
{
    class Program
    {
        private static PandoraRuntime _runtime;
        private static GameControllerBase _instance;

        static void Main()
        {
            var consolethread = new Thread(new ThreadStart(ConsoleRuntime));

            InitializeLogger();

            _runtime = new PandoraRuntime();

            // Da PandoraRuntime.Start() die Ausführung anhält bis die Runtime verlassen wird, starten wir einen seperaten Thread für die Konsole
            consolethread.Start();

            // Daten für das Hauptmenu Cachen
            _runtime.Interaction.Cache.Font.Load("default", Environment.CurrentDirectory, "data", "Catamaran-ExtraLight.ttf");
            _runtime.Interaction.Cache.Font.Load("digit", Environment.CurrentDirectory, "data", "digital-7.ttf");

            // Styles laden
            var styleset = StyleSet.LoadStyle(Path.Combine(Environment.CurrentDirectory, "data", "pandorastyle.xml"));

            // Style festlegen
            _runtime.Interaction.Visuals.StyleSet = styleset;

#if DEBUG && GAME
            _runtime.Start(new TestScene(_runtime));
#else
            _runtime.Start(new MainMenuScene(_runtime));
#endif
        }

        private static void ConsoleRuntime()
        {
            while (true)
            {
                var line = Console.ReadLine();

                switch (line.ToLower())
                {
                    case "exit":
                        _runtime.Stop();
                        return;

                    case "sp":
  
                        break;

                    default:
                        break;
                }
            }
        }

        private static void InitializeLogger()
        {
            LoggerManager.IsEnabled = true;
            LoggerManager.MinLevel = LoggerMessageType.Trace;
            LoggerManager.MaxLevel = LoggerMessageType.Error;

            LoggerManager.Add("console", new ConsoleLoggerTarget());
        }
    }
}
