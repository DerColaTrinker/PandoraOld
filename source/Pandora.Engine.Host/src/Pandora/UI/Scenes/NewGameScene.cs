﻿using Pandora.Interactions.Visuals.Controls;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.UI.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game;
using Pandora.Interactions.Visuals;

namespace Pandora.UI.Scenes
{
    public class NewGameScene : Scene
    {
        private PandoraRuntime _runtime;
        private Label _version;
        private Button _singleplayer;
        private Button _returnbutton;

        public NewGameScene(PandoraRuntime runtime)
        {
            _runtime = runtime;
        }

        public GameControllerBase GameController { get; private set; }

        protected override void OnLoad()
        {
            BackgroundColor = new Color(80, 80, 80);

            _version = new Label() { Name = "Version" };
            _version.Position = new Vector2f(3, 774);
            _version.TextColor = new Color(60, 60, 60);
            _version.Text = string.Format("Version  {0}     ( {1} )", EngineInfo.__AssemblyFullVersion, EngineInfo.__CompileDateTime);

            _singleplayer = new Button();
            _singleplayer.Size = new Vector2f(120, 35);
            _singleplayer.Position = new Vector2f(700, 500);
            _singleplayer.Text = "Multiplayer Server";
            _singleplayer.ButtonClick += CreateMultiplayerServer;

            _returnbutton = new Button();
            _returnbutton.Size = new Vector2f(120, 35);
            _returnbutton.Position = new Vector2f(700, 550);
            _returnbutton.Text = "Zurück";
            _returnbutton.ButtonClick += ReturnButtonClick;

            Controls.Add(_version);
            Controls.Add(_singleplayer);
            Controls.Add(_returnbutton);
        }

        private void CreateMultiplayerServer(Control control)
        {
          
        }

        private void ReturnButtonClick(Control control)
        {
            GameController = null;

            Close();
        }
    }
}
