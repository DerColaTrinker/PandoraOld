﻿using Pandora.Interactions.Visuals.Controls;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.UI.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Game;

namespace Pandora.UI.Scenes
{
    public class MainMenuScene : Scene
    {
        private PandoraRuntime _runtime;
        private Label _version;
        private Button _newgamebutton;
        private NewGameScene _newgamescene;
        private GameControllerBase _gamecotroler;

        public MainMenuScene(PandoraRuntime runtime)
        {
            _runtime = runtime;
        }

        protected override void OnLoad()
        {
            // Die NewGame Szene schonmal vorbereiten
            _newgamescene = new NewGameScene(_runtime);

            _version = new Label() { Name = "Version" };
            _version.Position = new Vector2f(3, 774);
            _version.TextColor = new Color(60, 60, 60);
            _version.Text = string.Format("Version  {0}     ( {1} )", EngineInfo.__AssemblyFullVersion, EngineInfo.__CompileDateTime);

            _newgamebutton = new Button();
            _newgamebutton.Size = new Vector2f(120, 35);
            _newgamebutton.Position = new Vector2f(700, 500);
            _newgamebutton.Text = "Neues Spiel";
            _newgamebutton.ButtonClick += NewGameButtonClick;

            Controls.Add(_version);
            Controls.Add(_newgamebutton);
        }

        private void NewGameButtonClick(Control control)
        {
            // Die Szene anzeigen
            Show(_newgamescene);

            // Wenn die Scene geschlossen wird, das Event hier verarbeiten und den GameController holen
            _newgamescene.SceneClose += delegate (Scene scene) { _gamecotroler = _newgamescene.GameController; };

            // Wenn kein GameController erstellt wurde, direkt verlassen
            if (_gamecotroler == null) return;

            //TODO: Spiellobby anzeigen
        }
    }
}