﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Pandora Framework - Component")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Projekt-Pandora.de")]
[assembly: AssemblyProduct("Pandora Framework - Component")]
[assembly: AssemblyCopyright("Copyright © Projekt-Pandora.de 2016-2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("f04b15ee-c8ad-4339-ac7a-3447c45f7d97")]

[assembly: AssemblyVersion("0.0.2.0")]
[assembly: AssemblyFileVersion("0.0.2.0")]
