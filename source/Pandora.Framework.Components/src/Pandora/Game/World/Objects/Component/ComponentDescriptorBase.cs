﻿using Pandora.Game.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects.Component
{
    /// <summary>
    /// Stellt eine Beschreibung einer Komponente dar die durch die aktivierung gesteuert wird
    /// </summary>
    public abstract class ComponentDescriptorBase : GameObjectBase
    {
        /// <summary>
        /// Gibt an ob diese Komponenete verfügbar ist
        /// </summary>
        public bool IsAvailable { get; internal set; }
    }

    /// <summary>
    /// Stellt eine Beschreibung einer Komponente dar die durch die aktivierung gesteuert wird
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    public abstract class ComponentDescriptorBase<TRace> : ComponentDescriptorBase
        where TRace : RaceBase
    {
        /// <summary>
        /// Liefert die Rasse die dieser Beschreibung gehört
        /// </summary>
        public new TRace OwnerRace { get { return (TRace)base.OwnerRace; } internal set { base.OwnerRace = value; } }

        /// <summary>
        /// Prüft ob diese Komponente verfügbar ist
        /// </summary>
        /// <param name="race"></param>
        /// <param name="activationlist"></param>
        /// <returns></returns>
        protected internal virtual bool CheckAvailable(TRace race, IEnumerable<string> activationlist)
        {
            return true;
        }
    }

    /// <summary>
    /// Stellt eine Beschreibung einer Komponente dar die durch die aktivierung gesteuert wird
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    /// <typeparam name="TTemplate"></typeparam>
    public abstract class ComponentDescriptorBase<TRace, TTemplate> : ComponentDescriptorBase<TRace>
        where TRace : RaceBase
        where TTemplate : TemplateBase
    {
        /// <summary>
        /// Liefert das Template
        /// </summary>
        public new TTemplate Template { get { return (TTemplate)base.Template; } set { base.Template = value; } }

        /// <summary>
        /// Prüft ob diese Komponente verfügbar ist
        /// </summary>
        /// <param name="race"></param>
        /// <param name="activationlist"></param>
        /// <returns></returns>
        protected internal override bool CheckAvailable(TRace race, IEnumerable<string> activationlist)
        {
            return (!string.IsNullOrEmpty(Template.Required1) && activationlist.Contains(Template.Required1))
                 & (!string.IsNullOrEmpty(Template.Required2) && activationlist.Contains(Template.Required2))
                 & (!string.IsNullOrEmpty(Template.Required3) && activationlist.Contains(Template.Required3))
                 & (!string.IsNullOrEmpty(Template.Required4) && activationlist.Contains(Template.Required4));
        }
    }
}
