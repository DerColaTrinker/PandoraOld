﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects.Component
{
    /// <summary>
    /// Stellt eine Komponente dar
    /// </summary>
    public abstract class ComponentBase : GameObjectBase
    {
    }
}
