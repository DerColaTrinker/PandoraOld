﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Objects
{
    /// <summary>
    /// Eine Schnittstelle die ein Spielobjekt markiert, das ein Container für Komponenten darstellt
    /// </summary>
    public interface IComponentContainer : IGameObject
    { }
}
