﻿using Pandora.Game.World.Objects;
using Pandora.Game.World.Objects.Component;
using Pandora.Game.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pandora.Runtime.Collections;

namespace Pandora.Game.World.Handlers.Component
{
    /// <summary>
    /// Ein Delegate das im ComponentDescriptorHandler verwendet wird
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    /// <typeparam name="TComponentDescriptor"></typeparam>
    /// <param name="race"></param>
    /// <param name="descriptor"></param>
    public delegate void ComponentDescriptorCheckDelegate<TRace, TComponentDescriptor>(TRace race, TComponentDescriptor descriptor);

    /// <summary>
    /// Ein Handler der pro Rasse die Komponentenbeschreibungen verwaltet
    /// </summary>
    /// <typeparam name="TRace"></typeparam>
    /// <typeparam name="TComponentDescriptor"></typeparam>
    public class ComponentDescriptorHandler<TRace, TComponentDescriptor> : WorldHandlerBase<TRace>
        where TRace : RaceBase
        where TComponentDescriptor : ComponentDescriptorBase<TRace>
    {
        private OwnerHashSet<TRace, TComponentDescriptor> _collection = new OwnerHashSet<TRace, TComponentDescriptor>();

        /// <summary>
        /// Wird aufgerufen wenn eine neue Rasse hinzugefügt wird
        /// </summary>
        public event RaceDelegate<TRace> RaceAdded;

        /// <summary>
        /// Wird aufgerufen wenn eine Rasse entfernt wird
        /// </summary>
        public event RaceDelegate<TRace> RaceRemoved;

        /// <summary>
        /// Wird aufgerufen wenn ein neuer Bezeichner hinzugefügt wurde
        /// </summary>
        public event ActivationDelegate<TRace> IdentifierAdded;

        /// <summary>
        /// Wird aufgerufen wenn ein Bezeichner entfernt wurde
        /// </summary>
        public event ActivationDelegate<TRace> IdentifierRemoved;

        /// <summary>
        /// Wird aufgerufen wenn eine neue Komponente verfügbar wird
        /// </summary>
        public event ComponentDescriptorCheckDelegate<TRace, TComponentDescriptor> ComponentDescriptorAvailable;

        /// <summary>
        /// Wird aufgerufen wenn Komponente nicht mehr verfügbar ist
        /// </summary>
        public event ComponentDescriptorCheckDelegate<TRace, TComponentDescriptor> ComponentDescriptorNotAvailable;

#pragma warning disable 1591
        protected override void OnStart()
        {
                     // Handler Events abonnieren
            Races.RaceAdded += delegate(TRace race) { _collection.AddOwner(race); OnAddRace(race); OnCreateComponentDescriptors(race, _collection.GetElementCollection(race)); };
            Races.RaceRemoved += delegate(TRace race) { _collection.RemoveOwner(race); OnRemoveRace(race); };

            Activations.IdentifierAdded += delegate(TRace race, TemplateBase template) { OnAddIdentifier(race, template); OnUpdateComponentDescriptors(race, template); };
            Activations.IdentifierRemoved += delegate(TRace race, TemplateBase template) { OnRemoveIdentifier(race, template); OnUpdateComponentDescriptors(race, template); };

            base.OnStart();
        }
#pragma warning restore 1591

        private void OnUpdateComponentDescriptors(TRace race, TemplateBase template)
        {
            var activationlist = Activations.GetActivations(race);

            foreach (var descriptor in _collection.GetElements())
            {
                var result = descriptor.CheckAvailable(race, activationlist);
                var temp = descriptor.IsAvailable;

                descriptor.IsAvailable = result;

                if (!temp & result) OnComponentAvailable(race, descriptor);
                if (temp & !result) OnComponentNotAvailable(race, descriptor);
            }
        }

        /// <summary>
        /// Löst eine Warnung aus, die angibt das die OnCreateComponentDescriptors nicht implementiert oder mit aufgerufen wurde
        /// </summary>
        /// <param name="race"></param>
        /// <param name="collection"></param>
        protected virtual void OnCreateComponentDescriptors(TRace race, OwnerHashSet<TRace, TComponentDescriptor>.ElementCollection collection)
        {
            Logger.Warning("Not 'ComponentDescriptorCheckDelegate.OnCreateComponentDescriptors' implemented");
        }

        /// <summary>
        /// Löst das ComponentDescriptorAvailable-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        /// <param name="descriptor"></param>
        protected virtual void OnComponentAvailable(TRace race, TComponentDescriptor descriptor)
        {
            if (ComponentDescriptorAvailable != null) ComponentDescriptorAvailable.Invoke(race, descriptor);
        }

        /// <summary>
        /// Löst das ComponentDescriptorNotAvailable-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        /// <param name="descriptor"></param>
        protected virtual void OnComponentNotAvailable(TRace race, TComponentDescriptor descriptor)
        {
            if (ComponentDescriptorNotAvailable != null) ComponentDescriptorNotAvailable.Invoke(race, descriptor);
        }

        /// <summary>
        /// Löst das AddRace-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        protected virtual void OnAddRace(TRace race)
        {
            if (RaceAdded != null) RaceAdded.Invoke(race);
        }

        /// <summary>
        /// Löst das RemoveRace-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        protected virtual void OnRemoveRace(TRace race)
        {
            if (RaceRemoved != null) RaceRemoved.Invoke(race);
        }

        /// <summary>
        /// Löst das OnAddIdentifier-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        /// <param name="template"></param>
        protected virtual void OnAddIdentifier(TRace race, TemplateBase template)
        {
            if (IdentifierAdded != null) IdentifierAdded.Invoke(race, template);
        }

        /// <summary>
        /// Löst das IdentifierRemoved-Ereignis aus
        /// </summary>
        /// <param name="race"></param>
        /// <param name="template"></param>
        protected virtual void OnRemoveIdentifier(TRace race, TemplateBase template)
        {
            if (IdentifierRemoved != null) IdentifierRemoved.Invoke(race, template);
        }
    }
}
