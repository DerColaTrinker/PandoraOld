﻿using Pandora.Game.World.Objects;
using Pandora.Game.World.Objects.Component;
using Pandora.Runtime.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.Game.World.Handlers.Component
{
    /// <summary>
    /// Ein Handler der Komponenten und desses Container verwaltet
    /// </summary>
    /// <typeparam name="TContainer"></typeparam>
    /// <typeparam name="TComponent"></typeparam>
    public class ComponentHandler<TContainer, TComponent> : WorldHandlerBase
        where TContainer : GameObjectBase, IComponentContainer
        where TComponent : ComponentBase
    {
        private OwnerHashSet<TContainer, TComponent> _collection = new OwnerHashSet<TContainer, TComponent>();

        /// <summary>
        /// Fügt einen neuen Container hinzu
        /// </summary>
        /// <param name="container"></param>
        public void AddContainer(TContainer container)
        {
            _collection.AddOwner(container);
        }

        /// <summary>
        /// Entfernt einen Kontainer
        /// </summary>
        /// <param name="container"></param>
        public void RemoveContainer(TContainer container)
        {
            _collection.RemoveOwner(container);
        }

        /// <summary>
        /// Fügt eine Komponente hinzu
        /// </summary>
        /// <param name="container"></param>
        /// <param name="component"></param>
        public void AddComponent(TContainer container, TComponent component)
        {
            _collection.AddElement(container, component);
        }

        /// <summary>
        /// Entfernt eine Komponente
        /// </summary>
        /// <param name="container"></param>
        /// <param name="component"></param>
        public void RemoveComponent(TContainer container, TComponent component)
        {
            _collection.RemoveElement(container, component);
        }
    }
}
