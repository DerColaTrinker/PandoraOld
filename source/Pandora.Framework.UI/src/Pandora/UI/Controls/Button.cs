﻿using Pandora.Interactions.Visuals.BindingProperties;
using Pandora.Interactions.Visuals.Controls;
using Pandora.Interactions.Visuals.Controls.Primitives;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Drawing2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.UI.Controls
{
    public class Button : Control, IFillColorProperty
    {
        private RoundedShapeControl _border;
        private TextControl _text;

        public event ControlEventDelegate ButtonClick;

        public Button()
        {
            _border = new RoundedShapeControl() { Name = "Button.Border" };
            _text = new TextControl() { Name = "Button.Text" };

            _text.Text = "Button";
        }

        protected override void OnLoad()
        {
            UIControls.Add(_border);
            UIControls.Add(_text);
        }

        public string Text
        {
            get { return _text.Text; }
            set { _text.Text = value; }
        }

        public Font Font
        {
            get { return _text.Font; }
            set { _text.Font = value; }
        }

        public string FontResourceCode { get { return _text.FontResourceCode; } set { _text.FontResourceCode = value; } }

        public uint FontSize
        {
            get { return _text.FontSize; }
            set { _text.FontSize = value; }
        }

        public override Vector2f Size
        {
            get
            {
                return base.Size;
            }
            set
            {
                base.Size = value;
                _border.Size = this.Size;
                _text.Size = this.Size;
            }
        }

        public Color OutlineColor { get { return _border.OutlineColor; } set { _border.OutlineColor = value; } }

        public float OutlineThickness { get { return _border.OutlineThickness; } set { _border.OutlineThickness = value; } }

        public Color FillColor { get { return _border.FillColor; } set { _border.FillColor = value; } }

        public Color TextColor { get { return _text.TextColor; } set { _text.TextColor = value; } }

        public TextAlignment TextAlignment { get { return _text.TextAlignment; } set { _text.TextAlignment = value; } }

        public float CornerRadius { get { return _border.Radius; } set { _border.Radius = value; } }

        protected override void OnParentControlSizeChanged()
        {
            base.OnParentControlSizeChanged();
        }

        protected override bool OnMouseButtonReleased(Vector2f position, Interactions.MouseButton button)
        {
            OnButtonClick();

            return true;
        }

        protected virtual void OnButtonClick()
        {
            if (ButtonClick != null) ButtonClick.Invoke(this);
        }

        protected override void OnRenderUpdate(Interactions.Visuals.Renderer.IRenderTarget target)
        {
            base.OnRenderUpdate(target);
        }

    }
}
