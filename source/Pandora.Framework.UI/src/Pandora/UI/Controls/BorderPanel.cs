﻿using Pandora.Interactions.Visuals.BindingProperties;
using Pandora.Interactions.Visuals.Controls;
using Pandora.Interactions.Visuals.Controls.Primitives;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Drawing2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.UI.Controls
{
    public class BorderPanel : ControlContainer, IOutlineColorProperty
    {
        private RectShapeControl _border;
        private RectShapeControl _textborder;
        private TextControl _text;

        public BorderPanel()
        {
            _border = new RectShapeControl() { Name = "BorderPanel.Border" };
            _textborder = new RectShapeControl() { Name = "BorderPanel.TextBorder" };
            _text = new TextControl() { Name = "BorderPanel.Text" };
        }

        protected override void OnLoad()
        {
            _text.Padding = new Thickness(1);

            UIControls.Add(_textborder);
            UIControls.Add(_border);
            UIControls.Add(_text);
        }

        public override Vector2f Size
        {
            get
            {
                return base.Size;
            }
            set
            {
                base.Size = value;
                _border.Size = this.Size;
                _text.Size = this.Size;
                UpdateHeaderHeight();
            }
        }

        private void UpdateHeaderHeight()
        {
            var height =  _text.TextBounds.Height+ _text.TextBounds.Top;

            _textborder.Size = new Vector2f(base.Size.X,height);
            Padding = new Thickness(1,height,1,1);
        }

        public Color OutlineColor { get { return _border.OutlineColor; } set { _border.OutlineColor = value; } }

        public float OutlineThickness { get { return _border.OutlineThickness; } set { _border.OutlineThickness = value; } }

        public Color FillColor { get { return _border.FillColor; } set { _border.FillColor = value; } }

        public Color HeaderBackgroundColor { get { return _textborder.FillColor; } set { _textborder.FillColor = value; } }

        public string Text { get { return _text.Text; } set { _text.Text = value; UpdateHeaderHeight(); } }

        public Font Font { get { return _text.Font; } set { _text.Font = value; UpdateHeaderHeight(); } }

        public string FontResourceCode { get { return _text.FontResourceCode; } set { _text.FontResourceCode = value; UpdateHeaderHeight(); } }

        public Color TextColor { get { return _text.TextColor; } set { _text.TextColor = value; } }

        public uint FontSize { get { return _text.FontSize; } set { _text.FontSize = value; UpdateHeaderHeight(); } }
    }
}
