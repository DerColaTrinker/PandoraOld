﻿using Pandora.Interactions.Visuals.Controls;
using Pandora.Interactions.Visuals.Controls.Primitives;
using Pandora.Interactions.Visuals.Drawing;
using Pandora.Interactions.Visuals.Drawing2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandora.UI.Controls
{
    public class Label : Control
    {
        private TextControl _text;

        public Label()
        {
            _text = new TextControl() { Name = "Label.Text" };
        }

        protected override void OnLoad()
        {
            UIControls.Add(_text);

            base.OnLoad();
        }

        public string Text
        {
            get { return _text.Text; }
            set { _text.Text = value; }
        }

        public Font Font
        {
            get { return _text.Font; }
            set { _text.Font = value; }
        }

        public string FontResourceCode { get { return _text.FontResourceCode; } set { _text.FontResourceCode = value; } }

        public Color TextColor { get { return _text.TextColor; } set { _text.TextColor = value; } }

        public uint FontSize
        {
            get { return _text.FontSize; }
            set { _text.FontSize = value; }
        }

        public override Vector2f Size
        {
            get
            {
                return base.Size;
            }
            set
            {
                base.Size = value;
                _text.Size = this.Size;
            }
        }
    }
}
